<?php

/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

spl_autoload_register([
    'Yii',
    'autoload'
], true, true);
Yii::$classMap = include(__DIR__ . '/vendor/yiisoft/yii2/classes.php');
Yii::$container = new yii\di\Container;

/**
 * Class BaseApplication
 * Used for properties that are identical for both WebApplication and ConsoleApplication
 *
 * @property \common\components\Core $core The core manager for this application.
 * @property \common\providers\UZ\UZ $UZ The UZ provider.
 * @property \common\components\KeyStorage $keyStorage The Key-Value Storage.
 * @property \common\components\SMSWorker $sms SMSWorker.
 * @property \common\components\TurboSMS $smsProvider SmsProvider.
 * @property \yii\web\AssetManager $assetManagerAPI AssetManager.
 * @property \yii\web\UrlManager $urlManagerAPI UrlManager.
 * @property \yii\web\UrlManager $urlManagerFront UrlManager.
 */
abstract class BaseApplication extends yii\base\Application
{
}

/**
 * Class WebApplication
 * Include only Web application related components here
 */
class WebApplication extends yii\web\Application
{
}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 */
class ConsoleApplication extends yii\console\Application
{
}
