<?php
namespace api\controllers;

use Yii;
use yii\rest\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionIndex()
    {
        return 'api';
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;

        return $exception->getMessage();
    }

    public function actionMaintenancemode()
    {
        return 'Maintenancemode';
    }

}
