<?php

namespace api\modules\v1\components;

use Yii;
use api\modules\v1\models\Shakhtar;
use api\modules\v1\models\Entertainments;
use common\models\Service;
use common\providers\tickets\BusTickets;
use common\providers\tickets\GdTickets;
use api\modules\v1\Module;
use api\modules\v1\components\validator\PassengersValidator;
use common\models\Order;
use api\modules\v1\models\ticketsua\Passenger;
use common\providers\tickets\Tickets;

class CoreTicketsUA extends CoreProducts
{
    const DATE_FORMAT = 'php:d-m-Y';
    /**
     * @var Passenger[]
     */
    public $_validPassengers = [];
    /**
     * @var string  Начало названия станции
     */
    public $searchStationName;
    /**
     * @var array Пассажиры
     */
    public $passengers;
    public $commissionOnly;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                ['searchStationName'],
                'required',
                'on' => 'searchstation',
            ],
            [
                ['searchStationName'],
                'trim',
                'on' => 'searchstation',
            ],
            [
                ['searchStationName'],
                'string',
                'min' => 3,
                'max' => 10,
                'on' => 'searchstation',
            ],
            [
                ['passengers'],
                'required',
                'on' => 'checkout'
            ],
            [
                ['passengers'],
                PassengersValidator::className(),
                'min' => 1,
                'max' => $this::MAX_PASSENGERS,
                'on' => 'checkout'
            ],
            [
                ['commissionOnly'],
                'safe',
                'on' => 'checkout',
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'searchStationName' => Module::t('app', 'Станція'),
            //'passengers' => Module::t('app', 'Пасажири (Максимум ' . $this::MAX_PASSENGERS . ')'),
            'passengers' => Module::t('app', 'Ви не обрали місце. Вкажіть, будь ласка, бажані місця на карті вагону'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'searchStationName' => Module::t('app', 'Введіть Станція'),
            //'passengers' => Module::t('app', 'Введіть Пасажири (Максимум ' . $this::MAX_PASSENGERS . ')'),
            'passengers' => Module::t('app', 'Ви не обрали місце. Вкажіть, будь ласка, бажані місця на карті вагону'),
        ]);

        return $result;
    }

//	public function attributePlaceholders()
//	{
//		$result = array_merge(parent::attributePlaceholders(), [
//			'searchStationName' => Module::t('app', '(Placeholder) Станція'),
//		]);
//
//		return $result;
//	}

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->passengers = json_decode($this->passengers);

        return parent::beforeValidate();
    }

    /**
     * Получение списка попыток оплаты заказа
     *
     * @param string $orderId Номер заказа
     * @param string $status Статус транзакции (через запятую) (created,success,failure,refunded)
     * @return array
     */
    public function Transactions($orderId, $status)
    {
        /** @var Order $order */
        $order = Order::findOne([
            'id' => $orderId,
        ]);
        if ($order) {
            if ($order->provider_order_id) {
                $result = Tickets::Transactions($this->getService()->provider_code, $order->provider_order_id,
                    'created,success,failure,refunded');
                if ($result['success']) {
                    $this->_response['data'] = $result['data'];
                } else {
                    $this->_response['code'] = $result['code'];
                    $this->_response['message'] = $result['msg'];
                }
            } else {
                $this->_response['code'] = -1;
                $this->_response['message'] = 'Бронювання не знайдене';
            }
        } else {
            $this->_response['code'] = -1;
            $this->_response['message'] = 'Замовлення не знайдене';
        }

        return $this->_response;
    }


    /**
     * Получить из истории название станции
     *
     * @param string $code Код станции
     * @return mixed
     */
    protected function GetStationFromHistory($code)
    {
        $result = $code;
        $form = $this->GetServiceForm();
        if (isset($form['searchStationsHistory']) && is_array($form['searchStationsHistory'])) {
            foreach ($form['searchStationsHistory'] as $key => $value) {
                if ($code == $key) {
                    $result = $value;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Запрос pdf для электронного билета
     *
     * @param Order $order Заказ
     * @return mixed Электронный билет в формате pdf файла, упакованный в base64
     */
    public static function ETicket($order)
    {
        $response['code'] = -1;
        $response['dialog'] = '';
        if ($order) {
            if (in_array($order->status_id, [
                Order::STATUS_SALES_COMPLETION,
                Order::STATUS_COMPLETED,
            ])) {
                /** @var Service $service */
                $service = $order->getService()
                    ->one();
                $encoded = true;
                $result = [
                    'success' => false,
                    'code' => -2,
                    'msg' => 'Не известный сервис'
                ];
                if (in_array($service->code, [
                    Service::TICKETS_GD,
                    Service::TICKETS_BUS
                ])) {
//					if (TEST_SERVER) {
//						// Если сервер в тестовом режиме то запросы в tickets.ua делать нет смысла, возвращаем пустой pdf файл.
//						$filePath =
//							Yii::$app->params['storage']['documents'] . 'testing' . DIRECTORY_SEPARATOR . 'empty_blank_'
//							. $service->provider_code . '.pdf';
//						$file = file_get_contents($filePath);
//						if ($file) {
//							$file = base64_encode($file);
//							$result = [
//								'success' => true,
//								'data' => [
//									$order->provider_order_id => $file
//								]
//							];
//						}
//						//$response['data'] = 'JVBERi0xLjQKJcOkw7zDtsOfCjIgMCBvYmoKPDwvTGVuZ3RoIDMgMCBSL0ZpbHRlci9GbGF0ZURlY29kZT4+CnN0cmVhbQp4nDPQM1Qo5ypUMFAwALJMLU31jBQsTAz1LBSKUrnCtRTyuAIVAIcdB3IKZW5kc3RyZWFtCmVuZG9iagoKMyAwIG9iago0MgplbmRvYmoKCjUgMCBvYmoKPDwKPj4KZW5kb2JqCgo2IDAgb2JqCjw8L0ZvbnQgNSAwIFIKL1Byb2NTZXRbL1BERi9UZXh0XQo+PgplbmRvYmoKCjEgMCBvYmoKPDwvVHlwZS9QYWdlL1BhcmVudCA0IDAgUi9SZXNvdXJjZXMgNiAwIFIvTWVkaWFCb3hbMCAwIDU5NSA4NDJdL0dyb3VwPDwvUy9UcmFuc3BhcmVuY3kvQ1MvRGV2aWNlUkdCL0kgdHJ1ZT4+L0NvbnRlbnRzIDIgMCBSPj4KZW5kb2JqCgo0IDAgb2JqCjw8L1R5cGUvUGFnZXMKL1Jlc291cmNlcyA2IDAgUgovTWVkaWFCb3hbIDAgMCA1OTUgODQyIF0KL0tpZHNbIDEgMCBSIF0KL0NvdW50IDE+PgplbmRvYmoKCjcgMCBvYmoKPDwvVHlwZS9DYXRhbG9nL1BhZ2VzIDQgMCBSCi9PcGVuQWN0aW9uWzEgMCBSIC9YWVogbnVsbCBudWxsIDBdCi9MYW5nKHJ1LVJVKQo+PgplbmRvYmoKCjggMCBvYmoKPDwvQ3JlYXRvcjxGRUZGMDA1NzAwNzIwMDY5MDA3NDAwNjUwMDcyPgovUHJvZHVjZXI8RkVGRjAwNEMwMDY5MDA2MjAwNzIwMDY1MDA0RjAwNjYwMDY2MDA2OTAwNjMwMDY1MDAyMDAwMzQwMDJFMDAzMT4KL0NyZWF0aW9uRGF0ZShEOjIwMTMxMjE0MjM1OTM0KzAyJzAwJyk+PgplbmRvYmoKCnhyZWYKMCA5CjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDIyNiAwMDAwMCBuIAowMDAwMDAwMDE5IDAwMDAwIG4gCjAwMDAwMDAxMzIgMDAwMDAgbiAKMDAwMDAwMDM2OCAwMDAwMCBuIAowMDAwMDAwMTUxIDAwMDAwIG4gCjAwMDAwMDAxNzMgMDAwMDAgbiAKMDAwMDAwMDQ2NiAwMDAwMCBuIAowMDAwMDAwNTYyIDAwMDAwIG4gCnRyYWlsZXIKPDwvU2l6ZSA5L1Jvb3QgNyAwIFIKL0luZm8gOCAwIFIKL0lEIFsgPEY0OUUxMTQ4OTVEQkIyNDhBMUI4NzMxNzg2ODhGNkJGPgo8RjQ5RTExNDg5NURCQjI0OEExQjg3MzE3ODY4OEY2QkY+IF0KL0RvY0NoZWNrc3VtIC84OTgzNTcwNjlFQTk4MTVEN0IyQ0I5ODNFNjY5NjMxMAo+PgpzdGFydHhyZWYKNzM2CiUlRU9GCg==';
//					} else {
                    switch ($service->code) {
                        case Service::TICKETS_GD :
                            $provider = new GdTickets();
                            $resultTmp = $provider->eticket($order->provider_order_id);
                            $result = [
                                'success' => $resultTmp['success'],
                                'code' => $resultTmp['code'],
                                'msg' => array_key_exists('msg', $resultTmp)
                                    ? $resultTmp['msg']
                                    : '',
                                'dialog' => $resultTmp['dialog'],
                                'data' => [
                                    $order->provider_order_id => $resultTmp['success']
                                        ? $resultTmp['data']
                                        : null,
                                ],
                            ];
                            break;
                        case Service::TICKETS_BUS :
                            $provider = new BusTickets();
                            $resultTmp = $provider->eticket($order->provider_order_id);
                            $result = [
                                'success' => $resultTmp['success'],
                                'code' => $resultTmp['code'],
                                'msg' => array_key_exists('msg', $resultTmp)
                                    ? $resultTmp['msg']
                                    : '',
                                'dialog' => $resultTmp['dialog'],
                                'data' => $resultTmp['data'],
                            ];
                            break;
                    }
                    //}
                } elseif ($service->code == Service::TICKETS_ENTERTAINMENTS) {
                    $encoded = false;
                    $resultTmp = Entertainments::eticket($order);
                    $result = [
                        'success' => $resultTmp['success'],
                        'code' => $resultTmp['code'],
                        'msg' => array_key_exists('msg', $resultTmp)
                            ? $resultTmp['msg']
                            : '',
                        'dialog' => $resultTmp['dialog'],
                        'data' => $resultTmp['data'],
                    ];
                } elseif ($service->code == Service::TICKETS_FOOTBALL) {
                    $encoded = false;
                    $resultTmp = Shakhtar::eticket($order);
                    $result = [
                        'success' => $resultTmp['success'],
                        'code' => $resultTmp['code'],
                        'msg' => array_key_exists('msg', $resultTmp)
                            ? $resultTmp['msg']
                            : '',
                        'dialog' => $resultTmp['dialog'],
                        'data' => $resultTmp['data'],
                    ];
                }
                if ($result['success']) {
                    $response['code'] = 0;
                    $response['encoded'] = $encoded;
                    $response['data'] = $result['data'];
                } else {
                    $response['code'] = (int)$result['code'];
                    $response['message'] = $result['msg'];
                }
                $response['dialog'] = $result['dialog'];
            } else {
                $response['code'] = -1;
                $response['message'] = 'Заказ не завершен успешно ' . $order->id;
                Yii::error($response['message'], __METHOD__);
            }
        } else {
            $response['code'] = -1;
            $response['message'] = 'Заказ не найден ' . $order->id;
            Yii::error($response['message'], __METHOD__);
        }

        return $response;
    }

    protected function getTimeOfDay(\DateTime $dateTime)
    {
        $midnight = 0;
        $morning = 6;
        $midday = 12;
        $evening = 18;
        $midnight2 = 23;
        $result = false;
        $variableTime = clone($dateTime);
        $from = $variableTime->setTime($midnight, 0);
        $variableTime = clone($dateTime);
        $to = $variableTime->setTime($morning, 59);
        if ($dateTime >= $from && $dateTime <= $to) {
            $result = sprintf('%02d-%02d', $midnight, $morning);
        }
        $variableTime = clone($dateTime);
        $from = $variableTime->setTime($morning, 0);
        $variableTime = clone($dateTime);
        $to = $variableTime->setTime($midday, 59);
        if ($dateTime >= $from && $dateTime <= $to) {
            $result = sprintf('%02d-%02d', $morning, $midday);
        }
        $variableTime = clone($dateTime);
        $from = $variableTime->setTime($midday, 0);
        $variableTime = clone($dateTime);
        $to = $variableTime->setTime($evening, 59);
        if ($dateTime >= $from && $dateTime <= $to) {
            $result = sprintf('%02d-%02d', $midday, $evening);
        }
        $variableTime = clone($dateTime);
        $from = $variableTime->setTime($evening, 0);
        $variableTime = clone($dateTime);
        $to = $variableTime->setTime($midnight2, 59);
        if ($dateTime >= $from && $dateTime <= $to) {
            $result = sprintf('%02d-%02d', $evening, $midnight);
        }

        return $result;
    }
}
