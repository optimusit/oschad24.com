<?php

namespace api\modules\v1\components;

use yii\web\UnauthorizedHttpException;

/**
 * @inheritdoc
 */
class QueryParamAuth extends \yii\filters\auth\QueryParamAuth
{
    /**
     * @inheritdoc
     */
    public $tokenParam = 'accessToken';

    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {
        $accessToken = $request->get($this->tokenParam);
        if (is_string($accessToken)) {
            $identity = $user->loginByAccessToken($accessToken, get_class($this));
            if ($identity !== null) {
                $identity->setAuthKeyExpiration();

                return $identity;
            }
        }
        if ($accessToken !== null) {
            $this->handleFailure($response);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function handleFailure($response)
    {
        throw new UnauthorizedHttpException('You are requesting with an invalid credential.');
    }
}
