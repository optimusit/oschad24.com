<?php
namespace api\modules\v1\components;

use Yii;
use common\models\OrderProviderHistory;
use common\models\Provider;
use common\models\Order;
use common\components\AppHelper;
use api\modules\v1\Module;
use api\modules\v1\components\validator\OnlyCyrAlphaValidator;
use api\modules\v1\components\validator\PhoneValidator;

class CoreProducts extends Core
{
    const PHONE_LENGTH = 13;

    /**
     * На 2 мин. меньше от времени отведенного на оплату бронирования
     */
    const RESERVATION_END_SPACE = 2;

    /**
     * @var string Номер телефона покупателя
     */
    public $phone;
    /**
     * @var string Адрес электронной почты покупателя
     */
    public $email;
    /**
     * @var string Имя покупателя
     */
    public $name;
    /**
     * @var string Фамилия покупателя
     */
    public $surname;
    //public $patronymic;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                'phone',
                'required',
                'on' => 'checkout'
            ],
            [
                'phone',
                PhoneValidator::className(),
                'length' => self::PHONE_LENGTH,
                'on' => 'checkout'
            ],
            [
                'email',
                'required',
                'on' => 'checkout'
            ],
            [
                'email',
                'email',
                'on' => 'checkout'
            ],
            [
                [
                    'name',
                    'surname',
                    //'patronymic',
                ],
                'required',
                'on' => 'checkout'
            ],
            [
                [
                    'name',
                    'surname',
                    //'patronymic',
                ],
                //OnlyCyrAlphaValidator::className(),
                'string',
                'min' => 1,
                'max' => 100,
                'on' => 'checkout'
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'phone' => Module::t('app', 'Номер телефону'),
            'email' => Module::t('app', 'Електронна пошта'),
            'name' => Module::t('app', 'Ім’я'),
            'surname' => Module::t('app', 'Прізвище'),
            //'patronymic' => Module::t('app', 'По батькові'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'phone' => Module::t('app', 'Введіть'),
            'email' => Module::t('app', 'Введіть Електронна пошта'),
            'name' => Module::t('app', 'Введіть Ім’я'),
            'surname' => Module::t('app', 'Введіть Прізвище'),
            //'patronymic' => Module::t('app', 'Введіть По батькові'),
        ]);

        return $result;
    }

//	public function attributePlaceholders()
//	{
//		$result = array_merge(parent::attributePlaceholders(), [
//			'phone' => Module::t('app', '(Placeholder) Номер телефону'),
//			'email' => Module::t('app', '(Placeholder) Електрона адреса'),
//			'name' => Module::t('app', '(Placeholder) Ім’я'),
//			'surname' => Module::t('app', '(Placeholder) Прізвище'),
//			//'patronymic' => Module::t('app', '(Placeholder) По батькові'),
//		]);
//
//		return $result;
//	}

    /**
     * @param \DateTime $dateTime
     * @return \DateTime
     */
    protected function getReservationEndSpace(\DateTime $dateTime)
    {
        return $dateTime->sub(new \DateInterval('PT' . $this::RESERVATION_END_SPACE . 'M'));
    }

    /**
     * @param string $reservationId
     * @param \DateTime $expirationTime
     * @param $incomeAmount
     * @param string $currency
     * @param int $unitCount
     * @param array $form
     * @param array $orderInfo
     * @param array $providerPrefix
     * @param array $reservationData
     * @return array
     */
    protected function saveData($reservationId, \DateTime $expirationTime, $incomeAmount, $currency = 'UAH', $unitCount, array $form, array &$orderInfo, $providerPrefix, $reservationData)
    {
        // заглушка
        if (TEST_SERVER) {
            //$incomeAmount = 1;
        }
        $custom = serialize([
            'form' => $form,
            'orderInfo' => $orderInfo
        ]);
        $providerId = Provider::findOne(['prefix' => $providerPrefix])->id;
        $order = new Order();
        $order->scenario = 'default';
        $order->attributes = [
            'provider_id' => $providerId,
            'provider_order_id' => (string)$reservationId,
            'user_id' => $this->_userId,
            'partner_id' => $this->_partnerId,
            'service_id' => $this->getService()->id,
            'fee' => $this::SERVICE_FEE,
            'e_fee' => $this::ACQUIRING_RATE_DEFAULT,
            'income_amount' => $incomeAmount,
            'unit_count' => $unitCount,
            'custom' => $custom,
            'status_id' => Order::STATUS_PENDING,
            'email' => $this->email,
            'name' => $this->name,
            'surname' => $this->surname,
            'phone' => $this->phone,
            'expiration_time' => AppHelper::DateTimeForMySQL($expirationTime),
        ];
        // 1. вначале считаем сумму для оплаты картой ощада
        $order->calculateCommission();
        $totalAmountDefault = round($order->amount, 2);
        // 2. потом считаем сумму для оплаты картой другого банка
        $order->e_fee = $this::ACQUIRING_RATE_OTHER;
        $order->calculateCommission();
        $totalAmountOther = round($order->amount, 2);
        // сохраняем расчет картой другого банка. если будет выбран ощад, то нужно пересчитать и сохранить
        if ($order->save()) {
            $orderProviderHistory = new OrderProviderHistory();
            $orderProviderHistory->order_id = $order->id;
            $orderProviderHistory->request = var_export($reservationData['request'], true);
            $orderProviderHistory->response = $reservationData['response'];
            $orderProviderHistory->response_format = $reservationData['response_format'];
            $orderProviderHistory->save();
        } else {
            Yii::error(var_export($order->errors, true), 'app');
        }

        // 3. комиссия для ощада
        $commissionDefault = $totalAmountOther - $totalAmountDefault;
        // 4. комиссия для других банков
        $commissionOther = 0;

        $this->SetServiceForm(null);
        $this->SetOrderData([
            'id' => $order->id,
        ]);
        $orderInfo['order_id'] = $order->id;
        $orderInfo['timestamp'] = $order->created_at;
        $orderInfo['amount'] = [
            'income_default' => AppHelper::FormatCurrency($totalAmountOther),
            'income_other' => AppHelper::FormatCurrency($totalAmountOther),
            'commission_default' => AppHelper::FormatCurrency($commissionDefault),
            'commission_other' => AppHelper::FormatCurrency($commissionOther),
            'total_default' => AppHelper::FormatCurrency($totalAmountDefault),
            'total_other' => AppHelper::FormatCurrency($totalAmountOther),
        ];
        $orderInfo['expiration_date'] = Yii::$app->formatter->asDate($expirationTime);
        $orderInfo['expiration_time'] = Yii::$app->formatter->asTime($expirationTime);

        return $order;
    }
}
