<?php
namespace api\modules\v1\components;

use Yii;
use api\modules\v1\components\validator\OnlyLatAlphaValidator;
use api\modules\v1\components\validator\avia\DestinationValidator;
use api\modules\v1\components\validator\avia\AviaClassValidator;
use api\modules\v1\components\validator\avia\PassengerTypeValidator;
use api\modules\v1\components\validator\avia\RecommendationIdValidator;
use api\modules\v1\components\validator\bus\DepartureArrivalCodeValidator;
use api\modules\v1\models\ticketsua\Passenger;
use api\modules\v1\components\validator\gd\OperationTypeValidator;
use api\modules\v1\components\validator\gd\ServicesValidator;
use api\modules\v1\components\validator\gd\TrainClassValidator;
use api\modules\v1\components\validator\gd\TrainNumberValidator;
use api\modules\v1\components\validator\gd\TrainRouteValidator;
use api\modules\v1\components\validator\gd\TrainSubClassValidator;
use api\modules\v1\components\validator\gd\CarValidator;
use api\modules\v1\components\validator\OnlyCyrAlphaValidator;
use api\modules\v1\components\validator\PhoneValidator;
use api\modules\v1\components\validator\PassengersValidator;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\validators\DateValidator;
use yii\validators\EachValidator;
use yii\validators\NumberValidator;
use yii\validators\RequiredValidator;
use yii\validators\SafeValidator;
use yii\validators\StringValidator;
use yii\validators\Validator;

trait ModelMetaDataTrait
{
    public function getValidationRulesAndHints($full = true)
    {
        $result = [];
        $modelName = strtolower((new \ReflectionClass($this))->getShortName());
        foreach ($this->scenarios() as $scenario => $requiredFields) {
            if ($scenario == 'default') {
                continue;
            }
            $this->setScenario($scenario);
            $this->validate();
            $validators = $this->activeValidators;
            if ($full) {
                $result[$scenario]['url'] = Url::to([
                    Yii::$app->controller->rulesValidationInModel[$modelName] . $scenario,
                    'side' => Yii::$app->controller->module->side
                ], true);
            }
            foreach ($validators as $validator) {
                foreach ($requiredFields as $requiredField) {
                    if (in_array($requiredField, $validator->attributes)) {
                        if (!array_key_exists($scenario, $result)) {
                            $result[$scenario] = [];
                        }
                        if (!array_key_exists('form', $result[$scenario])) {
                            $result[$scenario]['form'] = [];
                        }
                        if (!array_key_exists($requiredField, $result[$scenario]['form'])) {
                            $result[$scenario]['form'][$requiredField] = [];
                        }
                        $fieldValidationRules = $this->getValidationData($requiredField, $validator);
                        $result[$scenario]['form'][$requiredField] = ArrayHelper::merge($result[$scenario]['form'][$requiredField],
                            $fieldValidationRules);
                        if ($validator instanceof PassengersValidator) {
                            $passengerModelName = 'api\modules\v1\models\ticketsua\Passenger'
                                . ucfirst(strtolower((new \ReflectionClass($this))->getShortName()));
                            /** @var Passenger $model */
                            $model = new $passengerModelName;
                            $rules = $model->getValidationRulesAndHints(false);
                            $result[$scenario]['form'][$requiredField]['passenger'] = $rules['checkout']['form'];
                        }
                        if ($validator instanceof DestinationValidator) {
                            $destinationModelName = 'api\modules\v1\models\ticketsua\avia\Destination';
                            /** @var Destination $model */
                            $model = new $destinationModelName;
                            $rules = $model->getValidationRulesAndHints(false);
                            $result[$scenario]['form'][$requiredField]['destination'] = $rules['searchaircraft']['form'];
                        }
                    }
                }
            }
        }

        return $result;
    }

    protected function getValidationData($attribute, Validator $validator)
    {
        $result = [];
        $type = str_replace('validator', '', strtolower((new \ReflectionClass($validator))->getShortName()));
        if ($type == 'required') {
            $result['rules']['required'] = true;
            $result['messages']['required'] = $this->getErrorMessage($attribute, $validator->message);
        } else {
            $result['rules']['type'] = $type;
        }
        if ($validator instanceof RequiredValidator) {
        } elseif ($validator instanceof PhoneValidator
            || $validator instanceof OnlyCyrAlphaValidator
            || $validator instanceof OnlyLatAlphaValidator
            || $validator instanceof DepartureArrivalCodeValidator
        ) {
            $result['messages']['valid'] = $this->getErrorMessage($attribute, $validator->message);
            if (!is_null($validator->length)) {
                $result['rules']['length'] = $validator->length;
                $result['messages']['notEqual'] = $this->getErrorMessage($attribute, $validator->notEqual,
                    ['length' => $validator->length]);
            }
            if (!is_null($validator->min)) {
                $result['rules']['min'] = $validator->min;
                $result['messages']['min'] = $this->getErrorMessage($attribute, $validator->tooShort,
                    ['min' => $validator->min]);
            }
            if (!is_null($validator->max)) {
                $result['rules']['max'] = $validator->max;
                $result['messages']['max'] = $this->getErrorMessage($attribute, $validator->tooLong,
                    ['max' => $validator->max]);
            }
        } elseif ($validator instanceof ServicesValidator) {
            $result['rules']['type'] = 'set';
            $result['rules']['set'] = $validator->range;
        } elseif ($validator instanceof AviaClassValidator) {
            $result['rules']['type'] = 'enum';
            $result['rules']['enum'] = $validator->range;
        } elseif ($validator instanceof PassengerTypeValidator) {
            $result['rules']['type'] = 'enum';
            $result['rules']['enum'] = $validator->range;
        } elseif ($validator instanceof OperationTypeValidator) {
            $result['rules']['type'] = 'enum';
            $result['rules']['enum'] = $validator->range;
        } elseif ($validator instanceof TrainNumberValidator) {
            $result['rules']['type'] = 'string';
        } elseif ($validator instanceof TrainClassValidator) {
            $result['rules']['type'] = 'string';
        } elseif ($validator instanceof TrainSubClassValidator) {
            $result['rules']['type'] = 'number';
        } elseif ($validator instanceof RecommendationIdValidator) {
            $result['rules']['type'] = 'string';
        } elseif ($validator instanceof CarValidator) {
            $result['rules']['type'] = 'number';
            if (!is_null($validator->min)) {
                $result['rules']['min'] = $validator->min;
                $result['messages']['min'] = $this->getErrorMessage($attribute, $validator->tooSmall,
                    ['min' => $validator->min]);
            }
            if (!is_null($validator->max)) {
                $result['rules']['max'] = $validator->max;
                $result['messages']['max'] = $this->getErrorMessage($attribute, $validator->tooBig,
                    ['max' => $validator->max]);
            }
        } elseif ($validator instanceof TrainRouteValidator) {
            $result['rules']['type'] = 'string';
            if (!is_null($validator->min)) {
                $result['rules']['min'] = $validator->min;
                $result['messages']['min'] = $this->getErrorMessage($attribute, $validator->tooShort,
                    ['min' => $validator->min]);
            }
            if (!is_null($validator->max)) {
                $result['rules']['max'] = $validator->max;
                $result['messages']['max'] = $this->getErrorMessage($attribute, $validator->tooLong,
                    ['max' => $validator->max]);
            }
        } elseif ($validator instanceof EachValidator) {
            $type = 'array';
            if (count($validator->rule) > 0) {
                $type .= 'Of' . ucfirst($validator->rule[0]);
            }
            $result['rules']['type'] = $type;
        } elseif ($validator instanceof NumberValidator) {
            if (!is_null($validator->min)) {
                $result['rules']['min'] = $validator->min;
                $result['messages']['min'] = $this->getErrorMessage($attribute, $validator->tooSmall,
                    ['min' => $validator->min]);
            }
            if (!is_null($validator->max)) {
                $result['rules']['max'] = $validator->max;
                $result['messages']['max'] = $this->getErrorMessage($attribute, $validator->tooBig,
                    ['max' => $validator->max]);
            }
        } elseif ($validator instanceof StringValidator) {
            if (!is_null($validator->length)) {
                $result['rules']['length'] = $validator->length;
                $result['messages']['notEqual'] = $this->getErrorMessage($attribute, $validator->notEqual,
                    ['length' => $validator->length]);
            }
            if (!is_null($validator->min)) {
                $result['rules']['min'] = $validator->min;
                $result['messages']['min'] = $this->getErrorMessage($attribute, $validator->tooShort,
                    ['min' => $validator->min]);
            }
            if (!is_null($validator->min)) {
                $result['rules']['max'] = $validator->max;
                $result['messages']['max'] = $this->getErrorMessage($attribute, $validator->tooLong,
                    ['max' => $validator->max]);
            }
        } elseif ($validator instanceof DateValidator) {
            if (!is_null($validator->min)) {
                $result['rules']['min'] = $validator->minString;
                $result['messages']['min'] = $this->getErrorMessage($attribute, $validator->tooSmall,
                    ['min' => $validator->minString]);
            }
            if (!is_null($validator->max)) {
                $result['rules']['max'] = $validator->maxString;
                $result['messages']['max'] = $this->getErrorMessage($attribute, $validator->tooBig,
                    ['max' => $validator->maxString]);
            }
            if (isset($validator->pseudoRequired) && $validator->pseudoRequired) {
                $result['messages']['required'] = $this->getErrorMessage($attribute, $validator->requiredMessage);;
            }
        } elseif ($validator instanceof PassengersValidator) {
            $result['rules']['min'] = $validator->min;
            $result['messages']['min'] = $this->getErrorMessage($attribute, $validator->tooSmall,
                ['min' => $validator->min]);
            $result['rules']['max'] = $validator->max;
            $result['messages']['max'] = $this->getErrorMessage($attribute, $validator->tooBig,
                ['max' => $validator->max]);
        } elseif ($validator instanceof SafeValidator) {
            $result['rules']['required'] = false;
        } else {
            //throw new ApiException('Не определенный тип валидации');
        }
        $result['label'] = $this->getAttributeLabel($attribute);
        $result['hint'] = $this->getAttributeHint($attribute);
        $result['placeholder'] = $this->getAttributePlaceholder($attribute);
        $result['value'] = $this->$attribute;

        return $result;
    }

    /**
     * @param string $attribute the attribute being validated
     * @param string $message the error message
     * @param array $params values for the placeholders in the error message
     * @return string
     */
    protected function getErrorMessage($attribute, $message, $params = [])
    {
        $value = $this->$attribute;
        $params['attribute'] = $this->getAttributeLabel($attribute);
        $params['value'] = is_array($value)
            ? 'array()'
            : $value;

        return Yii::$app->getI18n()
            ->format($message, $params, Yii::$app->language);
    }
}
