<?php

namespace api\modules\v1\components;

use Yii;

class I18N extends \yii\i18n\I18N
{
    public function translateForPartner($partnerId, $category, $message, $params, $language)
    {
        $messageSource = $this->getMessageSource($category);
        if ($messageSource instanceof DbMessageSource) {
            $translation = $messageSource->translateForPartner($partnerId, $category, $message, $language);
        } else {
            $translation = $messageSource->translate($category, $message, $language);
        }
        if ($translation === false) {
            return $this->format($message, $params, $messageSource->sourceLanguage);
        } else {
            return $this->format($translation, $params, $language);
        }
    }
}
