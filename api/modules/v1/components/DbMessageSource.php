<?php

namespace api\modules\v1\components;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\i18n\MissingTranslationEvent;

class DbMessageSource extends \yii\i18n\DbMessageSource
{
    private $_messages = [];

    public function translateForPartner($partnerId, $category, $message, $language)
    {
        if ($this->forceTranslation || $language !== $this->sourceLanguage) {
            return $this->translateMessageForPartner($partnerId, $category, $message, $language);
        } else {
            return false;
        }
    }

    protected function translateMessageForPartner($partnerId, $category, $message, $language)
    {
        $key = $language . '/' . $category;
        if (!isset($this->_messages[$key])) {
            $this->_messages[$key] = $this->loadMessagesForPartner($partnerId, $category, $language);
        }
        if (isset($this->_messages[$key][$message]) && $this->_messages[$key][$message] !== '') {
            return $this->_messages[$key][$message];
        } elseif ($this->hasEventHandlers(self::EVENT_MISSING_TRANSLATION)) {
            $event = new MissingTranslationEvent([
                'category' => $category,
                'message' => $message,
                'language' => $language,
            ]);
            $this->trigger(self::EVENT_MISSING_TRANSLATION, $event);
            if ($event->translatedMessage !== null) {
                return $this->_messages[$key][$message] = $event->translatedMessage;
            }
        }

        return $this->_messages[$key][$message] = false;
    }

    protected function loadMessagesForPartner($partnerId, $category, $language)
    {
        if ($this->enableCaching) {
            $key = [
                __CLASS__,
                $partnerId,
                $category,
                $language,
            ];
            $messages = $this->cache->get($key);
            if ($messages === false) {
                $messages = $this->loadMessagesFromDbForPartner($partnerId, $category, $language);
                $this->cache->set($key, $messages, $this->cachingDuration);
            }

            return $messages;
        } else {
            return $this->loadMessagesFromDbForPartner($partnerId, $category, $language);
        }
    }

    protected function loadMessagesFromDbForPartner($partnerId, $category, $language)
    {
        $mainQuery = new Query();
        $mainQuery->select([
            't1.message message',
            't2.translation translation'
        ])
            ->from([
                "$this->sourceMessageTable t1",
                "$this->messageTable t2"
            ])
            ->where('t1.id = t2.id AND t1.category = :category AND t2.language = :language AND t2.partner_id = :partnerId')
            ->params([
                ':category' => $category,
                ':language' => $language,
                ':partnerId' => $partnerId
            ]);

        $fallbackLanguage = substr($language, 0, 2);
        if ($fallbackLanguage != $language) {
            $fallbackQuery = new Query();
            $fallbackQuery->select([
                't1.message message',
                't2.translation translation'
            ])
                ->from([
                    "$this->sourceMessageTable t1",
                    "$this->messageTable t2"
                ])
                ->where('t1.id = t2.id AND t1.category = :category AND t2.language = :fallbackLanguage AND t2.partner_id = :partnerId')
                ->andWhere("t2.id NOT IN (SELECT id FROM $this->messageTable WHERE language = :language)")
                ->params([
                    ':category' => $category,
                    ':language' => $language,
                    ':fallbackLanguage' => $fallbackLanguage,
                    ':partnerId' => $partnerId
                ]);

            $mainQuery->union($fallbackQuery, true);
        }

        $messages = $mainQuery->createCommand($this->db)
            ->queryAll();

        return ArrayHelper::map($messages, 'message', 'translation');
    }
}
