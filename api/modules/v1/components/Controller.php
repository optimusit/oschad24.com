<?php
namespace api\modules\v1\components;

use Yii;
use common\models\Service;
use yii\base\InlineAction;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\rest\Controller as BaseController;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
use api\modules\v1\Module;

class Controller extends BaseController
{
    /** @var Core $_core */
    protected $_core;
    /** @var array $_freeSessionActions Экшены, вызовы которых разрешены без сессии (для проверки истечения сессии) */
    protected $_freeSessionActions = [
        'ticketsua\gd' => [
            'balance',
            'transactions',
            'searchstation',
            'trainroute',
            'bookingslist',
            'bookingshow',
            'cancel',
            'eticket',
        ],
        'ticketsua\bus' => [
            'balance',
            'transactions',
            'searchstation',
            'bookingslist',
            'bookingshow',
            'cancel',
            'eticket',
        ],
        'ticketsua\avia' => [
            'balance',
            'transactions',
            'searchstation',
            'bookingslist',
            'bookingshow',
            'cancel',
            'eticket',
            'citizenship',
        ],
        'entertainments' => [
            'index',
            'event'
        ],
        'shakhtar' => [
            'index',
        ],
        'aiwa\accidents5' => [
            'calculate',
        ],
        'application' => [
            'config',
        ],
        'order' => [
            'paymentstatus',
            'authorizationdata',
        ],
        'arm\order' => [
            'index',
        ],
        'arm\user' => [
            'login'
        ],
    ];

    protected $_timeStart;
    protected $_timeEnd;

    public $rulesValidationInModel = [
        'gd' => 'ticketsua/gd/',
        'bus' => 'ticketsua/bus/',
        'avia' => 'ticketsua/avia/',
        'entertainments' => 'entertainments/',
        'shakhtar' => 'shakhtar/',
        'order' => 'order/',
        'payment' => 'payment/',
    ];

    protected $_aboveMaintenance = [
        'application' => ['config'],
        'egateway' => ['notify'],
    ];

    /**
     * @inheritdoc
     */
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        parent::beforeAction($action);
        // разрулим пути
        $pathParts = explode('/', $action->controller->id);
        $controllerId = array_pop($pathParts);
        if (count($pathParts)) {
            $pathParts = implode('\\', $pathParts) . '\\';
        } else {
            $pathParts = '';
        }
        // проверим, открыт ли сейчас сервис для продажи и не закрыт ли магазин
        $services = ArrayHelper::map(Service::find()
            ->select('controller,active')
            ->asArray()
            ->all(), 'controller', 'active');
        $maintenance = (Yii::$app->params['maintenance'] == true)
            ? true
            : false;
        if (($maintenance
                && (!array_key_exists($controllerId, $this->_aboveMaintenance)
                    || (array_key_exists($controllerId, $this->_aboveMaintenance)
                        && !in_array($action->id, $this->_aboveMaintenance[$controllerId]))))
            || (array_key_exists($controllerId, $services) && !$services[$controllerId])
        ) {
            throw new ApiException(Module::t('app', 'Сервіс тимчасово зачинено'),
                Core::RESPONSE_CODE_ERROR_RECOMMENDATIONS);
        }
        $core = 'api\modules\v1\models\\' . $pathParts . ucfirst($controllerId);
        $this->_core = new $core($this->module->response, $this->module->session,
            Yii::$app->request->get('partner', null));
        // такая вот подмостырка для того, чтобы протащить партнера для статической функции переводов сообщений
        Yii::$app->params['.']['response']['partnerId'] = $this->_core->getPartnerId();
        if ((is_null($this->_core->GetServiceForm()) && is_null($this->_core->GetOrderData()))
            && (!array_key_exists($pathParts . $controllerId, $this->_freeSessionActions)
                || (array_key_exists($pathParts . $controllerId, $this->_freeSessionActions)
                    && !in_array($action->id, $this->_freeSessionActions[$pathParts . $controllerId])))
        ) {
            throw new ApiException(Module::t('app', 'Сесія скінчилась'), Core::RESPONSE_CODE_ERROR_RECOMMENDATIONS);
        }
        $this->_timeStart = microtime(true);
        // todo потихоньку перетянуть валидацию в модели
        if (array_key_exists($controllerId, $this->rulesValidationInModel)) {
            $params = Yii::$app->request->get();
            $this->_core->setScenario($action->id);
            $this->_core->setAttributes($params);
            if (!$this->_core->validate()) {
                Yii::$app->params['.']['response']['data'] = $this->_core->errors;
                throw new ApiException(Module::t('app', 'Помилки перевірки вхідних даних'),
                    Core::RESPONSE_CODE_VALIDATION_ERROR);
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        $this->_timeEnd = microtime(true);
        $timeDiff = round($this->_timeEnd - $this->_timeStart, 5);
        $result['time'] = $timeDiff;

        if (!is_null($this->module->jsonp)) {
            $result = [
                'data' => $result,
                'callback' => $this->module->jsonp,
            ];
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        /** @var $action InlineAction */
                        $service = $action->controller->id;
//						if ($this->module->side == Module::SIDE_FRONT) {
//							$permission = 'permissionPartnerUseService';
//						} else {
//							$permission = 'permissionUserUseService';
//						}
                        $permission = 'permissionUserUseService';

                        return Yii::$app->user->can($permission, ['service' => $service]);
                    }
                ],
                [
                    'allow' => true,
                    'actions' => [
                        'login',
                        'error'
                    ],
                ],
            ],
        ];

        if (!is_null($this->module->jsonp)) {
            $behaviors['contentNegotiator'] = [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSONP,
                ],
            ];
        }

        return $behaviors;
    }

    public function getCore()
    {
        return $this->_core;
    }
}
