<?php
namespace api\modules\v1\components;

use Yii;

trait ModelAttributePlaceholdersTrait
{
    public function attributePlaceholders()
    {
        return [];
    }

    public function getAttributePlaceholder($attribute)
    {
        $placeholders = $this->attributePlaceholders();

        return isset($placeholders[$attribute])
            ? $placeholders[$attribute]
            : '';
    }
}
