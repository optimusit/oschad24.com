<?php
namespace api\modules\v1\components\validator\shakhtar;

use Yii;
use api\modules\v1\models\Shakhtar;
use api\modules\v1\Module;
use yii\validators\NumberValidator;

class SeatsValidator extends NumberValidator
{
    /**
     * @param Shakhtar $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $seats = $model->$attribute;
        if (count($seats) == 0) {
            $model->addError($attribute, Module::t('app', 'Місця не вибрані'));
        }
        if (count($seats) > $this->max) {
            $model->addError($attribute,
                Module::t('app', 'Перевищена кількість вібраних місць ({s})', ['s' => $this->max]));
        }
        $form = Yii::$app->controller->core->GetServiceForm();
        if (array_key_exists('Event', $form)) {
            if (count($seats) > $form['Event']['limit']) {
                $model->addError($attribute,
                    Module::t('app', 'Кількість вибраних місць більше дозволеного ({maxSeats})',
                        ['maxSeats' => $form['Event']['limit']]));
            }
        } else {
            $this->addError($model, $attribute, Module::t('app', 'Відсутня історія пошуку'));
        }
    }
}
