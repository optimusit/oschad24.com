<?php
namespace api\modules\v1\components\validator\shakhtar;

use Yii;
use api\modules\v1\models\Shakhtar;
use api\modules\v1\Module;
use yii\helpers\ArrayHelper;
use yii\validators\NumberValidator;

class EventValidator extends NumberValidator
{
    /**
     * @param Shakhtar $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $form = Yii::$app->controller->core->GetServiceForm();
        if (array_key_exists('Events', $form)) {
            $events = ArrayHelper::getColumn($form['Events'], 'id');
            if (!in_array($model->$attribute, $events)) {
                $model->addError($attribute, Module::t('app', 'Відсутні подія'));
            }
        } else {
            $this->addError($model, $attribute, Module::t('app', 'Відсутня історія пошуку'));
        }
    }
}
