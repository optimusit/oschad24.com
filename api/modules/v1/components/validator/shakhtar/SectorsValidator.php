<?php
namespace api\modules\v1\components\validator\shakhtar;

use Yii;
use api\modules\v1\models\Shakhtar;
use api\modules\v1\Module;
use yii\helpers\ArrayHelper;
use yii\validators\NumberValidator;

class SectorsValidator extends NumberValidator
{
    /**
     * @param Shakhtar $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $form = Yii::$app->controller->core->GetServiceForm();
        if (array_key_exists('Sectors', $form)) {
            $sectors = ArrayHelper::getColumn($form['Sectors'], 'Id');
            if (!in_array($model->$attribute, $sectors)) {
                $model->addError($attribute, Module::t('app', 'Вибраний сектор не існує'));
            }
        } else {
            $this->addError($model, $attribute, Module::t('app', 'Відсутня історія пошуку'));
        }
    }
}
