<?php
namespace api\modules\v1\components\validator;

use Yii;
use api\modules\v1\Module;

class PhoneValidator extends StringValidator
{
    protected $_pattern = '/\+[0-9]{12}$/';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->message = Module::t('app', '{attribute} не є вірним');
    }
}
