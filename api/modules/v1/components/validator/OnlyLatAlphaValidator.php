<?php
namespace api\modules\v1\components\validator;

use Yii;
use api\modules\v1\Module;

class OnlyLatAlphaValidator extends StringValidator
{
    protected $_pattern = '/^[a-zA-Z}]+/';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->message = Module::t('app', '{attribute} повинен містити тільки Латинські літери');
    }
}
