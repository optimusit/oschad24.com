<?php
namespace api\modules\v1\components\validator;

use common\components\AppHelper;
use Yii;
use api\modules\v1\models\ticketsua\Avia;
use api\modules\v1\models\ticketsua\PassengerAvia;
use common\providers\tickets\AviaTickets;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\Passenger;
use api\modules\v1\components\CoreTicketsUA;
use yii\validators\NumberValidator;

class PassengersValidator extends NumberValidator
{
    /**
     * @param CoreTicketsUA $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $passengers = $model->$attribute;
        if (count($passengers) == 0) {
            $model->addError($attribute, Module::t('app', 'Відсутні пасажири'));

            return;
        }
        if (count($passengers) > $model::MAX_PASSENGERS) {
            $model->addError($attribute,
                Module::t('app', 'Кількість пасажиров більше більше дозволеного ({maxPassengers})',
                    ['maxPassengers' => $model::MAX_PASSENGERS]));

            return;
        }
        $errors = [];
        $modelName = ucfirst(strtolower((new \ReflectionClass($model))->getShortName()));
        $passengerModelName = 'api\modules\v1\models\ticketsua\Passenger' . $modelName;
        foreach ($passengers as $passenger) {
            /** @var Passenger $passengerModel */
            $passengerModel = new $passengerModelName;
            $passengerModel->setScenario('checkout');
            if ($passenger instanceof \stdClass) {
                $passenger = get_object_vars($passenger);
            }
            $passengerModel->setAttributes($passenger);
            if ($passengerModel->validate()) {
                $model->_validPassengers[] = $passengerModel;
            } else {
                if (isset($passengerModel->seat)) {
                    if (isset($passengerModel->carNumber)) {
                        $key = $passengerModel->carNumber . '_' . $passengerModel->seat;
                    } else {
                        $key = $passengerModel->seat;
                    }
                } else {
                    $key = $passengerModel->n;
                }
                $errors[$key] = $passengerModel->errors;
            }
        }
        if (count($errors)) {
            $model->addError($attribute, $errors);

            return;
        }
        // если это самолет то проверим дополнительно на правила для соответствия колиества типов и возрастов пассажиров
        if ($model instanceof Avia) {
            // в начале проверим на соответствие количества типов пассажиров для бронирования и ранее заявленных количеств типов пассажира при поиске варинтов перелета
            $form = $model->GetServiceForm();
            if (!array_key_exists('search', $form)) {
                $this->addError($model, $attribute, Module::t('app', 'Відсутня пошукова сессія'));

                return;
            }
            $passengerTypeNumber = [
                AviaTickets::PASSENGER_TYPE_ADULT => 0,
                AviaTickets::PASSENGER_TYPE_CHILD => 0,
                AviaTickets::PASSENGER_TYPE_INFANT => 0
            ];
            /** @var PassengerAvia $passenger */
            foreach ($model->_validPassengers as $passenger) {
                $passengerTypeNumber[$passenger->type]++;
            }
            $allPassengerTypes = AviaTickets::PASSENGER_TYPES;
            foreach ($passengerTypeNumber as $k => $v) {
                if (array_key_exists(strtolower($k), $form['search'])) {
                    if ((int)$form['search'][strtolower($k)] != (int)$passengerTypeNumber[$k]) {
                        $this->addError($model, $attribute, Module::t('app',
                            'Кількість пасажирів типу "{passengerType}" для бронювання не відповідає кількості для пошуку варіантів перельоту',
                            ['passengerType' => $allPassengerTypes[$k]]));

                        return;
                    }
                } else {
                    $this->addError($model, $attribute, Module::t('app', 'Відсутня пошукова сессія'));

                    return;
                }
            }
            // потом проверим на соответствие заявленным типам пассажиров и указаным датам рождиний

            // посчитаем фактические типы пасажиров (по датм рождений)
            $passengerTypeNumber = [
                AviaTickets::PASSENGER_TYPE_ADULT => 0,
                AviaTickets::PASSENGER_TYPE_CHILD => 0,
                AviaTickets::PASSENGER_TYPE_INFANT => 0
            ];
            $now = AppHelper::CreateDateNow();
            foreach ($model->_validPassengers as $passenger) {
                $birthDay = AppHelper::CreateDateTimeFromString($passenger->birthday);
                $diff = $birthDay->diff($now);
                if (($type = AviaTickets::getPassengerTypeByAge($diff->y)) !== false) {
                    $passengerTypeNumber[$type]++;
                }
            }
            // проверим на соответствие фактических типов пассажиров, искомым вариантам перелетов
            foreach ($passengerTypeNumber as $k => $v) {
                if (array_key_exists(strtolower($k), $form['search'])) {
                    if ((int)$form['search'][strtolower($k)] != (int)$passengerTypeNumber[$k]) {
                        $this->addError($model, $attribute, Module::t('app',
                            'Кількість пасажирів типу "{passengerType}" для бронювання не відповідає кількості для пошуку варіантів перельоту',
                            ['passengerType' => $allPassengerTypes[$k]]));

                        return;
                    }
                } else {
                    $this->addError($model, $attribute, Module::t('app', 'Відсутня пошукова сессія'));

                    return;
                }
            }
        }
    }
}
