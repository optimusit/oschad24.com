<?php
namespace api\modules\v1\components\validator\avia;

use Yii;
use common\providers\tickets\AviaTickets;
use api\modules\v1\models\ticketsua\Avia;
use api\modules\v1\Module;
use yii\validators\RangeValidator;

class PassengerTypeValidator extends RangeValidator
{
    /**
     * @param Avia $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!array_key_exists($model->$attribute, AviaTickets::PASSENGER_TYPES)) {
            $this->addError($model, $attribute, Module::t('app', 'Невірний тип пасажира'));
        }
    }
}
