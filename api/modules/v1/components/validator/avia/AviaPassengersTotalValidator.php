<?php
namespace api\modules\v1\components\validator\avia;

use Yii;
use api\modules\v1\models\ticketsua\Avia;
use api\modules\v1\Module;
use yii\validators\Validator;

class AviaPassengersTotalValidator extends Validator
{
    /**
     * @param Avia $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $totalPassengers = $model->adult + $model->child + $model->infant;
        if ($totalPassengers > $model::MAX_PASSENGERS) {
            $this->addError($model, $attribute,
                Module::t('app', 'Кількість пасажиров більше дозволеного ({maxPassengers})',
                    ['maxPassengers' => $model::MAX_PASSENGERS]));
        }
        if ($model->infant > $model->adult) {
            $this->addError($model, $attribute,
                Module::t('app', 'Кількість пасажиров немовлят не повинно бути більше ніж дорослих'));
        }
    }
}
