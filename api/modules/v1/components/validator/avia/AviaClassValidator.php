<?php
namespace api\modules\v1\components\validator\avia;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\avia\Destination;
use yii\validators\RangeValidator;

class AviaClassValidator extends RangeValidator
{
    /**
     * @param Destination $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!in_array($model->$attribute, array_keys($this->range))) {
            $this->addError($model, $attribute, Module::t('app', 'Не вірний клас. Доступні класи {classes}',
                ['classes' => implode(',', array_keys($this->range))]));
        }
    }
}
