<?php
namespace api\modules\v1\components\validator\avia;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\Avia;
use yii\helpers\ArrayHelper;
use yii\validators\Validator;

class RecommendationIdValidator extends Validator
{
    /**
     * @param Avia $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $form = $model->GetServiceForm();
        if (array_key_exists('aircrafts', $form)) {
            $keys = ArrayHelper::getColumn($form['aircrafts'], 'id');
            if (!in_array($model->recommendationId, $keys)) {
                $this->addError($model, $attribute, Module::t('app', 'Відсутня рекомендація'));
            }
        } else {
            $this->addError($model, $attribute, Module::t('app', 'Відсутня історія пошуку'));
        }
    }
}
