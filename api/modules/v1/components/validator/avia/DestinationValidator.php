<?php
namespace api\modules\v1\components\validator\avia;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\avia\Destination;
use api\modules\v1\components\CoreTicketsUA;
use yii\validators\NumberValidator;

class DestinationValidator extends NumberValidator
{
    /**
     * @param CoreTicketsUA $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $destinations = $model->$attribute;
        if (count($destinations) == 0) {
            $model->addError($attribute, Module::t('app', 'Відсутні пункти перельоту'));
        }
        $errors = [];
        $destinationModels = [];
        $hasErrors = false;
        foreach ($destinations as &$destination) {
            /** @var Destination $destinationModel */
            $destinationModel = new Destination();
            $destinationModel->setScenario('searchaircraft');
            if ($destination instanceof \stdClass) {
                $destinationFields = get_object_vars($destination);
                $destinationModel->setAttributes($destinationFields);
                if (!$destinationModel->validate()) {
                    $hasErrors = true;
                }
                $destinationModels[] = $destinationModel;
                $errors[] = $destinationModel->errors;
            }
        }
        if ($hasErrors) {
            $model->addError($attribute, $errors);
        } else {
            $model->$attribute = $destinationModels;
        }
    }
}
