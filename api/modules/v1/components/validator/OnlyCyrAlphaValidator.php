<?php
namespace api\modules\v1\components\validator;

use Yii;
use api\modules\v1\Module;

class OnlyCyrAlphaValidator extends StringValidator
{
    protected $_pattern = '/^[\x{0430}-\x{044F}\x{0410}-\x{042F}]+/u';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        switch (Yii::$app->language) {
            case  'ru' :
                $language = Module::t('app', 'Російські');
                break;
            default:
                $language = Module::t('app', 'Українські');
                break;
        }
        $this->message = Module::t('app', '{attribute} повинен містити тільки {language} літери',
            ['language' => $language]);
    }

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
    }
}
