<?php
namespace api\modules\v1\components\validator;

use Yii;

class StringValidator extends \yii\validators\StringValidator
{
    protected $_pattern = '';

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        $length = mb_strlen($value, $this->encoding);
        $valid = true;
        if ($this->min !== null && $length < $this->min) {
            $this->addError($model, $attribute, $this->tooShort, ['min' => $this->min]);
            $valid = false;
        }
        if ($this->max !== null && $length > $this->max) {
            $this->addError($model, $attribute, $this->tooLong, ['max' => $this->max]);
            $valid = false;
        }
        if ($this->length !== null && $length !== $this->length) {
            $this->addError($model, $attribute, $this->notEqual, ['length' => $this->length]);
            $valid = false;
        }
        if ($valid && !preg_match($this->_pattern, trim($value))) {
            $this->addError($model, $attribute, $this->message);
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        $valid = false;
        if (preg_match($this->_pattern, trim($value))) {
            $valid = true;
        }

        return $valid
            ? null
            : [
                $this->message,
                []
            ];
    }
}
