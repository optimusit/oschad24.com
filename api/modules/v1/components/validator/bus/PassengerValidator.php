<?php
namespace api\modules\v1\components\validator\bus;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\PassengerBus;
use yii\validators\Validator;

class PassengerValidator extends Validator
{
    protected $_form;

    /**
     * @param PassengerBus $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $this->_form = Yii::$app->controller->core->GetServiceForm();
        if (!array_key_exists('trip', $this->_form) && !array_key_exists('places', $this->_form['trip'])) {
            $this->addError($model, 'carNumber', Module::t('app', 'Рейс не вибраний на попередньому кроці'));
        }
    }
}
