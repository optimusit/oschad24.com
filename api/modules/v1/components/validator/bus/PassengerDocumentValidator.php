<?php
namespace api\modules\v1\components\validator\bus;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\PassengerBus;

class PassengerDocumentValidator extends PassengerValidator
{
    /**
     * @param PassengerBus $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $numberErrors = count($model->errors);
        parent::validateAttribute($model, $attribute);
        if ($numberErrors == count($model->errors)) {
            if ($this->_form['bus']['need_document'] == 0 && is_null($model->$attribute)) {
                $this->addError($model, $attribute, Module::t('app', 'Документ є обовязковим для вибраного рейсу'));
            }
        }
    }
}
