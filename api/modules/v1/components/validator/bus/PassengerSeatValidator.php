<?php
namespace api\modules\v1\components\validator\bus;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\PassengerBus;

class PassengerSeatValidator extends PassengerValidator
{
    /**
     * @param PassengerBus $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $numberErrors = count($model->errors);
        parent::validateAttribute($model, $attribute);
        if ($numberErrors == count($model->errors)) {
            $seat = $model->$attribute;
            if (!in_array($seat, $this->_form['trip']['places'])) {
                $this->addError($model, $attribute, Module::t('app', 'Місце №{seat} зайняте', ['seat' => $seat]));
            }
        }
    }
}
