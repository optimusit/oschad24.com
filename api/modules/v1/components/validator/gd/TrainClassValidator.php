<?php
namespace api\modules\v1\components\validator\gd;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\Gd;
use yii\helpers\ArrayHelper;
use yii\validators\StringValidator;

class TrainClassValidator extends StringValidator
{
    /**
     * @param Gd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $numberOfErrors = count($model->errors);
        parent::validateAttribute($model, $attribute);
        if ($numberOfErrors == count($model->errors)) {
            $form = $model->GetServiceForm();
            if (array_key_exists('trains', $form)) {
                $trains = ArrayHelper::map($form['trains'], 'number', 'class_list');
                if (array_key_exists($model->trainNumber, $trains)) {
                    $trainClasses = ArrayHelper::map($trains[$model->trainNumber], 'name', 'name');
                    if (!array_key_exists($model->$attribute, $trainClasses)) {
                        $this->addError($model, 'carClass', Module::t('app', 'Відсутній клас вагону у потязі'));
                    }
                } else {
                    $this->addError($model, 'trainNumber', Module::t('app', 'Потяг відсутній'));
                }
            } else {
                $this->addError($model, $attribute, Module::t('app', 'Відсутня історія пошуку'));
            }
        }
    }
}
