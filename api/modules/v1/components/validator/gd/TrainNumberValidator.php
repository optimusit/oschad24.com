<?php
namespace api\modules\v1\components\validator\gd;

use Yii;
use api\modules\v1\components\ApiException;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\Gd;
use yii\helpers\ArrayHelper;
use yii\validators\StringValidator;

class TrainNumberValidator extends StringValidator
{
    /**
     * @param Gd $model
     * @param string $attribute
     * @throws ApiException
     */
    public function validateAttribute($model, $attribute)
    {
        $numberOfErrors = count($model->errors);
        parent::validateAttribute($model, $attribute);
        if ($numberOfErrors == count($model->errors)) {
            $form = $model->GetServiceForm();
            if (is_null($form)) {
                throw new ApiException(Module::t('app', 'Сесія скінчилась'), 100);
            }
            if (array_key_exists('trains', $form)) {
                $trains = ArrayHelper::map($form['trains'], 'number', 'class_list');
                if (!array_key_exists($model->$attribute, $trains)) {
                    $this->addError($model, $attribute, Module::t('app', 'Потяг відсутній'));
                }
            } else {
                $this->addError($model, $attribute, Module::t('app', 'Відсутня історія пошуку'));
            }
        }
    }
}
