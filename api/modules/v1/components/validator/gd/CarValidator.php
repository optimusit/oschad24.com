<?php
namespace api\modules\v1\components\validator\gd;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\Gd;
use yii\helpers\ArrayHelper;
use yii\validators\NumberValidator;

class CarValidator extends NumberValidator
{
    /**
     * @param Gd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $numberOfErrors = count($model->errors);
        parent::validateAttribute($model, $attribute);
        if ($numberOfErrors == count($model->errors)) {
            $form = $model->GetServiceForm();
            $train = $form['selected_train_info'];
            $cars = ArrayHelper::map($train['class']['cars'], 'id', 'number');
            if (!array_key_exists($model->$attribute, $cars)) {
                $this->addError($model, $attribute, Module::t('app', 'Вагон відсутній'));
            }
        }
    }
}
