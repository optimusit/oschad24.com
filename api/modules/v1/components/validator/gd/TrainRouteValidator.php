<?php
namespace api\modules\v1\components\validator\gd;

use api\modules\v1\Module;
use Yii;
use api\modules\v1\models\ticketsua\Gd;
use yii\helpers\ArrayHelper;
use yii\validators\StringValidator;

class TrainRouteValidator extends StringValidator
{
    /**
     * @param Gd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $numberOfErrors = count($model->errors);
        parent::validateAttribute($model, $attribute);
        if ($numberOfErrors == count($model->errors)) {
            $form = $model->GetServiceForm();
            if (array_key_exists('trains', $form) && array_key_exists('search', $form)) {
                $trains = ArrayHelper::map($form['trains'], 'number', 'number');
                if (!array_key_exists($model->$attribute, $trains)) {
                    $this->addError($model, $attribute, Module::t('app', 'Потяг відсутній'));
                }
            } else {
                $this->addError($model, $attribute, Module::t('app', 'Відсутня історія пошуку'));
            }
        }
    }
}
