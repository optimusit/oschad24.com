<?php
namespace api\modules\v1\components\validator\gd;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\Gd;
use yii\validators\RangeValidator;

class OperationTypeValidator extends RangeValidator
{
    /**
     * @param Gd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $form = $model->GetServiceForm();
        $className = $form['className'];
        $subClass = $form['subclass'];
        $carId = $form['carId'];
        $carOperationTypes = $model->GetCarOperationTypes($form['selected_train_info']['class'], $className, $subClass,
            $carId);
        if (!in_array($model->$attribute, $carOperationTypes)) {
            $this->addError($model, $attribute,
                Module::t('app', 'Тип документу повинен бути одним з наступних - [{documentType}]',
                    ['documentType' => implode(',', $carOperationTypes)]));
        }
    }
}
