<?php
namespace api\modules\v1\components\validator\gd;

use api\modules\v1\Module;
use Yii;
use api\modules\v1\models\ticketsua\Gd;
use yii\validators\RangeValidator;

class ServicesValidator extends RangeValidator
{
    /**
     * @param Gd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $form = $model->GetServiceForm();
        $className = $form['className'];
        $subClass = $form['subclass'];
        $allowedCarServices = $model->GetCarServices($form['selected_train_info']['class'], $className, $subClass);
        if (count($allowedCarServices)) {
            foreach ($model->$attribute as $selectedServices) {
                if (!array_key_exists($selectedServices, $allowedCarServices)) {
                    $this->addError($model, $attribute,
                        Module::t('app', 'Сервіс відсутній у вагоні. Доступні сервіси {services}',
                            ['services' => implode(',', array_keys($allowedCarServices))]));
                    break;
                }
            }
        } else {
            $this->addError($model, $attribute,
                Module::t('app', 'У вибраному вагоні не передбачені додаткові сервіси'));
        }
    }
}
