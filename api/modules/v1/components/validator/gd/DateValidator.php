<?php
namespace api\modules\v1\components\validator\gd;

use Yii;

class DateValidator extends \yii\validators\DateValidator
{
    public $pseudoRequired = false;
    public $requiredMessage;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->pseudoRequired) {
            $this->requiredMessage = Yii::t('yii', '{attribute} cannot be blank.');
        }
    }
}
