<?php
namespace api\modules\v1\components\validator\gd;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\PassengerGd;
use yii\validators\Validator;

class PassengerSeatValidator extends Validator
{
    /**
     * @param PassengerGd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $form = Yii::$app->controller->core->GetServiceForm();
        if (array_key_exists('carNumber', $form) && array_key_exists('carSeats', $form)) {
            if ($form['carNumber'] == $model->carNumber) {
                $seat = $model->$attribute;
                if (array_key_exists($seat, $form['carSeats'])) {
                    if ($form['carSeats'][$seat]['type'] > 10) {
                        $this->addError($model, $attribute,
                            Module::t('app', 'Місце №{seat} зайняте', ['seat' => $seat]));
                    }
                } else {
                    $this->addError($model, $attribute,
                        Module::t('app', 'У вибраному вагоні відсутнє місце №{seat}', ['seat' => $seat]));
                }
            }
        } else {
            $this->addError($model, 'carNumber', Module::t('app', 'Вагон не вибраний на попередньому кроці'));
        }
    }
}
