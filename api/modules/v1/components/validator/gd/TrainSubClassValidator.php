<?php
namespace api\modules\v1\components\validator\gd;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\Gd;
use yii\helpers\ArrayHelper;
use yii\validators\NumberValidator;

class TrainSubClassValidator extends NumberValidator
{
    /**
     * @param Gd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $numberOfErrors = count($model->errors);
        parent::validateAttribute($model, $attribute);
        if ($numberOfErrors == count($model->errors)) {
            $form = $model->GetServiceForm();
            if (array_key_exists('trains', $form)) {
                $trains = ArrayHelper::map($form['trains'], 'number', 'class_list');
                if (array_key_exists($model->trainNumber, $trains)) {
                    foreach ($trains[$model->trainNumber] as $class) {
                        if ($class['name'] == $model->carClass && $class['subclass'] == $model->carSubclass) {
                            return;
                        }
                    }
                    $this->addError($model, 'carSubclass', Module::t('app', 'Відсутній підклас вагону у потязі'));
                } else {
                    $this->addError($model, 'trainNumber', Module::t('app', 'Потяг відсутній'));
                }
            } else {
                $this->addError($model, $attribute, Module::t('app', 'Відсутня історія пошуку'));
            }
        }
    }
}
