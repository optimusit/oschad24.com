<?php
namespace api\modules\v1\components\validator\gd;

use api\modules\v1\Module;
use Yii;
use common\providers\tickets\GdTickets;
use api\modules\v1\models\ticketsua\Gd;
use api\modules\v1\models\ticketsua\PassengerGd;
use yii\validators\RequiredValidator;
use yii\validators\StringValidator;
use yii\validators\Validator;

class PassengerPrivilegesValidator extends Validator
{
    /**
     * @param PassengerGd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $privileges = $model->$attribute;
        $requiredValidator = new RequiredValidator();
        $countErrors = count($model->errors);
        if ($privileges == Gd::PRIVILEGES_CHILD) {
            $requiredValidator->validateAttribute($model, 'date');
            if ($countErrors == count($model->errors)) {
                $dateValidator = new \yii\validators\DateValidator();
                $dateValidator->validateAttribute($model, 'date');
                if ($countErrors == count($model->errors)) {
                    $model->date = Yii::$app->formatter->asDate($model->date);
                }
            }

        } elseif ($privileges == Gd::PRIVILEGES_STUDENT) {
            $form = Yii::$app->controller->core->GetServiceForm();
            $class = $form['className'];
            $studentPrivilege = Yii::$app->controller->core->PrivilegeStudentPresent($class);
            if ($studentPrivilege) {
                $requiredValidator->validateAttribute($model, 'document');
                if ($countErrors == count($model->errors)) {
                    $stringValidator = new StringValidator(['length' => 10]);
                    $stringValidator->validateAttribute($model, 'document');
                    if ($countErrors == count($model->errors)) {
                        $model->document_type = GdTickets::PRIVILEGES_STUDENT;
                    }
                }
            } else {
                $this->addError($model, $attribute,
                    Module::t('app', 'На вибраний вагон не поширюється студентська знижка'));
            }
        }
    }
}
