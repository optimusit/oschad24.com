<?php
namespace api\modules\v1\components\validator\gd;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\models\ticketsua\PassengerGd;
use yii\validators\Validator;

class PassengerCarValidator extends Validator
{
    /**
     * @param PassengerGd $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $form = Yii::$app->controller->core->GetServiceForm();
        if (array_key_exists('carNumber', $form)) {
            if ($form['carNumber'] != $model->$attribute) {
                $this->addError($model, $attribute,
                    Module::t('app', 'Вагон не відповидає вибраному на попередньому кроці'));
            }
        } else {
            $this->addError($model, $attribute, Module::t('app', 'Вагон не вибраний на попередньому кроці'));
        }
    }
}
