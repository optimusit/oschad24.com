<?php
namespace api\modules\v1\components\validator\entertainments;

use Yii;
use api\modules\v1\models\Entertainments;
use api\modules\v1\Module;
use yii\validators\NumberValidator;

class SeatsValidator extends NumberValidator
{
    /**
     * @param Entertainments $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $seats = $model->$attribute;
        if (count($seats) == 0) {
            $model->addError($attribute, Module::t('app', 'Місця не вибрані'));
        }
        if (count($seats) > $this->max) {
            $model->addError($attribute,
                Module::t('app', 'Перевищена кількість вібраних місць ({s})', ['s' => $this->max]));
        }
    }
}
