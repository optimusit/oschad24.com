<?php

namespace api\modules\v1\components;

use Yii;
use api\modules\v1\Module;
use common\models\Service;
use yii\base\Model;

class Core extends Model
{
    /**
     * Комиссия эквайринга для карт Ощадбанка
     */
    const ACQUIRING_RATE_DEFAULT = 3.5;
    /**
     * Комиссия эквайринга для карт остальных банков
     */
    const ACQUIRING_RATE_OTHER = 3.5;
    /**
     * Успех
     */
    const RESPONSE_CODE_SUCCESS = 0;
    /**
     * Ошибка валидации входных данных, data содержит список полей и пояснения
     */
    const RESPONSE_CODE_VALIDATION_ERROR = 10;
    /**
     * Ошибка, message содержит детали ошибки (может быть использована для всплывающего окна)
     */
    const RESPONSE_CODE_ERROR = 20;
    /**
     * Ошибка, message содержит детали ошибки, data содержит список рекоммендаций (может быть использована для редиректа на стартовоую страницу)
     */
    const RESPONSE_CODE_ERROR_RECOMMENDATIONS = 30;

    /** @var array $_response */
    protected $_response;
    protected $_session;
    protected $_userId;
    protected $_partnerId;
    protected $_side;
    protected $_controller;
    protected $_provider;
    /** @var Service */
    private $_service;

    use ModelMetaDataTrait;
    use ModelAttributePlaceholdersTrait;

    public function __construct($response, $session, $partner = null, $config = [])
    {
        $this->_response = $response;
        $this->_session = $session;
        $this->_side = Yii::$app->controller->module->side;
        $this->_partnerId = Module::Partner($partner)->id;
        $this->_userId = is_null(Module::User())
            ? null
            : Module::User()->id;
        $this->_controller = strtolower((new \ReflectionClass($this))->getShortName());
        $this->_service = Service::findOne(['provider_code' => strtolower((new \ReflectionClass($this))->getShortName())]);
        parent::__construct($config);
    }

    protected function getCacheKeyStationSearch($name)
    {
        return $this->_controller . 'Stations_' . Yii::$app->language . '_' . md5($name);
    }

    /**
     * @return Service|null|static
     */
    public function getService()
    {
        return $this->_service;
    }

    public function getPartnerId()
    {
        return $this->_partnerId;
    }

    public function GetServiceForm()
    {
        $form = $this->_session[$this->_controller . 'Form'];

        return is_array($form)
            ? $form
            : [];
    }

    public function SetServiceForm($data)
    {
        $this->_session[$this->_controller . 'Form'] = $data;
    }

    public function GetOrderData()
    {
        return $this->_session['OrderData'];
    }

    public function SetOrderData($data)
    {
        $this->_session['OrderData'] = $data;
    }

    /**
     * @return bool
     */
    public function isFrontCall()
    {
        return $this->_side == Module::SIDE_FRONT;
    }

    /**
     * @return bool
     */
    public function isBackCall()
    {
        return $this->_side == Module::SIDE_BACK;
    }

    /**
     * @return bool
     */
    public function isServiceCall()
    {
        return $this->_side == Module::SIDE_SERVICE;
    }
}
