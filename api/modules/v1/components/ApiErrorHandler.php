<?php

namespace api\modules\v1\components;

use Yii;
use yii\web\ErrorHandler;
use yii\web\HttpException;
use yii\web\Response;
use api\modules\v1\Module;

class ApiErrorHandler extends ErrorHandler
{
    protected $_jsonp;

    /**
     * @inheridoc
     */
    public function __construct($_jsonp)
    {
        parent::__construct();
        $this->_jsonp = $_jsonp;
    }

    /**
     * @inheridoc
     */
    protected function renderException($exception)
    {
        if (Yii::$app->has('response')) {
            $response = Yii::$app->getResponse();
        } else {
            $response = new Response();
        }

        $response->data = $this->convertExceptionToArray($exception);
//		$response->setStatusCode($exception->statusCode);

        $response->send();
    }

    /**
     * @inheritdoc
     */
    protected function convertExceptionToArray($exception)
    {
        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ($exception instanceof ApiException) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
        } elseif ($code == 400) {
//			$message = Module::t('app', 'У запиті клієнта виявлена синтаксична помилка');
            $message = $exception->getMessage();
        } elseif ($code == 401) {
            $message = Module::t('app', 'Доступ заборонено');
        } elseif ($code == 403) {
            $message = Module::t('app', 'Вам не дозволено виконувати дану дію.');
        } elseif ($code == 404) {
            $message = Module::t('app', 'Не знайдено');
        } else {
            $code = 500;
            $message = Module::t('app', 'Внутрішня помилка');
        }
        $result = [
            'code' => $code,
            'message' => $message,
            'data' => isset(Yii::$app->params['.']['response']['data'])
                ? Yii::$app->params['.']['response']['data']
                : ''
        ];
        if (!is_null($this->_jsonp)) {
            $result = [
                'data' => $result,
                'callback' => $this->_jsonp,
            ];
        }

        return $result;
    }
}
