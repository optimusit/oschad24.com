<?php

namespace api\modules\v1\models;

use api\modules\v1\components\CoreProducts;
use Yii;
use api\modules\v1\components\Core;
use common\models\GatewayRequest;
use common\models\GatewayResponseDescription;
use api\modules\v1\Module;
use common\payments\CommerceGateway\CommerceGateway;
use api\modules\v1\components\ApiException;

class Order extends Core
{
    /**
     * @var int Id заказа
     */
    public $orderId;
    /**
     * @var int Отметка времени
     */
    public $timestamp;
    /**
     * @var int показывать внутренню ошибку (меньше 0) ответа отпроцессинга при отрицательной оплате
     */
    public $showInternalInfo = 0;
    /**
     * @var bool Применить дефолтную коммисию эквайринга
     */
    public $defaultAcquiringRate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                'orderId',
                'required',
                'on' => [
                    'authorizationdata',
                    'paymentstatus',
                ],
            ],
            [
                [
                    'defaultAcquiringRate',
                ],
                'safe',
                'on' => [
                    'authorizationdata',
                ],
            ],
            [
                [
                    'defaultAcquiringRate',
                ],
                'boolean',
                'on' => [
                    'authorizationdata',
                ],
            ],
            [
                [
                    'orderId',
                ],
                'integer',
                'on' => [
                    'authorizationdata',
                    'paymentstatus',
                ],
            ],
            [
                'timestamp',
                'required',
                'on' => [
                    'authorizationdata',
                    'paymentstatus',
                ],
            ],
            [
                [
                    'timestamp',
                ],
                'integer',
                'on' => [
                    'paymentstatus',
                ],
            ],
        ]);

        return $result;
    }

    public function AuthorizationData()
    {
        /** @var \common\models\Order $order */
        $order = \common\models\Order::findOne([
            'id' => $this->orderId,
            'created_at' => $this->timestamp
        ]);
        if (!$order) {
            throw new ApiException(Module::t('app', 'Замовлення відсутнє'), $this::RESPONSE_CODE_ERROR);
        }
        // если параметр не задан, то и пересчитывать не будем.
        if (!is_null($this->defaultAcquiringRate)) {
            if ($this->defaultAcquiringRate) {
                $order->e_fee = CoreProducts::ACQUIRING_RATE_DEFAULT;
            } else {
                $order->e_fee = CoreProducts::ACQUIRING_RATE_OTHER;
            }
            $order->calculateCommission();
            $order->save();
        }
        $this->_response['data'] = CommerceGateway::createAuthorizationData($order);

        return $this->_response;
    }

    /**
     * Возвращает состояние оплаты заказа
     *
     * @return array
     */
    public function PaymentStatus()
    {
        $code = self::RESPONSE_CODE_ERROR_RECOMMENDATIONS;
        $data = null;
        $message = '';
        /** @var \common\models\Order $order */
        $order = \common\models\Order::findOne([
            'id' => $this->orderId,
            'partner_id' => $this->_partnerId
        ]);
        if ($order) {
            /** @var array $gatewayRequest */
            /** @var GatewayRequest $gatewayRequest */
            /** @var GatewayResponseDescription $responseDescription */
            $gatewayRequest = $order->getGatewayRequest()
                ->where([
                    'order_id' => $this->orderId,
                    'timestamp' => $this->timestamp,
                ])//->andWhere(['not', ['action' => null]])
                //->andWhere(['not', ['rc' => null]])
                ->one();
            if ($gatewayRequest) {
                $responseDescription = $gatewayRequest->getGatewayResponseDescription()
                    ->one();
                if ($responseDescription) {
                    $canBePaid = false;
                    if ($gatewayRequest->rc < 0 && $this->showInternalInfo == 0) {
                        $message = Module::t('app', 'Внутрішня помилка оплати');
                        $recommendations = [
                            Module::t('app', 'Внутрішня помилка оплати')
                        ];
                    } else {
                        $message = $responseDescription->getName();
                        $recommendations = $responseDescription->getDescription();
                        if (!$order->isExpired()) {
                            $canBePaid = true;
                        }
                    }
                    $data = [
                        'orderId' => $this->orderId,
                        'timestamp' => $order->created_at,
                        'canBePaid' => $canBePaid,
                        'recommendations' => $recommendations,
                    ];
                    if ($gatewayRequest->rc == 0) {
                        $code = self::RESPONSE_CODE_SUCCESS;
                    }
                } else {
                    $message = Module::t('app', 'Відсутня спроба оплати');
                }
            } else {
                $message = Module::t('app', 'Відсутній платіж по замовленню');
            }
        } else {
            $message = Module::t('app', 'Немає такого замовлення або доступ заборонений');
        }
        $this->_response['code'] = $code;
        $this->_response['data'] = $data;
        $this->_response['message'] = $message;

        return $this->_response;
    }
}
