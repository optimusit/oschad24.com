<?php

namespace api\modules\v1\models;

use Yii;
use api\modules\v1\components\Core;
use api\modules\v1\Module;
use common\models\GatewayRequest;
use api\modules\v1\components\ApiException;
use common\models\Order;
use common\payments\CommerceGateway\CommerceGateway;

class Payment extends Core
{
    public $orderId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'orderId',
                ],
                'required',
                'on' => [
                    'getauthdata',
                    'refund',
                ]
            ],
        ]);

        return $result;
    }

    /**
     * Возвращает данные для оплаты заказа в системе e-Commerce Gateway
     *
     * @param $orderId int № Замовлення
     * @return array
     * @throws ApiException
     */
    public function GetAuthData()
    {
        /** @var Order $order */
        $order = Order::findOne(['id' => $this->orderId]);
        $orderData = $this->GetOrderData();
        if (!$order || $order->id != $orderData['id']) {
            $this->_response = [
                'code' => 101,
                'message' => 'Немає такого замовлення або доступ заборонений',
                'data' => ''
            ];
        } else {
            $this->_response['data'] = CommerceGateway::createAuthorizationData($order);
        }

        return $this->_response;
    }

    /**
     * Запрос в систему e-Commerce Gateway на возврат средств за оплату заказа
     *
     * @param $orderId int № Замовлення
     * @return array
     */
    public function Refund()
    {
        $result = false;
        /** @var Order $order */
        $order = Order::findOne(['id' => $this->orderId]);
        $orderData = $this->GetOrderData();
        if (!$order || $order->id != $orderData['id']) {
            $this->_response = [
                'code' => 101,
                'message' => Module::t('app', 'Немає такого замовлення або доступ заборонений'),
            ];
        } else {
            $gatewayRequest = $order->getGatewayRequest()
                ->where([
                    'action' => GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY,
                    'rc' => 0
                ])
                ->one();
            if (!$gatewayRequest) {
                $this->_response = [
                    'code' => 101,
                    'message' => Module::t('app', 'Відсутній платіж по замовленню'),
                ];
            } else {
                $result = CommerceGateway::reversalRequest($gatewayRequest);
            }
            $this->_response['data'] = $result;
        }

        return $this->_response;
    }
}
