<?php

namespace api\modules\v1\models;

use Yii;
use common\models\Order;
use kartik\mpdf\Pdf;
use api\modules\v1\components\ApiException;
use api\modules\v1\components\CoreProducts;
use common\components\AppHelper;
use common\payments\CommerceGateway\CommerceGateway;
use common\models\karabas\Cities;
use common\models\karabas\ActivityTypes;
use common\models\karabas\PartnerEventActivityType;
use common\providers\karabas\Karabas;
use api\modules\v1\Module;
use common\models\karabas\PartnerEvents;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use api\modules\v1\components\validator\entertainments\SeatsValidator;
use common\models\karabas\PartnerEventsCarousel;

class Entertainments extends CoreProducts
{
    const DATE_FORMAT = 'php:d-m-Y';
    const SEARCH_MAX_DAYS = 180;
    const SEARCH_TO_DAYS = 30;
    const RESERVATION_END_SPACE = 30;
    const CACHE_DURATION_KARABAS = 86400;
    //если это карабас, то коммисия не начисляется, т.к. она внутренняя
    const SERVICE_FEE = 0;
    const MAX_SEATS = 5;

    protected $_form;
    protected $_providers = ['Karabas'];
    protected $_provider = 'Karabas';
    protected $_allowedFilters = [
        'CityId'
    ];

    public $id;
    public $dateFrom;
    public $dateTo;
    public $searchEventName;
    public $cityId;
    public $activityTypeId;
    public $pageSize;
    public $currentPage;
    public $cityName;
    public $orderId;
    public $itemId;
    public $seats;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'pageSize',
                    'currentPage',
                    'cityId',
                    'activityTypeId',
                    'dateFrom',
                    'dateTo',
                    'searchEventName',
                ],
                'safe',
                'on' => 'search'
            ],
            [
                [
                    'pageSize',
                    'currentPage',
                ],
                'integer',
                'on' => 'search'
            ],
            [
                [
                    'cityId',
                    'activityTypeId',
                ],
                'each',
                'rule' => ['integer'],
                'on' => 'search'
            ],
            [
                [
                    'cityId',
                    'activityTypeId',
                ],
                'default',
                'value' => [],
                'on' => 'search'
            ],
            [
                [
                    'dateFrom',
                ],
                'default',
                'value' => date(AppHelper::GetDateFormat()),
                'on' => 'search'
            ],
            [
                [
                    'dateFrom',
                ],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(), strtotime('+' . self::SEARCH_MAX_DAYS . ' days')),
                'on' => 'search'
            ],
            [
                [
                    'dateTo',
                ],
                'default',
                'value' => date(AppHelper::GetDateFormat(), strtotime('+' . self::SEARCH_TO_DAYS . ' days')),
                'on' => 'search'
            ],
            [
                [
                    'dateTo',
                ],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(),
                    strtotime('+' . (string)(intval(self::SEARCH_MAX_DAYS) + 1) . ' days')),
                'on' => 'search'
            ],
            [
                'searchEventName',
                'default',
                'value' => '',
                'on' => 'search'
            ],
            [
                'searchEventName',
                'string',
                'min' => 3,
                'max' => 10,
                'on' => 'search'
            ],
            [
                [
                    'id',
                ],
                'required',
                'on' => 'event'
            ],
            [
                [
                    'id',
                ],
                'integer',
                'on' => 'event'

            ],
            [
                [
                    'cityName',
                ],
                'required',
                'on' => 'cities'
            ],
            [
                [
                    'cityName',
                ],
                'trim',
                'on' => 'cities'
            ],
            [
                [
                    'cityName',
                ],
                'string',
                'min' => 1,
                'max' => 10,
                'on' => 'cities'
            ],
            [
                'itemId',
                'required',
                'on' => 'deleteplacefromcart'
            ],
            [
                [
                    'orderId',
                ],
                'required',
                'on' => [
                    'orderdata',
                    //'checkout',
                ]
            ],
            [
                [
                    'seats',
                ],
                'required',
                'on' => [
                    'checkout',
                ]
            ],
            [
                [
                    'seats',
                ],
                'default',
                'value' => [],
                'on' => 'checkout'
            ],
            [
                [
                    'seats',
                ],
                SeatsValidator::className(),
                'min' => 1,
                'max' => self::MAX_SEATS,
                'on' => 'checkout'
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $result = array_merge(parent::scenarios(), [
            'activitytypes' => [],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'searchEventName' => Module::t('app', 'Пошук за назвою'),
            'cityName' => Module::t('app', 'Місто'),
            'dateFrom' => Module::t('app', 'Дата З'),
            'dateTo' => Module::t('app', 'Дата По'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'searchEventName' => Module::t('app', 'Пошук за назвою'),
            'cityName' => Module::t('app', 'Місто'),
            'dateFrom' => Module::t('app', 'Дата З'),
            'dateTo' => Module::t('app', 'Дата По'),
        ]);

        return $result;
    }

    public function __construct($response, $session)
    {
        parent::__construct($response, $session);
        $this->_form = $this->GetServiceForm();
        if (is_null($this->_form)) {
            $this->_form = [];
            $this->SetServiceForm($this->_form);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        try {
            $this->cityId = json_decode($this->cityId);
        } catch (\Exception $e) {
        }
        try {
            $this->activityTypeId = json_decode($this->activityTypeId);
        } catch (\Exception $e) {
        }
        try {
            $this->seats = array_unique(json_decode($this->seats));
        } catch (\Exception $e) {
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
    }

    public function Search()
    {
        $filters = [];
        if (!is_null($this->dateFrom)) {
            $filters['DateFrom'] = $this->dateFrom;
        }
        if (!is_null($this->dateTo)) {
            $filters['DateTo'] = $this->dateTo;
        }
        $filters['searchEventName'] = $this->searchEventName;
        if (!is_null($this->activityTypeId)) {
            $filters['ActivityTypeId'] = $this->activityTypeId;
        }
        if (!is_null($this->cityId)) {
            $filters['CityId'] = $this->cityId;
        }
        $data = $this->Line($this->pageSize, $this->currentPage, $filters);
        $this->_response['data'] = $data;

        return $this->_response;
    }

    protected function Line($pageSize, $currentPage, $filters = [])
    {
        return $this->KarabasLine($pageSize, $currentPage, $filters);
    }

    protected function KarabasLine($pageSize, $currentPage, $filters = [])
    {
        $partnerEventsTable = PartnerEvents::tableName();
//		$dbDependency = new DbDependency();
//		$dbDependency->sql = 'SELECT pe.updated_at FROM ' . $partnerEventsTable
//			. ' as pe where pe.EventId = (select max(pe.EventId) FROM ' . $partnerEventsTable
//			. ' as pe order by pe.EventId)';
//		$dbDependency->resetReusableData();
        // закешируем резултат запроса максимум на 1 день или до следующей загрузки событий от првайдера
//		$events = PartnerEvents::getDb()
//			->cache(function ($db) use ($filters, $partnerEventsTable) {
        /** @var ActiveQuery $events */
        $events = PartnerEvents::find()
            ->select([
                $partnerEventsTable . '.EventId',
                $partnerEventsTable . '.EventName',
                $partnerEventsTable . '.LineImage',
                $partnerEventsTable . '.LineDateTime',
                $partnerEventsTable . '.LinePriceRange',
                $partnerEventsTable . '.LinePlace',
                '("' . $this->_provider . '") as Provider'
            ]);
        foreach ($this->_allowedFilters as $allowedFilterKey => $allowedFilterValue) {
            if (array_key_exists($allowedFilterValue, $filters) && count($filters[$allowedFilterValue]) > 0) {
                $events->andWhere([$allowedFilterValue => $filters[$allowedFilterValue]]);
            }
        }
        if (isset($filters['ActivityTypeId']) && count($filters['ActivityTypeId']) > 0) {
            $events = $events->innerJoin(PartnerEventActivityType::tableName() . ' as peat',
                'peat.EventId = ' . PartnerEvents::tableName() . '.EventId')
                ->innerJoin(ActivityTypes::tableName() . ' as at', 'at.id = peat.ActivityTypeId');
            $events->andWhere(['peat.ActivityTypeId' => $filters['ActivityTypeId']]);
        }
        $from = AppHelper::CreateDateTimeFromString($filters['DateFrom']);
        $minDate = AppHelper::getDateTimeNowMidNight();
        if (!$from || $from->getTimestamp() < $minDate->getTimestamp()) {
            $from = $minDate;
        }
        $events->andWhere([
            '>=',
            $partnerEventsTable . '.EventDate',
            $from->format('Y-m-d')
        ]);
        $to = AppHelper::CreateDateTimeFromString($filters['DateTo']);
        if ($to) {
            $to->add(new \DateInterval('P1D'));
            $events->andWhere([
                '<',
                $partnerEventsTable . '.EventDate',
                $to->format('Y-m-d')
            ]);
        }
        if (!is_null($filters['searchEventName']) && !empty($filters['searchEventName'])) {
            $events->andWhere([
                'like',
                'EventName',
                '%' . $filters['searchEventName'] . '%',
                false
            ]);
        }
        $events = $events->orderBy(['EventDate' => SORT_ASC])
            ->asArray()
            ->with('activityTypes')
            ->all();

//				return $events;
//			}, self::CACHE_DURATION_KARABAS, $dbDependency);
        if ($currentPage > 0) {
            $currentPage--;
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $events,
            'key' => 'EventId',
            'pagination' => [
                'pageSize' => $pageSize,
                'page' => $currentPage,
            ],
        ]);
        $result = [
            'total' => $dataProvider->totalCount,
            'events' => array_values($dataProvider->models),
        ];

        return $result;
    }

    public function Event()
    {
        $this->AuthProvider();
        $data = null;
        $providerEvent = $this->_provider . __FUNCTION__;
        if (!method_exists($this, $providerEvent) || !($data = $this->$providerEvent($this->id))) {
            throw new ApiException(Module::t('app', 'Подія не знайдена'), $this::RESPONSE_CODE_ERROR);
        }
        $this->_response['data'] = $data;

        return $this->_response;
    }

    public function OrderData()
    {
        $data = null;
        $providerOrderData = $this->_provider . __FUNCTION__;
        if (method_exists($this, $providerOrderData)) {
            $result = $this->$providerOrderData($this->id);
            if ($result) {
                $this->_response['data'] = $result['data'];
            } else {
                throw new ApiException(Module::t('app', 'Замовлення не знайдене'), $this::RESPONSE_CODE_ERROR);
            }
        }

        return $this->_response;
    }

    public function DeletePlaceFromCart()
    {
        $providerDeletePlaceFromCard = $this->_provider . __FUNCTION__;
        if (method_exists($this, $providerDeletePlaceFromCard)) {
            $result = $this->$providerDeletePlaceFromCard($this->itemId);
            if ($result['success']) {
                $this->_response['data'] = '';
            } else {
                throw new ApiException($result['message'], $this::RESPONSE_CODE_ERROR);
            }
        }

        return $this->_response;
    }

    public function Checkout()
    {
        $form = $this->GetServiceForm();
        $providerCheckout = $this->_provider . __FUNCTION__;
        if (method_exists($this, $providerCheckout)) {
            $providerOrderData = $this->_provider . 'OrderData';
            $orderDetails = $this->$providerOrderData();
            if ($orderDetails['success']) {
                $orderDetailsData = get_object_vars($orderDetails['data']);
                // если в заказе пусто, то это значит, что это заказ уже со следующей сессии
                if (count($orderDetailsData['items'])) {
                    $fullName = $this->name . ' ' . $this->surname;
                    $result = $this->$providerCheckout($orderDetailsData['orderId'], $this->email, $fullName);
                    if ($result['success']) {
                        $reservation = get_object_vars($result['data']);
                        $eventDetails = $this->KarabasEvent($orderDetailsData['items'][0]->event);
                        $ticketsCount = $orderDetailsData['orderItemsCount'];
                        $expirationTime = $this->getReservationEndSpace(new \DateTime());
                        $tickets = [];
                        foreach ($orderDetailsData['items'] as $item) {
                            $tickets[] = [
                                'id' => $item->id,
                                'pid' => $item->pid,
                                'zone' => $item->zone,
                                'row' => $item->row,
                                'number' => $item->number,
                                'barCode' => $item->barCode,
                                'amount' => $item->totalAmount,
                            ];
                        }
                        $orderInfo = [
                            //'event' => $reservation['orderEvents'],
                            'event' => $eventDetails,
                            'tickets' => $tickets,
                        ];
                        /** @var \common\models\Order $order */
                        $order = $this->saveData($reservation['order'], $expirationTime, $reservation['amount'], null,
                            $ticketsCount, $form, $orderInfo, Karabas::PREFIX, $result['dialog']);
                        $this->_response['code'] = 0;
                        $this->_response['data'] = [
                            'OrderInfo' => $orderInfo,
                            'PaymentAuthorizationData' => CommerceGateway::createAuthorizationData($order)
                        ];
                        if ($this->isFrontCall()) {
                            Yii::$app->core->reservationCreated($order);
                        }
                    } else {
                        throw new ApiException($result['message'], $this::RESPONSE_CODE_ERROR);
                    }
                } else {
                    throw new ApiException(Module::t('app', 'Виберіть місця.'), $this::RESPONSE_CODE_ERROR);
                }
            } else {
                throw new ApiException(Module::t('app', 'Виникла помилка при створенні рахунку. Виберіть місце.'),
                    $this::RESPONSE_CODE_ERROR);
            }
        }

        return $this->_response;
    }

    public function ActivityTypes()
    {
        $data = ActivityTypes::find()
            ->select('id, name, color')
            ->where(['active' => 1])
            ->asArray()
            ->all();
        $this->_response['data'] = $data;

        return $this->_response;
    }

    public function Cities()
    {
        $this->cityName = AppHelper::reverseTransliteration($this->cityName);
        $data = Cities::find()
            ->select('id, name')
            ->distinct()
            ->where([
                'like',
                'name',
                $this->cityName . '%',
                false
            ])
            ->asArray()
            ->all();
        $this->_response['data'] = $data;

        return $this->_response;
    }

    protected function KarabasEvent($id)
    {
        $event = null;
        $karabas = new Karabas();
        if (!is_null($event = PartnerEvents::find()
            ->select([
                '("' . $this->GetProviderAuth() . '") as ProviderAuth',
                '("' . $karabas->getAPIUrl() . '") as ProviderUrl',
                'EventName',
                'HallName',
                'DetailsDescription',
                'DetailsImage',
                'DetailsDate',
                'DetailsTime',
                'DetailsPlace',
                'DetailsAddress',
                'DetailsType',
            ])
            ->where([
                'EventId' => $id,
            ])
            ->asArray()
            ->one())
        ) {
            $event['DetailsType'] = unserialize($event['DetailsType']);
            // перед выводом данных по событию очистим корзину у карабаса т.к. карабас позволяет в одном заказе
            // покупать более 1 мероприятия, что для нас не приемлемо
            $this->KarabasClearCurrentOrder();
        }

        return $event;
    }

    protected function KarabasClearProductCart()
    {
        $karabas = new Karabas($this->GetProviderAuth());

        return $karabas->ClearProductCart();
    }

    protected function KarabasClearCurrentOrder()
    {
        $karabas = new Karabas($this->GetProviderAuth());

        return $karabas->ClearCurrentOrder();
    }

    protected function KarabasOrderData($id = null)
    {
        $karabas = new Karabas($this->GetProviderAuth());

        return $karabas->GetOrder($id);
    }

    protected function KarabasDeletePlaceFromCart($cartId)
    {
        $karabas = new Karabas($this->GetProviderAuth());

        return $karabas->DeletePlaceFromCart($cartId);
    }

    protected function KarabasCheckout($id, $email, $fullName, $deliveryType = '', $paymentType = '')
    {
        $karabas = new Karabas($this->GetProviderAuth());

        return $karabas->ConfirmOrder($id, $email, $fullName);
    }

    protected function AuthProvider()
    {
        if (!isset($this->_form['auth' . $this->_provider])) {
            $providerFullName = '\common\providers\\' . strtolower($this->_provider) . '\\' . $this->_provider;
            $provider = new $providerFullName();
            $this->_form['auth' . $this->_provider] = $provider->Login();
            $this->SetServiceForm($this->_form);
        }
    }

    protected function GetProviderAuth()
    {
        return $this->_form['auth' . $this->_provider];
    }

    /**
     * @param \DateTime $dateTime
     * @return \DateTime
     */
    protected function getReservationEndSpace(\DateTime $dateTime)
    {
        return $dateTime->add(new \DateInterval('PT' . $this::RESERVATION_END_SPACE . 'M'));
    }

    /**
     * @param Order $order
     * @return array
     */
    public static function eticket($order)
    {
        $success = false;
        $documents = [];
        if (!is_null(($eticket = $order->getEticket()))) {
            $params = unserialize($order->custom);
            $params['eticket'] = $eticket;
            $params['extra'] = [
                'oschad_logo' => Yii::getAlias('@common/storage/documents/layouts/images/oschad_logo.png'),
                'forbidden_items' => Yii::getAlias('@common/storage/documents/layouts/images/karabas_forbidden_items.png'),
                'karabas_logo' => Yii::getAlias('@common/storage/documents/layouts/images/karabas_logo.png'),
                'vebua_logo' => Yii::getAlias('@common/storage/documents/layouts/images/vebua_logo.png'),
                'scan' => Yii::getAlias('@common/storage/documents/layouts/images/scan.png'),
                'owner' => $order->getCustomerFullName(),
            ];
            $documentLayout = Yii::getAlias('@common/storage/documents/layouts/entertainments.php');
            //$eticketData = ArrayHelper::index($eticket['tickets'], 'id');
            foreach ($params['eticket']['tickets'] as $ticket) {
                if (!is_null($ticket['Barcode']) && !empty($ticket['Barcode'])) {
                    $params['ticket'] = $ticket;
                    $content = Yii::$app->controller->renderFile($documentLayout, $params);
                    $pdf = new Pdf([
                        'mode' => Pdf::MODE_UTF8,
                        'format' => Pdf::FORMAT_A4,
                        'orientation' => Pdf::ORIENT_PORTRAIT,
                        //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                        'cssFile' => '@common/storage/documents/layouts/entertainments.css',
                        //'cssInline' => 'body{font-size:18px}',
                    ]);
                    $data = $pdf->output($content, '', Pdf::DEST_STRING);
                    $documents[$ticket['Barcode']] = $data;
                    $success = true;
                }
            }
        }
        $result = [
            'success' => $success,
            'code' => 0,
            'data' => $documents,
        ];

        return $result;
    }

    public static function getCarouselData()
    {
        $partnerEventsTable = PartnerEvents::tableName();
        $partnerEventsCarousel = PartnerEventsCarousel::tableName();
        /** @var ActiveQuery $events */
        $events = PartnerEvents::find()
            ->select([
                $partnerEventsTable . '.EventId',
                $partnerEventsTable . '.ActivityImageLarge',
                $partnerEventsTable . '.LineDateTime',
                $partnerEventsTable . '.LinePlace'
            ]);
//		$events->andWhere([
//			'in',
//			$partnerEventsTable . '.EventId',
//			$eventsId
//		]);
        $events->innerJoin("$partnerEventsCarousel as pec", "pec.EventId = $partnerEventsTable.EventId");
        $events->andWhere($partnerEventsTable . '.EventDate>now()');
        $events = $events->orderBy(['EventDate' => SORT_ASC])
            ->asArray()
            ->all();

        return $events;
    }

    public static function getCitiesById(array $id = [])
    {
        $result = Cities::find()
            ->select('id, name')
            ->distinct()
            ->where([
                'in',
                'id',
                $id
            ])
            ->asArray()
            ->all();

        return $result;
    }
}
