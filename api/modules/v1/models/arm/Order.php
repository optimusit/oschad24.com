<?php

namespace api\modules\v1\models\arm;

use Yii;
use api\modules\v1\components\Core;

class Order extends Core
{
    public function getAll()
    {
        $data = \common\models\Order::find()
            ->asArray()
            ->all();
        //$this->_response['code'] = 0;
        //$this->_response['message'] = ';
        $this->_response['data'] = $data;

        return $this->_response;
    }
}
