<?php

namespace api\modules\v1\models\arm;

use Yii;
use api\modules\v1\components\Core;
use common\models\LoginForm;

class User extends Core
{
    public function Login($params)
    {
        $model = new LoginForm();
        $model->setAttributes($params);
        if ($model->login()) {
            $userRoles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
            $data = [
                'accessToken' => \Yii::$app->user->identity->getAuthKey(),
                'role' => $userRoles,
            ];
            $this->_response['data'] = $data;
        } else {
            $model->validate();

            $this->_response['code'] = 101;
            $this->_response['message'] = $model->errors;
        }

        return $this->_response;
    }
}
