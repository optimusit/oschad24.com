<?php

namespace api\modules\v1\models;

use common\models\Order;
use Yii;
use common\models\Carousel;
use common\models\Service;
use common\components\AppHelper;
use api\modules\v1\components\Core;
use api\modules\v1\Module;
use yii\caching\ExpressionDependency;
use yii\helpers\ArrayHelper;
use api\modules\v1\models\ticketsua\Avia;

class Application extends Core
{
    public function Config()
    {
        return [
            'products' => [
                'main' => 'АТ «Ощадбанк»',
                'bus' => 'АТ «Ощадбанк» Автобусні квитки',
                'train' => 'АТ «Ощадбанк» Залізничні квитки',
                'avia' => 'АТ «Ощадбанк» Авіаквитки',
                'entertainments' => 'АТ «Ощадбанк» Розважальні заходи',
                'insurance' => 'АТ «Ощадбанк» Страхування',
                'football' => 'АТ «Ощадбанк» Квитки на футбол',
            ],
            'strings' => [
                'Payment' => [
                    'Error' => Module::t('app', 'Вибачте, сталася помилка'),
                    'WhatToDo' => Module::t('app', 'Что делать?'),
                    'OtherProducts' => Module::t('app', 'Другие проукты'),
                    'OtherTickets' => Module::t('app', 'Возможно, Вас интересуют другие билеты?'),
                ],
                'Header' => [
                    'Languages' => [
                        'uk' => 'Український',
                        'ru' => 'Русский',
                        'en' => 'English',

                    ],
                    'Phones' => AppHelper::GetSupportPhones(),
                    'Email' => 'support@oshad24.com',
                    'Login' => Module::t('app', 'Доброго дня, вхід в кабінет'),
                    'Help' => Module::t('app', 'Допомога онлайн'),
                    'Chat' => Module::t('app', 'Онлайн чат'),
                    'Call' => Module::t('app', 'Онлайн дзвінок'),
                    'CallMob' => Module::t('app', 'Дзвінки з мобільного'),

                ],
                'Content' => [
                    'SliderPhotos' => [
                        [
                            'id' => 0,
                            'url' => "images/banners/ban_1.jpg",
                            'redirect' => "#train",
                            'title' => Module::t('app', 'Залізничні квитки'),
                            'text' => Module::t('app', 'Забудь про каси купуй квитки на поїзд онлайн'),
                            'background_color' => '#a6a5cd',
                        ],
                        [
                            'id' => 1,
                            'url' => "images/banners/ban_2.jpg",
                            'redirect' => "#fly",
                            'title' => Module::t('app', 'Авіаквитки'),
                            'text' => Module::t('app', 'Забудь про каси купуй квитки на літак онлайн'),
                            'background_color' => '#8fc5ea',
                        ],
                        [
                            'id' => 2,
                            'url' => "images/banners/ban_3.jpg",
                            'redirect' => "#bus",
                            'title' => Module::t('app', 'Автобусні квитки'),
                            'text' => Module::t('app', 'Подорожуй разом з нами - замовляй квитки онлайн!'),
                            'background_color' => '#88abd5',
                        ],
                        [
                            'id' => 3,
                            'url' => "images/banners/ban_4.jpg",
                            'redirect' => "#entertainments",
                            'title' => Module::t('app', 'Розважальні заходи'),
                            'text' => Module::t('app', 'Зроби своє життя яскравим разом з нами!'),
                            'background_color' => '#aec953',
                        ],
                        [
                            'id' => 4,
                            'url' => "images/banners/ban_5.jpg",
                            'redirect' => "#shakhtar",
                            'title' => Module::t('app', 'Квитки на футбол'),
                            'text' => Module::t('app', 'Прийшов час дивитись футбол!'),
                            'background_color' => '#6cbe6e',
                        ],
                        [
                            'id' => 5,
                            'url' => "images/banners/ban_6.jpg",
                            'title' => Module::t('app', 'Страхування'),
                            'text' => Module::t('app', 'Легко замовити. Просто купити.'),
                            'background_color' => '#97d8c5',
                        ],
                    ],
                ],
                'Footer' => [
                    'Years' => '©1991-2015',
                    'BankName' => Module::t('app', 'АТ Ощадбанк'),
                    'License' => Module::t('app', 'Ліцензія Національного банку Україні № 148 від 05.11.2011 р.'),
                    'Phone' => '0-800-210-800',
                    'FreeCalls' => Module::t('app', 'безкоштовно зі стаціонарних телефонів по території України'),
                    'PhoneMobile' => '+38-044-363-33',
                    'TicketsOnline' => Module::t('app', 'Квитки онлайн'),
                    'FAQ' => Module::t('app', 'Довідкові матеріали'),
                    'SocialNetwork' => Module::t('app', 'Ми в соц. мережах'),
                ],

                'Fly' => [
                    'Titles' => [
                        'Search' => Module::t('app', 'далі'),
                    ],
                    'BarSteps' => [
                        [
                            'id' => 0,
                            'title' => Module::t('app', 'Маршрут'),
                            'dotTitle' => 1,
                        ],
                        [
                            'id' => 1,
                            'title' => Module::t('app', 'Рейс'),
                            'dotTitle' => 2,
                        ],
                        [
                            'id' => 2,
                            'title' => Module::t('app', 'Бронювання'),
                            'dotTitle' => 3,
                        ],
                        [
                            'id' => 3,
                            'title' => Module::t('app', 'Финиш'),
                            'dotTitle' => 4,
                        ],
                    ],
                    'flyType' => [
                        [
                            'id' => 0,
                            'title' => Module::t('app', 'В обидва боки'),
                        ],
                        [
                            'id' => 1,
                            'title' => Module::t('app', 'В один бiк'),
                        ],
                        [
                            'id' => 2,
                            'title' => Module::t('app', 'Складний маршрут'),
                        ],
                    ],
                    'Class' => [
                        [
                            'id' => 0,
                            'value' => 'A',
                            'title' => Module::t('app', 'Будь-який'),
                        ],
                        [
                            'id' => 1,
                            'value' => 'E',
                            'title' => Module::t('app', 'Економ'),
                        ],
                        [
                            'id' => 2,
                            'value' => 'B',
                            'title' => Module::t('app', 'Бізнес'),
                        ],
                    ],
                    'Step' => [
                        [
                        ],
                        [
                            'TableHeaders' => [
                                [
                                    'id' => 0,
                                    'title' => Module::t('app', 'Ціна'),
                                    'sort' => 'amount',
                                ],
                                [
                                    'id' => 1,
                                    'title' => Module::t('app', 'Тривалість'),
                                    'sort' => 'duration',
                                ],
                                [
                                    'id' => 1,
                                    'title' => Module::t('app', 'Кількість пересадок'),
                                    'sort' => 'totalPartsSegments',
                                ],
                            ],
                        ],
                    ],
                ],

                'Entertainments' => [
                    'TicketTypes' => [
                        "type-3" => [
                            Module::t('app',
                                "Після оплати перевірте пошту, ми відправимо квиток на вказану Вами e-mail адресу"),
                            Module::t('app', "Роздрукуйте Ваш квиток на принтері"),
                            Module::t('app', "Надайте квиток на вході"),
                        ],
                    ],
                ],

                'Shakhtar' => [
                    'TicketTypes' => [
                        "type-3" => [
                            Module::t('app',
                                "Після оплати перевірте пошту, ми відправимо квиток на вказану Вами e-mail адресу"),
                            Module::t('app', "Роздрукуйте Ваш квиток на лазерному принтері, зі 100%  форматом печаті"),
                            Module::t('app', "Надайте квиток на вході"),
                        ],
                    ],
                ],

                'Bus' => [
                    'TicketTypes' => [
                        "type-3" => [
                            Module::t('app',
                                "Після оплати перевірте пошту, ми відправимо бланк на вказану Вами e-mail адресу"),
                            Module::t('app', "Роздрукуйте Ваш квиток на принтері"),
                            Module::t('app', "Надайте роздрукований квиток при посадці на автобус"),
                        ],
                    ],
                    'Titles' => [
                        'DaysForward' => Module::t('app', 'Искать варианты +3 дня'),
                    ],
                    'BarSteps' => [
                        [
                            'id' => 0,
                            'title' => Module::t('app', 'Маршрут'),
                            'dotTitle' => 1,
                        ],
                        [
                            'id' => 1,
                            'title' => Module::t('app', 'Рейс'),
                            'dotTitle' => 2,
                        ],
                        [
                            'id' => 2,
                            'title' => Module::t('app', 'Бронювання'),
                            'dotTitle' => 3,
                        ],
                        [
                            'id' => 3,
                            'title' => Module::t('app', 'Финиш'),
                            'dotTitle' => 4,
                        ],
                        [
                            'id' => 4,
                            'title' => '',

                        ],
                    ],
                    'Step' => [
                        [
                            'PlaceHolderFrom' => Module::t('app', 'звiдки'),
                            'PlaceHolderTo' => Module::t('app', 'куди'),
                            'Search' => Module::t('app', 'Пошук квитка'),
                        ],
                        [
                            'TableHeaders' => [
                                [
                                    'id' => 0,
                                    'title' => Module::t('app', 'Рейс №'),
                                    'sort' => 'name',
                                ],
                                [
                                    'id' => 1,
                                    'title' => Module::t('app', 'Модель'),
                                    'sort' => 'bus_type',
                                ],
                                [
                                    'id' => 2,
                                    'title' => Module::t('app', 'Відправлення'),
                                    'sort' => 'departure_from_point_date_time',
                                ],
                                [
                                    'id' => 4,
                                    'title' => Module::t('app', 'Прибуття'),
                                    'sort' => 'arrival_to_point_date_time',
                                ],
                                [
                                    'id' => 5,
                                    'title' => Module::t('app', 'Вільних місць'),
                                    'sort' => 'free_places',
                                ],
                            ],
                            'Choose' => Module::t('app', 'вибрати'),
                        ],
                        [
                            'TableHeaders' => [
                                [
                                    'id' => 0,
                                    'title' => '#',
                                ],
                                [
                                    'id' => 2,
                                    'title' => Module::t('app', 'Пассажир'),
                                ],
                                [
                                    'id' => 3,
                                    'title' => Module::t('app', 'Ціна'),
                                ],

                            ],
                            'DocumentTypes' => [
                                [
                                    'id' => 1,
                                    'title' => Module::t('app', 'загранпаспорт'),
                                ],
                                [
                                    'id' => 2,
                                    'title' => Module::t('app', 'внутренний паспорт'),
                                ],
                                [
                                    'id' => 3,
                                    'title' => Module::t('app', 'свидетельство о рождении'),
                                ],


                            ],
                        ],
                    ],
                ],
                'Week' => [
                    Module::t('app', 'Воскресенье'),
                    Module::t('app', 'Понедельник'),
                    Module::t('app', 'Вторник'),
                    Module::t('app', 'Среда'),
                    Module::t('app', 'Четверг'),
                    Module::t('app', 'Пятница'),
                    Module::t('app', 'Суббота'),
                ],
                'Train' => [
                    'TicketTypes' => [
                        "type-3" => [
                            Module::t('app',
                                "Після оплати перевірте пошту, ми відправимо бланк на вказану Вами e-mail адресу"),
                            Module::t('app', "Роздрукуйте Ваш квиток на принтері"),
                            Module::t('app', "Надайте квиток на вході"),
                        ],
                        "type-2" => [
                            Module::t('app',
                                "Після оплати перевірте пошту, ми відправимо бланк на вказану Вами e-mail адресу"),
                            Module::t('app', "Роздрукуйте бланк на принтері або випишіть код"),
                            Module::t('app', "У граничні терміни зверніться до каси для викупу замовлення"),
                        ],
                    ],
                    'Titles' => [
                        'departure' => Module::t('app', "Відправлення"),
                        'arrival' => Module::t('app', "Прибуття"),
                        'MainPage' => Module::t('app', "Головна"),
                        'TrainPage' => Module::t('app', "Залізничні квитки"),
                        'Choose' => Module::t('app', "вибрати"),
                        'number' => Module::t('app', "Номер поезда"),
                        'train_departure_code' => Module::t('app', "Код станции отправления поезда"),
                        'train_arrival_code' => Module::t('app', "Код станции прибытия поезда"),
                        'train_departure_name' => Module::t('app', "Название станции отправления поезда"),
                        'train_arrival_name' => Module::t('app', "Название станции прибытия поезда"),
                        'passenger_departure_code' => Module::t('app', "Код станции отправления пассажира"),
                        'passenger_arrival_code' => Module::t('app', "Код станции отправления пассажира"),
                        'passenger_departure_name' => Module::t('app', "Название станции отправления пассажира"),
                        'passenger_arrival_name' => Module::t('app', "Название станции отправления пассажира"),
                        'travel_time' => Module::t('app', "Время в пути в минутах"),
                        'departure_time' => Module::t('app', "Время отправления пассажира по киевскому времени"),
                        'departure_date' => Module::t('app', "Дата отправления пассажира"),
                        'arrival_time' => Module::t('app', "Часовая зона отправления пассажира"),
                        'arrival_date' => Module::t('app', "Часовая зона прибытия пассажира"),
                        'train_class' => Module::t('app', "Класс поезда"),
                        'train_speed' => Module::t('app', "Скорость поезда"),
                        'arrival_timezone' => Module::t('app', "Часовая зона отправления пассажира"),
                        'departure_timezone' => Module::t('app', "Часовая зона прибытия пассажира"),
                        'train' => Module::t('app', "Поїзд"),
                        'in_race' => Module::t('app', "В пути"),
                        'choose_car' => Module::t('app', "Оберіть вагон"),
                        'type_of_car' => Module::t('app', "Тип вагона"),
                        'free_seats' => Module::t('app', "Свободних місць"),
                        'choose_seats' => Module::t('app', "Вкажіть бажані місця на карті вагону"),
                        'seats_description' => Module::t('app', "Позначення місць"),
                        'seat_available' => Module::t('app', "Доступні"),
                        'seat_reserve' => Module::t('app', "Недоступні"),
                        'seat_booked' => Module::t('app', "Вибрані"),
                        'seat_places' => Module::t('app', "Розташування місць"),
                        'seat_odd' => Module::t('app', "Непарні - нижні"),
                        'seat_even' => Module::t('app', "Парні - верхні"),
                        'hour_short' => Module::t('app', "ч"),
                        'min_short' => Module::t('app', "мин"),
                        'carwagon' => Module::t('app', "Вагон"),
                        'seat' => Module::t('app', "Місце"),
                        'name' => Module::t('app', "Ім'я"),
                        'surname' => Module::t('app', "Прізвіще"),
                        'cancel' => Module::t('app', "Відмінити"),
                        'children' => Module::t('app', "дітячий"),
                        'fields_notify' => Module::t('app',
                            "Обов'язково введіть прізвище та ім'я, який здійскюватиме поїздку."),
                        'next' => Module::t('app', "далі"),
                        'race' => Module::t('app', "Маршрут"),
                        'options' => Module::t('app', "Опції"),
                        'operation_types' => Module::t('app', "Тип покупки"),
                        'no_clothes' => Module::t('app', "Відмова від постільного"),
                        'documents' => Module::t('app', "Документ(и)"),
                        'passenger' => Module::t('app', "Пасажир"),
                        'passenger_name' => Module::t('app', "Прізвище та ім'я"),
                        'passenger_type' => Module::t('app', "Тип пассажира"),
                        'cloth' => Module::t('app', "Постільна білизна"),
                        'yes' => Module::t('app', "Да"),
                        'no' => Module::t('app', "Нет"),
                        'other_services' => Module::t('app', "Додаткові послуги"),

                        'cost' => Module::t('app', "Вартість"),
                        'total_price' => Module::t('app', "Ціна"),
                        'tariff' => Module::t('app', "Тариф"),
                        'service_price' => Module::t('app', "Сервисніе сборы"),

                        'buyer' => Module::t('app', "Контактні дані покупця"),
                        'buyer_phone' => Module::t('app', "Телефон"),
                        'buyer_email' => 'email',
                        'buyer_name' => Module::t('app', "ФИО"),
                        'payment' => Module::t('app', "Оплата"),
                        'pay_with_oshad' => Module::t('app', "банківська карта Ощадбанку"),
                        'pay_with_other' => Module::t('app', "банківська карта іншого банку"),
                        'pay' => Module::t('app', "Сплатити"),


                        'class' => [
                            'name' => Module::t('app', "Тип вагону"),
                            'cost' => Module::t('app', "Стоимость билета в гривнах"),
                            'seats' => Module::t('app', "Количество свободных мест данного класса"),
                            'lower' => Module::t('app', "Количество свободных нижних мест данного класса"),
                            'upper' => Module::t('app', "Количество свободных верхних мест данного класса"),
                            'side_lower' => Module::t('app', "Количество свободных нижних боковых мест данного класса"),
                            'side_upper' => Module::t('app',
                                "Количество свободных верхних боковых мест данного класса"),
                            'nutrition' => Module::t('app', "Наличие питания в данном классе вагонов"),
                            'not_firm_car' => Module::t('app', "Признак нефирменного вагона"),
                            'subclass' => Module::t('app', "Подтип вагона"),

                        ],
                        'car' => [
                            'id' => Module::t('app', "ID вагона"),
                            'number' => Module::t('app', "Номер вагона"),
                            'seats' => Module::t('app', "Количество свободных мест в вагоне"),
                            'lower' => Module::t('app', "Количество свободных нижних мест в вагоне"),
                            'upper' => Module::t('app', "Количество свободных верхних мест в вагоне"),
                            'side_lower' => Module::t('app', "Количество свободных нижних боковых мест в вагоне"),
                            'side_upper' => Module::t('app', "Количество свободных верхних боковых мест в вагоне"),
                            'operation_types' => Module::t('app', "Список доступных операций для вагона"),
                            'train_model' => Module::t('app', "Модель поезда"),
                            'seats_count' => Module::t('app', "Общее количество мест в вагоне"),
                        ],
                        'exchange_rate' => [
                            'currency' => Module::t('app', "Валюта"),
                            'cost' => Module::t('app', "Стоимость"),
                        ],
                        'timezone_values' => [
                            '0' => Module::t('app', "время Киевское"),
                            '1' => Module::t('app', "время Московское"),
                            '2' => Module::t('app', "время местное"),
                        ],
                        'train_class_values' => [
                            '1' => Module::t('app', "нефирменный"),
                            '4' => Module::t('app', "фирменный"),
                            '8' => Module::t('app', "экспресс"),
                        ],
                        'train_speed_values' => [
                            '1' => Module::t('app', "пассажирский"),
                            '2' => Module::t('app', "быстрий"),
                            '4' => Module::t('app', "скоросной"),
                        ],
                        'train_types' => [
                            'first' => Module::t('app', "Люкс"),
                            'second' => Module::t('app', "Купе"),
                            'third' => Module::t('app', "Плацкарт"),
                            'reserved' => Module::t('app', "Сидячий"),
                            'non_reserved' => Module::t('app', "Общий"),
                            'comfortable' => Module::t('app', "Мягкий"),
                        ],
                        'time_types' => [
                            '00-06' => Module::t('app', "ніч"),
                            '06-12' => Module::t('app', "ранок"),
                            '12-18' => Module::t('app', "день"),
                            '18-00' => Module::t('app', "вечір"),
                        ],
                        'train_model_value' => [
                            '0' => 'Остальные',
                            '1' => 'Hyundai',
                            '2' => 'Scoda',
                            '3' => 'Tarpan',
                        ],
                        'seat_type_value' => [
                            '1' => 'Свободное верхнее место',
                            '2' => 'Свободное нижнее место',
                            '3' => 'Свободное боковое верхнее место',
                            '4' => 'Свободное боковое нижнее место',
                            '5' => 'Свободное среднее место',
                            '11' => 'Занятое верхнее место',
                            '12' => 'Занятое нижнее место',
                            '13' => 'Занятое боковое верхнее место',
                            '14' => 'Занятое боковое нижнее место',
                            '15' => 'Занятое среднее место',
                        ],
                        'operation_types_values' => [
                            '1' => Module::t('app', 'бронювання квитків з подальшою оплатою вартості у квитковій касі'),
                            '2' => Module::t('app', 'покупка квитків з подальшим обміном на паперові у квитковій касі'),
                            '3' => Module::t('app', 'покупка електронних квитків'),
                        ],
                    ],

                    'Icon' => "../images/navigation/icon-large-train.png",

                    'BarSteps' => [
                        [
                            'id' => 0,
                            'title' => Module::t('app', 'Маршрут'),
                            'dotTitle' => 1,
                        ],
                        [
                            'id' => 1,
                            'title' => Module::t('app', 'Рейс'),
                            'dotTitle' => 2,
                        ],
                        [
                            'id' => 2,
                            'title' => Module::t('app', 'Бронювання'),
                            'dotTitle' => 3,
                        ],
                        [
                            'id' => 3,
                            'title' => Module::t('app', 'Финиш'),
                            'dotTitle' => 4,
                        ],
                    ],

                    'Step' => [
                        [
                            'Date' => Module::t('app', 'Дата відправлення'),
                            'From' => Module::t('app', 'з'),
                            'PlaceHolderFrom' => Module::t('app', 'звiдки'),
                            'PlaceHolderTo' => Module::t('app', 'куди'),
                            'Search' => Module::t('app', 'Шукати'),
                            'Notify1' => Module::t('app',
                                'Згідно постанови КМУ, громадяни РФ зможуть в’їжджати в Україну виключно на підставі документів, дійсних для виїзду за кордон'),
                            'Notify2' => Module::t('app',
                                'В касах АР Крим друк проїзних документів, оформлених через Інтернет, не здійснюється'),
                            'Notify3' => Module::t('app',
                                'Поїзди №223/224 Костянтинівка – Харків, №205/206 Артемівськ 2 –Харків курсують з сидячими місцями. При оформленні необхідно зняти ознаку «Постільна білизна»'),
                            'RoundTicket' => Module::t('app', 'Туди і назад'),
                            'FilterBy' => Module::t('app', 'фільтрувати по'),
                            'SortBy' => Module::t('app', 'сортувати по'),
                            'CarType' => Module::t('app', 'Тип вагона'),
                            'TrainDepartureArrival' => Module::t('app', 'Время отправления / прибытя'),
                            'Departure' => Module::t('app', 'Відправлення'),
                            'Arrival' => Module::t('app', 'Прибуття'),
                            'TravelTime' => Module::t('app', 'В дорозі'),
                            'TrainNumber' => Module::t('app', 'Номер поезда'),

                        ],
                        [
                            'Direction' => Module::t('app', 'Напрямок'),
                            'TrainTitle' => Module::t('app', 'Поїзд'),
                            'TableHeaders' => [
                                [
                                    'id' => 0,
                                    'title' => Module::t('app', 'Поїзд №'),
                                    'sort' => 'number',
                                ],
                                [
                                    'id' => 1,
                                    'title' => Module::t('app', 'Звідки / Куди'),
                                    'sort' => 'train_departure_name',
                                ],
                                [
                                    'id' => 2,
                                    'title' => Module::t('app', 'Відправлення'),
                                    'sort' => 'departure_datetime',
                                ],
                                [
                                    'id' => 3,
                                    'title' => Module::t('app', 'Прибуття'),
                                    'sort' => 'arrival_datetime',
                                ],
                                [
                                    'id' => 4,
                                    'title' => Module::t('app', 'Тривалість'),
                                    'sort' => 'travel_time',
                                ],
                                [
                                    'id' => 6,
                                    'title' => Module::t('app', 'Вільних місць'),
                                    'sort' => 'total_seats'
                                ],
                            ],

                            'Search' => Module::t('app', 'Пошук'),
                        ],
                        [
                            'TableHeaders' => [
                                [
                                    'id' => 0,
                                    'title' => Module::t('app', '№'),
                                ],
                                [
                                    'id' => 1,
                                    'title' => Module::t('app', 'Місце'),
                                ],
                                [
                                    'id' => 2,
                                    'title' => Module::t('app', "Пасажир"),
                                ],
                                [
                                    'id' => 4,
                                    'title' => Module::t('app', 'Ціна'),
                                ],
                                [
                                    'id' => 5,
                                    'title' => '',
                                ],

                            ],
                            'Search' => Module::t('app', 'Пошук'),
                        ],
                    ],
                ],
            ],
            'validationRules' => $this->validationRules(),
            'maintenance' => (Yii::$app->params['maintenance'] == true)
                ? true
                : false,
            'serviceAvailability' => $this->serviceAvailability(),
            'news' => [
                'oschadbank' => Yii::$app->urlManagerAPI->createUrl([
                    '/v1/front/application/news',
                    'accessToken' => Module::User()->auth_key,
                    'source' => 'oschadbank',
                ], true),
                'oschad24' => Yii::$app->urlManagerAPI->createUrl([
                    '/v1/front/application/news',
                    'accessToken' => Module::User()->auth_key,
                    'source' => 'oschad24',
                ], true)
            ],
            'extraParams' => [
                'entertainments' => [
                    'carousel' => $this->getEntertainmentsCarouselData(),
                    'cities' => $this->getEntertainmentsCities(),
                ],
                'avia' => [
                    'passenger' => [
                        'AgeBirthDateRanges' => Avia::AgeBirthDateRanges(),
                    ],
                ],
                'shakhtar' => [
                    'carousel' => $this->getShakhtarCarouselData()
                ],
                'gd' => [
                    'patternError' => Module::t('app', 'Вибачте, для даного вагона відсутня карта'),
                ],
            ],
        ];
    }

    protected function validationRules()
    {
        $result = [];
        /** @var Module $module */
        $module = Yii::$app->controller->module;
        $allModels = [
            'ticketsua\\Gd',
            'ticketsua\\Bus',
            'ticketsua\\Avia',
            'Entertainments',
            'Shakhtar',
            'Order',
        ];
        foreach ($allModels as $item) {
            $modelName = 'api\modules\v1\models\\' . $item;
            /** @var Core $model */
            $model = new $modelName($module->response, $module->session, false);
            $rules = $model->getValidationRulesAndHints();
            $result[strtolower((new \ReflectionClass($model))->getShortName())] = $rules;
        }

        return $result;
    }

    public function News($source = null)
    {
        if ($source == 'oschad24') {
            $result = Yii::$app->cache->get('news.' . $source);
        } else {
            $result = Yii::$app->cache->get('news.oschadbank');
        }

        return $result
            ? json_decode($result, true)
            : null;
    }

    protected function getEntertainmentsCarouselData()
    {
        $cacheKey = 'application.config.extraParams.entertainments.carousel';
        $result = Yii::$app->cache->get($cacheKey);
        if ($result === false) {
            $result = Entertainments::getCarouselData();
            //$dependency = new ExpressionDependency(['expression' => 'md5(serialize(PartnerEventsCarousel::find()->asArray()->all()))']);
            Yii::$app->cache->set($cacheKey, $result, 86400);
        }

        return $result;
    }

    protected function getShakhtarCarouselData()
    {
        return Carousel::getData(Service::findOne(['code' => Service::TICKETS_FOOTBALL])->id);
    }

    protected function getEntertainmentsCities()
    {
        $cacheKey = 'application.config.extraParams.entertainments.cities';
        $result = Yii::$app->cache->get($cacheKey);
        if ($result === false) {
            $cities = Yii::$app->params['application']['config']['extraParams']['entertainments']['cities'];
            $result = ArrayHelper::map(Entertainments::getCitiesById($cities), 'id', 'name');
            $sorted = [];
            foreach ($cities as $id) {
                if (array_key_exists($id, $result)) {
                    $sorted[$id] = $result[$id];
                }
            }
            $result = $sorted;
            $dependency = new ExpressionDependency(['expression' => 'md5(serialize(Yii::$app->params["application"]["config"]["extraParams"]["entertainments"]["cities"]))']);
            Yii::$app->cache->set($cacheKey, $result, 86400, $dependency);
        }

        return $result;
    }

    protected function serviceAvailability()
    {
        return ArrayHelper::map(Service::find()
            ->select([
                'provider_code as service',
                'active as available'
            ])
            ->where([])
            ->asArray()
            ->all(), 'service', 'available');
    }
}
