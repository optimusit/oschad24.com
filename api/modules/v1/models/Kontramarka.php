<?php

namespace api\modules\v1\models;

use Yii;
use api\modules\v1\Module;
use common\components\AppHelper;
use api\modules\v1\components\CoreProducts;

class Kontramarka extends CoreProducts
{
    const DATE_FORMAT = 'php:d-m-Y';
    const SEARCH_MAX_DAYS = 180;
    const SEARCH_TO_DAYS = 30;
    const RESERVATION_END_SPACE = 30;
    const CACHE_DURATION = 86400;

    protected $_form;
    protected $_allowedFilters = [
    ];

    public $id;
    public $dateFrom;
    public $dateTo;
    public $searchEventName;
    public $cityId;
    public $activityTypeId;
    public $pageSize;
    public $currentPage;
    public $cityName;
    public $orderId;
    public $itemId;
    public $seats;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'pageSize',
                    'currentPage',
                    'cityId',
                    'activityTypeId',
                    'dateFrom',
                    'dateTo',
                    'searchEventName',
                ],
                'safe',
                'on' => 'search'
            ],
            [
                [
                    'pageSize',
                    'currentPage',
                ],
                'integer',
                'on' => 'search'
            ],
            [
                [
                    'cityId',
                    'activityTypeId',
                ],
                'each',
                'rule' => ['integer'],
                'on' => 'search'
            ],
            [
                [
                    'cityId',
                    'activityTypeId',
                ],
                'default',
                'value' => [],
                'on' => 'search'
            ],
            [
                [
                    'dateFrom',
                ],
                'default',
                'value' => date(AppHelper::GetDateFormat()),
                'on' => 'search'
            ],
            [
                [
                    'dateFrom',
                ],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(), strtotime('+' . self::SEARCH_MAX_DAYS . ' days')),
                'on' => 'search'
            ],
            [
                [
                    'dateTo',
                ],
                'default',
                'value' => date(AppHelper::GetDateFormat(), strtotime('+' . self::SEARCH_TO_DAYS . ' days')),
                'on' => 'search'
            ],
            [
                [
                    'dateTo',
                ],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(),
                    strtotime('+' . (string)(intval(self::SEARCH_MAX_DAYS) + 1) . ' days')),
                'on' => 'search'
            ],
            [
                'searchEventName',
                'default',
                'value' => '',
                'on' => 'search'
            ],
            [
                'searchEventName',
                'string',
                'min' => 3,
                'max' => 10,
                'on' => 'search'
            ],
            [
                [
                    'id',
                ],
                'required',
                'on' => 'event'
            ],
            [
                [
                    'id',
                ],
                'integer',
                'on' => 'event'

            ],
            [
                [
                    'cityName',
                ],
                'required',
                'on' => 'cities'
            ],
            [
                [
                    'cityName',
                ],
                'trim',
                'on' => 'cities'
            ],
            [
                [
                    'cityName',
                ],
                'string',
                'min' => 1,
                'max' => 10,
                'on' => 'cities'
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'searchEventName' => Module::t('app', 'Пошук за назвою'),
            'cityName' => Module::t('app', 'Місто'),
            'dateFrom' => Module::t('app', 'Дата З'),
            'dateTo' => Module::t('app', 'Дата По'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'searchEventName' => Module::t('app', 'Пошук за назвою'),
            'cityName' => Module::t('app', 'Місто'),
            'dateFrom' => Module::t('app', 'Дата З'),
            'dateTo' => Module::t('app', 'Дата По'),
        ]);

        return $result;
    }

    public function __construct($response, $session)
    {
        parent::__construct($response, $session);
        $this->_form = $this->GetServiceForm();
        if (is_null($this->_form)) {
            $this->_form = [];
            $this->SetServiceForm($this->_form);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        try {
            $this->cityId = json_decode($this->cityId);
        } catch (\Exception $e) {
        }
        try {
            $this->activityTypeId = json_decode($this->activityTypeId);
        } catch (\Exception $e) {
        }
        try {
            $this->seats = array_unique(json_decode($this->seats));
        } catch (\Exception $e) {
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
    }

//	protected function AuthProvider()
//	{
//		if (!isset($this->_form['auth' . $this->_provider])) {
//			$providerFullName = '\common\providers\\' . strtolower($this->_provider) . '\\' . $this->_provider;
//			$provider = new $providerFullName();
//			$this->_form['auth' . $this->_provider] = $provider->Login();
//			$this->SetServiceForm($this->_form);
//		}
//	}

//	protected function GetProviderAuth()
//	{
//		return $this->_form['auth' . $this->_provider];
//	}
}
