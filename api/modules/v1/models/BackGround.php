<?php

namespace api\modules\v1\models;

use Yii;
use common\providers\shakhtar\Shakhtar;
use common\models\OrderProviderHistory;
use common\providers\karabas\Karabas;
use api\modules\v1\components\CoreTicketsUA;
use common\models\Service;
use common\models\Order;
use common\providers\tickets\Tickets;

class BackGround
{
    public function Balance()
    {
        $result = (new Tickets())->getBalance();

        return $result;
    }

    /**
     * @param Order $order
     * @return array|bool
     * @throws \Exception
     */
    public static function Commit(Order $order)
    {
        $result = false;
        $success = false;
        $service = $order->getService()
            ->one();
        if (in_array($service->code, [
            Service::TICKETS_GD,
            Service::TICKETS_BUS,
            Service::TICKETS_AVIA,
        ])) {
            $result = (new Tickets())->commit($service->provider_code, $order->provider_order_id,
                $order->income_amount);
            if ($result['success'] == true) {
                $success = true;
            }
        } elseif ($service->code == Service::TICKETS_ENTERTAINMENTS) {
            $karabas = new Karabas();
            $result = $karabas->ConfirmOrder($order->provider_order_id, $order->email, $order->getCustomerFullName(),
                Karabas::STATUS_CONFIRM_PAYMENT);
            if ($result['success'] == true) {
                $success = true;
                if (is_null(($eticket = $order->getEticket()))) {
                    $order->eticket = json_decode(json_encode($result['data']), true);
                    $order->save();
                }
            }
        } elseif ($service->code == Service::TICKETS_FOOTBALL) {
            $shakhtar = new Shakhtar();
            $result = $shakhtar->ReservPayment([
                'reservationid' => $order->provider_order_id,
                'amount' => $order->income_amount
            ]);
            if ($result['success'] == true && $result['data']['Result'] == 0) {
                $success = true;
            }
        } else {
            throw new \Exception('Невідомий сервіс ' . $service->code);
        }
        if ($result) {
            $orderProviderHistory = new OrderProviderHistory();
            $orderProviderHistory->order_id = $order->id;
            $orderProviderHistory->request = var_export($result['dialog']['request'], true);
            $orderProviderHistory->response = $result['dialog']['response'];
            $orderProviderHistory->response_format = $result['dialog']['response_format'];
            $orderProviderHistory->save();
        }

        return $success;
    }

    /**
     * @param Order $order
     * @return array|bool
     * @throws \Exception
     */
    public static function Cancel(Order $order)
    {
        $result = false;
        $service = $order->getService()
            ->one();
        if ($service->code == Service::TICKETS_ENTERTAINMENTS) {
            $karabas = new Karabas();
            $result = $karabas->ConfirmOrder($order->provider_order_id, $order->email, $order->getCustomerFullName(),
                Karabas::STATUS_CANCEL_ORDER);
        } else {
            //throw new \Exception('Невідомий сервіс ' . $service->code);
        }
        if ($result) {
            $orderProviderHistory = new OrderProviderHistory();
            $orderProviderHistory->order_id = $order->id;
            $orderProviderHistory->request = var_export($result['dialog']['request'], true);
            $orderProviderHistory->response = $result['dialog']['response'];
            $orderProviderHistory->response_format = $result['dialog']['response_format'];
            $orderProviderHistory->save();
        }

        return $result;
    }

    /**
     * @param Order $order
     * @return array|bool|mixed|string
     * @throws \Exception
     */
    public static function GetDocument(Order $order)
    {
        $result = false;
        /** @var Service $service */
        $service = $order->getService()
            ->one();
        if (in_array($service->code, [
            Service::TICKETS_GD,
            Service::TICKETS_BUS,
            Service::TICKETS_AVIA,
            Service::TICKETS_ENTERTAINMENTS,
            Service::TICKETS_FOOTBALL,
        ])) {
            $result = CoreTicketsUA::ETicket($order);
        } else {
            throw new \Exception('Невідомий сервіс ' . $service->code);
        }
        if (array_key_exists('dialog', $result)) {
            $orderProviderHistory = new OrderProviderHistory();
            $orderProviderHistory->order_id = $order->id;
            $orderProviderHistory->request = var_export($result['dialog']['request'], true);
            $orderProviderHistory->response = $result['dialog']['response'];
            $orderProviderHistory->response_format = $result['dialog']['response_format'];
            $orderProviderHistory->save();
        }
        if ($result['code'] == 0) {
            foreach ($result['data'] as $fileKey => $fileData) {
                if ($result['encoded']) {
                    $fileData = base64_decode($fileData);
                }
                $filePath = Yii::$app->params['storage']['documents'] . $service->provider_code . DIRECTORY_SEPARATOR
                    . $order->id . '_' . $fileKey . '.pdf';
                if (file_put_contents($filePath, $fileData) !== false) {
                    $result[] = $filePath;
                }
            }
        }

        return $result;
    }
}
