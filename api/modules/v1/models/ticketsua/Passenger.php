<?php

namespace api\modules\v1\models\ticketsua;

use Yii;
use api\modules\v1\Module;
use api\modules\v1\components\ModelMetaDataTrait;
use api\modules\v1\components\ModelAttributePlaceholdersTrait;
use yii\base\Model;

class Passenger extends Model
{
    use ModelMetaDataTrait;
    use ModelAttributePlaceholdersTrait;

    /**
     * @var string Имя
     */
    public $name;
    /**
     * @var string Фамилия
     */
    public $surname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'name',
                    'surname',
                ],
                'required',
                'on' => 'checkout',
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'name' => Module::t('app', 'Ім’я'),
            'surname' => Module::t('app', 'Прізвище'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            //'name' => Module::t('app', 'Введіть Ім’я'),
            //'surname' => Module::t('app', 'Введіть Прізвище'),
        ]);

        return $result;
    }

//	public function attributePlaceholders()
//	{
//		return [
//			'name' => Module::t('app', '(Placeholder) Ім’я'),
//			'surname' => Module::t('app', '(Placeholder) Прізвище'),
//		];
//	}

    public function getFullName()
    {
        return sprintf('%s %s', $this->name, $this->surname);
    }
}
