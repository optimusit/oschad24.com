<?php

namespace api\modules\v1\models\ticketsua;

use Yii;
use common\models\Airlines;
use api\modules\v1\components\validator\avia\AviaPassengersTotalValidator;
use common\providers\tickets\Tickets;
use api\modules\v1\components\validator\avia\RecommendationIdValidator;
use api\modules\v1\components\validator\avia\AviaClassValidator;
use api\modules\v1\components\validator\avia\DestinationValidator;
use common\models\Country;
use common\models\Order;
use api\modules\v1\components\CoreTicketsUA;
use common\components\AppHelper;
use common\providers\tickets\AviaTickets;
use api\modules\v1\Module;
use common\payments\CommerceGateway\CommerceGateway;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Avia extends CoreTicketsUA
{
    const SEARCH_MAX_DAYS = 360;
    const MAX_PASSENGERS = AviaTickets::MAX_PASSENGERS;

    /**
     * Комиссия системы
     */
    const SERVICE_FEE = 50;

    public $searchCitizenshipCountry;
    public $destinations = [];
    public $adult;
    public $child;
    public $infant;
    public $class = 'A';
    public $recommendationId;

    public function __construct($response, $session, $partner = null, $config = [])
    {
        parent::__construct($response, $session, $partner, $config);
        $this->_provider = new AviaTickets();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'searchCitizenshipCountry',
                ],
                'safe',
                'on' => 'citizenship',
            ],
            [
                [
                    'searchCitizenshipCountry',
                ],
                'trim',
                'on' => 'citizenship',
            ],
            [
                [
                    'searchCitizenshipCountry',
                ],
                'string',
                'on' => 'citizenship',
            ],
            [
                [
                    'destinations',
                ],
                'required',
                'on' => 'searchaircraft',
            ],
            [
                [
                    'adult',
                    'child',
                    'infant',
                    'class'
                ],
                'safe',
                'on' => 'searchaircraft',
            ],
            [
                [
                    'destinations',
                ],
                DestinationValidator::className(),
                'min' => 1,
                'max' => 10,
                'on' => 'searchaircraft',
            ],
            [
                ['adult',],
                'default',
                'value' => 1,
                'on' => 'searchaircraft',
            ],
            [
                ['adult',],
                'integer',
                'min' => 1,
                'on' => 'searchaircraft',
            ],
            [
                ['child',],
                'default',
                'value' => 0,
                'on' => 'searchaircraft',
            ],
            [
                ['child',],
                'integer',
                'min' => 0,
                'on' => 'searchaircraft',
            ],
            [
                ['infant',],
                'default',
                'value' => 0,
                'on' => 'searchaircraft',
            ],
            [
                ['infant',],
                'integer',
                'min' => 0,
                'on' => 'searchaircraft',
            ],
            [
                [
                    'adult',
                    'child',
                    'infant',
                ],
                AviaPassengersTotalValidator::className(),
                'on' => 'searchaircraft',
            ],
            [
                ['class',],
                'default',
                'value' => AviaTickets::CLASS_A,
                'on' => 'searchaircraft',
            ],
            [
                ['class',],
                AviaClassValidator::className(),
                'range' => AviaTickets::CLASSES,
                'on' => 'searchaircraft',
            ],
            [
                'recommendationId',
                'required',
                'on' => [
                    'conditions',
                    'checkout',
                ],
            ],
            [
                'recommendationId',
                RecommendationIdValidator::className(),
                'on' => [
                    'conditions',
                    'checkout',
                ],
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'searchCitizenshipCountry' => Module::t('app', 'Громадянство'),
            'destinations' => Module::t('app', ''),
            'adult' => Module::t('app', 'Дорослих'),
            'child' => Module::t('app', 'Дітей'),
            'infant' => Module::t('app', 'Немовлят'),
            'class' => Module::t('app', 'Клас'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'searchCitizenshipCountry' => Module::t('app', 'Громадянство'),
            'destinations' => Module::t('app', ''),
            'adult' => Module::t('app', 'Дорослих'),
            'child' => Module::t('app', 'Дітей'),
            'infant' => Module::t('app', 'Немовлят'),
            'class' => Module::t('app', 'Клас'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!is_array($this->destinations)) {
            $destinations = json_decode($this->destinations);
            if (!is_array($destinations)) {
                $destinations = [];
            }
            $this->destinations = $destinations;
        }

        return parent::beforeValidate();
    }

    /**
     * Поиск станции по указанному имени
     *
     * @return array Список станций
     */
    public function SearchStation()
    {
        $name = trim($this->searchStationName);
        $name = AppHelper::reverseTransliteration($name);
        $cacheKey = $this->getCacheKeyStationSearch($name);
        $cache = Yii::$app->cache->get($cacheKey);
        if ($cache === false) {
            $result = $this->_provider->searchStation($name);
            if ($result['success']) {
                $this->_response['data'] = $result['data'];
                Yii::$app->cache->set($cacheKey, $this->_response['data'],
                    Yii::$app->params['providers']['dictionaries']['cacheDuration']);
            } else {
                $this->_response['code'] = -2;
                $this->_response['data'] = [];
                $this->_response['message'] = 'Тимчасова помилка';
            }
        } else {
            $this->_response['data'] = $cache;
        }
        $form = $this->GetServiceForm();
        $form['searchStationsHistory'][] = $this->_response['data'];
        $this->SetServiceForm($form);

        return $this->_response;
    }

    /**
     * Запрос списка возможных перелетов
     *
     * @return mixed Список рейсов подходящих по критерию
     */
    public function SearchAircraft()
    {
        $searchParams = [];
        $i = 0;
        foreach ($this->destinations as $destination) {
            $searchParams['destinations[' . $i . '][departure]'] = $destination->departure;
            $searchParams['destinations[' . $i . '][arrival]'] = $destination->arrival;
            $searchParams['destinations[' . $i . '][date]'] = $destination->date;
            $i++;
        }
        $searchParams[strtolower(AviaTickets::PASSENGER_TYPE_ADULT)] = $this->adult;
        $searchParams[strtolower(AviaTickets::PASSENGER_TYPE_CHILD)] = $this->child;
        $searchParams[strtolower(AviaTickets::PASSENGER_TYPE_INFANT)] = $this->infant;
        $searchParams['service_class'] = $this->class;
        $result = $this->_provider->searchAircraft($searchParams);
        if ($result['success']) {
            $form = $this->GetServiceForm();
            unset($form['aircrafts']);
            if ($result['data']) {
                $recommendationPrice = [];
                foreach ($result['data'] as &$recommendation) {
                    $order = new Order();
                    $order->scenario = 'commission';
                    $order->fee = self::SERVICE_FEE;
                    $order->attributes = [
                        'income_amount' => $recommendation['amount'],
                        $order->e_fee = self::ACQUIRING_RATE_OTHER,
                    ];
                    $order->calculateCommission();
                    $recommendationPrice[$recommendation['id']] = $recommendation['amount'];
                    $recommendation['amount'] = $order->amount;
                    // посмотреть на скорость
                    foreach ($recommendation['parts'] as &$part) {
                        if (count($part['segments']) > 1) {
                            $part['is_flight_direct'] = 0;
                            $part['is_flight_direct_description'] = Module::t('app', 'Пересадки');
                        } else {
                            $part['is_flight_direct'] = 1;
                            $part['is_flight_direct_description'] = Module::t('app', 'Прямий');
                        }
                        $departureDate = \DateTime::createFromFormat('d.m.Y H:i',
                            $part['departure_date'] . ' ' . $part['departure_time']);
                        $arrivalDate = \DateTime::createFromFormat('d.m.Y H:i',
                            $part['arrival_date'] . ' ' . $part['arrival_time']);
                        $part['departure_airport_name'] = $this->GetAirport($part['departure_airport'])['name'];
                        $part['departure_city_name'] = $this->GetCity($part['departure_airport'])['name'];
                        $part['departure_day_of_week'] = Yii::$app->formatter->asDate($departureDate, 'php:l');
                        $part['departure_day_month_in_words'] = Yii::$app->formatter->asDate($departureDate, 'php:j F');
                        $part['arrival_airport_name'] = $this->GetAirport($part['arrival_airport'])['name'];
                        $part['arrival_city_name'] = $this->GetCity($part['arrival_airport'])['name'];
                        $part['arrival_day_of_week'] = Yii::$app->formatter->asDate($arrivalDate, 'php:l');
                        $part['arrival_day_month_in_words'] = Yii::$app->formatter->asDate($arrivalDate, 'php:j F');
                        $part['arrival_time_of_day'] = $this->getTimeOfDay($arrivalDate);
                        $part['departure_time_of_day'] = $this->getTimeOfDay($departureDate);
                        foreach ($part['segments'] as &$segment) {
                            $supplier = Airlines::getByIATA($segment['supplier']);
                            $segment['supplier_name'] = $supplier['name'];
                            $segment['supplier_logo'] = Url::to(Yii::$app->controller->module->assetBaseUrl
                                . '/air/suppliers/' . $supplier['logo'] . '.png', true);
                            $departureDate = \DateTime::createFromFormat('d.m.Y', $segment['departure_date']);
                            $arrivalDate = \DateTime::createFromFormat('d.m.Y', $segment['arrival_date']);
                            $segment['departure_country_name'] = $this->GetCountry($segment['departure_country'])['name'];
                            $segment['departure_airport_name'] = $this->GetAirport($segment['departure_airport'])['name'];
                            $segment['departure_city_name'] = $this->GetCity($segment['departure_airport'])['name'];
                            $segment['departure_day_of_week'] = Yii::$app->formatter->asDate($departureDate, 'php:l');
                            $part['departure_day_month_in_words'] = Yii::$app->formatter->asDate($departureDate,
                                'php:j F');
                            $segment['arrival_country_name'] = $this->GetCountry($segment['arrival_country'])['name'];
                            $segment['arrival_airport_name'] = $this->GetAirport($segment['arrival_airport'])['name'];
                            $segment['arrival_city_name'] = $this->GetCity($segment['arrival_airport'])['name'];
                            $segment['arrival_day_of_week'] = Yii::$app->formatter->asDate($arrivalDate, 'php:l');
                            $part['arrival_day_month_in_words'] = Yii::$app->formatter->asDate($arrivalDate, 'php:j F');
                        }
                    }
                }
                $form['recommendationPrice'] = $recommendationPrice;
            }
            $this->_response['data'] = $result['data'];
            $form['search'] = $searchParams;
            $form['aircrafts'] = $result['data'];
            $this->SetServiceForm($form);
        } else {
            $this->_response['code'] = (int)$result['code'];
            $this->_response['message'] = $result['message'];
        }

        return $this->_response;
    }

    /**
     * Условия тарифа
     *
     * @return mixed Условия тарифа
     */
    public function Conditions()
    {
        $form = $this->GetServiceForm();
        $conditions = [];
        if (isset($form['conditions']) && isset($form['conditions'][$this->recommendationId])) {
            $conditions = $form['conditions'][$this->recommendationId];
        } else {
            $recommendations = ArrayHelper::index($form['aircrafts'], 'id');
            $result = $this->_provider->fareConditions($recommendations[$this->recommendationId]['id']);
            if ($result['success']) {
                $conditions = $result['data'];
                $form['conditions'][$this->recommendationId] = $conditions;
                $this->SetServiceForm($form);
            } else {
                $this->_response['code'] = (int)$result['code'];
                $this->_response['message'] = $result['message'];
            }
        }
        $this->_response['data'] = $conditions;

        return $this->_response;
    }

    /**
     * Список стран для гражданства
     *
     * @return array Список стран для гражданства
     */
    public function Citizenship()
    {
        $list = [];
        $cacheKey = 'Air_Citizenship_' . Yii::$app->language;
        $cache = Yii::$app->cache->get($cacheKey);
        if ($cache === false) {
            $rows = Country::getList();
            if ($rows) {
                foreach ($rows as $row) {
                    $list[$row['alpha2']] = $row['name_' . Yii::$app->language];
                }
                Yii::$app->cache->set($cacheKey, $list,
                    Yii::$app->params['providers']['dictionaries']['cacheDuration']);
            }
        } else {
            $list = $cache;
        }
        if (mb_strlen($this->searchCitizenshipCountry)) {
            $part = mb_strtolower($this->searchCitizenshipCountry);
            $newList = [];
            foreach ($list as $k => $v) {
                if (mb_strpos(mb_strtolower($v), $part) === 0) {
                    $newList[$k] = $v;
                }
            }
            $list = $newList;
        }
        $this->_response['data'] = $list;

        return $this->_response;
    }

    /**
     * Оформление заказа
     *
     * @return mixed
     */
    public function Checkout()
    {
        $form = $this->GetServiceForm();
        if (!is_null($form)) {
            $recommendations = ArrayHelper::index($form['aircrafts'], 'id');
            $form['aircraft'] = $recommendations[$this->recommendationId];
            $this->_response['code'] = 10;
            //$this->SetServiceForm($form);
            $aircraft = $form['aircraft'];
            $form['passengers'] = $this->_validPassengers;
            $result = $this->_provider->reservation($form);
            if ($result['success'] && $result['code'] == 0) {
                $reservation = $result['reservation'];
                $recommendationPrice = $form['recommendationPrice'][$this->recommendationId];
                $expirationTime = \DateTime::createFromFormat('d.m.Y H:i:s', $reservation['ticketing_time_limit'])
                    ->sub(new \DateInterval('PT30M'));
                $duration = floor($aircraft['duration'] / 60) . ' ' . Module::t('app', 'год.') . ' ' .
                    $aircraft['duration'] % 60 . ' ' . Module::t('app', 'хв.');
                $orderInfo = [
                    'duration' => $duration,
                    'parts' => $aircraft['parts'],
                    'passengers' => $form['passengers'],
                ];
                unset($form['searchStationsHistory']);
                unset($form['search']);
                unset($form['aircrafts']);
                unset($form['recommendationPrice']);
                /** @var Order $order */
                $order = $this->saveData($reservation['locator'], $expirationTime, $recommendationPrice, null,
                    count($this->_validPassengers), $form, $orderInfo, Tickets::PREFIX, $result['dialog']);
                $this->_response['code'] = 0;
                $this->_response['data'] = [
                    'OrderInfo' => $orderInfo,
                    'PaymentAuthorizationData' => CommerceGateway::createAuthorizationData($order)
                ];
                if ($this->isFrontCall()) {
                    Yii::$app->core->reservationCreated($order);
                }
            } else {
                $this->_response['message'] = $result['message'];
                $this->_response['code'] = 20;
            }
        } else {
            $this->_response['message'] = 'Сессия истекла';
            $this->_response['code'] = 20;
        }

        return $this->_response;
    }

    /**
     * Запрос pdf для электронного билета
     *
     * @param int $orderId Номер заказа
     * @return mixed Электронный билет в формате pdf файла, упакованный в base64
     */
//	public function ETicket($orderId)
//	{
//		/** @var Order $order */
//		$order = Order::findOne([
//			'id' => $orderId,
//		]);
//		if ($order) {
//			if ($order->status_id == Order::STATUS_COMPLETED) {
//				if (TEST_SERVER) {
//					// Если сервер в тестовом режиме то запросы в tickets.ua делать нет смысла, возвращаем пустой pdf файл.
//					$this->_response['data'] = 'JVBERi0xLjQKJcOkw7zDtsOfCjIgMCBvYmoKPDwvTGVuZ3RoIDMgMCBSL0ZpbHRlci9GbGF0ZURlY29kZT4+CnN0cmVhbQp4nDPQM1Qo5ypUMFAwALJMLU31jBQsTAz1LBSKUrnCtRTyuAIVAIcdB3IKZW5kc3RyZWFtCmVuZG9iagoKMyAwIG9iago0MgplbmRvYmoKCjUgMCBvYmoKPDwKPj4KZW5kb2JqCgo2IDAgb2JqCjw8L0ZvbnQgNSAwIFIKL1Byb2NTZXRbL1BERi9UZXh0XQo+PgplbmRvYmoKCjEgMCBvYmoKPDwvVHlwZS9QYWdlL1BhcmVudCA0IDAgUi9SZXNvdXJjZXMgNiAwIFIvTWVkaWFCb3hbMCAwIDU5NSA4NDJdL0dyb3VwPDwvUy9UcmFuc3BhcmVuY3kvQ1MvRGV2aWNlUkdCL0kgdHJ1ZT4+L0NvbnRlbnRzIDIgMCBSPj4KZW5kb2JqCgo0IDAgb2JqCjw8L1R5cGUvUGFnZXMKL1Jlc291cmNlcyA2IDAgUgovTWVkaWFCb3hbIDAgMCA1OTUgODQyIF0KL0tpZHNbIDEgMCBSIF0KL0NvdW50IDE+PgplbmRvYmoKCjcgMCBvYmoKPDwvVHlwZS9DYXRhbG9nL1BhZ2VzIDQgMCBSCi9PcGVuQWN0aW9uWzEgMCBSIC9YWVogbnVsbCBudWxsIDBdCi9MYW5nKHJ1LVJVKQo+PgplbmRvYmoKCjggMCBvYmoKPDwvQ3JlYXRvcjxGRUZGMDA1NzAwNzIwMDY5MDA3NDAwNjUwMDcyPgovUHJvZHVjZXI8RkVGRjAwNEMwMDY5MDA2MjAwNzIwMDY1MDA0RjAwNjYwMDY2MDA2OTAwNjMwMDY1MDAyMDAwMzQwMDJFMDAzMT4KL0NyZWF0aW9uRGF0ZShEOjIwMTMxMjE0MjM1OTM0KzAyJzAwJyk+PgplbmRvYmoKCnhyZWYKMCA5CjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDIyNiAwMDAwMCBuIAowMDAwMDAwMDE5IDAwMDAwIG4gCjAwMDAwMDAxMzIgMDAwMDAgbiAKMDAwMDAwMDM2OCAwMDAwMCBuIAowMDAwMDAwMTUxIDAwMDAwIG4gCjAwMDAwMDAxNzMgMDAwMDAgbiAKMDAwMDAwMDQ2NiAwMDAwMCBuIAowMDAwMDAwNTYyIDAwMDAwIG4gCnRyYWlsZXIKPDwvU2l6ZSA5L1Jvb3QgNyAwIFIKL0luZm8gOCAwIFIKL0lEIFsgPEY0OUUxMTQ4OTVEQkIyNDhBMUI4NzMxNzg2ODhGNkJGPgo8RjQ5RTExNDg5NURCQjI0OEExQjg3MzE3ODY4OEY2QkY+IF0KL0RvY0NoZWNrc3VtIC84OTgzNTcwNjlFQTk4MTVEN0IyQ0I5ODNFNjY5NjMxMAo+PgpzdGFydHhyZWYKNzM2CiUlRU9GCg==';
//				} else {
//					$result = $this->_provider->eticket($order->reservation->reservation_id);
//					if ($result) {
//						$this->_response['data'] = $result['data'];
//					} else {
//						$this->_response['code'] = (int)$result['code'];
//						$this->_response['message'] = $result['msg'];
//					}
//				}
//			} else {
//				$this->_response['code'] = -2;
//				$this->_response['message'] = 'Заказ не завершен успешно';
//			}
//		} else {
//			$this->_response['code'] = -1;
//			$this->_response['message'] = 'Заказ не найден';
//		}
//
//		return $this->_response;
//	}

    protected function GetCity($code)
    {
        $code = trim($code);
        $result = $code;
        if (!empty($code)) {
            $cacheKey = 'Air_City_' . ($code);
            $cache = Yii::$app->cache->get($cacheKey);
            if ($cache === false) {
                $response = $this->_provider->getCity($code);
                if ($response['success']) {
                    $result = $response['data'];
                    Yii::$app->cache->set($cacheKey, $result);
                } else {
                    $result = [
                        'code' => $code,
                        'name' => $code,
                    ];
                }
            } else {
                $result = $cache;
            }
        }

        return $result;
    }

    protected function GetCountry($code)
    {
        $code = trim($code);
        $result = $code;
        if (!empty($code)) {
            $cacheKey = 'Air_Country_' . ($code);
            $cache = Yii::$app->cache->get($cacheKey);
            if ($cache === false) {
                $response = $this->_provider->getCountry($code);
                if ($response['success']) {
                    $result = $response['data'];
                    Yii::$app->cache->set($cacheKey, $result);
                }
            } else {
                $result = $cache;
            }
        }

        return $result;
    }

    protected function GetAirport($code)
    {
        $code = trim($code);
        $result = $code;
        if (!empty($code)) {
            $cacheKey = 'Air_AirPort_' . ($code);
            $cache = Yii::$app->cache->get($cacheKey);
            if ($cache === false) {
                $response = $this->_provider->getAirport($code);
                if ($response['success']) {
                    $result = $response['data'];
                    Yii::$app->cache->set($cacheKey, $result);
                }
            } else {
                $result = $cache;
            }
        }

        return $result;
    }

    protected function GetAirline($code)
    {
        $code = trim($code);
        $result = $code;
        if (!empty($code)) {
            $cacheKey = 'Air_Airline_' . ($code);
            $cache = Yii::$app->cache->get($cacheKey);
            if ($cache === false) {
                $response = $this->_provider->getAirline($code);
                if ($response['success']) {
                    $result = $response['data'];
                    Yii::$app->cache->set($cacheKey, $result);
                }
            } else {
                $result = $cache;
            }
        }

        return $result;
    }

    public static function AgeBirthDateRanges()
    {
        $result = [];
        foreach (AviaTickets::PASSENGER_AGE_RANGES as $k => $v) {
            $range = [];
            $max = (new \DateTime())->sub(new \DateInterval('P' . $v['min'] . 'Y'))
                ->sub(new \DateInterval('P1D'));
            $min = (new \DateTime())->sub(new \DateInterval('P' . $v['max'] . 'Y'));
            $range['min'] = Yii::$app->formatter->asDate($min);
            $range['max'] = Yii::$app->formatter->asDate($max);
            $result[$k] = $range;
        }

        return $result;
    }
}
