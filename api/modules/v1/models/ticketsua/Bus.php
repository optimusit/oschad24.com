<?php

namespace api\modules\v1\models\ticketsua;

use Yii;
use common\providers\tickets\Tickets;
use api\modules\v1\Module;
use api\modules\v1\components\validator\bus\DepartureArrivalCodeValidator;
use common\models\Order;
use api\modules\v1\components\CoreTicketsUA;
use common\components\AppHelper;
use common\providers\tickets\BusTickets;
use common\payments\CommerceGateway\CommerceGateway;
use yii\helpers\ArrayHelper;

class Bus extends CoreTicketsUA
{
    const MAX_PASSENGERS = 5;

    /**
     * Максимальное количество дней для диапазона дат поиска
     */
    const SEARCH_MAX_DAYS = 90;
    /**
     * Комиссия системы
     */
    const SERVICE_FEE = 1;

    /**
     * @var string Код станции отправления
     */
    public $from;
    /**
     * @var string Код станции прибытия
     */
    public $to;
    /**
     * @var string Дата отправления
     */
    public $date;
    /**
     * @var int Тип поиска (По умолчанию 0 - точная дата, 1 - +3 дня)
     */
    public $daysForward;
    /**
     * @var int Код станции отправления
     */
    public $departureCode;
    /**
     * @var int Код станции прибытия
     */
    public $arrivalCode;
    /**
     * @var string Номер автобуса
     */
    public $busNumber;
    /**
     * @var string Дата отправления
     */
    public $departureDate;

    public function __construct($response, $session, $partner = null, $config = [])
    {
        parent::__construct($response, $session, $partner, $config);
        $this->_provider = new BusTickets();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'from',
                    'to',
                    'date',
                ],
                'required',
                'on' => 'searchbus',
            ],
            [
                [
                    'from',
                    'to',
                ],
                'string',
                'min' => 3,
                'max' => 250,
                'on' => 'searchbus',
            ],
            [
                ['date'],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(), strtotime('+' . self::SEARCH_MAX_DAYS . ' days')),
                'on' => 'searchbus',
            ],
            [
                ['daysForward'],
                'safe',
                'on' => 'searchbus',
            ],
            [
                ['daysForward'],
                'boolean',
                'on' => 'searchbus',
            ],
            [
                ['daysForward'],
                'default',
                'value' => 0,
                'on' => 'searchbus',
            ],
            [
                [
                    'departureCode',
                    'arrivalCode',
                    'busNumber',
                    'departureDate'
                ],
                'required',
                'on' => 'tripinfo',
            ],
            [
                [
                    'busNumber',
                ],
                'string',
                'min' => 3,
                'max' => 100,
                'on' => 'tripinfo',
            ],
            [
                [
                    'departureCode',
                    'arrivalCode',
                ],
                DepartureArrivalCodeValidator::className(),
                'min' => 2,
                'on' => 'tripinfo',
            ],
            [
                ['departureDate'],
                'required',
                'on' => 'tripinfo',
            ],
            [
                ['departureDate'],
                'date',
                'on' => 'tripinfo',
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'from' => Module::t('app', 'Станція відправлення'),
            'to' => Module::t('app', 'Станція прибуття'),
            'date' => Module::t('app', 'Дата відправлення'),
            'daysForward' => Module::t('app', '+3 дні'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'from' => Module::t('app', 'Введіть Станція відправлення'),
            'to' => Module::t('app', 'Введіть Станція прибуття'),
            'date' => Module::t('app', 'Введіть Дата відправлення'),
            'daysForward' => Module::t('app', '+3 дні'),
        ]);

        return $result;
    }

//	public function attributePlaceholders()
//	{
//		$result = array_merge(parent::attributePlaceholders(), [
//			'from' => Module::t('app', '(Placeholder) Станція відправлення'),
//			'to' => Module::t('app', '(Placeholder) Станція прибуття'),
//			'date' => Module::t('app', '(Placeholder) Дата відправлення'),
//			'daysForward' => Module::t('app', '(Placeholder) +3 дні'),
//		]);
//
//		return $result;
//	}

    /**
     * Список всех станций
     *
     * @return array Список станций
     */
    public function Stations()
    {
        $cacheKey = 'busStations';
        $cache = Yii::$app->cache->get($cacheKey);
        if ($cache === false) {
            $result = $this->_provider->stations();
            if ($result['success']) {
                $this->_response['data'] = $result['data'];
                Yii::$app->cache->set($cacheKey, $this->_response['data'],
                    Yii::$app->params['providers']['dictionaries']['cacheDuration']);
            } else {
                $this->_response['data'] = [];
            }
        } else {
            $this->_response['data'] = $cache;
        }

        return $this->_response;
    }

    /**
     * Поиск станции по указанному имени
     *
     * @return array Список станций
     */
    public function SearchStation()
    {
        $name = AppHelper::reverseTransliteration(trim($this->searchStationName));
        $cacheKey = $this->getCacheKeyStationSearch($name);
        $cache = Yii::$app->cache->get($cacheKey);
        if ($cache === false) {
            $result = $this->_provider->searchStation($name);
            if ($result['success']) {
                $this->_response['data'] = $result['data'];
                Yii::$app->cache->set($cacheKey, $this->_response['data'],
                    Yii::$app->params['providers']['dictionaries']['cacheDuration']);
            } else {
                $this->_response['data'] = [];
            }
        } else {
            $this->_response['data'] = $cache;
        }
        $form = $this->GetServiceForm();
        if (!$form) {
            $form = [];
        }
        if (!array_key_exists('stationsHistory', $form)) {
            $form['stationsHistory'] = [];
        }
        $form['stationsHistory'] = ArrayHelper::merge($form['stationsHistory'],
            ArrayHelper::map($this->_response['data'], 'id', 'value'));
        $this->SetServiceForm($form);

        return $this->_response;
    }

    /**
     * Запрос списка автобусов на указанную дату
     *
     * @return mixed Список автобусов подходящих по критерию
     */
    public function SearchBus()
    {
        $form = $this->GetServiceForm();
        $date = Yii::$app->formatter->asDate($this->date, self::DATE_FORMAT);
        $searchParams = [
            'from' => $this->from,
            'to' => $this->to,
            'date' => $date,
            'days_forward' => is_null($this->daysForward)
                ? 0
                : intval($this->daysForward),
        ];
        $result = $this->_provider->searchBus($searchParams);
        if ($result['success']) {
            unset($searchParams['date']);
            $dateTo = $date;
            if ($this->daysForward == 1) {
                $dateTo = \DateTime::createFromFormat('d-m-Y', $date)
                    ->add(new \DateInterval('P3D'))
                    ->format('d-m-Y');
            }
            $searchParams['date_from'] = Yii::$app->formatter->asDate($date);
            $searchParams['date_to'] = Yii::$app->formatter->asDate($dateTo);
            $searchParams['departure_name'] = $form['stationsHistory'][$this->from];
            $searchParams['arrival_name'] = $form['stationsHistory'][$this->to];
            $form['search'] = $searchParams;
            foreach ($result['data'] as &$row) {
                $station = $row['@children']['station'];
                unset($row['@children']);
                $departureFromPoint = \DateTime::createFromFormat('d.m.Y H:i', $station['departure_from_point']);
                $arrivalToPoint = \DateTime::createFromFormat('d.m.Y H:i', $station['arrival_to_point']);
                $row['departure_address'] = $station['departure_address'];
                $row['departure_code'] = $station['departure_code'];
                $row['arrival_code'] = $station['arrival_code'];
                $row['departure_from_point_date'] = Yii::$app->formatter->asDate($departureFromPoint);
                $row['departure_from_point_time'] = Yii::$app->formatter->asTime($departureFromPoint);
                $row['departure_from_point_date_time'] = Yii::$app->formatter->asDatetime($departureFromPoint);
                $row['arrival_to_point_date'] = Yii::$app->formatter->asDate($arrivalToPoint);
                $row['arrival_to_point_time'] = Yii::$app->formatter->asTime($arrivalToPoint);
                $row['arrival_to_point_date_time'] = Yii::$app->formatter->asDatetime($arrivalToPoint);
                $row['arrival_time_of_day'] = $this->getTimeOfDay($arrivalToPoint);
                $row['departure_time_of_day'] = $this->getTimeOfDay($departureFromPoint);
                if (empty($row['bus_type'])) {
                    $row['bus_type'] = Module::t('app', 'Без марки');
                }
            }
            $form['buses'] = $result['data'];
            $this->SetServiceForm($form);
            $this->_response['data'] = [
                'search' => $searchParams,
                'buses' => $result['data'],
            ];
        } else {
            Yii::error($result, __METHOD__);
            $code = 20;
            switch ($result['code']) {
                case 201 :
                    $message = Module::t('app', 'Не знайдені рейси на зазначену дату');
                    break;
                default :
                    $message = Module::t('app', 'Виникла тимчасова помилка, спробуйте повторити пізніше.');
                    break;
            }
            $this->_response['code'] = $code;
            $this->_response['message'] = $message;
        }

        return $this->_response;
    }

    /**
     * Проверка наличия мест и параметров оплаты
     *
     * @return mixed Наличие мест и параметров оплаты
     */
    public function TripInfo()
    {
        $form = $this->GetServiceForm();
        foreach ($form['buses'] as $item) {
            if ($item['departure_code'] == $this->departureCode
                && $item['arrival_code'] == $this->arrivalCode
                && $this->departureDate == $item['departure_from_point_date']
                && ($item['number'] == $this->busNumber || $item['route_number'] == $this->busNumber)
            ) {
                $form['bus'] = $item;
                break;
            }
        }
        if (array_key_exists('bus', $form)) {
            $params = [
                'from' => $form['bus']['departure_code'],
                'to' => $form['bus']['arrival_code'],
                'date' => \DateTime::createFromFormat('d.m.Y', $form['bus']['departure_from_point_date'])
                    ->format('d-m-Y'),
                'bus_number' => $this->busNumber,
                'provider' => $form['bus']['provider'],
            ];
            if ($form['bus']['provider'] == 'bus_com_ua') {
                $params['server_code'] = $form['bus']['server_code'];
            }
            $result = $this->_provider->tripInfo($params);
            if ($result['success']) {
                $result['data']['ticket_in_bus'] = $form['bus']['ticket_in_bus'];
                $result['data']['need_birth'] = $form['bus']['need_birth'];
                $result['data']['need_doc'] = $form['bus']['need_doc'];
                $result['data']['departure_address'] = $form['bus']['departure_address'];
                $result['data']['departure_from_point_date_time'] = $form['bus']['departure_from_point_date_time'];
                $result['data']['arrival_to_point_date_time'] = $form['bus']['arrival_to_point_date_time'];
                $order = new Order();
                $order->scenario = 'commission';
                $order->fee = self::SERVICE_FEE;
                $order->income_amount = $result['data']['amount'];
                $order->e_fee = self::ACQUIRING_RATE_DEFAULT;
                $order->calculateCommission();
                $result['data']['amount'] = $order->amount;
                $form['trip'] = $result['data'];
                $this->_response['data'] = $result['data'];
                $this->SetServiceForm($form);
            } else {
                Yii::error($result, __METHOD__);
                $code = 20;
                switch ($result['code']) {
                    case 213 :
                        $message = Module::t('app',
                            'Немає вільних місць. Виберіть інший рейс або повторіть спробу пізніше.');
                        break;
                    default :
                        $message = Module::t('app', 'Виникла тимчасова помилка, спробуйте повторити пізніше.');
                        break;
                }
                $this->_response['code'] = $code;
                $this->_response['message'] = $message;
            }
        } else {
            $this->_response['code'] = 20;
            $this->_response['message'] = Module::t('app', 'Автобус відсутній');
        }

        return $this->_response;
    }

    /**
     * Оформление заказа
     *
     * @return mixed
     */
    public function Checkout()
    {
        $form = $this->GetServiceForm();
        $form['passengers'] = $this->_validPassengers;
        $busInfo = $form['bus'];
        $tripInfo = $form['trip'];
        $orderInfo = [
            'number' => $busInfo['number'],
            'voyage' => $tripInfo['departure_name'] . ' - ' . $tripInfo['arrival_name'],
            'departure' => [
                'date_time' => $busInfo['departure_from_point_date_time'],
                'name' => $busInfo['departure_address'] . ' ' . $tripInfo['departure_name'],
            ],
            'arrival' => [
                'date_time' => $busInfo['arrival_to_point_date_time'],
                'name' => $tripInfo['arrival_name'],
            ],
            'amount' => $tripInfo['amount'],
        ];
        $passengersForTickets = [];
        /** @var PassengerBus $passenger */
        foreach ($this->_validPassengers as $passenger) {
            $orderInfo['passengers'][] = [
                'seat' => $passenger->seat,
                'passenger' => $passenger->getFullName(),
            ];
            $passengersForTickets[] = [
                'firstname' => $passenger->name,
                'lastname' => $passenger->surname,
                'seat' => $passenger->seat,
                'b_day' => $passenger->date,
            ];
        }
        $form['passengersForTickets'] = $passengersForTickets;
        $form['departureDate'] = Yii::$app->formatter->asDate($busInfo['departure_from_point_date'], self::DATE_FORMAT);
        $seatsNumber = count($this->_validPassengers);
        $result = $this->_provider->reservation($form);
        if ($result['success'] && is_array($reservation = $result['reservation'])) {
            $expirationTime = $this->getReservationEndSpace((new \DateTime())->setTimestamp($reservation['time_limit']));
            /** @var Order $order */
            $order = $this->saveData($reservation['reservation_id'], $expirationTime, $reservation['cost'],
                $reservation['currency'], $seatsNumber, $form, $orderInfo, Tickets::PREFIX, $result['dialog']);
            $this->_response['code'] = 0;
            $this->_response['data'] = [
                'OrderInfo' => $orderInfo,
                'PaymentAuthorizationData' => CommerceGateway::createAuthorizationData($order)
            ];
            if ($this->isFrontCall()) {
                Yii::$app->core->reservationCreated($order);
            }
        } else {
            Yii::error($result, __METHOD__);
            switch ($result['code']) {
                default :
                    $code = 50;
                    break;
            }
            $this->_response['code'] = $code;
            $this->_response['message'] = $result['message'];
        }

        return $this->_response;
    }

    /**
     * Запрос получения списка бронирований пользователя
     *
     * @return mixed
     */
    public function BookingsList()
    {
        $result = $this->_provider->bookingsList();
        if ($result) {
            $this->_response['data'] = $result['data'];
        } else {
            $this->_response['code'] = (int)$result['code'];
            $this->_response['message'] = $result['msg'];
        }

        return $this->_response;
    }

    /**
     * Запрос информации по одному бронированию
     *
     * @param int $orderId Номер заказа
     * @return mixed
     */
    public function BookingShow($orderId)
    {
        /** @var Order $order */
        $order = Order::findOne([
            'id' => $orderId,
        ]);
        if ($order) {
            if ($order->provider_order_id) {
                $result = $this->_provider->bookingShow($order->provider_order_id);
                if ($result) {
                    $this->_response['data'] = $result['data'];
                } else {
                    $this->_response['code'] = (int)$result['code'];
                    $this->_response['message'] = $result['msg'];
                }
            } else {
                $this->_response['code'] = -1;
                $this->_response['message'] = 'Бронирование не найдено';
            }
        } else {
            $this->_response['code'] = -1;
            $this->_response['message'] = 'Заказ не найден';
        }

        return $this->_response;
    }

//	/**
//	 * Аннулирование билетов
//	 *
//	 * @param string $orderId Номер заказа
//	 * @return array
//	 */
//	public function Cancel($orderId)
//	{
//		/** @var Order $order */
//		$order = Order::findOne([
//			'id' => $orderId,
//		]);
//		if ($order) {
//			if ($order->provider_order_id) {
//				if ($order->canBeeCancelled()) {
//					$result = $this->_provider->cancel($order->provider_order_id);
//					if ($result['success']) {
//						$this->_response['data'] = $result['data'];
//					} else {
//						$this->_response['code'] = $result['code'];
//						$this->_response['message'] = $result['msg'];
//					}
//				} else {
//					$this->_response['code'] = -1;
//					$this->_response['message'] = 'Не може буте скасоване';
//				}
//			} else {
//				$this->_response['code'] = -1;
//				$this->_response['message'] = 'Бронювання не знайдене';
//			}
//		} else {
//			$this->_response['code'] = -1;
//			$this->_response['message'] = 'Замовлення не знайдене';
//		}
//
//		return $this->_response;
//	}
}
