<?php

namespace api\modules\v1\models\ticketsua\avia;

use Yii;
use api\modules\v1\components\ModelAttributePlaceholdersTrait;
use api\modules\v1\components\ModelMetaDataTrait;
use api\modules\v1\models\ticketsua\Avia;
use common\components\AppHelper;
use api\modules\v1\Module;
use yii\base\Model;

class Destination extends Model
{
    use ModelMetaDataTrait;
    use ModelAttributePlaceholdersTrait;

    /**
     * @var string Пункт відправлення
     */
    public $departure;
    /**
     * @var string Пункт прибуття
     */
    public $arrival;
    /**
     * @var string Дата відправлення
     */
    public $date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'departure',
                    'arrival',
                    'date',
                ],
                'required',
                'on' => 'searchaircraft'
            ],
            [
                ['date'],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(), strtotime('+' . Avia::SEARCH_MAX_DAYS . ' days')),
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'departure' => Module::t('app', 'Пункт відправлення'),
            'arrival' => Module::t('app', 'Пункт прибуття'),
            'date' => Module::t('app', 'Дата відправлення'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'departure' => Module::t('app', 'Пункт відправлення'),
            'arrival' => Module::t('app', 'Пункт прибуття'),
            'date' => Module::t('app', 'Дата відправлення'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
        if (isset($this->date)) {
            $this->date = Yii::$app->formatter->asDate($this->date, Avia::DATE_FORMAT);
        }
    }
}
