<?php

namespace api\modules\v1\models\ticketsua;

use Yii;
use api\modules\v1\components\validator\OnlyCyrAlphaValidator;
use api\modules\v1\components\validator\gd\DateValidator;
use api\modules\v1\Module;
use api\modules\v1\components\validator\gd\PassengerPrivilegesValidator;
use common\components\AppHelper;
use api\modules\v1\components\validator\gd\PassengerCarValidator;
use api\modules\v1\components\validator\gd\PassengerSeatValidator;
use common\providers\tickets\GdTickets;

class PassengerGd extends Passenger
{
    const CHILD_AGE = 15;

    public $carNumber;
    public $seat;
    public $privileges;
    public $date;
    public $document;
    public $document_type;
    public $animal;
    public $facilities;
    public $excess_baggage;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'name',
                    'surname',
                ],
                OnlyCyrAlphaValidator::className(),
                'min' => 1,
                'max' => 50,
                'on' => 'checkout',
            ],
            [
                [
                    'carNumber',
                    'seat',
                ],
                'required',
                'on' => 'checkout',
            ],
            [
                [
                    'carNumber',
                ],
                PassengerCarValidator::className(),
                'on' => 'checkout',
            ],
            [
                [
                    'seat',
                ],
                PassengerSeatValidator::className(),
                'on' => 'checkout',
            ],
            [
                [
                    'date',
                    'document',
                ],
                'safe',
                'on' => 'checkout',
            ],
            [
                [
                    'document',
                ],
                'string',
                'length' => 10,
                'on' => 'checkout',
            ],
            [
                [
                    'date',
                ],
                DateValidator::className(),
                'pseudoRequired' => true,
                'min' => date(AppHelper::GetDateFormat(), strtotime('-' . self::CHILD_AGE . ' years')),
                'max' => date(AppHelper::GetDateFormat()),
                'on' => 'checkout',
            ],
            [
                ['privileges'],
                PassengerPrivilegesValidator::className(),
                'on' => 'checkout',
            ],
            [
                [
                    'animal',
                    'facilities',
                    'excess_baggage'
                ],
                'validateLuggage',
                'on' => 'checkout',
            ],
        ]);

        return $result;
    }

    public function validateLuggage($attribute, $params)
    {
        if (isset($this->$attribute)
            && !in_array($this->$attribute, [
                '0',
                '1'
            ])
        ) {
            $this->$attribute = null;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'date' => Module::t('app', 'Дата народження'),
            'document' => Module::t('app', 'Документ'),
            'privileges' => Module::t('app', 'Пільги'),
            'animal' => Module::t('app', GdTickets::LUGGAGE_DESCRIPTION_ANIMAL),
            'facilities' => Module::t('app', GdTickets::LUGGAGE_DESCRIPTION_FACILITIES),
            'excess_baggage' => Module::t('app', GdTickets::LUGGAGE_DESCRIPTION_EXCESS_BAGGAGE),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'date' => Module::t('app', 'Введіть Дата народження'),
            'document' => Module::t('app', 'Введіть Документ'),
            'privileges' => Module::t('app', 'Введіть Пільги'),
            'animal' => Module::t('app', 'Введіть ' . GdTickets::LUGGAGE_DESCRIPTION_ANIMAL),
            'facilities' => Module::t('app', 'Введіть ' . GdTickets::LUGGAGE_DESCRIPTION_FACILITIES),
            'excess_baggage' => Module::t('app', 'Введіть ' . GdTickets::LUGGAGE_DESCRIPTION_EXCESS_BAGGAGE),
        ]);

        return $result;
    }

//	public function attributePlaceholders()
//	{
//		$result = array_merge(parent::attributeHints(), [
//			'date' => Module::t('app', 'Дата (Placeholder) народження'),
//			'document' => Module::t('app', '(Placeholder) Документ'),
//			'privileges' => Module::t('app', '(Placeholder) Пільги'),
//			'animal' => Module::t('app', '(Placeholder) ' . GdTickets::LUGGAGE_DESCRIPTION_ANIMAL),
//			'facilities' => Module::t('app', '(Placeholder) ' . GdTickets::LUGGAGE_DESCRIPTION_FACILITIES),
//			'excess_baggage' => Module::t('app', '(Placeholder) ' . GdTickets::LUGGAGE_DESCRIPTION_EXCESS_BAGGAGE),
//		]);
//
//		return $result;
//	}

    public function getType()
    {
        if ($this->privileges == Gd::PRIVILEGES_CHILD) {
            $type = Module::t('app', 'Дитячий');
        } else {
            $type = Module::t('app', 'Дорослий');
        }

        return $type;
    }

    public function getFullName()
    {
        return sprintf('%s %s', $this->surname, $this->name);
    }

    public function getLuggage()
    {
        $luggage = [];
        if (isset($this->animal) && $this->animal == 1) {
            $luggage[] = Module::t('app', GdTickets::LUGGAGE_DESCRIPTION_ANIMAL);
        }
        if (isset($this->facilities) && $this->facilities == 1) {
            $luggage[] = Module::t('app', GdTickets::LUGGAGE_DESCRIPTION_FACILITIES);
        }
        if (isset($this->excess_baggage) && $this->excess_baggage == 1) {
            $luggage[] = Module::t('app', GdTickets::LUGGAGE_DESCRIPTION_EXCESS_BAGGAGE);
        }

        return $luggage;
    }
}
