<?php

namespace api\modules\v1\models\ticketsua;

use Yii;
use common\providers\tickets\Tickets;
use api\modules\v1\Module;
use api\modules\v1\components\validator\gd\ServicesValidator;
use api\modules\v1\components\validator\gd\OperationTypeValidator;
use api\modules\v1\components\validator\gd\CarValidator;
use api\modules\v1\components\validator\gd\TrainClassValidator;
use api\modules\v1\components\validator\gd\TrainSubClassValidator;
use api\modules\v1\components\validator\gd\TrainNumberValidator;
use common\payments\CommerceGateway\CommerceGateway;
use common\providers\tickets\GdTickets;
use common\models\Order;
use api\modules\v1\components\CoreTicketsUA;
use common\components\AppHelper;
use yii\helpers\ArrayHelper;

class Gd extends CoreTicketsUA
{
    const MAX_PASSENGERS = 4;

    const PRIVILEGES_CHILD = 1;
    const PRIVILEGES_STUDENT = 2;

    const PRIVILEGES_STUDENT_BY_CAR_CLASS = [
        GdTickets::CAR_CLASS_FIRST => false,
        GdTickets::CAR_CLASS_SECOND => false,
        GdTickets::CAR_CLASS_THIRD => true,
        GdTickets::CAR_CLASS_RESERVED => false,
        GdTickets::CAR_CLASS_NON_RESERVED => true,
        GdTickets::CAR_CLASS_COMFORTABLE => false,
    ];

    /**
     * Максимальное количество дней для диапазона дат поиска
     */
    const SEARCH_MAX_DAYS = 44;
    /**
     * Комиссия системы
     */
    const SERVICE_FEE = 5;

    /**
     * @var string Код станции отправления
     */
    public $from;
    /**
     * @var string Код станции прибытия
     */
    public $to;
    /**
     * @var string Дата отправления (DD.MM.YYYY)
     */
    public $date;
    /**
     * @var string Начальное время отправления поезда (необязательный) (HH:MM)
     */
    public $timeFrom;
    /**
     * @var string Конечное время отправления поезда (необязательный) (HH:MM)
     */
    public $timeTo;
    /**
     * @var string Номер поезда
     */
    public $trainNumber;
    /**
     * @var string Класс
     */
    public $carClass;
    /**
     * @var string Подласс
     */
    public $carSubclass;
    /**
     * @var string Id Вагона
     */
    public $carId;
    /**
     * @var bool Отказ от постельного белья
     */
    public $noClothes;
    /**
     * @var string Тип операции (обменный ваучер / электронный билет)
     */
    public $operationType;
    public $services;
    public $stationCode;

    public function __construct($response, $session, $partner = null, $config = [])
    {
        parent::__construct($response, $session, $partner, $config);
        $this->_provider = new GdTickets();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'from',
                    'to',
                    'date',
                ],
                'required',
                'on' => 'searchtrain',
            ],
            [
                [
                    'timeFrom',
                    'timeTo',
                ],
                'safe',
                'on' => 'searchtrain',
            ],
            [
                [
                    'timeFrom',
                    'timeTo',
                ],
                'string',
                'length' => 5,
                'on' => 'searchtrain',
            ],
            [
                [
                    'from',
                    'to',
                ],
                'string',
                'length' => 7,
                'on' => 'searchtrain',
            ],
            [
                ['date'],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(), strtotime('+' . self::SEARCH_MAX_DAYS . ' days')),
                'on' => [
                    'searchtrain',
                    'timetable'
                ],
            ],
            [
                [
                    'from',
                    'to',
                    'date',
                    'trainNumber',
                ],
                'required',
                'on' => 'trainroute',
            ],
            [
                ['trainNumber'],
                'trim',
                'on' => 'trainroute',
            ],
            [
                ['trainNumber'],
                //TrainRouteValidator::className(),
                'string',
                'min' => 3,
                'max' => 5,
                'on' => 'trainroute',
            ],
            [
                [
                    'trainNumber',
                    'carClass',
                    'carSubclass'
                ],
                'required',
                'on' => 'traininfo',
            ],
            [
                [
                    'trainNumber',
                ],
                TrainNumberValidator::className(),
                'min' => 3,
                'max' => 5,
                'on' => 'traininfo',
            ],
            [
                [
                    'carClass',
                ],
                TrainClassValidator::className(),
                'min' => 3,
                'max' => 10,
                'on' => 'traininfo',
            ],
            [
                [
                    'carSubclass'
                ],
                TrainSubClassValidator::className(),
                'on' => 'traininfo',
            ],
            [
                [
                    'carId'
                ],
                'required',
                'on' => 'carinfo',
            ],
            [
                [
                    'carId'
                ],
                CarValidator::className(),
                'min' => 0,
                'on' => 'carinfo',
            ],
            [
                ['noClothes'],
                'safe',
                'on' => 'checkout'
            ],
            [
                ['operationType'],
                'required',
                'on' => 'checkout'
            ],
            [
                ['operationType'],
                OperationTypeValidator::className(),
                'range' => [
                    GdTickets::OPERATION_TYPE_V,
                    GdTickets::OPERATION_TYPE_E
                ],
                'on' => 'checkout'
            ],
            [
                ['services'],
                'safe',
                'on' => 'checkout'
            ],
            [
                ['services'],
                ServicesValidator::className(),
                'range' => GdTickets::getServicesAll(),
                'on' => 'checkout'
            ],
            [
                [
                    'stationCode',
                    'date'
                ],
                'required',
                'on' => 'timetable',
            ],
            [
                'stationCode',
                'string',
                'length' => 7,
                'on' => 'timetable',
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'from' => Module::t('app', 'Станція відправлення'),
            'to' => Module::t('app', 'Станція прибуття'),
            'date' => Module::t('app', 'Дата відправлення'),
            'timeFrom' => Module::t('app', 'Час відправлення від'),
            'timeTo' => Module::t('app', 'Час до'),
            'trainNumber' => Module::t('app', 'Номер потягу'),
            'carClass' => Module::t('app', 'Клас вагона'),
            'carSubclass' => Module::t('app', 'Підклас вагона'),
            'carId' => Module::t('app', 'ID вагона'),
            'noClothes' => Module::t('app', 'Відмова від постільної білизни'),
            'operationType' => Module::t('app', 'Тип документу що купується'),
            'services' => Module::t('app', 'Додаткові сервіси'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'from' => Module::t('app', 'Введіть Станція відправлення'),
            'to' => Module::t('app', 'Введіть Станція прибуття'),
            'date' => Module::t('app', 'Введіть Дата відправлення'),
            'timeFrom' => Module::t('app', 'Введіть Час відправлення від'),
            'timeTo' => Module::t('app', 'Введіть Час до'),
            'trainNumber' => Module::t('app', 'Введіть Номер потягу'),
            'carClass' => Module::t('app', 'Введіть Клас вагона'),
            'carSubclass' => Module::t('app', 'Введіть Підклас вагона'),
            'carId' => Module::t('app', 'Введіть ID вагона'),
            'noClothes' => Module::t('app', 'Введіть Відмова від постільної білизни'),
            'operationType' => Module::t('app', 'Введіть Тип документу що купується'),
            'services' => Module::t('app', 'Введіть Додаткові сервіси'),
        ]);

        return $result;
    }

//	public function attributePlaceholders()
//	{
//		$result = array_merge(parent::attributePlaceholders(), [
//			'from' => Module::t('app', '(Placeholder) Станція відправлення'),
//			'to' => Module::t('app', '(Placeholder) Станція прибуття'),
//			'date' => Module::t('app', '(Placeholder) Дата відправлення'),
//			'timeFrom' => Module::t('app', '(Placeholder) Час з'),
//			'timeTo' => Module::t('app', '(Placeholder) Час до'),
//			'noClothes' => Module::t('app', '(Placeholder) Відмова від постільної білизни'),
//			'operationType' => Module::t('app', '(Placeholder) Тип документу що купується'),
//			'passengers' => Module::t('app', '(Placeholder) Пасажири'),
//			'services' => Module::t('app', '(Placeholder) Додаткові сервіси'),
//		]);
//
//		return $result;
//	}

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        try {
            $services = json_decode($this->services);
            if (!is_array($services)) {
                $services = [];
            }
            $this->services = $services;
        } catch (\Exception $e) {
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
        if (isset($this->date)) {
            $this->date = Yii::$app->formatter->asDate($this->date, self::DATE_FORMAT);
        }
    }

    /**
     * Поиск станции по указанному имени
     *
     * @return array Список станций
     */
    public function SearchStation()
    {
        $name = trim($this->searchStationName);
        $name = AppHelper::reverseTransliteration($name);
        $cacheKey = $this->getCacheKeyStationSearch($name);
        $cache = Yii::$app->cache->get($cacheKey);
        $form = $this->GetServiceForm();
        if ($cache === false) {
            $result = $this->_provider->searchStations($name);
            if ($result['success']) {
//				if (!isset($form['stationHistory'])) { // История нужна для того чтобы определить где находится станция, в украине или за пределами
//					$form['stationHistory'] = [];
//				}
//				if ($result['data']) {
//					foreach ($result['data'] as $s) {
//						$form['stationHistory'][$s['id']] = $s['internal'];
//					}
//				}
                $order = Yii::$app->params['providers']['dictionaries']['order']['stations']['gd'];
                $items = array_map(function ($item) use ($order) {
                    if (array_key_exists($item['id'], $order)) {
                        $item['order'] = $order[$item['id']];
                    }

                    return $item;
                }, $result['data']);
                $items = array_values($items);
                $sortOptions = [
                    'order' => ArrayHelper::getColumn($items, 'order'),
                    'value' => ArrayHelper::getColumn($items, 'value'),
                ];
                array_multisort($sortOptions['order'], SORT_ASC, $sortOptions['value'], SORT_ASC, $items);
                $this->_response['data'] = $items;
                Yii::$app->cache->set($cacheKey, $this->_response['data'],
                    Yii::$app->params['providers']['dictionaries']['cacheDuration']);
            } else {
                $this->_response['data'] = [];
            }
        } else {
//			if (!isset($form['stationHistory'])) { // История нужна для того чтобы определить где находится станция, в украине или за пределами
//				$form['stationHistory'] = [];
//			}
//			if ($cache) {
//				foreach ($cache as $s) {
//					$form['stationHistory'][$s['id']] = $s['internal'];
//				}
//			}
            $this->_response['data'] = $cache;
        }
        if (!array_key_exists('searchStationsHistory', $form)) {
            $form['searchStationsHistory'] = [];
        }
        $form['searchStationsHistory'] = ArrayHelper::merge($form['searchStationsHistory'],
            ArrayHelper::map($this->_response['data'], 'id', 'value'));
        $this->SetServiceForm($form);

        return $this->_response;
    }

    /**
     * Запрос списка поездов на указанную дату
     *
     * @return mixed
     */
    public function SearchTrain()
    {
        $form = $this->GetServiceForm();
        $searchParams = [
            'from' => $this->from,
            'to' => $this->to,
            'date' => $this->date,
        ];

        if ($this->date != Yii::$app->formatter->asDate(time(), self::DATE_FORMAT)) {
            if ($this->timeFrom) {
                $searchParams['time_from'] = $this->timeFrom;
            }
            if ($this->timeTo) {
                $searchParams['time_to'] = $this->timeTo;
            }
        }

        $result = $this->_provider->searchTrains($searchParams);
        if ($result['success']) {
            $searchParams['from_title'] = $this->GetStationFromHistory($this->from);
            $searchParams['to_title'] = $this->GetStationFromHistory($this->to);
            $searchParams['date'] = Yii::$app->formatter->asDate($searchParams['date']);
            $form['search'] = $searchParams;
            foreach ($result['data'] as $k => &$train) {
                // как показывает практика, список классов может отсутствовать
                if (is_null($train['class_list'])
                    || (is_array($train['class_list']
                        && count($train['class_list']) == 0))
                ) {
                    unset($result['data'][$k]);
                    continue;
                }
                $totalSeats = 0;
                foreach ($train['class_list'] as $class) {
                    $totalSeats += (int)$class['seats'];
                }
                $departureDateTime = \DateTime::createFromFormat('d-m-Y H:i',
                    $train['departure_date'] . ' ' . $train['departure_time']);
                $train['departure_date'] = Yii::$app->formatter->asDate($departureDateTime);
                $train['departure_datetime'] = Yii::$app->formatter->asDatetime($departureDateTime);
                $train['departure_dayofweek'] = AppHelper::GetDayOfWeek($departureDateTime);
                $train['departure_time_of_day'] = $this->getTimeOfDay($departureDateTime);
                $arrivalDateTime = \DateTime::createFromFormat('d-m-Y H:i',
                    $train['arrival_date'] . ' ' . $train['arrival_time']);
                $train['arrival_date'] = Yii::$app->formatter->asDate($arrivalDateTime);
                $train['arrival_datetime'] = Yii::$app->formatter->asDatetime($arrivalDateTime);
                $train['arrival_dayofweek'] = AppHelper::GetDayOfWeek($arrivalDateTime);
                $train['arrival_time_of_day'] = $this->getTimeOfDay($arrivalDateTime);
                $train['total_seats'] = $totalSeats;
                $train['name'] = '';
            }
            $trains = array_values($result['data']);
            $result['data'] = [
                'search' => $form['search'],
                'trains' => $trains,
            ];
            $form['trains'] = $trains;
            $this->_response['data'] = $result['data'];
            $this->SetServiceForm($form);
        } else {
            $code = 20;
            switch ($result['code']) {
                case 1104 :
                    $message = Module::t('app',
                        'Запит не може бути виконаний для зазначених станцій відправлення / призначення. На задану дату відправлення в системі відсутні поїзди, які слідують між зазначеними станціями, для яких система може визначити кількість вільних місць і виділити місця для бронювання');
                    break;
                case 1051 :
                    $message = Module::t('app',
                        'Запит не може бути виконаний для зазначених станцій відправлення / призначення. На задану дату відправлення в системі відсутні поїзди, які слідують між зазначеними станціями, для яких система може визначити кількість вільних місць і виділити місця для бронювання');
                    break;
                default :
                    Yii::error($result, __METHOD__);
                    $message = Module::t('app', 'Виникла тимчасова помилка, спробуйте повторити пізніше.');
                    break;
            }
            $this->_response['code'] = $code;
            $this->_response['message'] = $message;
        }

        return $this->_response;
    }

    /**
     * Запрос промежуточных станций следования поезда
     *
     * @return mixed
     */
    public function TrainRoute()
    {
        $params = [
            'from' => $this->from,
            'to' => $this->to,
            'date' => $this->date,
            'train_number' => $this->trainNumber,
        ];
        $key = md5(json_encode($params));
        $cache = Yii::$app->cache->get($key);
        if ($cache === false) {
            $result = $this->_provider->trainRoute($params);
            if ($result['success']) {
                $result = $result['data'];
                Yii::$app->cache->set($key, $result, Yii::$app->params['providers']['dictionaries']['cacheDuration']);
            } else {
                $this->_response['code'] = $this::RESPONSE_CODE_ERROR;
                $this->_response['message'] = $result['message'];
                $result = null;
            }
        } else {
            $result = $cache;
        }
        $this->_response['data'] = $result;

        return $this->_response;
    }

    /**
     * Запрос информации по одному поезду
     *
     * @return mixed
     */
    public function TrainInfo()
    {
        $trainInfo = $this->_trainInfo($this->trainNumber, $this->carClass, $this->carSubclass);
        if ($trainInfo['success']) {
            $carInfo = $this->_carInfo($trainInfo['data']['class']['cars'][0]['id']);
            if ($carInfo['success']) {
                $trainInfo['data']['class']['cars'][0]['seats'] = $carInfo['data']['free_seats'];
                $this->_response['data'] = [
                    'trainInfo' => $trainInfo['data'],
                    'carInfo' => $carInfo['data'],
                ];
            } else {
                $this->_response['code'] = $carInfo['code'];
                $this->_response['message'] = $carInfo['message'];
            }
        } else {
            $this->_response['code'] = $trainInfo['code'];
            $this->_response['message'] = $trainInfo['message'];
        }

        return $this->_response;
    }

    protected function _trainInfo($trainNumber, $carClass, $carSubclass)
    {
        $params = [
            'train_number' => $trainNumber,
        ];
        $result = $this->_provider->getTrainInfo($params);
        if ($result['success']) {
            $form = $this->GetServiceForm();
//			$arrivalCode = $result['data']['passenger_arrival_code'];
//			$result['data']['internal'] = ($form['stationHistory'][$arrivalCode] == 1)
//				? true
//				: false;
            $trainInfo = &$result['data'];
            $departureDate = Yii::$app->formatter->asDate($trainInfo['departure_date']);
            $trainInfo['departure_date'] = $departureDate;
            $trainInfo['departure_date_time'] = Yii::$app->formatter->asDatetime($trainInfo['departure_date'] . ' '
                . $trainInfo['departure_time']);
            $dateTime = \DateTime::createFromFormat(AppHelper::GetDateTimeFormat(), $trainInfo['departure_date_time']);
            $trainInfo['departure_day'] = AppHelper::mb_ucfirst(AppHelper::GetDayOfWeek($dateTime));
            $trainInfo['departure_day_short'] = AppHelper::mb_ucfirst(AppHelper::GetDayOfWeekShort($dateTime));
            $trainInfo['departure_month'] = AppHelper::mb_ucfirst(AppHelper::GetMonth($dateTime));
            $arrivalDate = Yii::$app->formatter->asDate($trainInfo['arrival_date']);
            $trainInfo['arrival_date'] = $arrivalDate;
            $trainInfo['arrival_date_time'] = Yii::$app->formatter->asDatetime($trainInfo['arrival_date'] . ' '
                . $trainInfo['arrival_time']);
            $dateTime = \DateTime::createFromFormat(AppHelper::GetDateTimeFormat(), $trainInfo['arrival_date_time']);
            $trainInfo['arrival_day'] = AppHelper::mb_ucfirst(AppHelper::GetDayOfWeek($dateTime));
            $trainInfo['arrival_day_short'] = AppHelper::mb_ucfirst(AppHelper::GetDayOfWeekShort($dateTime));
            $trainInfo['arrival_month'] = AppHelper::mb_ucfirst(AppHelper::GetMonth($dateTime));
            $travelTimeHours = floor($trainInfo['travel_time'] / 60);
            $travelTimeMinutes = $trainInfo['travel_time'] % 60;
            $trainInfo['travel_time_hours'] = $travelTimeHours;
            $trainInfo['travel_time_minutes'] = $travelTimeMinutes;
            $trainInfo['train_class_label'] = GdTickets::getTrainClassLabel($trainInfo['train_class']);
            $trainInfo['train_speed_label'] = GdTickets::getTrainSpeedLabel($trainInfo['train_speed']);
            if ($result['data']['classes']) {
                $availableOperationTypes = GdTickets::GetAvailableOperationTypes();
                $classes = $result['data']['classes'];
                unset($result['data']['classes']);
                $order = new Order();
                $order->scenario = 'commission';
                $order->fee = self::SERVICE_FEE;
                foreach ($classes as &$class) {
                    if ($class['name'] == $carClass && $class['subclass'] == $carSubclass) {
                        unset($class['exchanges']);
                        $order->income_amount = $class['cost'];
                        $order->e_fee = self::ACQUIRING_RATE_OTHER;
                        $order->calculateCommission();
                        $class['cost'] = AppHelper::FormatCurrency($order->amount);

                        foreach ($class['cars'] as &$car) {
                            $carOperationTypes = [];
                            foreach (explode(',', $car['operation_types']) as $operationType) {
                                if (in_array($operationType, $availableOperationTypes)) {
                                    $carOperationTypes[] = $operationType;
                                }
                            }
                            $car['operation_types'] = $carOperationTypes;
                        }
                        $result['data']['class'] = $class;
                        break;
                    }
                }
            }
            $form['selected_train_info'] = $result['data'];
            $form['selected_train_number'] = $trainNumber;
            $this->SetServiceForm($form);
        } else {
            Yii::error($result, __METHOD__);
            $code = self::RESPONSE_CODE_ERROR;
            $message = Module::t('app', 'Виникла помилка, повторіть спробу');
            if ($result['code'] < 99999) {
                $message = $result['message'];
            }
            $this->_response['code'] = $code;
            $this->_response['message'] = $message;
        }

        return $result;
    }

    /**
     * Запрос списка свободных мест по указанному вагону
     *
     * @return mixed
     */
    public function CarInfo()
    {
        $result = $this->_carInfo($this->carId);
        if ($result['success']) {
            $this->_response['data'] = $result['data'];
        } else {
            $this->_response['code'] = $result['code'];
            $this->_response['message'] = $result['message'];
        }

        return $this->_response;
    }

    /**
     * Запрос списка свободных мест по указанному вагону
     *
     * @return mixed
     */
    protected function _carInfo($carId)
    {
        $form = $this->GetServiceForm();
        $params = [
            'car_id' => $carId
        ];
        $result = $this->_provider->getCarInfo($params);
        if ($result['success']) {
            $freeSeats = 0;
            $carSeats = [];
            foreach ($result['data']['seats'] as $seat) {
                if ($seat['type'] <= 10) {
                    $freeSeats++;
                }
                $carSeats[$seat['number']] = $seat;
            }
            $result['data']['free_seats'] = $freeSeats;
            $carNumber = $result['data']['number'];
            $carClass = $result['data']['class'];
            $trainModel = $result['data']['train_model'];
            $result['data']['clothes_available'] = $this->IsClothesAvailable($trainModel, $carClass);
            $result['data']['privileges'] = $this->GetPrivilegesInCar($carClass);
            $carsOperationTypes = ArrayHelper::map($form['selected_train_info']['class']['cars'], 'id',
                'operation_types');
            $result['data']['operation_types'] = $carsOperationTypes[$carId];
            $form['carSeats'] = $carSeats;
            $form['className'] = $result['data']['class'];
            $form['subclass'] = $result['data']['subclass'];
            $form['carId'] = $carId;
            $form['carNumber'] = $carNumber;
            $this->SetServiceForm($form);
        }

        return $result;
    }

    /**
     * Оформление заказа
     *
     * @return array
     */
    public function Checkout()
    {
        $noClothes = (bool)$this->noClothes;
        $form = $this->GetServiceForm();
        if (!is_null($form)) {
            $this->_response['code'] = 10;
            $trainInfo = $form['selected_train_info'];
            $trainDeparture = $trainInfo['train_departure_name'];
            $trainArrival = $trainInfo['train_arrival_name'];
            $passengerDeparture = $trainInfo['passenger_departure_name'];
            $passengerArrival = $trainInfo['passenger_arrival_name'];
            $travelTime = $trainInfo['travel_time_hours'] . ' ' . Module::t('app', 'год.') . ' '
                . $trainInfo['travel_time_minutes'] . ' ' . Module::t('app', 'хв.');
            $orderInfo = [
                'train' => $form['selected_train_number'],
                'voyage' => $trainDeparture . ' - ' . $trainArrival,
                'departure' => [
                    'date_time' => $trainInfo['departure_date_time'],
                    'name' => $passengerDeparture,
                ],
                'arrival' => [
                    'date_time' => $trainInfo['arrival_date_time'],
                    'name' => $passengerArrival,
                ],
                'travel_time' => $travelTime,
            ];
            /** @var PassengerGd $passenger */
            foreach ($this->_validPassengers as $passenger) {
                $orderInfo['passengers'][] = [
                    'car' => $passenger->carNumber,
                    'seat' => $passenger->seat,
                    'class' => GdTickets::getClassLabel($form['className']),
                    'passenger' => $passenger->getFullName(),
                    'type' => $passenger->getType(),
                    'train_class_label' => $trainInfo['train_class_label'],
                    'train_speed_label' => $trainInfo['train_speed_label'],
                    'luggage' => $passenger->getLuggage(),
                    'services' => $this->getServices(),
                ];
            }
            $selectedSeats = [];
            /** @var PassengerGd $validPassenger */
            foreach ($this->_validPassengers as $validPassenger) {
                $selectedSeats[] = [
                    'seat' => $validPassenger->seat,
                    'name' => $validPassenger->name,
                    'surname' => $validPassenger->surname,
                    'date' => $validPassenger->date,
                    'document_type' => $validPassenger->document_type,
                    'document' => $validPassenger->document,
                    'animal' => $validPassenger->animal,
                    'facilities' => $validPassenger->facilities,
                    'excess_baggage' => $validPassenger->excess_baggage,
                ];
            }
            $form['selected_seats'] = $selectedSeats;
            $form['operation_type'] = $this->operationType;
            $form['services'] = $this->services;
            $form['no_clothes'] = $noClothes;
            $result = $this->_provider->reservation($form);
            if ($result['success'] && is_array($reservation = $result['reservation'])) {
                $seatNumbers = explode(',', $reservation['seats']);
                $seatsNumber = 0;
                foreach ($form['selected_seats'] as $seat_key => $seat) {
                    $form['selected_seats'][$seat_key]['seat'] = $seatNumbers[$seatsNumber];
                    $seatsNumber++;
                }
                $form['operation_type'] = $reservation['operation_type'];
                $orderInfo['operation_type_id'] = $form['operation_type'];
                $orderInfo['operation_type'] = GdTickets::getOperationTypeLabel($form['operation_type']);
                $orderInfo['no_clothes'] = $noClothes;
                $expirationTime = $this->getReservationEndSpace(new \DateTime($reservation['expiration_time']));
                /** @var Order $order */
                $order = $this->saveData($reservation['id'], $expirationTime, $reservation['cost'],
                    $reservation['currency'], $seatsNumber, $form, $orderInfo, Tickets::PREFIX, $result['dialog']);
                $this->_response['code'] = 0;
                $this->_response['data'] = [
                    'OrderInfo' => $orderInfo,
                    'PaymentAuthorizationData' => CommerceGateway::createAuthorizationData($order)
                ];
                if ($this->isFrontCall()) {
                    Yii::$app->core->reservationCreated($order);
                }
            } else {
                Yii::error($result, __METHOD__);
                switch ($result['code']) {
                    case 138 :
                        $code = 20;
                        break;
                    case 1280 :
                        $code = 20;
                        break;
                    default :
                        $code = 50;
                        break;
                }
                $this->_response['code'] = $code;
                $this->_response['message'] = $result['message'];
            }
        } else {
            $this->_response['message'] = 'Сессия истекла';
            $this->_response['code'] = 20;
        }

        return $this->_response;
    }

//	/**
//	 * Получение списка попыток оплаты заказа
//	 *
//	 * @param string $orderId Номер заказа
//	 * @param string $status Статус транзакции (через запятую) (created,success,failure,refunded)
//	 * @return array
//	 */
//	public function Transactions($orderId, $status)
//	{
//		/** @var Order $order */
//		$order = Order::findOne([
//			'id' => $orderId,
//			//todo
//			//'user_id' => CSession::_()->userId,
//		]);
//		if ($order) {
//			//todo непонятно, номер какого заказа в тикетс передавать
//			$result = Payments::getTransactions('gd', $orderId, $status);
//			if ($result['success']) {
//				$this->_response['data'] = $result['data'];
//			} else {
//				$this->_response['code'] = -2;
//				$this->_response['message'] = 'Query parameter is empty';
//			}
//		} else {
//			$this->_response['code'] = -1;
//			$this->_response['message'] = 'Замовлення не знайдене';
//		}
//
//		return $this->_response;
//	}

    /**
     * Аннулирование билетов
     *
     * @param string $orderId Номер заказа
     * @return array
     */
    public function Cancel($orderId)
    {
        /** @var Order $order */
        $order = Order::findOne([
            'id' => $orderId,
            //todo
            //'user_id' => CSession::_()->userId,
        ]);
        if ($order && $order->provider_order_id) {
            if ($order->canBeeCancelled()) {
                $result = $this->_provider->cancel($order->provider_order_id);
                if ($result['success']) {
                    $this->_response['data'] = $result['data'];
                } else {
                    $this->_response['code'] = $result['code'];
                    $this->_response['message'] = $result['msg'];
                }
            } else {
                $this->_response['code'] = -1;
                $this->_response['message'] = 'Не може буте скасоване';
            }
        } else {
            $this->_response['code'] = -1;
            $this->_response['message'] = 'Бронювання не знайдене';
        }

        return $this->_response;
    }

    /**
     * Запрос расписания поездов по станции на указанную дату
     *
     * @return array
     */
    public function TimeTable()
    {
        $result = $this->_provider->timeTable($this->stationCode, $this->date);
        if ($result['success']) {
            $this->_response['data'] = $result['data'];
        } else {
            $this->_response['code'] = $result['code'];
            $this->_response['message'] = $result['msg'];
        }

        return $this->_response;
    }

    /**
     * Запрос времени следующей технической остановки сервиса
     *
     * @return array
     */
    public function NextTechStop()
    {
        $result = $this->_provider->nextTechStop();
        if ($result['success']) {
            $this->_response['data'] = $result['data'];
        } else {
            $this->_response['code'] = $result['code'];
            $this->_response['message'] = $result['msg'];
        }

        return $this->_response;
    }

    private function getCost($className, $subclass)
    {
        $form = $this->GetServiceForm();
        $class = $form['selected_train_info']['class'];
        if ($class['name'] == $className && $class['subclass'] == $subclass) {
            return $class['cost'];
        }

        return null;
    }

    /**
     * Возвращает список разрешенных типов операций по вагону
     *
     * @param array $class
     * @param string $className
     * @param string $subClass
     * @param string $carId
     * @return array
     */
    public function GetCarOperationTypes(array $class, $className, $subClass, $carId)
    {
        if ($class['name'] == $className && $class['subclass'] == $subClass) {
            foreach ($class['cars'] as $car) {
                if ($car['id'] == $carId) {
                    $result = [];
                    $availableOperationTypes = GdTickets::GetAvailableOperationTypes();
                    foreach ($car['operation_types'] as $operationType) {
                        if (in_array($operationType, $availableOperationTypes)) {
                            $result[] = $operationType;
                        }
                    }

                    return $result;
                }
            }
        }

        return [GdTickets::OPERATION_TYPE_V];
    }

    /**
     * Возвращает список доступных сервисов по вагону
     *
     * @param array $class
     * @param string $className
     * @param string $subClass
     * @return array
     */
    public function GetCarServices(array $class, $className, $subClass)
    {
        $services = [];
        if ($class['name'] == $className && $class['subclass'] == $subClass) {
            $services = $class['services'];
        }

        return $services;
    }

    public static function PrivilegeStudentPresent($class)
    {
        $result = false;
        $items = self::PRIVILEGES_STUDENT_BY_CAR_CLASS;
        if (array_key_exists($class, $items)) {
            $result = $items[$class];
        }

        return $result;
    }

    public static function GetPrivilegesInCar($class)
    {
        return [
            self::PRIVILEGES_CHILD => true,
            self::PRIVILEGES_STUDENT => self::PrivilegeStudentPresent($class),
        ];
    }

    public function getServices()
    {
        $result = [];
        foreach (GdTickets::SERVICES as $key => $value) {
            if (in_array($key, $this->services)) {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    protected function IsClothesAvailable($trainModel, $carClass)
    {
        $result = false;
        if ($trainModel == GdTickets::TRAIN_MODEL_OTHER
            && in_array($carClass, [
                GdTickets::CAR_CLASS_FIRST,
                GdTickets::CAR_CLASS_SECOND,
                GdTickets::CAR_CLASS_THIRD,
            ])
        ) {
            $result = true;
        }

        return $result;
    }
}
