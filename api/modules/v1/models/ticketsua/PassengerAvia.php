<?php

namespace api\modules\v1\models\ticketsua;

use Yii;
use api\modules\v1\components\validator\OnlyLatAlphaValidator;
use api\modules\v1\components\validator\avia\PassengerTypeValidator;
use common\providers\tickets\AviaTickets;
use api\modules\v1\components\validator\gd\DateValidator;
use api\modules\v1\Module;
use common\components\AppHelper;

class PassengerAvia extends Passenger
{
    public $n;
    public $patronymic;
    public $gender;
    public $type;
    public $birthday;
    public $citizenship;
    public $docnum;
    public $doc_type;
    public $doc_expire_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'n',
                ],
                'required',
                'on' => 'checkout',
            ],
            [
                [
                    'name',
                    'surname',
                ],
                OnlyLatAlphaValidator::className(),
                //'on' => 'checkout',
            ],
            [
                [
                    'type',
                ],
                'required',
                'on' => 'checkout',
            ],
            [
                [
                    'type',
                ],
                PassengerTypeValidator::className(),
                'range' => AviaTickets::PASSENGER_TYPES,
                'on' => 'checkout',
            ],
            [
                [
                    'docnum',
                ],
                'safe',
                'on' => 'checkout',
            ],
            [
                [
                    'docnum',
                ],
                'string',
                'length' => 10,
                'on' => 'checkout',
            ],
            [
                [
                    'doc_expire_date',
                ],
                'safe',
                'on' => 'checkout',
            ],
            [
                [
                    'doc_expire_date',
                ],
                DateValidator::className(),
                'pseudoRequired' => true,
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(), strtotime('+25 years')),
                'on' => 'checkout',
            ],

            [
                [
                    'birthday',
                ],
                'required',
                'on' => 'checkout',
            ],
            [
                [
                    'birthday',
                ],
                DateValidator::className(),
                'pseudoRequired' => true,
                'min' => date(AppHelper::GetDateFormat(), strtotime('-150 years')),
                'max' => date(AppHelper::GetDateFormat()),
                'on' => 'checkout',
            ],
            [
                'citizenship',
                'required',
                'on' => 'checkout',
            ],
            [
                'citizenship',
                'string',
                'length' => 2,
                'on' => 'checkout',
            ],
            [
                'gender',
                'required',
                'on' => 'checkout',
            ],
            [
                'gender',
                'in',
                'range' => [
                    'M',
                    'F'
                ],
                'on' => 'checkout',
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'n' => Module::t('app', 'Номер пасажира'),
            'birthday' => Module::t('app', 'Дата народження'),
            'docnum' => Module::t('app', 'Документ'),
            'doc_type' => Module::t('app', 'Тип Документа'),
            'gender' => Module::t('app', 'Стать'),
            'citizenship' => Module::t('app', 'Громадянство'),
            'doc_expire_date' => Module::t('app', 'Термін дії документа'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'n' => Module::t('app', 'Номер пасажира'),
            'birthday' => Module::t('app', 'Введіть Дата народження'),
            'docnum' => Module::t('app', 'Введіть Документ'),
            'doc_type' => Module::t('app', 'Введіть Тип Документа'),
            'gender' => Module::t('app', 'Введіть Стать'),
            'citizenship' => Module::t('app', 'Виберіть Громадянство'),
            'doc_expire_date' => Module::t('app', 'Введіть дії документа'),
        ]);

        return $result;
    }

    public function getFullName()
    {
        return sprintf('%s %s', $this->name, $this->surname);
    }
}
