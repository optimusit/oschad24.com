<?php

namespace api\modules\v1\models\ticketsua;

use Yii;
use api\modules\v1\components\validator\OnlyCyrAlphaValidator;
use api\modules\v1\Module;
use api\modules\v1\components\validator\bus\PassengerDocumentValidator;
use api\modules\v1\components\validator\bus\PassengerBirthDayValidator;
use common\components\AppHelper;
use api\modules\v1\components\validator\bus\PassengerSeatValidator;

class PassengerBus extends Passenger
{
    const MAX_AGE = 150;

    public $seat;
    public $date;
    public $documentType;
    public $documentNumber;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'name',
                    'surname',
                ],
                OnlyCyrAlphaValidator::className(),
                'min' => 1,
                'max' => 50,
                'on' => 'checkout',
            ],
            [
                [
                    'seat',
                ],
                'required',
                'on' => 'checkout',
            ],
            [
                [
                    'seat',
                ],
                PassengerSeatValidator::className(),
                'on' => 'checkout',
            ],
            [
                [
                    'date',
                    'documentType',
                    'documentNumber',
                ],
                'safe',
                'on' => 'checkout',
            ],
            [
                [
                    'date',
                ],
                'date',
                'min' => date(AppHelper::GetDateFormat(), strtotime('-' . self::MAX_AGE . ' years')),
                'max' => date(AppHelper::GetDateFormat()),
                'on' => 'checkout',
            ],
            [
                [
                    'date',
                ],
                PassengerBirthDayValidator::className(),
                'on' => 'checkout',
            ],
            [
                'documentType',
                //'range' => array_keys(BusTickets::DOCUMENT_TYPE),
                'integer',
                'on' => 'checkout',
            ],
            [
                [
                    'documentNumber',
                ],
                'string',
                'min' => 5,
                'max' => 10,
                'on' => 'checkout',
            ],
            [
                [
                    'documentNumber',
                ],
                PassengerDocumentValidator::className(),
                'on' => 'checkout',
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'date' => Module::t('app', 'Дата народження'),
            'documentType' => Module::t('app', 'Тип документу'),
            'documentNumber' => Module::t('app', 'Номер документу'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'date' => Module::t('app', 'Введіть Дата народження'),
            'documentType' => Module::t('app', 'Введіть Тип документу'),
            'documentNumber' => Module::t('app', 'Введіть Номер документу'),
        ]);

        return $result;
    }

    public function attributePlaceholders()
    {
        $result = array_merge(parent::attributeHints(), [
            'date' => Module::t('app', 'Дата народження'),
            'documentType' => Module::t('app', 'Тип документу'),
            'documentNumber' => Module::t('app', 'Номер документу'),
        ]);

        return $result;
    }

    public function getFullName()
    {
        return sprintf('%s %s', $this->surname, $this->name);
    }
}
