<?php

namespace api\modules\v1\models\aiwa;

use Yii;
use api\modules\v1\components\ApiException;
use common\models\Order;
use common\models\aiwa\Document;
use common\models\Customer;
use common\models\aiwa\Participant;
use common\providers\aiwa\AIWAprovider;
use api\modules\v1\components\CoreTicketsUA;

class Accidents5 extends CoreTicketsUA
{
    const ACCIDENTS5_PROGRAMM = 8;
    const ACCIDENTS5_PERIOD = 12;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'cover',
                ],
                'required',
                'on' => 'calculate'
            ],
            [
                'cover',
                'integer',
                'min' => 10000,
                'max' => 100000,
            ],
        ];
    }

    /**
     * Расчитать сумму
     *
     * @param $cover
     * @param $plan
     * @return array
     */
    public function calculate($cover, $plan = 'A')
    {
        $period = '+' . intval(self::ACCIDENTS5_PERIOD) . 'm';
        $params = [
            'kind' => Document::TYPE_ACCIDENTS5,
            'date-period' => $period,
            'cover' => $cover,
            'plan' => $plan,
            'vendorCount' => 1,
            'programm' => self::ACCIDENTS5_PROGRAMM,
        ];
        try {
            $result = AIWAprovider::getInstance()
                ->calculate($params);
            if ($result['success']) {
                $form = $this->GetServiceForm();
                $form['calculate']['request'] = $params;
                $form['calculate']['response'] = $result['data'];
                $this->SetServiceForm($form);
                $this->_response['data'] = $result['data'];
            } else {
                $this->_response['code'] = $result['code'];
                $this->_response['message'] = $result['message'];
            }
        } catch (ApiException $e) {
            $this->_response['code'] = $e->getCode() == 0
                ? 500
                : $e->getCode();
            $this->_response['message'] = $e->getMessage();
        }

        return $this->_response;
    }

    /**
     * Расчитать сумму
     *
     * @param array $params
     * @return array
     */
    public function document($params)
    {
        $form = $this->GetServiceForm();
        if (!is_null($form)) {
            // создадим/обновим кастомера
            $customer = null;
            if (isset($params['phone'])) {
                $customer = Customer::findOne(['phone' => $params['phone']]);
            }
            if (!$customer) {
                $customer = new Customer();
            }
            $customer->setAttributes($params);
            $customer->partner_id = $this->_partnerId;
            if ($customer->save()) {
                // создадим/обновим ордер
                if (isset($form['order_id'])) {
                    $order = Order::findOne($form['order_id']);
                } else {
                    $order = new Order();
                }
                $order->scenario = 'commission';
                $order->attributes = [
                    'user_id' => $this->_userId,
                    'customer_id' => $customer->id,
                    'partner_id' => $this->_partnerId,
                    'service_id' => $this->getService()->id,
                    'bank_fee' => 0,
                    'income_amount' => $form['calculate']['response']['amount'],
                    'unit_count' => 1,
                    'custom' => serialize($form),
                    'status_id' => Order::STATUS_PENDING,
                    'email' => $customer->email,
                    'customer_fio' => $customer->getFullName(),
                    'phone' => $customer->phone,
                ];
                $order->calculateCommission();
                $order->scenario = 'default';
                if ($order->save()) {
                    $form['order_id'] = $order->id;
                    $this->SetServiceForm($form);
                    // создадим/обновим документ
                    if (array_key_exists('document', $form)) {
                        $document = Document::findOne($form['document']['id']);
                    } else {
                        $document = new Document();
                    }

                    $document->order_id = $order->id;
                    $document->author = $customer->email;
                    $document->doc_type = Document::TYPE_ACCIDENTS5;
                    $document->cover = (int)$form['calculate']['request']['cover'];
                    $document->amount = $form['calculate']['response']['amount'];
                    $document->date_from = $params['date_from'];
                    $document->date_period = $params['date_period'];
                    $document->plan = $form['calculate']['request']['plan'];
                    $document->programm = self::ACCIDENTS5_PROGRAMM;
                    if ($document->save()) {
                        $form['document']['id'] = $document->id;
                        $this->SetServiceForm($form);
                        // создадим/обновим участников
                        if (array_key_exists('document', $form)
                            && array_key_exists(Participant::TYPE_VENDOR, $form['document'])
                        ) {
                            $participant = Participant::findOne($form['document'][Participant::TYPE_VENDOR]['id']);
                        } else {
                            $participant = new Participant();
                        }
                        $participantForm = $customer->getAttributes();
                        $participantForm['type'] = Participant::TYPE_VENDOR;
                        $participantForm['kind'] = Participant::KIND_PRIVATE;
                        $participantForm['document_id'] = $document->id;
                        $participant->setAttributes($participantForm);
                        if ($participant->save()) {
                            $form['document'][Participant::TYPE_VENDOR] = $participant->getAttributes();
                            $this->SetServiceForm($form);
                            $success = false;
                            $providerDoc = $document->getAccidents5DataForProvider();

                            try {
                                $result = AIWAprovider::getInstance()
                                    ->saveDoc($providerDoc);
                            } catch (ApiException $e) {
                                $code = $e->getCode() == 0
                                    ? 500
                                    : $e->getCode();
                                $message = $e->getMessage();
                            }
                            if (isset($result['code'])) {
                                if ($result['code'] == 0) {
                                    $success = true;
                                } else {
                                    $code = 3;
                                    $message = $result['message'];
                                }
                            }
                            if ($success) {
                                $form['document']['response'] = $result;
                                $this->SetServiceForm($form);
                                //$this->_response['data'] = $result['data'];
                            } else {
                                $this->_response['code'] = $code;
                                $this->_response['message'] = $message;
                            }
                        } else {
                            $this->_response['code'] = 100;
                            $this->_response['message'] = $participant->getErrors();
                        }
                    } else {
                        $this->_response['code'] = 100;
                        $this->_response['message'] = $document->getErrors();
                    }

                } else {
                    $this->_response['code'] = 100;
                    $this->_response['message'] = $order->getErrors();
                }
            } else {
                $this->_response['code'] = 100;
                $this->_response['message'] = $customer->getErrors();
            }
        } else {
            $this->_response['message'] = 'Сессия истекла';
            $this->_response['code'] = -3;
        }

        return $this->_response;
    }
}
