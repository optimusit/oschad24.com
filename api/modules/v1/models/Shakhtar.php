<?php

namespace api\modules\v1\models;

use Yii;
use kartik\mpdf\Pdf;
use common\payments\CommerceGateway\CommerceGateway;
use api\modules\v1\Module;
use api\modules\v1\components\validator\shakhtar\SectorsValidator;
use api\modules\v1\components\validator\shakhtar\EventValidator;
use api\modules\v1\components\validator\shakhtar\SeatsValidator;
use common\components\AppHelper;
use common\models\Order;
use api\modules\v1\components\CoreProducts;
use yii\helpers\ArrayHelper;
use common\providers\shakhtar\Shakhtar as ShakhtarProvider;
use common\models\Shakhtar as ShakhtarModel;

class Shakhtar extends CoreProducts
{
    const SEARCH_MAX_DAYS = 180;
    const SEARCH_TO_DAYS = 30;
    const SERVICE_FEE = 0;
    const MAX_SEATS = 4;

    public $dateFrom;
    public $dateTo;
    public $eventId;
    public $sectorId;
    public $seats;
    public $fanId;
    protected $_provider;

    public function __construct($response, $session, $partner = null)
    {
        parent::__construct($response, $session, $partner);
        $this->_provider = new ShakhtarProvider();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), [
            [
                [
                    'dateFrom',
                    'dateTo'
                ],
                'safe',
                'on' => [
                    'events',
                ]
            ],
            [
                [
                    'dateFrom',
                ],
                'default',
                'value' => date(AppHelper::GetDateFormat()),
                'on' => 'events'
            ],
            [
                [
                    'dateFrom',
                ],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(), strtotime('+' . self::SEARCH_MAX_DAYS . ' days')),
                'on' => 'events'
            ],
            [
                [
                    'dateTo',
                ],
                'default',
                'value' => date(AppHelper::GetDateFormat(), strtotime('+' . self::SEARCH_TO_DAYS . ' days')),
                'on' => 'events'
            ],
            [
                [
                    'dateTo',
                ],
                'date',
                'min' => date(AppHelper::GetDateFormat()),
                'max' => date(AppHelper::GetDateFormat(),
                    strtotime('+' . (string)(intval(self::SEARCH_MAX_DAYS) + 1) . ' days')),
                'on' => 'events'
            ],
            [
                [
                    'eventId',
                ],
                'required',
                'on' => [
                    'sectors',
                    'sector',
                    'checkout',
                ]
            ],
            [
                [
                    'eventId',
                ],
                EventValidator::className(),
                'on' => [
                    'sectors',
                ]
            ],
            [
                [
                    'sectorId',
                ],
                'required',
                'on' => [
                    'sector',
                    'checkout',
                ]
            ],
            [
                [
                    'sectorId',
                ],
                SectorsValidator::className(),
                'on' => [
                    'sector',
                    'checkout',
                ]
            ],
            [
                [
                    'sectorId',
                ],
                SectorsValidator::className(),
                'on' => 'sector'
            ],
            [
                [
                    'seats',
                ],
                'required',
                'on' => [
                    'checkout',
                ]
            ],
            [
                [
                    'seats',
                ],
                'default',
                'value' => [],
                'on' => 'checkout'
            ],
            [
                [
                    'seats',
                ],
                SeatsValidator::className(),
                'min' => 1,
                'max' => self::MAX_SEATS,
                'tooBig' => Module::t('app',
                    'Звертаємо Вашу увагу, що в одному замовленні дозволений вибір не більше {max} місць.',
                    ['max' => self::MAX_SEATS]),
                'on' => 'checkout'
            ],
            [
                [
                    'fanId',
                ],
                'safe',
                'on' => [
                    'checkout',
                ]
            ],
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $result = array_merge(parent::attributeLabels(), [
            'dateFrom' => Module::t('app', 'Дата З'),
            'dateTo' => Module::t('app', 'Дата По'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $result = array_merge(parent::attributeHints(), [
            'dateFrom' => Module::t('app', 'Дата З'),
            'dateTo' => Module::t('app', 'Дата По'),
        ]);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        try {
            $this->seats = array_unique(json_decode($this->seats));
        } catch (\Exception $e) {
        }

        return parent::beforeValidate();
    }

    public function Events()
    {
        $result = [];
        /** @var ShakhtarModel $events */
        $events = ShakhtarModel::find()
            ->select('id,name,date_start,date_end,limit')
            ->andWhere('date_start >= now()')
            ->orWhere('date_end >= now()')
            ->asArray()
            ->all();
        foreach ($events as &$event) {
            if (!TEST_SERVER && mb_strpos($event['name'], 'Тест') !== false) {
                continue;
            }
            $dateStart = AppHelper::CreateDateTimeFromMySQL($event['date_start']);
            $event['date_start'] = Yii::$app->formatter->asDatetime($dateStart);
            $event['date_start_day'] = $dateStart->format('j');
            $event['date_start_month'] = AppHelper::GetMonth($dateStart);
            $event['date_start_year'] = $dateStart->format('Y');
            $event['date_start_time'] = $dateStart->format('H:i');
            $event['date_end'] = Yii::$app->formatter->asDatetime($event['date_end']);
            $result[] = $event;
        }
        $this->_response['data'] = $result;
        $form = $this->GetServiceForm();
        $form['Events'] = $result;
        $this->SetServiceForm($form);

        return $this->_response;
    }

    public function Sectors()
    {
        $result = $this->_provider->SectorsList(['eventId' => $this->eventId]);
        if ($result['success']) {
            $this->_response['data'] = $result['data']['Sector'];
            $form = $this->GetServiceForm();
            $form['Sectors'] = $result['data']['Sector'];
            foreach ($form['Events'] as $event) {
                if ($event['id'] == $this->eventId) {
                    $form['Event'] = $event;
                }
            }
            $this->SetServiceForm($form);
        } else {
            $this->_response['code'] = $result['code'];
            $this->_response['message'] = $result['message'];
        }

        return $this->_response;
    }

    public function Sector()
    {
        $result = [];
        $sector = $this->_provider->GetSectorStructure([
            'eventId' => $this->eventId,
            'sectorId' => $this->sectorId,
        ]);
        if ($sector['success']) {
            $seats = $this->_provider->SeatsAvailability([
                'eventId' => $this->eventId,
                'sectorId' => $this->sectorId,
            ]);
            if ($seats['success']) {
                $freeSeats = [];
                if (array_key_exists('SeatStatus', $seats['data']['Seats'])) {
                    $freeSeats = ArrayHelper::map($seats['data']['Seats']['SeatStatus'], 'SeatId', 'Status');
                }
                foreach ($sector['data']['Seats']['SectorSeat'] as &$sectorSeat) {
                    $sectorSeat['IsFree'] = array_key_exists($sectorSeat['SeatId'], $freeSeats);
                }
                $form = $this->GetServiceForm();
                $sectors = ArrayHelper::index($form['Sectors'], 'Id');
                $sectorInfo = $sectors[$this->sectorId];
                $result = [
                    'Name' => $sectorInfo['Name'],
                    'Zone' => $sectorInfo['Zone'],
                    'Level' => $sectorInfo['Level'],
                    'Price' => $seats['data']['Price'],
                    'Seats' => $sector['data']['Seats']['SectorSeat'],
                ];
                $form['Sector'] = $result;
                $this->SetServiceForm($form);
            }
        }
        $this->_response['data'] = $result;

        return $this->_response;
    }

    public function Checkout()
    {
        $form = $this->GetServiceForm();
        if (!is_null($form)) {
            $reservation = $this->_provider->Reservation([
                'eventId' => $this->eventId,
                'sectorId' => $this->sectorId,
                'seats' => implode(',', $this->seats),
                'fanId' => $this->fanId,
            ]);
            if ($reservation['success']) {
                $reservationData = $reservation['data'];
                if ($reservationData['Result'] == 0) {
                    $reservedSeat = $reservationData['ReservedSeats']['ReservedSeat'];
                    // если это не массив, а 1 место, то поместим его в массив
                    if (array_keys($reservedSeat) !== range(0, count($reservedSeat) - 1)) {
                        $reservationData['ReservedSeats']['ReservedSeat'] = [$reservedSeat];
                    }
                    $orderInfo = [
                        'event' => $form['Event'],
                        'sectorInfo' => [
                            'Name' => $form['Sector']['Name'],
                            'Zone' => $form['Sector']['Zone'],
                            'Level' => $form['Sector']['Level'],
                        ],
                        'reservation' => $reservationData,
                    ];
                    $expirationTime = $this->getReservationEndSpace(new \DateTime($reservationData['ConfirmDate']));
                    /** @var Order $order */
                    $order = $this->saveData($reservationData['ReservationId'], $expirationTime,
                        $reservationData['TotalPrice'], null, count($reservationData['ReservedSeats']['ReservedSeat']),
                        $form, $orderInfo, ShakhtarProvider::PREFIX, $reservation['dialog']);
                    $this->_response['code'] = 0;
                    $this->_response['data'] = [
                        'OrderInfo' => $orderInfo,
                        'PaymentAuthorizationData' => CommerceGateway::createAuthorizationData($order)
                    ];
                    if ($this->isFrontCall()) {
                        Yii::$app->core->reservationCreated($order);
                    }
                } else {
                    switch ($reservationData['Result']) {
                        case 9 :
                            $code = 20;
                            $message = Module::t('app', 'Одне або більше вибраних місць вже продані');
                            break;
                        default :
                            $code = 50;
                            $message = Module::t('app', 'Невідома помилка');
                            Yii::error($reservationData, __METHOD__);
                            break;
                    }
                    $this->_response['code'] = $code;
                    $this->_response['message'] = $message;
                }
            }
        } else {
            $this->_response['message'] = 'Сессия истекла';
            $this->_response['code'] = 20;
        }

        return $this->_response;
    }

    public function ClearReservation($reservationNumber)
    {
        $result = $this->_provider->ClearReservation($reservationNumber);
        $this->_response['data'] = $result;
        $form = $this->GetServiceForm();
        $form['ClearReservation'] = $result;
        $this->SetServiceForm($form);

        return $this->_response;
    }

    /**
     * @param Order $order
     * @return array
     */
    public static function eticket($order)
    {
        $success = false;
        $documents = [];
        if (is_null(($eticket = $order->getEticket()))) {
            $provider = new \common\providers\shakhtar\Shakhtar();
            $eticket = $provider->SellReservation(['reservationid' => $order->provider_order_id]);
            if ($eticket['success'] && $eticket['data']['Result'] == 0) {
                $reservedSeat = $eticket['data']['SellSeats']['SellSeat'];
                // если это не массив, а 1 место, то поместим его в массив
                if (array_keys($reservedSeat) !== range(0, count($reservedSeat) - 1)) {
                    $eticket['data']['SellSeats']['SellSeat'] = [$reservedSeat];
                }
                $order->eticket = $eticket['data'];
                $order->save();
            }
        }
        if (!is_null(($eticket = $order->getEticket()))) {
            $params = unserialize($order->custom);
            $params['extra'] = [
                'oschad_logo' => Yii::getAlias('@common/storage/documents/layouts/images/oschad_logo.png'),
                'vebua_logo' => Yii::getAlias('@common/storage/documents/layouts/images/vebua_logo.png'),
                'scan' => Yii::getAlias('@common/storage/documents/layouts/images/scan.png'),
                'owner' => $order->getCustomerFullName(),
            ];
            $documentLayout = Yii::getAlias('@common/storage/documents/layouts/shakhtar.php');
            $eticketData = ArrayHelper::index($eticket['SellSeats']['SellSeat'], 'SeatId');
            foreach ($params['orderInfo']['reservation']['ReservedSeats']['ReservedSeat'] as $ticket) {
                if (array_key_exists($ticket['SeatId'], $eticketData)) {
                    $params['tickets'][] = [
                        'Event' => $params['orderInfo']['event'],
                        'SectorInfo' => $params['orderInfo']['sectorInfo'],
                        'RowText' => $ticket['RowText'],
                        'SeatText' => $ticket['SeatText'],
                        'Price' => $ticket['Price'],
                        'BarCode' => $eticketData[$ticket['SeatId']]['BarCode'],
                        'Series' => $eticketData[$ticket['SeatId']]['Series'],
                    ];
                }
            }
            $content = Yii::$app->controller->renderFile($documentLayout, $params);
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'marginTop' => 7,
                'marginBottom' => 7,
                'marginLeft' => 7,
                'marginRight' => 7,
                'cssFile' => '@common/storage/documents/layouts/shakhtar.css',
            ]);
            $data = $pdf->output($content, '', Pdf::DEST_STRING);
            $documents[$order->provider_order_id] = $data;
            $success = true;
        }

        $result = [
            'success' => $success,
            'code' => 0,
            'data' => $documents,
        ];

        return $result;
    }
}
