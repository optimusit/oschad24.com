# Список стран для гражданства

## Пример запроса

air/citizenship?lang=ru&accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data":
	{
		"AB": "Абхазия",
		"AU": "Австралия",
		"AT": "Австрия",
		...
		...
		...
	},
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```
