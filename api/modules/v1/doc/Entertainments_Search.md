# Лента событий

## Запрос
**api_url/entertainments/search**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |
| pageSize   | string  |             | Размер страницы (по умолчанию 15)* |
| currentPage    | string  |             | Текущая страница (нумерация с 1, по умолчанию 1)* |
| cityId    | массив  |             | id городов (Entertainments_Cities.md)* |
| activityTypeId    | массив  |             | id типов мероприятий (Entertainments_ActivityTypes.md)* |
| dateFrom    | дд.мм.гггг  |             | дата с * |
| dateTo    | дд.мм.гггг  |             | дата по * |

* (не обязательный)

## Пример запроса
entertainments/search?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl&сityId=[49078786]&activityType=[49078801,49078800]&dateFrom=01.08.2015&dateT=01.12.2015

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": {
		"total": 39,
		"events": [
			{
				"EventId": "396060087",
				"EventName": "Аквадискотека на борту комфортабельного теплохода",
				"LineImage": "http://test.frontmanager.com.ua/img/EventImage/395906101_Image635693794359845567.jpg",
				"LineDateTime": "26 серпня 23:59",
				"LinePriceRange": "120 грн",
				"LinePlace": "Киев. Причал №8",
				"Provider": "Karabas",
				"activityTypes": [
					{
						"id": "49078803",
						"name": "Экскурсии",
						"color": "748B00"
					}
				]
			},
			...
			...
			...
		]
	},
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```
