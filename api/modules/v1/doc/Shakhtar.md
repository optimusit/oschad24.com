# Список доступных для продажи мероприятий

## Запрос
**api_url/shakhtar**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |



## Пример запроса
shakhtar?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": [
		{
			"EventName": "25.08.2015 Шахтер-Рапид",
			"EventId": 28212339,
			"EventStart": "2015-08-25T21:45:00",
			"EventEnd": "2015-08-25T23:15:00",
			"EventPriceId": 27084467,
			"IsActiv": true,
			"Limit": 10,
			"Priorities": 
			{
				"EventPriority": 
			{
				"PriorityName": "None",
				"PriorityType": 1
			}
		}
	],
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```

## Значения полей
| Название | Тип | Назначение |
|-|-|-|-|
| EventName | string | Имя матча |
| EventId | int | Идентификатор матча |
| EventStart | dateTime | Время начала матча |
| EventEnd | dateTime | Время окончания матча |
| EventPriceId | int | Идентификатор ценовой схемы – НЕ ИСПОЛЬЗУЕТСЯ |
| IsActiv | boolean | Доступно/недоступно для продажи |
| Limit | unsignedByte | Сколько можно продать билетов в одни руки |
| Priorities |  | Существую матчи, на которые есть приоритетное время продаж для владельцев абонементов и карточек фан-айди – это матчи Еврокубков.  Если у матча установлен приоритет, то продажу необходимо проводить только для владельцев фан-айди или абонементов, в зависимости от установленного приоритета. Подробнее… |
