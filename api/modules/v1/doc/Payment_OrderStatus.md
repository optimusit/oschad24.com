# Получение данных о состоянии заказа

## Запрос
**api_url/order/paymentstatus**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
|sessionId|string|65|ID сессии|
|orderId|int||№ заказа|

## Пример запроса

order/status?lang=ru&accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&orderId=100000

### Пример ответа
```json
	...
```
