# API veb.ua v1

## Введение
* [Общее положение](Intro_Common.md)
* [Допустимые типы данных](Intro_ValidDataTypes.md)

## ЖД
* [Поиск станции по указанному имени](Rail_SearchStation.md)
* [Запрос списка поездов на указанную дату](Rail_SearchTrain.md)
* [Запрос информации по одному поезду](Rail_TrainInfo.md)
* [Запрос маршрута поезда](Rail_TrainRoute.md)
* [Запрос списка свободных мест по указанному вагону](Rail_CarInfo.md)
* [Оформление заказа](Rail_Checkout.md)
* [Запрос информации по одному бронированию](Rail_BookingShow.md)
* [Запрос получения списка бронирований](Rail_BookingsList.md)
* [Запрос pdf для электронного билета](Rail_ETicket.md)

## Автобус
* [Поиск станции по указанному имени](Bus_SearchStation.md)
* [Запрос списка автобусов на указанную дату](Bus_SearchBus.md)
* [Проверка наличия мест и параметров оплаты](Bus_TripInfo.md)
* [Оформление заказа](Bus_Checkout.md)

## Авиа
* [Поиск аэропорта по указанному имени](Air_SearchStation.md)
* [Запрос списка самолетов на указанную дату](Air_SearchAirCraft.md)
* [Условия тарифа](Air_Conditions.md)
* [Список стран для гражданства](Air_Citizenship.md)
* [Оформление заказа](Air_Checkout.md)

## Мероприятия
* [Типы мероприятий](Entertainments_ActivityTypes.md)
* [Города](Entertainments_Cities.md)
* [Лента событий](Entertainments_Search.md)
* [Информация по событию](Entertainments_Event.md)
* [Информация по заказу](Entertainments_OrderData.md)
* [Удаление пункта (места) из корзины](Entertainments_DeletePlaceFromCart.md)
* [Оформление заказа](Entertainments_Checkout.md)

## ФК Донецк "Шахтер"
* [Введение](Shakhtar_Intro.md)
* [Список доступных для продажи мероприятий](Shakhtar.md)
* [Список секторов с количеством мест](Shakhtar_Sectors.md)
* [Описание сектора - геометрия расположения мест, цена, доступность для продажи](Shakhtar_Sector.md)
* [Оформление заказа](Shakhtar_Checkout.md)

## Оплата
* [Общий принцип](Payment.md)
* [Получение данных о состоянии заказа](Payment_OrderStatus.md)

## АРМ
* [Вход](User_Login.md)
