# Запрос списка поездов на указанную дату

## Запрос
**api_url/rail/searchtrain**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| from | integer | 7 | Код станции отправления |
| to | integer | 7 | Код станции прибытия |
| date | string | (DD-MM-YYYY) | Дата отправления | 
| timeFrom | string | (HH:MM) | Начальное время отправления поезда (необязательный) |
| timeTo | string | (HH:MM) | Конечное время отправления поезда (необязательный) |
| sessionId | string | 65 | ID поисковой сессии |

* Дата отправления не может быть раньше дня текущей даты или позже чем через 44 дня после неё.
* Параметры timeFrom и timeTo служат фильтрами для поисковой выдачи. По умолчанию, их значения «00:00» и «23:59» и в результате запроса возвращаются все поезда с наличием свободных мест на указанную дату.

## Пример запроса
rail/searchtrain?to=2210800&from=2200001&date=20-06-2015&lang=ru&accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": 
	{
		"search": 
		{
			"from": "2200001",
			"to": "2210800",
			"date": "03-06-2015",
			"from_title": "КИЇВ-ПАСАЖИРСЬКИЙ",
			"to_title": "ЗАПОРОЖЬЕ 1"
		},
		"trains": 
		[
			{
				"number": "732К",
				"train_departure_code": "2200001",
				"train_arrival_code": "2210800",
				"train_departure_name": "КИЇВ-ПАСАЖИРСЬКИЙ",
				"train_arrival_name": "ЗАПОРІЖЖЯ 1",
				"passenger_departure_code": "2200001",
				"passenger_arrival_code": "2210800",
				"passenger_departure_name": "КИЇВ-ПАСАЖИРСЬКИЙ",
				"passenger_arrival_name": "ЗАПОРІЖЖЯ 1",
				"travel_time": "418",
				"departure_time": "07:15",
				"departure_date": "03-06-2015",
				"arrival_time": "14:13",
				"arrival_date": "03-06-2015",
				"departure_datetime": "21-06-2015 07:15",
				"departure_dayofweek": "воскресенье",
				"arrival_datetime": "21-06-2015 14:13",
				"arrival_dayofweek": "воскресенье",
				"total_seats": 339
				"class_list": 
				[
					{
						"name": "reserved",
						"seats": "168",
						"lower": "168",
						"upper": "0",
						"side_lower": "0",
						"side_upper": "0",
						"subclass": "1"
					},
					{
						"name": "reserved",
						"seats": "192",
						"lower": "192",
						"upper": "0",
						"side_lower": "0",
						"side_upper": "0",
						"subclass": "2"
					}
				]
			},
			...
			...
			...
		]
	},
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Заначение полей ответа
| Имя объекта | Имя поля                | Значение                                          |
| ----------- | ----------------------- | ------------------------------------------------- |
| -           | sessionId               | ID поисковой сессии                               |
| train       | number                  | Номер поезда                                      |
| train       | train_departure_code    | Код станции отправления поезда                    |
| train       | train_arrival_code      | Код станции прибытия поезда                       |
| train       | train_departure_name    | Название станции отправления поезда               |
| train       | train_arrival_name      | Название станции прибытия поезда                  |
| train       | passenger_departure_code| Код станции отправления пассажира                 |
| train       | passenger_arrival_code  | Код станции прибытия пассажира                    |
| train       | passenger_departure_name| Название станции отправления пассажира            |
| train       | passenger_arrival_name  | Название станции прибытия пассажира               |
| train       | departure_time          | Время отправления пассажира по киевскому времени  |
| train       | departure_date          | Дата отправления пассажира по киевскому времени   |
| train       | travel_time             | Время в пути в минутах                            |
| class       | name                    | Тип вагона                                        |
| class       | seats                   | Количество свободных мест данного класса          |
| class       | lower                   | Количество свободных нижних мест                  |
| class       | upper                   | Количество свободных верхних мест                 |
| class       | side_lower              | Количество свободных нижних боковых мест          |
| class       | side_upper              | Количество свободных верхних боковых мест         |
| class       | subclass                | Подтип вагона                                     |


* Аттрибут "subclass" для сидячих вагонов обозначает класс сидячего вагона (1, 2 или 3). Для остальных типов вагонов значение этого аттрибута можно игнорировать.

### Типы вагонов
| Имя класса    | Значение |
| ------------- | -------- |
| first         | Люкс     |
| second        | Купе     |
| third	        | Плацкарт |
| reserved      | Сидячий  |
| non_reserved	| Общий    |
| comfortable	| Мягкий   |


