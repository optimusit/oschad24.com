# Поиск станции по указанному имени

## Запрос
**api_url/bus/searchstation**

## Заначение параметров URL

| Название  | Тип    | Длина/Формат | Назначение                                              |
|-----------|:------:|:------------:|---------------------------------------------------------|
| searchStationName      | string | 3-30         | Начало названия станции                                 |
| sessionId | string | 65           | ID поисковой сессии (необязательный)                    |

* Значение параметра sessionId следует брать из предыдущего запроса. При отсутствии параметра sessionId, будет создана новая поисковая сессия.

## Пример запроса
/bus/searchstation?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&dest=from&searchStationName=зап

## Пример ответа

```json
{
    "code": 0,
    "message": "",
    "data": 
	[
		{
			"id": "IS333,BAUA2310100000",
			"value": "Запоріжжя, Украина"
		},
		{
			"id": "IS3843",
			"value": "Zapolye (псковская обл.), Россия"
		}
		...
		...
		...
	],
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Заначение полей ответа

| Имя объекта | Имя поля | Значение
|-|-|-
| station | id | Код станции
| station | value | Имя станции
