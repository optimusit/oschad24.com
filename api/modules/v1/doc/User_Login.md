# Вход

## Запрос
**back/user/login**

## Заначение параметров URL

|Название|Тип|Длина/Формат|Назначение
|-|-|-|-
|partner|string||Код партнера
|email|string||Email
|password|string||Пароль

## Пример запроса
back/user/login?partner=oschad24&email=operator01@oschad.ua&password=operator

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data":
	trainInfo": {
		"accessToken": "29iw3_JJhIu0RnGwRjijLkN-ec1xsMWM",
		"role": "cashier"
    },
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```
