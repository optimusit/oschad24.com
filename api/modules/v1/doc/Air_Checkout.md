# Оформление заказа

## Запрос
**api_url/air/checkout**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
|sessionId|string|65|ID поисковой сессии|
| key      | string | 3-32         | ключ рекомендации в списке рекомендаций |
|passengers|array||Пассажиры (Максимум 6)|
|phone|string|13|Номер телефона|
|email |string|50|Мыло|
|fullName |string|50|ФИО|

Параметр passengers - кодированый массив объектов (пример - [PHP](http://php.net/manual/en/function.json-encode.php), [JS](https://github.com/douglascrockford/JSON-js))

Список обязательных полей элемента массива
* type - Тип пассажира (ADT — взрослый, CHD — ребенок, INF — младенец)
* name - Имя 
* surname - Фамилия
* birthday - Дата рождения (DD-MM-YYYY)
* gender - Пол (M — мужской, F — женский)
* citizenship - Гражданство (2-х буквенный код страны, см. [справочник](Air_Citizenship.md))
* docnum - Номер паспорта
* doc_expire_date - Срок действия паспорта (не обязательно для внутреннего паспорта) (DD-MM-YYYY)

## Пример запроса при невалидной ифнормации по пассажирам

air/checkout?lang=ru&accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&key=sd8z0v98xc9v8xc9v8x&passengers=[{"name":"Ivan","surname":"Ivanov","gender":"M","citizenship":"UA","docnum":"ASD-123","doc_expire_date":"01-01-2020","birthday":""},{"name":"Ivan","surname":"Ivanov","gender":"","citizenship":"UA","docnum":"ASD-123","doc_expire_date":"01-01-2020"}]&phone=+380957925818&email=andrej.lola@gmail.com&fullName=Иванов Иван

### Пример ответа

* если code = -3, то внутри будет errors - массив ошибок валидации пассажиров
```json
data": {
	"errors": 
	[
		[
		"type",
		"birthday"
		],
		[
			"type",
			"gender",
			"birthday"
		]
	]
}
```

## Пример запроса при валидной ифнормации по пассажирам

air/checkout?lang=ru&accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&key=sd8z0v98xc9v8xc9v8x&passengers=[{"type":"ADT","name":"Ivan","surname":"Ivanov","gender":"M","citizenship":"UA","docnum":"ASD-123","doc_expire_date":"01-01-2020","birthday":"01-01-1970"},{"type":"ADT","name":"Semen","surname":"Gorbunkoff","gender":"M","citizenship":"UA","docnum":"zxc-987","doc_expire_date":"01-01-2020","birthday":"10-10-1950"}]&phone=+380957925818&email=andrej.lola@gmail.com&fullName=Иванов Иван

### Пример ответа

если code = 0, то внутри будет reservationInfo - информация по бронированию

```json
"amount": 5955,
"duration": "5 год. 35 хв.",
"parts": 
[
	{
		"departure_date": "27.07.2015",
		"departure_time": "16:30",
		"departure_city": "Київ, Україна",
		"departure_airport": "KBP",
		"arrival_date": "27.07.2015",
		"arrival_time": "18:15",
		"arrival_city": "Москва, Росія",
		"arrival_airport": "DME"
	},
	{
		"departure_date": "30.09.2015",
		"departure_time": "16:15",
		"departure_city": "Москва, Росія",
		"departure_airport": "DME",
		"arrival_date": "30.09.2015",
		"arrival_time": "18:05",
		"arrival_city": "Лондон, Великобританія",
		"arrival_airport": "LHR"
	}
],
"passengers": 
[
	{
		"name": "Ivan",
		"surname": "Ivanov",
		"gender": "M",
		"citizenship": "UA",
		"docnum": "ASD-123",
		"doc_expire_date": "01-01-2020",
		"birthday": "01-01-1970"
	},
	{
		"name": "Semen",
		"surname": "Gorbunkoff",
		"gender": "M",
		"citizenship": "UA",
		"docnum": "zxc-987",
		"doc_expire_date": "01-01-2020",
		"birthday": "10-10-1950"
	}
]
```

## Заначение полей ответа
| Имя объекта | Имя поля                | Значение                                          |
| ----------- | ----------------------- | ------------------------------------------------- |
| -           | sessionId               | ID поисковой сессии                               |
