# Запрос маршрута поезда

## Запрос
**api_url/rail/trainroute**

## Заначение параметров URL

|Название|Тип|Длина/Формат|Назначение
|-|-|-|-
| from | integer | 7 | Код станции отправления |
| to | integer | 7 | Код станции прибытия |
| date | string | (DD-MM-YYYY) | Дата отправления |
|trainNumber|string|4|Номер поезда

* Значение параметра session_id следует брать из предыдущего запроса.

## Пример запроса
rail/trainroute?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&from=2200001&to=2210793&date=19.10.2015&trainNumber=084%D0%94

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	data": {
		"number": "084Д",
		"name": "",
		"first_station": "МАРІУПОЛЬ",
		"last_station": "КИЇВ-ПАСАЖИРСЬКИЙ",
		"stations": [
		{
			"name": "МАРІУПОЛЬ",
			"time_departure": "17:50",
			"time_arrival": "",
			"code": "2214400",
			"longitude": "37.5554",
			"latitude": "47.0861"
		},
		{
			"name": "САРТАНА",
			"time_departure": "18:40",
			"time_arrival": "18:20",
			"code": "2214275",
			"longitude": "37.5753",
			"latitude": "47.1518"
		},
		{
			"name": "ВОЛНОВАХА",
			"time_departure": "20:00",
			"time_arrival": "19:40",
			"code": "2214230",
			"longitude": "37.4887",
			"latitude": "47.6021"
		},
		...
		...
		...
		]
	},
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```
