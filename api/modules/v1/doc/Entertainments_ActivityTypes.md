# Типы мероприятий

## Запрос
**api_url/entertainments/activitytypes**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |

## Пример запроса
entertainments/activitytypes?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": [
		{
			"id": 49078800,
			"name": "Концерты",
			"color": "FF7A05"
		},
		{
			"id": 49078801,
			"name": "Театры",
			"color": "EB0074"
		},
		{
			"id": 49078802,
			"name": "Детям",
			"color": "0DAF0B"
		},
		...
		...
		...
	], 
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```
