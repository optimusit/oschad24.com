# Список секторов с количеством мест

## Запрос
**api_url/shakhtar/sectors**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |
| eventId    | string  |             | ID события |


## Пример запроса
shakhtar/sectors?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl?eventId=28212339

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": [
		{
			"Id": 25776097,
			"Name": "Не использовать 1А   GATE 1",
			"SeatsCount": 110,
			"FreeSeatsCount": 110,
			"IsFree": true,
			"Level": "1",
			"Zone": "Запад"
		},
		...
		...
		...
	],
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```


## Значения полей
| Название | Тип | Назначение |
|-|-|-|-|
| Id | int | Идентификатор сектора |
| Name | string | Имя сектора |
| SeatsCount | int | Всего мест в секторе |
| FreeSeatsCount | int | Свободных мест в секторе |
| IsFree | boolean | Доступность для продажи |
| Level | string | Наименования уровня (яруса) |
| Zone | string | Имя зоны стадиона – стадион делится на четыре зоны A, B, C, D |
