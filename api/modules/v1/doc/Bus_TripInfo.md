# Проверка наличия мест и параметров оплаты

## Запрос
**api_url/bus/tripinfo**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| departureCode | string |  | Код станции отправления |
| arrivalCode | string |  | Код станции прибытия |
| busNumber | string |  | Номер автобуса |
| deparureDate | string | дд.мм.гггг | Дата отправления |

* Дата отправления не может быть раньше дня текущей даты.

## Пример запроса
bus/searchbus?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&from=BAUA1810100000&to=BAUA8000000000&deparureDate=17.06.2015

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": 
	{
		"arrival_name": "КИЇВ 1",
		"departure_name": "ЖИТОМИР",
		"number": "1657",
		"arrival_code": "500100",
		"departure_code": "680100",
		"buy": "",
		"carrier": "2942 ПП Авто-Експерт вул.Калнишевського_34,_м.Хмельницький,_29000 067-350-77-20_,код_ЄДРПОУ_:_36122984",
		"insurance": "",
		"amount": "94.68",
		"ticket_in_bus": "0",
		"need_birth": "0",
		"need_doc": "0",
		"places": 
		[
		"35",
		"36",
		"37",
		"38",
		"39"
		]
	},
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Заначение полей ответа
| Имя объекта | Имя поля                | Значение                                          |
| ----------- | ----------------------- | ------------------------------------------------- |
| bus           | amount                     | Стоимость бронирования                 |
