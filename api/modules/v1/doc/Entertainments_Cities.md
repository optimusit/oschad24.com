# Города

## Запрос
**api_url/entertainments/cities**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |
| cityName    | string  |             | Название города (минимум 2 сивола) |

## Пример запроса
entertainments/cities?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl&cityName=зап

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": [
		{
			"id": "49270118",
			"name": "Запорожье"
		},
		...
		...
		...
	], 
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```
