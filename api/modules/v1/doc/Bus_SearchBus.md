# Запрос списка автобусов на указанную дату

## Запрос
**api_url/bus/searchbus**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| from | string |  | Код станции отправления |
| to | string |  | Код станции прибытия |
| date | string | (DD.MM.YYYY) | Дата отправления | 
| daysForward | int | 0 или 1 | Искать варианты +3 дня |
| sessionId | string | 65 | ID поисковой сессии |

* Дата отправления не может быть раньше дня текущей даты.

## Пример запроса
bus/searchbus?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&from=IS7,BAUA4610100000&to=IS3,BACZ0100000000&date=05.09.2015

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": 
	{
		"stationsHistory":
		{
			"IS7,BAUA4610100000": "Львів, Украина",
			"IS3,BACZ0100000000": "Прага, CZ",
			"IS5273": "Прага аеропорт, CZ"
		},
		"search": 
		{
			"from": "IS7,BAUA4610100000",
			"to": "IS3,BACZ0100000000",
			"days_forward": 0,
			"date_from": "05-09-2015",
			"date_to": "05-09-2015",
			"departure_name": "Львів, Украина",
			"arrival_name": "Прага, CZ"
		},
		"buses": [
		{
			"provider": "infobus",
			"server_code": "",
			"name": "436 К.Подольский- Краков - Прага",
			"number": "1290297",
			"route_number": "303",
			"free_places": "15",
			"place_type": "сидячее с указанием места",
			"bus_type": "Mercedes (51 seats)",
			"distance": "",
			"ticket_in_bus": "0",
			"need_birth": "0",
			"need_doc": "0",
			"station": 
			{
				"departure_address": "Автовокзал, пл.4, ул. Стрийска 109",
				"departure_code": "7",
				"arrival_code": "3",
				"bus_images": "https://www.infobus.eu/images/linky/436/IMG_5615.JPG",
				"departure_from_point_date": "05.09.2015",
				"departure_from_point_time": "14:25",
				"departure_from_point_date_time": "05.09.2015 14:25",
				"arrival_to_point_date": "06.09.2015",
				"arrival_to_point_time": "07:40",
				"arrival_to_point_date_time": "06.09.2015 07:40"
			}
		},
	},
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Заначение полей ответа
| Имя объекта | Имя поля                | Значение                                          |
| ----------- | ----------------------- | ------------------------------------------------- |
| bus           | name                       | Название рейса                         |
| bus           | server_code                       | Код сервера                         |
| bus           | number                     | Номер автобуса                         |
| bus           | free_places                | Кол-во свободный мест                  |
| bus           | bus_type                   | Марка автобуса                         |
| bus           | distance                   | Длительность маршрута в км             |
| bus           | free_place_numbers         | Номера свободных мест                  |
| bus           | provider                   | Поставщик услуг                        |
| bus           | ticket_in_bus              | Обмен ваучера производится в автобусе  |
| bus           | need_birth                 | Необходима дата рождения пассажира     |
| bus           | need_doc                   | Необходима информация о документах пассажира  |
| station        | departure_code             | Код станции отправления                |
| station        | arrival_code               | Код станции прибытия                   |
| station        | departure_address          | Адрес отправления автобуса             |
| station        | departure_from_point       | Время отправления в начальном пункте   |
| station        | arrival_to_point           | Время прибытия в конечный пункт        |
