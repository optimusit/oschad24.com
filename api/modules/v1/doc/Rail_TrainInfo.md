# Запрос информации по одному поезду

## Запрос
**api_url/rail/traininfo**

## Заначение параметров URL

|Название|Тип|Длина/Формат|Назначение
|-|-|-|-
|trainNumber|string|4|Номер поезда
|sessionId|string|65|ID поисковой сессии

* Значение параметра session_id следует брать из предыдущего запроса.

## Пример запроса
rail/traininfo?trainNumber=732%D0%9A&class=reserved&subclass=1&lang=ru&accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data":
	trainInfo": {
		"number": "072П",
		"train_departure_code": "2210800",
		"train_arrival_code": "2200001",
		"train_departure_name": "ЗАПОРОЖЬЕ 1",
		"train_arrival_name": "КИЕВ-ПАССАЖИРСКИЙ",
		"passenger_departure_code": "2210800",
		"passenger_arrival_code": "2200001",
		"passenger_departure_name": "ЗАПОРОЖЬЕ 1",
		"passenger_arrival_name": "КИЕВ-ПАССАЖИРСКИЙ",
		"travel_time": "658",
		"departure_time": "19:00",
		"departure_date": "31-07-2015",
		"arrival_time": "05:58",
		"arrival_date": "01-08-2015",
		"arrival_timezone": "0",
		"departure_timezone": "0",
		"train_class": "1",
		"train_speed": "2",
		"internal": true,
		"departure_day": "Пятница",
		"departure_shortday": "Пт",
		"departure_month": "Июля",
		"arrival_day": "Суббота",
		"arrival_shortday": "Сб",
		"arrival_month": "Августа",
		"travel_time_hours": 10,
		"travel_time_minutes": 58,
		"train_class_label": "Фірмовий",
        "train_speed_label": "Швидкісний",
		"class": 
		{
			"name": "second",
			"subclass": "0",
			"nutrition": "0",
			"not_firm_car": "0",
			"seats": "43",
			"lower": "19",
			"upper": "24",
			"side_lower": "0",
			"side_upper": "0",
			"cost": 321.38,
			"services": "2,3",
			"cars": 
			[
				{
					"id": "1",
					"number": "10",
					"seats": "34",
					"lower": "16",
					"upper": "18",
					"side_lower": "0",
					"side_upper": "0",
					"operation_types": 
					["3","2"]
				},
				...
				...
				...
			]
		}
	},
	carInfo:
    {
    	"number": "1",
    	"class": "reserved",
    	"subclass": "2",
    	"nutrition": "0",
    	"not_firm_car": "0",
    	"train_model": "1",
    	"seats_count": "56",
    	"free_seats": 10,
    	"seats": 
    	[
    		{
    			"number": "1",
    			"type": "12",
    			"coupe": "1"
    		},
    		{
    			"number": "2",
    			"type": "12",
    			"coupe": "1"
    		},
    		{
    			"number": "3",
    			"type": "12",
    			"coupe": "1"
    		},
    		...
    		...
    		...
    	]
    },
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Заначение тегов и атрибутов XML

| Имя объекта | Имя поля | Значение
|-|-|-
|            | sessionId                 | ID поисковой сессии                                      |
| train      | number                    | Номер поезда                                             |
| train      | train_departure_code      | Код станции отправления поезда                           |
| train      | train_arrival_code        | Код станции прибытия поезда                              |
| train      | train_departure_name      | Название станции отправления поезда                      |
| train      | train_arrival_name        | Название станции прибытия поезда                         |
| train      | passenger_departure_code  | Код станции отправления пассажира                        |
| train      | passenger_arrival_code    | Код станции отправления пассажира                        |
| train      | passenger_departure_name  | Название станции отправления пассажира                   |
| train      | passenger_arrival_name    | Название станции отправления пассажира                   |
| train      | travel_time               | Время в пути в минутах                                   |
| train      | departure_time            | Время отправления пассажира по киевскому времени         |
| train      | departure_date            | Дата отправления пассажира                               |
| train      | arrival_timezone          | Часовая зона отправления пассажира                       |
| train      | departure_timezone        | Часовая зона прибытия пассажира                          |
| train      | train_class               | Класс поезда                                             |
| train      | train_speed               | Скорость поезда                                          |
| class      | name                      | Тип вагона                                               |
| class      | cost                      | Стоимость билета в гривнах                               |
| class      | seats                     | Количество свободных мест данного класса                 |
| class      | lower                     | Количество свободных нижних мест данного класса          |
| class      | upper                     | Количество свободных верхних мест данного класса         |
| class      | side_lower                | Количество свободных нижних боковых мест данного класса  |
| class      | side_upper                | Количество свободных верхних боковых мест данного класса |
| class      | nutrition                 | Наличие питания в данном классе вагонов                  |
| class      | not_firm_car              | Признак нефирменного вагона                              |
| class      | subclass                  | Подтип вагона                                            |
| car        | id                        | ID вагона                                                |
| car        | number                    | Номер вагона                                             |
| car        | seats                     | Количество свободных мест в вагоне                       |
| car        | lower                     | Количество свободных нижних мест в вагоне                |
| car        | upper                     | Количество свободных верхних мест в вагоне               |
| car        | side_lower                | Количество свободных нижних боковых мест в вагоне        |
| car        | side_upper                | Количество свободных верхних боковых мест в вагоне       |
| car        | operation_types           | Список доступных операций для вагона                     |
| exchange_rate        | currency        | Валюта                                                   |
| exchange_rate        | cost            | Стоимость                                                |

* Поля "arrival_timezone" и "departure_timezone" могут принимать следующие значения:
	* 0 - время Киевское;
	* 1 - время Московское;
	* 2 - время местное;
* Поле "train_class" может принимать следующие значения:
	* 1 - нефирменный;
	* 4 - фирменный;
	* 8 - экспресс;
* Поле "train_speed" может принимать следующие значения:
	* 1 - пассажирский;
	* 2 - быстрий;
	* 4 - скоросной;
* Поле "exchanges" содержит стоимости билетов в разных валютах по курсу НБУ на момент совершения запроса.
* Поле "subclass" для сидячих вагонов обозначает класс сидячего вагона (1, 2 или 3). Для остальных типов вагонов значение этого аттрибута можно игнорировать.
* Поле "operation_types" означает тип операции заказа. Список всех допустимых операций:
	* 2 - покупка билетов с последующим обменом на бумажные в билетной кассе;
	* 3 - покупка электронных билетов;

## Типы вагонов

| Имя класса    | Значение |
|-|-
| first         | Люкс     |
| second	| Купе     |
| third	        | Плацкарт |
| reserved	| Сидячий  |
| non_reserved	| Общий    |
| comfortable	| Мягкий   |
