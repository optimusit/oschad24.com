# Оформление заказа

## Запрос
**api_url/shakhtar/checkout**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |
| eventId    | string  |             | ID события |
| sectorId    | string  |             | ID сектора |
| seats    | string  |             | список мест (разделитель - запятая) |
|phone|string|13|Номер телефона|
|email |string|50|Мыло|
|fullName |string|50|ФИО|

## Пример запроса
shakhtar/checkout?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&dest=from&sessionId=CrjjsBvnIlttC2t54CJl2&eventId=27260208&sectorId=25796579&seats=25796637,25797414&phone=%2b380957925818&email=andrej.lola@gmail.com&fullName=Иванов Иван

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": {
		"Result": 0,
		"ReservationNumber": "0058157",
		"ReservationId": 28298577,
		"ConfirmDate": "2015-08-12T12:25:45.36+03:00",
		"TotalPrice": 40,
		"ReservedSeats": {
			"ReservedSeat": [
				{
					"SeatId": 25796637,
					"RowText": "12",
					"SeatText": "3",
					"Price": 20
				},
				{
					"SeatId": 25797414,
					"RowText": "5",
					"SeatText": "37",
					"Price": 20
				}
			]
		}
	},
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```


## Значения полей
| Название | Тип | Назначение |
|-|-|-|-|
|  |  |  |
