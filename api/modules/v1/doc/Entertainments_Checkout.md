# Оформление заказа

## Запрос
**api_url/entertainments/checkout**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |
| id    | string  |             | ID заказа |
| email    | string  |             | email |
| fullName    | string  |             | ФИО |
| phone    | string  |             | ФИО |
 

## Пример запроса
entertainments/checkout?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl&id=2308320798&email=gorbunkoff@gmail.com&fullName=Горбункофф Семен Семеныч&phone=+380505553377

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	data": {
		"OrderInfo": {
			"id": 100017,
			"cost": 3,
			"expiration_date": "04.08.2015",
			"expiration_time": "13:28:17",
			"details": {
				"amount": "250.00",
				"currency": "64658764",
				"currencyName": "грн",
				"currencyCode": "UAH",
				"orderId": "345951685",
				"status": "0",
				"orderItemsCount": "1",
				"orderItemsTotal": "250.00",
				"cashServiceTotal": "0.00",
				"orderClearPrice": "250.00",
				"orderTotal": "250.00",
				"deliveryType": "129948494",
				"paymentType": "129948495",
				"deliveryAddress": "",
				"deliveryPrice": "0.00",
				"image": "http://test.frontmanager.com.ua/img/EventImage/317812893_Image635718840326946818.jpg",
				"eventType": 
				{
					"Концерты": "FF7A05",
					"Клубы": "8A00B6"
				},
				"items": 
					[
						{
							"id": "345951686",
							"pid": "245340871",
							"zone": "Балкон",
							"row": "1",
							"number": "44",
							"barCode": "341941676366",
							"event": "317812893",
							"eventName": "METALLICA Cover Show",
							"eventDate": "15.10.2015 19:00",
							"isFan": "0",
							"price": "250.00",
							"totalAmount": "250.00",
							"bonusAmount": "0.00",
							"bonusValuedDescription": "",
							"bonusDiscountAmount": "0.00",
							"bonusDescription": "",
							"cashServiceAmount": "0.00",
							"cashServiceDescription": "",
							"HasProducts": "0",
							"currency": "64658764",
							"currencyName": "грн",
							"salesLine": "Line5",
							"eventPacket": "0",
							"IsETicketPassportRequired": "0",
							"IsThermoTicketPassportRequired": "0",
							"ClientInfoRequired": "0"
						}
					]
				},
			},
		},
		"PaymentAuthorizationData": 
		[
		{
			"Form": {
				"AMOUNT": "3.00",
				"CURRENCY": "UAH",
				"ORDER": 100017,
				"DESC": "",
				"MERCH_NAME": "Oschad24",
				"MERCH_URL": "http://admin.veb.loc",
				"MERCHANT": "20901833",
				"TERMINAL": "20903058",
				"EMAIL": "dev@veb.ua",
				"TRTYPE": 0,
				"COUNTRY": "UA",
				"MERCH_GMT": "+3",
				"TIMESTAMP": "20150804095817",
				"NONCE": "4B535051754357387268656674733747",
				"BACKREF": "http://admin.veb.loc/#/payment?orderId=100017&timestamp=20150804095817",
				"P_SIGN": "4d20840d269abb922de5636cd0a6513aafc1cd6d"
			},
			"Action": "https://3ds.oschadnybank.com/cgi-bin/cgi_link"
		}
		]
	}, 
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```
