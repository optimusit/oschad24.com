# Оформление заказа

## Запрос
**api_url/rail/checkout**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
|sessionId|string|65|ID поисковой сессии|
|operationType|int|1|Тип операции (null - Автоматически с приоритетом электронного билета)|
|noClothes|int|1|Отказ от постельного белья|
|services|int|1|Дополнительные сервисы|
|getCommissionOnly|int|1|Если true метод выполняют только проверку данных и вернет предварительную комиссию, сумму|
|passengers|array||Пассажиры (Максимум 4)|
|phone|string|13|Номер телефона (в формате +38)|
|email |string|50|Мыло|
|name |string|50|Имя покупателя|
|surname |string|50|Фамилия покупателя|

* Параметр «operationType» может принимать следующие значения (доступные значения для конкретного вагона указаны в результате запроса train):
	* 2 - покупка билетов с последующим обменом на бумажные в билетной кассе;
	* 3 - покупка электронных билетов;


* Дополнительные сервисы может принимать одно или несколько следующих значений (или вобще ниодного).
	* 1 - Харчовий набір;
	* 2 - Чай;
	* 3 - 2 Чая;

* Параметр passengers - кодированый массив объектов (пример - [PHP](http://php.net/manual/en/function.json-encode.php), [JS](https://github.com/douglascrockford/JSON-js))

Список обязательных полей элемента массива
** name - Имя
** surname - Фамилия
** carNumber - № вагона
** seat - № места

## Для возможных вариантов льгот обязательные поля:
### детский
* privileges - 1
* date - Дата рождения (DD.MM.YYYY)
### студенческий
* privileges - 2
* document - номер студенческого билета (две русские буквы и 8 цифр, например СМ08352502)

## Для оформления багажных квитанций дополнительно для каждого из пассажиров нужно передать 0 (не оформлять) или 1 (оформлять). Есть возможность выбрать все три типа багажа.
* animal - Животные/птицы
* facilities - Аппаратура
* excess_baggage - Избыток

## Пример запроса при валидной ифнормации по пассажирам
rail/checkout?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=47bkHath51fmkwCiDmhh&passengers=[{"carNumber":"7","seat":"2","name":"Ігор","surname":"Коваленко","privileges":"2","document":"СМ08352502"}]&operationType=2&phone=%2b380957925818&email=andrej.lola@gmail.com&name=ячсячсяч&surname=Иванов&services=[ >> apapi.veb.loc/v1/front/rail/checkout?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=47bkHath51fmkwCiDmhh&passengers=[{"carNumber":"7","seat":"2","name":"Ігор","surname":"Коваленко","privileges":"2","document":"СМ08352502"}]&operationType=2&phone=%2b380957925818&email=andrej.lola@gmail.com&name=ячсячсяч&surname=Иванов&services=[2,3]

## Пример ответа
```json
{
"code": 0,
"message": "",
"data": {
	"OrderInfo": {
		"train": "732К",
		"voyage": "КИЕВ-ПАССАЖИРСКИЙ - ЗАПОРОЖЬЕ 1",
		"departure": {
			"time": "20-07-2015 07:15",
			"name": "КИЕВ-ПАССАЖИРСКИЙ"
		},
		"arrival": {
			"time": "20-07-2015 14:13",
			"name": "ЗАПОРОЖЬЕ 1"
		},
		"travelTime": "6 год. 58 хв.",
		"approximate_total_amount": 511.54,
		"passengers": [
			{
				"car": "5",
				"seat": "54",
				"class": "Сидячий",
				"passenger": "иван asd",
				"type": "Дорослий",
				"train_class_label": "Фірмовий",
				"train_speed_label": "Швидкісний"
			}
		],
		"order_id": 100007,
		"cost": 471.54,
		"expiration_date": "30.06.2015",
		"expiration_time": "16:52:52",
		"operation_type": "Ваучер"
	},
	"PaymentAuthorizationData": [
		{
			"Form": {
				"AMOUNT": "471.54",
				"CURRENCY": "UAH",
				"ORDER": 100007,
				"DESC": "Zh/d bil. x1 20-07-2015 v ZAPOROZhE 1",
				"MERCH_NAME": "Oschad24",
				"MERCH_URL": "http://admin.veb.loc",
				"MERCHANT": "20901833",
				"TERMINAL": "20903058",
				"EMAIL": "dev@veb.ua",
				"TRTYPE": 0,
				"COUNTRY": "UA",
				"MERCH_GMT": "+3",
				"TIMESTAMP": "20150630132453",
				"NONCE": "70366B3855466C3541564B744C6E6D79",
				"BACKREF": "http://admin.veb.loc/site/testpaymentinfo?id=100007&phone=1234567890&back=1",
				"P_SIGN": "b194dbcdf61e1a19150fdffde1373fcf2e1210a1"
			},
			"Action": "https://3ds.oschadnybank.com/cgi-bin/cgi_link"
		}
	]
},
"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Заначение полей ответа

|Имя объекта| Имя поля | Значение                                          |
|-|-|-|
| -           | sessionId               | ID поисковой сессии                               |
|ReservationInfo|id|ID заказа в системе|
|ReservationInfo|operation_type|Тип операции|
|ReservationInfo|cost|Общая стоимость заказа|
|ReservationInfo|seats|Список сидячих мест|
|ReservationInfo|expiration_time|Время истечения возможности оплаты|
|PaymentAuthorizationData||Поля формы запроса к платежной системе|
|Action||URL запроса к платежной системе|

Из полученого ответа строется форма для отправки запроса на ввод платежной карты, в которой ключ свойства объекта 
**PaymentAuthorizationData** - это значение атрибута **"name"**, а значение свойства объекта 
**PaymentAuthorizationData** - это значение атрибута **"value"** HTML-тэга **"input"**.

Значение атрибута **"action"** формы должно быть взято из ответа **data.Action**.

Метод передачи формы - **"POST"**.
