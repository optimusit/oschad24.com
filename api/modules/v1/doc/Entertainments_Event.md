# Информация по событию

## Запрос
**api_url/entertainments/event**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |
| id        | string     |            | ID события           |

## Пример запроса
entertainments/event?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl&id=317811087

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": {
		"ProviderAuth": "10c1fde8a0dc43ce8516d8f06e5343cauyVAt83FlUNEV9F8PLtULXQuFf0=",
		"ProviderUrl": "http://test.frontmanager.com.ua/",
		"EventName": "METALLICA Cover Show",
		"HallName": "Sentrum",
		"DetailsDescription": "<p align=\"center\"><strong>METALLICA COVER SHOW</strong></p>\n<p><strong>Official \"Metallica\" tribute - </strong><strong>группа</strong><strong> Scream Inc.!</strong></p>\n<p>Одна из лучших трибьют-групп легендарной группы <strong>\"Metallica\",</strong> в Восточной Европе! <br /> В программе концерта - все лучшие песни легендарной группы <strong>\"Metallica\"</strong> и не только.</p>\n<p>Группа была основана в 2008 году и до весны 2013 называлась Y-axis. Именно это название за четыре года нашумело как лучшая Metallica-трибьют команда Украины и возможно стран бывшего СССР. За период существования группы было сыграно многочисленное количество концертов на территории стран СНГ. Достаточно частые изменения в составе коллектива заставляли останавливаться для поиска новых людей и переоценки ценностей и планов на будущее, что привело к переименованию группы и ориентирования творчества на свой ​​материал, над чем Scream Inc. сейчас упорно работает. Но ребята также не забывают о тех, кто вдохновил их на занятия музыкой &ndash; Великую группу Metallica.</p>\n<p>Каждый концерт<strong>&laquo;</strong><strong>Scream</strong><strong>Inc</strong><strong>.<strong>&raquo;</strong></strong> пронизан сногсшибательной энергетикой и ни с чем не сравнимым драйвом. Музыканты как будто стремятся превзойти самих себя, с каждым разом поднимая планку профессионализма все выше и выше. <br /> &nbsp;Шоу состоится в новом&nbsp;&nbsp; концертном клубе <strong>&laquo;S</strong><strong>ENTRUM</strong><strong>&raquo;,</strong> который находится в центре Киева, по адресу ул. Шота Руставели, 16-а. (метро &laquo;Дворец Спорта&raquo;), первая в Украине концертная площадка европейского формата.</p>\n<p><strong>Начало: 15 ноября в 19:00 </strong></p>\n<p><strong>ФАН-ЗОНА 90 грн. ПЕРВЫЕ 100 БИЛЕТОВ!</strong></p>\n<p><strong>VIP 180 &ndash; 250 грн. </strong></p>",
		"DetailsImage": "http://test.frontmanager.com.ua/img/EventImage/317811087_ImageLarge635718840865073151.jpg",
		"DetailsDate": "15 жовтня, четвер",
		"DetailsTime": "19:00",
		"DetailsPlace": "Киев. Sentrum",
		"DetailsAddress": "ул. Шота Руставели, 16а",
		"DetailsType": {
			"Концерты": "FF7A05",
			"Клубы": "8A00B6"
		}
	}, 
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```
