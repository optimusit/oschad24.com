# Запрос списка самолетов на указанную дату

## Запрос
**api_url/air/searchaircraft**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| destinations | array   |            | маршрут (массив пунктов перелета)                 |
| adt          | int     | 1             | количество взрослых                                  |
| chd          | int     | 1             | количество детей (не обязательно)                    |
| inf          | int     | 1             | количество младенцев (не обязательно)                |
| class| string  | А / Е / В     | класс бронирования (А-все, Е-эконом, В-бизнес )      |

destinations (массив пунктов перелета):
departure - IATA-код города вылета
arrival - IATA-код города прилета
date - Дата отправления (DD-MM-YYYY)

* Дата отправления не может быть раньше дня текущей даты или позже, чем через 360 дней после неё.
* Количество младенцев не может превышать количество взрослых пассажиров.
* Ребенок — пассажир, возраст которого на момент перелета — не старше 12 лет.
* Младенец — пассажир, возраст которого на момент перелета — не старше 2 лет.
* Максимальное количество рекомендаций в одном запросе — 99.

## Пример запроса
## Для простого перелета (только туда)
air/searchaircraft?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&destinations=[{"departure":"IEV","arrival":"MOW","date":"27-07-2015"}]

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": {
		"a04c9a602": {
			"id": "a04c9a-6:0^^2",
			"amount": 10433,
			"duration": 1285,
			"parts": [
				{
					"flight_number": "405",
					"aircraft": "De Havilland DHC-8 Dash 8-400",
					"departure_terminal": "",
					"departure_airport": "KBP",
					"departure_city": "IEV",
					"departure_date": "27.12.2015",
					"departure_time": "20:35",
					"arrival_terminal": "E",
					"arrival_airport": "SVO",
					"arrival_city": "MOW",
					"arrival_date": "28.12.2015",
					"arrival_time": "10:10",
					"segments": [
					{
						"segment_index": "0",
						"departure_country": "UA",
						"departure_city": "IEV",
						"departure_airport": "KBP",
						"departure_terminal": null,
						"departure_date": "27.12.2015",
						"departure_time": "20:35",
						"arrival_country": "LV",
						"arrival_city": "RIX",
						"arrival_airport": "RIX",
						"arrival_terminal": null,
						"arrival_date": "27.12.2015",
						"arrival_time": "22:30",
						"supplier": "Air Baltic",
						"flight_number": "405",
						"aircraft_code": "DH4",
						"aircraft": "De Havilland DHC-8 Dash 8-400",
						"service_class_type": "economy",
						"service_class": "A",
						"fare_code": "AUATBAS",
						"departure_country_name": "Україна",
						"departure_airport_name": "Бориспiль",
						"departure_city_name": "Київ",
						"arrival_country_name": "Латвiя",
						"arrival_airport_name": "Рига",
						"arrival_city_name": "Рига"
					},
					{
						"segment_index": "1",
						"departure_country": "LV",
						"departure_city": "RIX",
						"departure_airport": "RIX",
						"departure_terminal": null,
						"departure_date": "28.12.2015",
						"departure_time": "07:30",
						"arrival_country": "RU",
						"arrival_city": "MOW",
						"arrival_airport": "SVO",
						"arrival_terminal": "E",
						"arrival_date": "28.12.2015",
						"arrival_time": "10:10",
						"supplier": "Air Baltic",
						"flight_number": "424",
						"aircraft_code": "737",
						"aircraft": "Boeing 737",
						"service_class_type": "economy",
						"service_class": "A",
						"fare_code": "AUATBAS",
						"departure_country_name": "Латвiя",
						"departure_airport_name": "Рига",
						"departure_city_name": "Рига",
						"arrival_country_name": "Росiя",
						"arrival_airport_name": "Шереметьєво",
						"arrival_city_name": "Москва"
					}
				],
			"departure_airport_name": "Бориспiль",
			"departure_city_name": "Київ",
			"arrival_airport_name": "Шереметьєво",
			"arrival_city_name": "Москва"
		},
		...
		...
		...
	],
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Для сложного перелета (маршрут)
air/searchaircraft?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&destinations=[{"departure":"IEV","arrival":"MOW","date":"27-07-2015"},{"departure":"MOW","arrival":"LON","date":"30-09-2015"}]

## Заначение полей ответа
| Имя тега  | Имя атрибута            | Значение                                          |
|-|-|-|
| recommendation | currency        | Код валюты системы бронирования |
| recommendation | id                  | ID рекомендации                   |
| recommendation | amount              | Сумма к оплате                   |
| recommendation | fare                | Значения тарифа в валюте системы бронирования                   |
| recommendation | validating-supplier | Валидирующий перевозчик                  |
| recommendation | route | Информация о части маршрута                  |
| recommendation | gds-id| Идентификатор системы бронирования                  |
| recommendation | is-lcc| Флаг лоукостера                  |
| recommendation | is-multi| Флаг мультитикета                  |
| route          | route-index| Индекс части маршрута                  |
| route          | route-duration| Время перелета для части маршрута (в минутах)                   |
| route          | segments| Информация о перелетах из которых состоит часть маршрута                   |
| segment        | segment-index| Индекс перелета                    |
| segment        | departure-country| Страна вылета                   |
| segment        | departure-city| Город вылета                   |
| segment        | departure-airport| Аэропорт вылета                   |
| segment        | departure-terminal| Терминал вылета                   |
| segment        | departure-time| Время вылета                   |
| segment        | arrival-country| Страна прилета                   |
| segment        | arrival-city| Город прилета                   |
| segment        | arrival-airport| Аэропорт прилета                   |
| segment        | arrival-terminal| Терминал прилета                   |
| segment        | arrival-time| Время прилета                   |
| segment        | supplier| IATA-код авиакомпании                   |
| segment        | flight-number| Номер рейса                   |
| segment        | aircraft-code| IATA-код модели самолета                   |
| segment        | aircraft| Название самолета                   |
| segment        | service-class-type| Класс обслуживания                   |
| segment        | service-class| Код класса обслуживания                   |
| segment        | fare-code| Код тарифа                   |
| segment        | baggage| Норма багажа                   |

## Идентификаторы систем бронирования
| Имя тега  | Имя атрибута            | Значение                                          |
|-|-|
| 1 | Amadeus     |
| 2 | Amadeus     |
| 3 | Galileo     |
| 4 | Sirena     |
| 5 | Gabriel     |
