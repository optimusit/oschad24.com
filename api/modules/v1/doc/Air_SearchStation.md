# Поиск аэропорта по указанному имени

## Запрос
**api_url/air/searchstation**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| name      | string | 3-30         | Начало названия станции                                 |

## Пример запроса
air/searchstation?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&name=лондон

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": 
	[
		{
			"id": "LON",
			"value": "Лондон, Великобританія"
		},
		{
			"id": "LDY",
			"value": "Лондондеррі, Великобританія"
		},
		{
			"id": "LYX",
			"value": "Лідд, Великобританія"
		},
		{
			"id": "OXF",
			"value": "Оксфорд, Великобританія"
		},
		{
			"id": "YXU",
			"value": "Лондон, Канада"
		}
	],
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Заначение полей ответа
