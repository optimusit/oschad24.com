# Описание сектора - геометрия расположения мест

## Запрос
**api_url/shakhtar/sector**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |
| eventId    | string  |             | ID события |
| sectorId    | string  |             | ID сектора |


## Пример запроса
shakhtar/sector?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl?eventId=28212339&sectorId=25776097

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": {
		"Sectorname": "10B   GATE 3",
		"SectorId": 25796579,
		"Price": 20,
		"Seats":  {
			"SectorSeat": [
				{
					"SeatId": 25796819,
					"X": 10,
					"Y": 26,
					"RowText": "1",
					"SeatText": "10",
					"IsFree": true
				},
				{
					"SeatId": 25796843,
					"X": 11,
					"Y": 26,
					"RowText": "1",
					"SeatText": "11",
					"IsFree": false
				},
				...
				...
				...
			]
		}
	},
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```


## Значения полей
| Название | Тип | Назначение |
|-|-|-|-|
| SeatId | int | Идентификатор места в секторе |
| X | int | Координаты сектора. Координата (0,0) начинается в нижнем левом углу |
| Y | int | Координаты сектора. Координата (0,0) начинается в нижнем левом углу |
| RowText | string | Название ряда |
| SeatText | string | Название места |
| IsFree | boolean | Свободно |
| Price | float | Цена |


![Схема сектора](images/ShakhtarSectorScheme.png)
