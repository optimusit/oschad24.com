# Условия тарифа

## Запрос
**api_url/air/conditions**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| key      | string | 3-32         | ключ рекомендации в списке рекомендаций |

## Пример запроса
air/conditions?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&key=ebc4d4305520cf152379f367941fe853

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data":
	[
		{
			"cancellation": 
			{
				"before": "false",
				"after": "false"
			},
			"change": 
			{
				"before": "true",
				"after": "true"
			},
			"departure": "IEV",
			"arrival": "MOW",
			"is-multi": "0",
			"text": "..."
		}
	],
	"sessionId": "KMMLmg9vGMVOef99WC71"
}
```

## Заначение полей ответа
| Имя тега  | Имя атрибута            | Значение                                          |
|-|-|-|
| fare-condition        | cancellation | Информация о правилах отмены                               |
| cancellation  | before| Отмена до вылета |
| cancellation | after| Отмена после вылета                   |
| fare-condition | change | Информация о правилах изменения                 |
| change | before  | Изменения до вылета                   |
| change | after| Изменения после вылета                   |
| fare-condition| departure | Город вылета                   |
| fare-condition| arrival | Город прилета                  |
| fare-condition| text| Полный текст условий тарифа                  |

* Условия тарифа отображаются посегментно.
* При наличии нескольких условий тарифа — применяются более строгие.
* Информация в блоках <cancellation> и <change> может содержать ошибки, так как данные базируются на основании синтаксического анализа полного текста правил тарифа.
