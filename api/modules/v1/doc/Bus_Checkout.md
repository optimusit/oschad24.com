# Оформление заказа

## Запрос
**api_url/bus/checkout**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
|sessionId|string|65|ID поисковой сессии|
|getCommissionOnly|int|1|Если true метод выполняют только проверку данных и вернет предварительную комиссию, сумму|
|passengers|array||Пассажиры (Максимум 4)|
|phone|string|13|Номер телефона (в формате +38)|
|email |string|50|Мыло|
|name |string|50|Имя покупателя|
|surname |string|50|Фамилия покупателя|

Параметр passengers - кодированый массив объектов (пример - [PHP](http://php.net/manual/en/function.json-encode.php), [JS](https://github.com/douglascrockford/JSON-js))

Список обязательных полей элемента массива
* name - Имя
* surname - Фамилия
* seat - № места
* date - дата рождения (DD.MM.YYYY) [необходимо передавать если need_birth]
* document_type - тип документа (1 || 2 || 3) [необходимо передавать если need_doc]
- 1 загранпаспорт
- 2 внутренний паспорт
- 3 свидетельство о рождении
* document_number - номер документа [необходимо передавать если need_doc]

Для международных рейсов **name** и **surname** заполняется латинскими буквами


## Пример запроса при невалидной ифнормации по пассажирам
bus/checkout?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&phone=+380957925818&email=andrej.lola@gmail.com&fullName=Иванов Иван&passengers=[{"name":"Иванов","surname":"","b_day":"16-05-2010","seat":"33"}]

### Пример ответа

* если code = -3, то внутри будет errors - массив ошибок валидации пассажиров
```json
	errors:[
		["lastname"]
	]
```

## Пример запроса при валидной ифнормации по пассажирам

bus/checkout?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl1&phone=+380957925818&email=andrej.lola@gmail.com&fullName=Иванов Иван&passengers=[{"name":"Иванов","surname":"Иванов","b_day":"16-05-2010","seat":"33"}]

### Пример ответа

