# Удаление пункта (места) из корзины

## Запрос
**api_url/entertainments/deleteplacefromcart**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |
| itemId    | string  |             | ID билета |

## Пример запроса
entertainments/deleteplacefromcart?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl&itemId=409384950

## Пример ответа при успешном удалении
```json
{
	"code": 0,
	"message": "",
	"data": "", 
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```
