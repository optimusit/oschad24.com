# Информация по заказу

## Запрос
**api_url/entertainments/orderdata**

## Заначение параметров URL

| Название | Тип | Длина/Формат | Назначение |
|-|-|-|-|
| sessionId    | string  | 65            | ID поисковой сессии |

## Пример запроса
entertainments/orderdata?accessToken=2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k&sessionId=CrjjsBvnIlttC2t54CJl

## Пример ответа
```json
{
	"code": 0,
	"message": "",
	"data": {
		будем согласовывать
	}, 
	"sessionId": "CrjjsBvnIlttC2t54CJl"
}
```
