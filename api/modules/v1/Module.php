<?php

namespace api\modules\v1;

use api\assets\AppAsset;
use Yii;
use api\modules\v1\components\ApiException;
use \api\modules\v1\components\ApiErrorHandler;
use yii\i18n\MissingTranslationEvent;
use common\models\Partner;
use common\models\User;
use yii\web\View;

/**
 * Модуль API v1
 *
 */
class Module extends \yii\base\Module
{
    /**
     * Интернет магазин
     */
    const SIDE_FRONT = 'front';
    /**
     * АРМ
     */
    const SIDE_BACK = 'back';
    /**
     * Вызов из крона
     */
    const SIDE_SERVICE = 'service';
    /**
     * Приставка в системе переводов сообщений
     */
    const MODULE_TRANSLATION_PREFIX = 'modules/v1/';

    private $_applicationLanguages = [
        'uk',
        'ru',
        'en',
    ];

    public $sessionIdLength = 20;
    public $partner;
    public $response;
    public $session;
    public $controllerNamespace = 'api\modules\v1\controllers';
    public $side;
    public $jsonp;
    public $assetBaseUrl;

    public function init()
    {
        parent::init();

        $this->SetLanguage();
        $this->SetSession();

        $this->response = [];
        $this->response['code'] = 0;
        $this->response['message'] = '';
        $this->response['data'] = null;
        $this->response['sessionId'] = $this->session->id;

        if (isset(Yii::$app->request->queryParams['callback'])) {
            $callback = Yii::$app->request->queryParams['callback'];
            if (is_null($callback) || empty($callback)) {
                $callback = 'JSON_DATA';
            }
            $this->jsonp = $callback;
        }

        if (!YII_ENV_DEV) {
            $handler = new ApiErrorHandler($this->jsonp);
            Yii::$app->set('errorHandler', $handler);
            $handler->register();
        }
        Yii::$app->user->enableSession = false;
        switch (Yii::$app->request->get('side', null)) {
            case self::SIDE_FRONT :
                $this->side = self::SIDE_FRONT;
                break;
            case self::SIDE_BACK :
                $this->side = self::SIDE_BACK;
                break;
            case self::SIDE_SERVICE;
                break;
        }
        Yii::$app->user->identityClass = User::className();
        $this->registerTranslations();
        $this->assetBaseUrl = AppAsset::register(new View())->baseUrl;
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        return true;
    }

    public function registerTranslations()
    {
        $translations = Yii::$app->params['translations'];
        $translations['sourceLanguage'] = Yii::$app->sourceLanguage;
        Yii::$app->i18n->translations[self::MODULE_TRANSLATION_PREFIX . '*'] = $translations['*'];
        Yii::$app->i18n->translations['yii'] = $translations['yii'];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t(self::MODULE_TRANSLATION_PREFIX . $category, $message, $params, $language);
//		return Yii::$app->getI18n()
//			->translateForPartner(Yii::$app->params['.']['response']['partnerId'], self::MODULE_TRANSLATION_PREFIX . $category, $message, $params, $language
//				?: Yii::$app->language);
    }

    /**
     * Возвращает текущего партнера
     *
     * @param null $partnerPrefix
     * @return array|null|\yii\db\ActiveRecord|static
     * @throws ApiException
     */
    public static function Partner($partnerPrefix = null)
    {
        $partner = null;
        if (Yii::$app->user->identityClass == Partner::className()) {
            // если это пользователь магазина
            $partner = Partner::findOne(['id' => Yii::$app->user->id]);
        } else {
            $user = User::findOne(['id' => Yii::$app->user->id]);
            if ($user) {
                // если это пользователь АРМа и он аутентифицирован
                $partner = $user->getPartner()
                    ->one();
            } elseif ($partnerPrefix) {
                // а если он не аутентифицирован, то попробуем проверить кто этот партнер
                $partner = Partner::findOne(['prefix' => $partnerPrefix]);
            }
        }

        if (!$partner) {
            throw new ApiException(Module::t('app', 'Невірний партнер'), 101);
        }

        return $partner;
    }

    /**
     * Возвращает текущего пользователя (оператор) системы
     *
     * @return User|null
     */
    public static function User()
    {
        if (Yii::$app->user->identityClass == User::className()) {
            return User::findOne(['id' => Yii::$app->user->id]);
        }

        return null;
    }

    private function SetLanguage()
    {
        if (isset(Yii::$app->request->queryParams['lang'])) {
            $lang = strtolower(trim(Yii::$app->request->queryParams['lang']));
        }
        if (isset($lang)
            && in_array($lang, $this->_applicationLanguages)
        ) {
            Yii::$app->language = $lang;
        }
    }

    private function SetSession()
    {
        if (isset(Yii::$app->request->queryParams['sessionId'])
            && !empty(Yii::$app->request->queryParams['sessionId'])
        ) {
            $sessionId = Yii::$app->request->queryParams['sessionId'];
        } else {
            $sessionId = str_replace('_', '', Yii::$app->security->generateRandomString($this->sessionIdLength));
        }
        /* @var $session \yii\web\Session */
        $session = Yii::$app->session;
        if ($session && !$session->getIsActive()) {
            $session->setId($sessionId);
        }
        $this->session = $session;
    }
}
