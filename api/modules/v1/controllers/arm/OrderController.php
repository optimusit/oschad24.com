<?php
namespace api\modules\v1\controllers\arm;

use Yii;
use api\modules\v1\components\Controller;

class OrderController extends Controller
{
    public function actionIndex()
    {
        $user = Yii::$app->user;
        $isGuest = $user->isGuest;

        return $this->_core->getAll();
    }
}
