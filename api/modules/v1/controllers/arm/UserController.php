<?php
namespace api\modules\v1\controllers\arm;

use Yii;
use api\modules\v1\components\Controller;

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);

        return $behaviors;
    }

    public function actionLogin()
    {
        $params = \Yii::$app->getRequest()
            ->get();

        return $this->_core->Login($params);
    }
}
