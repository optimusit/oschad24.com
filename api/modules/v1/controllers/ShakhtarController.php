<?php
namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\components\Controller;

class ShakhtarController extends Controller
{
    public function actionEvents()
    {
        return $this->_core->Events();
    }

    public function actionSectors()
    {
        return $this->_core->Sectors();
    }

    public function actionSector()
    {
        return $this->_core->Sector();
    }

    public function actionSeats()
    {
        return $this->_core->Seats();
    }

    public function actionCheckout()
    {
        return $this->_core->Checkout();
    }
//
//	public function actionReservpayment($reservationId, $amount)
//	{
//		return $this->_core->ReservPayment($reservationId, $amount);
//	}
//
//	public function actionSellreservation($reservationId)
//	{
//		return $this->_core->SellReservation($reservationId);
//	}
//
//	public function actionClearreservation($reservationNumber)
//	{
//		return $this->_core->ClearReservation($reservationNumber);
//	}
}
