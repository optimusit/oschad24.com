<?php
namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\components\Controller;
use api\modules\v1\Module;

class ApplicationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['access']);

        return $behaviors;
    }

    public function actionConfig()
    {
        return $this->_core->Config();
    }

    public function actionNews($source = null)
    {
        return $this->_core->News($source);
    }
}
