<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use common\payments\CommerceGateway\CommerceGateway;
use common\models\GatewayRequest;

class EgatewayController extends Controller
{
    public function actionNotify()
    {
        Yii::info(var_export($_REQUEST, true), 'egatewayNotify');
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $remoteAddr = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $remoteAddr = $_SERVER['REMOTE_ADDR'];
        }
        if (strcmp($remoteAddr, CommerceGateway::getGateWayIP()) === 0) {
            $httpRequest = Yii::$app->request;
            if ($httpRequest->post('Function') == 'TransResponse' && !is_null($httpRequest->post('Result'))) {
                $authorizationRequestNonce = $httpRequest->post('NONCE');
                $authorizationRequest = GatewayRequest::findOne(['nonce' => $authorizationRequestNonce]);
                if (is_null($authorizationRequest)) {
                    Yii::error('Commerce Gateway IPN is not saved. Request not found.', 'egatewayNotify');
                } else {
                    $authorizationRequest->attributes = [
                        'action' => intval($httpRequest->post('Result')),
                        'rc' => intval($httpRequest->post('RC')),
                        'rrn' => $httpRequest->post('RRN'),
                        'int_ref' => $httpRequest->post('IntRef'),
                        'auth_code' => $httpRequest->post('AuthCode'),
                        'trtype' => $httpRequest->post('TRTYPE'),
                    ];
                    $authorizationRequest->save(false);
                }
            } else {
                Yii::error('Commerce Gateway IPN is not saved. Is not TransResponse.', 'egatewayNotify');
            }
        } else {
            Yii::error('Commerce Gateway IPN cannot be trusted. Access deny for ' . $remoteAddr . ', Request params: '
                . var_export($_REQUEST, true), 'egatewayNotify');
            //throw new HttpException(404);
        }

        return '';
    }
}
