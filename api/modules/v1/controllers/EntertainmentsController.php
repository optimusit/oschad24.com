<?php
namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\components\Controller;

class EntertainmentsController extends Controller
{
    public function actionActivitytypes()
    {
        return $this->_core->ActivityTypes();
    }

    public function actionCities()
    {
        return $this->_core->Cities();
    }

    public function actionSearch()
    {
        return $this->_core->Search();
    }

    public function actionEvent()
    {
        return $this->_core->Event();
    }

    public function actionOrderdata()
    {
        return $this->_core->OrderData();
    }

    public function actionDeleteplacefromcart()
    {
        return $this->_core->DeletePlaceFromCart();
    }

    public function actionCheckout()
    {
        return $this->_core->Checkout();
    }
}
