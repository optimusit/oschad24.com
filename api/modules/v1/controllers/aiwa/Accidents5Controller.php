<?php
namespace api\modules\v1\controllers\aiwa;

use Yii;
use api\modules\v1\components\Controller;

class Accidents5Controller extends Controller
{
    /**
     * Расчитать сумму
     *
     * @param $cover
     * @param $plan
     * @return mixed
     */
    public function actionCalculate($cover, $plan = 'A')
    {
        return $this->_core->Calculate($cover, $plan);
    }

    /**
     * Создать документ
     *
     * @param $form
     * @return mixed
     */
    public function actionDocument($form)
    {
        $form = json_decode($form, true);

        return $this->_core->Document($form);
    }
}
