<?php
namespace api\modules\v1\controllers\ticketsua;

use Yii;
use api\modules\v1\components\Controller;

/**
 * Автобусные билеты
 *
 */
class BusController extends Controller
{
    /**
     * Поиск всех станции
     *
     * @return array Список станций
     */
    public function actionStations()
    {
        return $this->_core->Stations();
    }

    /**
     * Поиск станции по указанному имени
     *
     * @return array Список станций
     */
    public function actionSearchstation()
    {
        return $this->_core->SearchStation();
    }

    /**
     * Запрос списка автобусов на указанную дату
     *
     * @return mixed Список автобусов подходящих по критерию
     */
    public function actionSearchbus()
    {
        return $this->_core->SearchBus();
    }

    /**
     * Проверка наличия мест и параметров оплаты
     *
     * @return mixed Наличие мест и параметров оплаты
     */
    public function actionTripinfo()
    {
        return $this->_core->TripInfo();
    }

    /**
     * Оформление заказа
     *
     * @return mixed
     */
    public function actionCheckout()
    {
        return $this->_core->Checkout();
    }

    /**
     * Запрос получения списка бронирований
     *
     * @return mixed
     */
    public function actionBookingslist()
    {
        return $this->_core->BookingsList();
    }

    /**
     * Запрос информации по одному бронированию
     *
     * @param int $id Номер бронировки
     * @return mixed
     */
    public function actionBookingshow($id)
    {
        return $this->_core->BookingShow($id);
    }

    /**
     * Аннулирование билетов
     *
     * @param string $id Номер бронирования
     * @return array
     */
    public function actionCancel($id)
    {
        return $this->_core->Cancel($id);
    }

    /**
     * Запрос pdf для электронного билета
     *
     * @param int $id Номер бронировки
     * @return mixed Электронный билет в формате pdf файла, упакованный в base64
     */
    public function actionEticket($id)
    {
        return $this->_core->ETicket($id);
    }
}
