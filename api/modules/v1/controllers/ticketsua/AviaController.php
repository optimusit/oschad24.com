<?php
namespace api\modules\v1\controllers\ticketsua;

use Yii;
use api\modules\v1\components\Controller;

class AviaController extends Controller
{
    /**
     * Поиск станции по указанному имени
     *
     * @return array Список станций
     */
    public function actionSearchstation()
    {
        return $this->_core->SearchStation();
    }

    /**
     * Запрос списка возможных перелетов
     *
     * @return mixed Список рейсов подходящих по критерию
     */
    public function actionSearchaircraft()
    {
        return $this->_core->Searchaircraft();
    }

    /**
     * Условия тарифа
     *
     * @return mixed Условия тарифа
     */
    public function actionConditions()
    {
        return $this->_core->Conditions();
    }

    /**
     * Список стран для гражданства
     *
     * @return array Список стран для гражданства
     */
    public function actionCitizenship()
    {
        return $this->_core->Citizenship();
    }

    /**
     * Оформление заказа
     *
     * @return mixed Бронирование
     */
    public function actionCheckout()
    {
        return $this->_core->Checkout();
    }
}
