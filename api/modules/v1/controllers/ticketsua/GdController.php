<?php

namespace api\modules\v1\controllers\ticketsua;

use Yii;
use api\modules\v1\components\Controller;

/**
 * ЖД билеты
 *
 */
class GdController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        parent::beforeAction($action);

        return true;
    }

    /**
     * Поиск станции по указанному имени
     *
     * @return array Список станций
     */
    public function actionSearchstation()
    {
        return $this->_core->SearchStation();
    }

    /**
     * Запрос списка поездов на указанную дату
     *
     * @return mixed
     */

    public function actionSearchtrain()
    {
        return $this->_core->SearchTrain();
    }

    /**
     * Запрос маршрута поезда
     *
     * @return mixed
     */
    public function actionTrainroute()
    {
        return $this->_core->TrainRoute();
    }

    /**
     * Запрос информации по одному поезду
     *
     * @return mixed
     */
    public function actionTraininfo()
    {
        return $this->_core->TrainInfo();
    }

    /**
     * Запрос списка свободных мест по указанному вагону
     *
     * @return mixed
     */
    public function actionCarinfo()
    {
        return $this->_core->CarInfo();
    }

    /**
     * Оформление заказа
     *
     * @return mixed
     */
    public function actionCheckout()
    {
        return $this->_core->Checkout();
    }

    /**
     * Запрос расписания поездов по станции на указанную дату
     *
     * @return array
     */
    public function actionTimetable()
    {
        return $this->_core->TimeTable();
    }
}
