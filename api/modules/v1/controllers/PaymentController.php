<?php
namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\components\Controller;

class PaymentController extends Controller
{
    /**
     * Возвращает данные для оплаты заказа в системе e-Commerce Gateway
     *
     * @param $orderId int № Замовлення
     * @return array
     */
    public function actionAuthdata()
    {
        return $this->_core->GetAuthData();
    }

    /**
     * Запрос в систему e-Commerce Gateway на возврат средств за оплату заказа
     *
     * @param $orderId int № Замовлення
     * @return array
     */
    public function actionRefund()
    {
        return $this->_core->Refund();
    }
}
