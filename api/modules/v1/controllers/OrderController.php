<?php
namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\components\Controller;

class OrderController extends Controller
{
    public function actionAuthorizationdata($orderId)
    {
        return $this->_core->AuthorizationData($orderId);
    }

    /**
     * Возвращает состояние оплаты заказа
     *
     * @param $orderId
     * @return mixed
     */
    public function actionPaymentstatus($orderId, $timestamp, $showInternalInfo = 0)
    {
        return $this->_core->PaymentStatus($orderId, $timestamp, $showInternalInfo);
    }
}
