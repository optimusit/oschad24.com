<?php
namespace console\components;

class Controller extends \yii\console\Controller
{
    protected $timestamp;

    public function beforeAction($action)
    {
        $this->timestamp = time();
        \Yii::info($action->controller->id . '/' . $action->id . '-b' . $this->timestamp, 'cron');
        echo PHP_EOL;

        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        \Yii::info($action->controller->id . '/' . $action->id . '-e' . $this->timestamp, 'cron');
        echo PHP_EOL;

        return parent::afterAction($action, $result);
    }
}
