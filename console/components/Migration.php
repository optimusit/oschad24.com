<?php

namespace console\components;

use yii\db\Migration as BaseMigration;

class Migration extends BaseMigration
{
    protected $_tableOptions = null;

    public function init()
    {
        parent::init();

        if ($this->db->driverName === 'mysql') {
            $this->_tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
    }

}
