<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110035_sms_queue extends Migration
{
    private $_tableName = '{{%sms_queue}}';

    public function up()
    {
        $this->_tableOptions .= " comment = 'Очередь SMS'";
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'phone' => Schema::TYPE_STRING . "(13) NOT NULL COMMENT 'Телефон'",
            'text' => Schema::TYPE_STRING . "(300) NOT NULL COMMENT 'Текст'",
            'hash' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'hash'",
            'number_tries' => "tinyint(1) NULL DEFAULT 0 COMMENT 'Кількість спроб'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'UNIQUE INDEX `hash` (`hash`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
