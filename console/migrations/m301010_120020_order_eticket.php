<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120020_order_eticket extends Migration
{
    private $_tableName = '{{%order}}';
    private $_column = 'eticket';

    public function up()
    {
        $this->addColumn($this->_tableName, $this->_column,
            Schema::TYPE_TEXT . " NULL COMMENT 'Дані для створення квитка' AFTER `viewed`");
    }

    public function down()
    {
        $this->dropColumn($this->_tableName, $this->_column);
    }
}
