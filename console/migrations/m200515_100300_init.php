<?php

use console\components\Migration;
use common\models\Commission;
use common\models\PartnerService;
use common\models\Service;
use common\models\Provider;
use common\models\ServiceProvider;
use common\models\User;
use common\models\Partner;

class m200515_100300_init extends Migration
{
    public function safeUp()
    {
        Yii::$app->keyStorage->set('ticketsua_token', 'NjQxZWMyN2YtMjZiMS00NjI2LWI5MDgtNWI3MzM2MWMxNDc5');
        Yii::$app->keyStorage->set('karabasAuth', 'f771770c0b954b1288c88447d83c1743yOxEVN7yWeAT06syIzkSErhcGJE=');

        $partner = new Partner([
            'name' => 'veb.ua',
            'prefix' => 'vebua',
            'logo' => 'vebua.png',
            'balance' => 1000000,
            'rr' => '00000000000000',
            'bank' => 'АТ "Ощадбанк"',
            'edrpou' => '00000',
            'email' => 'admin@veb.ua',
            'phone' => '00000',
            'access_token' => 'oZUALqwqiEPXB9M5TLmB5AWCIPCzGsyT',
            'secret_key' => '$2y$13$uIvv/IQ0Ta5E4AxSS0RAVe3fbNRnVo/SAOA24pcL1fAFq6wREgCUm',
        ]);
        if (!$partner->makeRoot()) {
            var_dump($partner->getFirstErrors());
            throw new Exception();
        }
        $partnerIdVebUa = $partner->id;

        $oschadBank = new Partner([
            'name' => 'АТ "Ощадбанк"',
            'prefix' => 'oschadbank',
            'logo' => 'oschadbank.png',
            'balance' => 1000000,
            'rr' => '00000000000000',
            'bank' => 'АТ "Ощадбанк"',
            'edrpou' => '00001',
            'email' => 'admin@oschadbank.ua',
            'phone' => '00000',
            'access_token' => 'x2342xvverMtCvnkzt-MbEF67xLn423a',
            'secret_key' => '$2y$13$uZvKc1LcUx1jzgYUQjt5d.NJdsh.TXW5vFlwr7Y3z62S5Bp3PKkoa',
        ]);
        if (!$oschadBank->makeRoot()) {
            var_dump($oschadBank->getFirstErrors());
            throw new Exception();
        }

        $oschadBankZp = new Partner();
        $oschadBankZp->attributes = [
            'name' => 'Запоріжжя',
            'prefix' => 'zp',
            'logo' => '',
            'balance' => 1000000,
            'rr' => '00000000000002',
            'bank' => 'АТ "Ощадбанк"',
            'edrpou' => '00002',
            'email' => 'admin@zp.oschadbank.ua',
            'phone' => '00000',
            'access_token' => Yii::$app->security->generateRandomString(),
            'secret_key' => Yii::$app->security->generatePasswordHash($oschadBankZp->prefix . time()),
        ];
        if (!$oschadBankZp->prependTo($oschadBank)) {
            var_dump($oschadBankZp->getFirstErrors());
            throw new Exception();
        }

        $oschadZp_10007_0000 = new Partner();
        $oschadZp_10007_0000->attributes = [
            'name' => '10007/0000',
            'prefix' => '10007/0000',
            'logo' => '',
            'balance' => 1000000,
            'rr' => '00000000000002',
            'bank' => 'АТ "Ощадбанк"',
            'edrpou' => '00002',
            'email' => 'admin@10007/0000.zp.oschadbank.ua',
            'phone' => '00000',
            'access_token' => Yii::$app->security->generateRandomString(),
            'secret_key' => Yii::$app->security->generatePasswordHash($oschadZp_10007_0000->prefix . time()),
        ];
        if (!$oschadZp_10007_0000->prependTo($oschadBankZp)) {
            var_dump($oschadZp_10007_0000->getFirstErrors());
            throw new Exception();
        }

        $user = new User();
        $user->attributes = [
            'email' => 'admin@veb.ua',
            'phone' => '1111111111',
            'name' => 'Админ',
            'patronymic' => 'Админович',
            'surname' => 'Супер',
            'confirm' => 1,
            'confirm_key' => '1',
            'partner_id' => $partnerIdVebUa,
            'auth_key' => '5htkQZVMtCvnkzt-MbEF67xLnjGf0s4j',
        ];
        $user->setPassword('admin');
        if (!$user->save()) {
            var_dump($user->getFirstErrors());
            throw new Exception();
        }

        $user = new User();
        $user->attributes = [
            'email' => 'oschad24@oschadbank.ua',
            'phone' => '1234567',
            'name' => 'oschad24',
            'patronymic' => 'oschad24',
            'surname' => 'oschad24',
            'auth_key_remember_forever' => 1,
            'confirm' => 1,
            'confirm_key' => '1',
            'partner_id' => $oschadBank->id,
            'auth_key' => '2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k',
            'auth_key_expiration' => strtotime(date('Y-m-d', strtotime('+10 years'))),
        ];
        $user->setPassword('A@XR#L@Rxq-02zkl;veaz3');
        if (!$user->save()) {
            var_dump($user->getFirstErrors());
            throw new Exception();
        }

        $user = new User();
        $user->attributes = [
            'email' => 'manager01@oschadbank.ua',
            'phone' => '123',
            'name' => 'Ощад',
            'patronymic' => 'Банка',
            'surname' => 'Менеджер',
            'confirm' => 1,
            'confirm_key' => '1',
            'partner_id' => $oschadBank->id,
            'auth_key' => 'oZUALqwqiEPXB9M5TLmB5AWCIPCzGsyT',
        ];
        $user->setPassword('manager');
        if (!$user->save()) {
            var_dump($user->getFirstErrors());
            throw new Exception();
        }

        $user = new User();
        $user->attributes = [
            'email' => '10007/0000_cashier01@oschadbank.ua',
            'phone' => '123',
            'name' => 'Ощад',
            'patronymic' => 'Банка',
            'surname' => 'Кассир',
            'confirm' => 1,
            'confirm_key' => '1',
            'partner_id' => $oschadZp_10007_0000->id,
            'auth_key' => '29iw3_JJhIu0RnGwRjijLkN-ec1xsMWM',
        ];
        $user->setPassword('cashier');
        if (!$user->save()) {
            var_dump($user->getFirstErrors());
            throw new Exception();
        }

        $ticketsUaProvider = new Provider();
        $ticketsUaProvider->attributes = [
            'prefix' => \common\providers\tickets\Tickets::PREFIX,
            'name' => 'tickets.ua',
            'email' => 'admin@tickets.ua',
            'phone' => '+38',
            'access_token' => '1',
            'secret_key' => '1',
            'status' => 1
        ];
        if (!$ticketsUaProvider->save()) {
            var_dump($ticketsUaProvider->getFirstErrors());
            throw new Exception();
        }

        $karabasProvider = new Provider();
        $karabasProvider->attributes = [
            'prefix' => \common\providers\karabas\Karabas::PREFIX,
            'name' => 'karabas',
            'email' => 'admin@karabas.ua',
            'phone' => '+38',
            'access_token' => '1',
            'secret_key' => '1',
            'status' => 1
        ];
        if (!$karabasProvider->save()) {
            var_dump($karabasProvider->getFirstErrors());
            throw new Exception();
        }

        $kontramarkaProvider = new Provider();
        $kontramarkaProvider->attributes = [
            'prefix' => 'kontramarka',
            'name' => 'kontramarka',
            'email' => 'admin@kontramarka.ua',
            'phone' => '+38',
            'access_token' => '1',
            'secret_key' => '1',
            'status' => 1
        ];
        if (!$kontramarkaProvider->save()) {
            var_dump($kontramarkaProvider->getFirstErrors());
            throw new Exception();
        }

        $vakarchukProvider = new Provider();
        $vakarchukProvider->attributes = [
            'prefix' => 'vakarchuk',
            'name' => 'vakarchuk',
            'email' => 'admin@vakarchuk.ua',
            'phone' => '+38',
            'access_token' => '1',
            'secret_key' => '1',
            'status' => 1
        ];
        if (!$vakarchukProvider->save()) {
            var_dump($vakarchukProvider->getFirstErrors());
            throw new Exception();
        }

        $aiwaAccidentProvider = new Provider();
        $aiwaAccidentProvider->attributes = [
            'prefix' => 'aiwa',
            'name' => 'Арсенал',
            'email' => 'admin@arsenal-ic.com',
            'phone' => '+38',
            'access_token' => '1',
            'secret_key' => '1',
            'status' => 1
        ];
        if (!$aiwaAccidentProvider->save()) {
            var_dump($aiwaAccidentProvider->getFirstErrors());
            throw new Exception();
        }

        $shakhtarProvider = new Provider();
        $shakhtarProvider->attributes = [
            'prefix' => 'SHAKHTAR',
            'name' => 'ФК Донецьк "Шахтар"',
            'email' => 'admin@shakhtar.com',
            'phone' => '+38',
            'access_token' => '1',
            'secret_key' => '1',
            'status' => 1
        ];
        if (!$shakhtarProvider->save()) {
            var_dump($shakhtarProvider->getFirstErrors());
            throw new Exception();
        }

        $serviceGd = new Service();
        $serviceGd->name_uk = 'Залізничні квитки';
        $serviceGd->short_name_uk = 'Залізничні квитки';
        $serviceGd->description_uk = 'Залізничні квитки';
        $serviceGd->name_ru = 'Ж/Д билеты';
        $serviceGd->short_name_ru = 'Ж/Д билеты';
        $serviceGd->description_ru = 'Ж/Д билеты';
        $serviceGd->name_en = 'Railway tickets';
        $serviceGd->short_name_en = 'Railway tickets';
        $serviceGd->description_en = 'Railway tickets';
        $serviceGd->short_name_translit = 'Zaliznychni kvytky';
        $serviceGd->provider_code = 'gd';
        $serviceGd->code = Service::TICKETS_GD;
        $serviceGd->controller = 'ticketsua/gd';
        $serviceGd->script_url = '-';
        $serviceGd->color = '#000000';
        $serviceGd->active = 1;
        if (!$serviceGd->save()) {
            var_dump($serviceGd->getFirstErrors());
            throw new Exception();
        }

        $serviceBus = new Service();
        $serviceBus->name_uk = 'Автобусні квитки';
        $serviceBus->short_name_uk = 'Автобусні квитки';
        $serviceBus->description_uk = 'Автобусні квитки';
        $serviceBus->name_ru = 'Билеты на автобус';
        $serviceBus->short_name_ru = 'Билеты на автобус';
        $serviceBus->description_ru = 'Билеты на автобус';
        $serviceBus->name_en = 'Bus';
        $serviceBus->short_name_en = 'Bus';
        $serviceBus->description_en = 'Bus';
        $serviceBus->short_name_translit = 'Avtobusni kvytky';
        $serviceBus->provider_code = 'bus';
        $serviceBus->code = Service::TICKETS_BUS;
        $serviceBus->controller = 'ticketsua/bus';
        $serviceBus->script_url = '-';
        $serviceBus->color = '#000000';
        $serviceBus->active = 1;
        if (!$serviceBus->save()) {
            var_dump($serviceBus->getFirstErrors());
            throw new Exception();
        }

        $serviceAvia = new Service();
        $serviceAvia->name_uk = 'Авіаквитки';
        $serviceAvia->short_name_uk = 'Авіаквитки';
        $serviceAvia->description_uk = 'Авіаквитки';
        $serviceAvia->name_ru = 'Авиабилеты';
        $serviceAvia->short_name_ru = 'Авиабилеты';
        $serviceAvia->description_ru = 'Авиабилеты';
        $serviceAvia->name_en = 'Flights';
        $serviceAvia->short_name_en = 'Flights';
        $serviceAvia->description_en = 'Flights';
        $serviceAvia->short_name_translit = 'Aviakvytky';
        $serviceAvia->provider_code = 'avia';
        $serviceAvia->code = Service::TICKETS_AVIA;
        $serviceAvia->controller = 'ticketsua/avia';
        $serviceAvia->script_url = '-';
        $serviceAvia->color = '#000000';
        if (!$serviceAvia->save()) {
            var_dump($serviceAvia->getFirstErrors());
            throw new Exception();
        }

        $serviceEntertainments = new Service();
        $serviceEntertainments->name_uk = 'Розважальні заходи';
        $serviceEntertainments->short_name_uk = 'Розважальні заходи';
        $serviceEntertainments->description_uk = 'Розважальні заходи';
        $serviceEntertainments->name_ru = 'Развлекательные мероприятия';
        $serviceEntertainments->short_name_ru = 'Развлекательные мероприятия';
        $serviceEntertainments->description_ru = 'Развлекательные мероприятия';
        $serviceEntertainments->name_en = 'Tickets for concerts';
        $serviceEntertainments->short_name_en = 'Tickets for concerts';
        $serviceEntertainments->description_en = 'Tickets for concerts';
        $serviceEntertainments->short_name_translit = 'Rozvazhalni zakhody';
        $serviceEntertainments->provider_code = 'entertainments';
        $serviceEntertainments->code = Service::TICKETS_ENTERTAINMENTS;
        $serviceEntertainments->controller = 'entertainments';
        $serviceEntertainments->script_url = '-';
        $serviceEntertainments->color = '#000000';
        if (!$serviceEntertainments->save()) {
            var_dump($serviceEntertainments->getFirstErrors());
            throw new Exception();
        }

        $serviceInsuranceAccident = new Service();
        $serviceInsuranceAccident->name_uk = 'Страхування';
        $serviceInsuranceAccident->short_name_uk = 'Страхування';
        $serviceInsuranceAccident->description_uk = 'Страхування';
        $serviceInsuranceAccident->name_ru = 'Страхование';
        $serviceInsuranceAccident->short_name_ru = 'Страхование';
        $serviceInsuranceAccident->description_ru = 'Страхование';
        $serviceInsuranceAccident->name_en = 'Insurance';
        $serviceInsuranceAccident->short_name_en = 'Insurance';
        $serviceInsuranceAccident->description_en = 'Insurance';
        $serviceInsuranceAccident->short_name_translit = 'Strakhuvannia';
        $serviceInsuranceAccident->provider_code = 'accidents5';
        $serviceInsuranceAccident->code = Service::AIWA_ACCIDENTS5;
        $serviceInsuranceAccident->controller = 'aiwa/accidents5';
        $serviceInsuranceAccident->script_url = '-';
        $serviceInsuranceAccident->color = '#000000';
        if (!$serviceInsuranceAccident->save()) {
            var_dump($serviceInsuranceAccident->getFirstErrors());
            throw new Exception();
        }

        $serviceFootballShakhtar = new Service();
        $serviceFootballShakhtar->name_uk = 'Квитки на футбол';
        $serviceFootballShakhtar->short_name_uk = 'Квитки на футбол';
        $serviceFootballShakhtar->description_uk = 'Квитки на футбол';
        $serviceFootballShakhtar->name_ru = 'Билеты на футбол';
        $serviceFootballShakhtar->short_name_ru = 'Билеты на футбол';
        $serviceFootballShakhtar->description_ru = 'Билеты на футбол';
        $serviceFootballShakhtar->name_en = 'Football';
        $serviceFootballShakhtar->short_name_en = 'Football';
        $serviceFootballShakhtar->description_en = 'Football';
        $serviceFootballShakhtar->short_name_translit = 'Kvytky na futbol';
        $serviceFootballShakhtar->provider_code = 'shakhtar';
        $serviceFootballShakhtar->code = Service::TICKETS_FOOTBALL;
        $serviceFootballShakhtar->controller = 'shakhtar';
        $serviceFootballShakhtar->script_url = '-';
        $serviceFootballShakhtar->color = '#000000';
        if (!$serviceFootballShakhtar->save()) {
            var_dump($serviceFootballShakhtar->getFirstErrors());
            throw new Exception();
        }

        $partnerService = new PartnerService();
        $partnerService->partner_id = $oschadBank->id;
        $partnerService->service_id = $serviceGd->id;
        $partnerService->insert();
        $partnerService = new PartnerService();
        $partnerService->partner_id = $oschadBank->id;
        $partnerService->service_id = $serviceBus->id;
        $partnerService->insert();
        $partnerService = new PartnerService();
        $partnerService->partner_id = $oschadBank->id;
        $partnerService->service_id = $serviceAvia->id;
        $partnerService->insert();
        $partnerService = new PartnerService();
        $partnerService->partner_id = $oschadBank->id;
        $partnerService->service_id = $serviceEntertainments->id;
        $partnerService->insert();
        $partnerService = new PartnerService();
        $partnerService->partner_id = $oschadBank->id;
        $partnerService->service_id = $aiwaAccidentProvider->id;
        $partnerService->insert();
        $partnerService = new PartnerService();
        $partnerService->partner_id = $oschadBank->id;
        $partnerService->service_id = $shakhtarProvider->id;
        $partnerService->insert();

        $serviceProvider = new ServiceProvider();
        $serviceProvider->service_id = $serviceGd->id;
        $serviceProvider->provider_id = $ticketsUaProvider->id;
        $serviceProvider->insert();
        $serviceProvider = new ServiceProvider();
        $serviceProvider->service_id = $serviceBus->id;
        $serviceProvider->provider_id = $ticketsUaProvider->id;
        $serviceProvider->insert();
        $serviceProvider = new ServiceProvider();
        $serviceProvider->service_id = $serviceAvia->id;
        $serviceProvider->provider_id = $ticketsUaProvider->id;
        $serviceProvider->insert();
        $serviceProvider = new ServiceProvider();
        $serviceProvider->service_id = $serviceEntertainments->id;
        $serviceProvider->provider_id = $karabasProvider->id;
        $serviceProvider->insert();
        $serviceProvider = new ServiceProvider();
        $serviceProvider->service_id = $serviceEntertainments->id;
        $serviceProvider->provider_id = $kontramarkaProvider->id;
        $serviceProvider->insert();
        $serviceProvider = new ServiceProvider();
        $serviceProvider->service_id = $serviceEntertainments->id;
        $serviceProvider->provider_id = $vakarchukProvider->id;
        $serviceProvider->insert();
        $serviceProvider = new ServiceProvider();
        $serviceProvider->service_id = $serviceInsuranceAccident->id;
        $serviceProvider->provider_id = $aiwaAccidentProvider->id;
        $serviceProvider->insert();
        $serviceProvider = new ServiceProvider();
        $serviceProvider->service_id = $serviceFootballShakhtar->id;
        $serviceProvider->provider_id = $shakhtarProvider->id;
        $serviceProvider->insert();

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceGd->id,
            'kind' => Commission::KIND_SYSTEM,
            'provider_id' => $ticketsUaProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceGd->id,
            'kind' => Commission::KIND_PARTNER,
            'provider_id' => $ticketsUaProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceBus->id,
            'kind' => Commission::KIND_SYSTEM,
            'provider_id' => $ticketsUaProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceBus->id,
            'kind' => Commission::KIND_PARTNER,
            'provider_id' => $ticketsUaProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceEntertainments->id,
            'kind' => Commission::KIND_PARTNER,
            'provider_id' => $karabasProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceEntertainments->id,
            'kind' => Commission::KIND_PARTNER,
            'provider_id' => $kontramarkaProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceEntertainments->id,
            'kind' => Commission::KIND_PARTNER,
            'provider_id' => $vakarchukProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceInsuranceAccident->id,
            'kind' => Commission::KIND_PARTNER,
            'provider_id' => $aiwaAccidentProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }

        $commission = new Commission();
        $commission->attributes = [
            'partner_id' => $oschadBank->id,
            'service_id' => $serviceInsuranceAccident->id,
            'kind' => Commission::KIND_PARTNER,
            'provider_id' => $shakhtarProvider->id,
            'internal' => 0,
            'min' => 1,
            'max' => 10,
            'value' => 1,
            'round_type' => 1,
            'type' => Commission::TYPE_STATIC,
        ];
        if (!$commission->save()) {
            var_dump($commission->getFirstErrors());
            throw new Exception();
        }
    }

    public function safeDown()
    {
        common\models\GatewayRequestHistory::deleteAll();
        common\models\OrderProviderHistory::deleteAll();
        common\models\aiwa\Participant::deleteAll();
        common\models\aiwa\Document::deleteAll();
        common\models\GatewayRequest::deleteAll();
        common\models\Order::deleteAll();
        common\models\Commission::deleteAll();
        common\models\PartnerService::deleteAll();
        common\models\ServiceProvider::deleteAll();
        common\models\Service::deleteAll();
        common\models\Provider::deleteAll();
        common\models\User::deleteAll();
        common\models\Partner::deleteAll();
    }
}
