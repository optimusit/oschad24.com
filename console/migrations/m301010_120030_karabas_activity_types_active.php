<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120030_karabas_activity_types_active extends Migration
{
    private $_tableName = '{{%karabas_activity_types}}';
    private $_column = 'active';

    public function up()
    {
        $this->addColumn($this->_tableName, $this->_column,
            "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Активний' AFTER `color`");
    }

    public function down()
    {
        $this->dropColumn($this->_tableName, $this->_column);
    }
}
