<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_111300_aiwa_participant extends Migration
{
    private $_tableName = '{{%aiwa_participant}}';
    private $_documentTableName = '{{%aiwa_document}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'document_id' => "bigint(20) NOT NULL COMMENT 'ID документу'",
            'type' => Schema::TYPE_STRING . "(11) NOT NULL COMMENT 'Тип учасника (vendor,tpl,beneficiary)'",
            'kind' => Schema::TYPE_STRING
                . "(13) NOT NULL COMMENT 'Вид контрагента-страхувальника (private,organization)'",
            'phone' => Schema::TYPE_STRING . "(13) NOT NULL COMMENT 'Телефон'",
            'ipn' => Schema::TYPE_STRING . "(12) NOT NULL COMMENT 'ІПН'",
            'surname' => Schema::TYPE_STRING . "(100) NOT NULL COMMENT 'Прізвище'",
            'name' => Schema::TYPE_STRING . "(100) NOT NULL COMMENT 'Ім`я'",
            'patronymic' => Schema::TYPE_STRING . "(100) NOT NULL COMMENT 'Побатькові'",
            'title' => Schema::TYPE_STRING . "(300) NOT NULL COMMENT 'Подання (Прізвище Ім`я Побатькові)'",
            'birthday' => Schema::TYPE_DATE . " NOT NULL COMMENT 'Дата народження'",
            'passport_series' => Schema::TYPE_STRING . "(2) NOT NULL COMMENT 'Серія паспорту'",
            'passport_number' => Schema::TYPE_STRING . "(8) NOT NULL COMMENT '№ паспорту'",
            'passport_date' => Schema::TYPE_DATE . " NOT NULL COMMENT 'Дата видачі паспорту'",
            'passport_issuer' => Schema::TYPE_STRING . "(250) NOT NULL COMMENT 'Орган що видав'",
            'address_city' => Schema::TYPE_STRING . "(250) NOT NULL COMMENT 'Населений пункт'",
            'address_index' => Schema::TYPE_STRING . "(6) NOT NULL COMMENT 'Індекс'",
            'address_address' => Schema::TYPE_STRING . "(250) NOT NULL COMMENT 'Адреса'",
            'address_title' => Schema::TYPE_STRING . "(250) NOT NULL COMMENT 'Повна адреса'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'UNIQUE INDEX `document_id-type-ipn` (`document_id`,`type`,`ipn`)',
        ], $this->_tableOptions);

        $this->addForeignKey('aiwa_participant_doc_ibfk_10', $this->_tableName, 'document_id',
            $this->_documentTableName, 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('aiwa_participant_doc_ibfk_10', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
