<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120010_shakhtar extends Migration
{
    private $_tableName = '{{%shakhtar}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => "bigint(20) NOT NULL COMMENT 'id матчу'",
            'name' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва матчу'",
            'date_start' => Schema::TYPE_DATETIME . " NOT NULL COMMENT 'Час початку'",
            'date_end' => Schema::TYPE_DATETIME . " NOT NULL COMMENT 'Час закінчення'",
            'limit' => "tinyint(2) NOT NULL COMMENT 'Ліміт на продаж в 1 руки'",
            'active' => "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Активний'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
