<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120070_entertainments_city extends Migration
{
    private $_tableName = '{{%entertainments_city}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'name_uk' => "varchar(50) NOT NULL COMMENT 'Назва Українською'",
            'name_ru' => "varchar(50) NOT NULL COMMENT 'Назва Російською'",
            'name_en' => "varchar(50) NOT NULL COMMENT 'Назва Англійською'",
            'active' => "tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Активний'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
