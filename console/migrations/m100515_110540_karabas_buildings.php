<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110540_karabas_buildings extends Migration
{
    private $_tableName = '{{%karabas_buildings}}';
    private $_citiesTableName = '{{%karabas_cities}}';
    private $_buildingTypesTableName = '{{%karabas_building_types}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL',
            'name' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва місця проведення'",
            'shortName' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Коротка назва'",
            'address' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Адреса'",
            'buildingTypeId' => "bigint(20) NULL COMMENT 'ID типу місця проведення'",
            'cityId' => "bigint(20) NULL COMMENT 'ID міста'",
            'description' => Schema::TYPE_STRING . "(512) NOT NULL COMMENT 'Опис'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'KEY `buildingTypeId` (`buildingTypeId`)',
        ], $this->_tableOptions);
//		$this->addForeignKey('building_types_ibfk_10', $this->_tableName, 'buildingTypeId',
//			$this->_buildingTypesTableName, 'id', 'RESTRICT', 'RESTRICT');
//		$this->addForeignKey('cities_ibfk_10', $this->_tableName, 'cityId', $this->_citiesTableName, 'id', 'RESTRICT',
//			'RESTRICT');
    }

    public function down()
    {
//		$this->dropForeignKey('building_types_ibfk_10', $this->_tableName);
//		$this->dropForeignKey('cities_ibfk_10', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
