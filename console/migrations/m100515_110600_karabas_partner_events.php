<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110600_karabas_partner_events extends Migration
{
    private $_tableName = '{{%karabas_partner_events}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'EventId' => "bigint(20) NOT NULL COMMENT 'ID заходу'",
            'EventName' => Schema::TYPE_STRING . "(255) NULL COMMENT 'Найменування заходу'",
            'EventDate' => Schema::TYPE_DATETIME . " NOT NULL COMMENT 'Дата заходу'",
            'EventDescription' => Schema::TYPE_TEXT . " NULL COMMENT 'Опис заходу'",
            'ActivityId' => 'bigint(20) NOT NULL',
            'ActivityName' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Найменування події'",
            'ActivityDescription' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Опис події'",
            'ActivityDescriptionLong' => Schema::TYPE_TEXT . " NULL COMMENT 'Описание події'",
            'CategoriesIDs' => Schema::TYPE_STRING . "(255) NULL COMMENT 'Перелік категорій заходу, розділені комою'",
            'Categories' => Schema::TYPE_STRING
                . "(1024) NULL COMMENT 'Перелік найменувань категорій заходи, розділені комою'",
            'IsETicketPassportRequired' => "tinyint(1) NOT NULL COMMENT 'Паспортні дані на електронному квитку обов`язкові'",
            'IsThermoTicketPassportRequired' => "tinyint(1) NOT NULL COMMENT 'Паспортні дані на термобілеті обов`язкові'",
            'ClientInfoRequired' => "tinyint(1) NOT NULL COMMENT 'Інформація про клієнта обов`язкове для електронного квитка'",
            'ETicketEnabled' => "tinyint(1) NOT NULL COMMENT 'дозволена / заборонено продаж електронних квитків на цей захід'",
            'PrintEnabled' => "tinyint(1) NOT NULL COMMENT 'дозволена / заборонена друк квитків на захід'",
            'HasPNG' => "tinyint(1) NOT NULL COMMENT 'є / немає PNG-версія залу. Якщо ні - необхідно вивести клієнту флешевих зал.'",
            'HasSVG' => "tinyint(1) NOT NULL COMMENT 'є / немає SVG-версія залу. Якщо ні - необхідно вивести клієнту PNG або флеш-версію залу.'",
            'SVG' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Посилання на SVG-файл схеми залу'",
            'City' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва міста'",
            'CityId' => "bigint(20) NOT NULL COMMENT 'ID міста'",
            'HallName' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Найменування залу'",
            'HallId' => "bigint(20) NOT NULL COMMENT 'ID залу'",
            'BuildingName' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Найменування місця проведення'",
            'BuildingId' => "bigint(20) NOT NULL COMMENT 'ID місця проведення'",
            'Organizer' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва компанії-організатора'",
            'OrganizerShortName' => Schema::TYPE_STRING . "(255) NULL COMMENT 'Коротка назва компанії-організатора'",
            'OrganizerId' => "bigint(20) NOT NULL COMMENT 'ID організатора'",
            'SaleStatus' => "tinyint(1) NOT NULL COMMENT 'Статус продажу (1 - дозволений продаж, 0 - заборонено продаж)'",
            'ReplaceAvailabeTicketsCount' => "tinyint(1) NOT NULL COMMENT 'Обмежити кількість доступних квитків в легенді'",
            'ReplaceAvailabeTicketsCountWith' => "tinyint(3) NOT NULL COMMENT 'Максимальна кількість квитків відображаються в легенді, по кожній ціновій категорії'",
            'ReplaceActivityDescription' => "tinyint(1) NOT NULL COMMENT 'Замінити опис події описом заходу. Використовується, коли у заході використовується альтернативна версія опису, а не доповнення.'",
            'SaleStatusDescription' => Schema::TYPE_STRING . "(255) NULL COMMENT 'Причина заборони'",
            'SalesEndTime' => Schema::TYPE_DATETIME . " NOT NULL COMMENT 'Час закінчення продажів'",
            'ReserveEndTime' => Schema::TYPE_DATETIME . " NOT NULL COMMENT 'Час закінчення резервування'",
            'Percent' => Schema::TYPE_TEXT . " NULL COMMENT 'Відсоток реалізатора від продажу заходи'",
            'ActivityImageSmall' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Маленька картинка події'",
            'ActivityImageMedium' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Середня картинка події'",
            'ActivityImageBig' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Велика картинка події'",
            'ActivityImageLarge' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Величезна картинка події'",
            'EventImageSmall' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Маленька картинка заходу'",
            'EventImageMedium' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Середня картинка заходу'",
            'EventImageBig' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Велика картинка заходу'",
            'EventImageLarge' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Величезна картинка заходу'",
            'EventType' => Schema::TYPE_STRING
                . "(255) NOT NULL COMMENT 'Тип події (Default - стандартне, AllTimeEvent - постійне)'",
            // ниже идут вспомогательные поля, значения которых были вычислины (чтобы максимальн ускорить вывод)
            'LineImage' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Картинка (Стрічка)'",
            'LinePriceRange' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Ціновий діапазон (Стрічка)'",
            'LinePlace' => Schema::TYPE_STRING . "(500) NOT NULL COMMENT 'Місце проведення (Стрічка)'",
            'LineDateTime' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Час проведення (Стрічка)'",
            'DetailsDate' => Schema::TYPE_STRING . "(100) NOT NULL COMMENT 'Дата проведення (Деталі)'",
            'DetailsTime' => Schema::TYPE_STRING . "(20) NOT NULL COMMENT 'Час проведення (Деталі)'",
            'DetailsPlace' => Schema::TYPE_STRING . "(1024) NOT NULL COMMENT 'Місце проведення (Деталі)'",
            'DetailsAddress' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Адреса проведення (Деталі)'",
            'DetailsImage' => Schema::TYPE_STRING . "(1024) NULL COMMENT 'Картинка (Деталі)'",
            'DetailsDescription' => Schema::TYPE_TEXT . " NULL COMMENT 'Опис події (Деталі)'",
            'DetailsType' => Schema::TYPE_TEXT . " NOT NULL COMMENT 'Тип події (серіализований масив - назва-колір)'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`EventId`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
