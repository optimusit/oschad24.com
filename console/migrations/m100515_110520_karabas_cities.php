<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110520_karabas_cities extends Migration
{
    private $_tableName = '{{%karabas_cities}}';
    private $_countriesTableName = '{{%karabas_countries}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL',
            'name' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва міста'",
            'countryId' => "bigint(20) NULL COMMENT 'ID країни'",
            'countryName' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва країни'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'KEY `countryId` (`countryId`)',
        ], $this->_tableOptions);
//		$this->addForeignKey('countries_ibfk_10', $this->_tableName, 'countryId', $this->_countriesTableName, 'id',
//			'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
//		$this->dropForeignKey('countries_ibfk_10', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
