<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100040_order_status_history extends Migration
{
    private $_tableName = '{{%order_status_history}}';
    private $_orderTableName = '{{%order}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'order_id' => "bigint(20) NOT NULL COMMENT 'Замовлення'",
            'status' => "tinyint(4) NOT NULL COMMENT 'Стан'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'KEY `order_id` (`order_id`)',
        ], $this->_tableOptions);

        $this->addForeignKey('order_status_history_ibfk_1', $this->_tableName, 'order_id', $this->_orderTableName, 'id',
            'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('order_status_history_ibfk_1', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
