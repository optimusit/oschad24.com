<?php

use yii\db\Schema;
use console\components\Migration;
use common\models\Service;

class m301010_120000_service_color extends Migration
{
    private $_tableName = 'service';

    public function safeUp()
    {
        try {
            $this->addColumn($this->_tableName, 'color',
                Schema::TYPE_STRING . "(7) NOT NULL DEFAULT '#000000' COMMENT 'Колір'");
        } catch (Exception $e) {
        }
        $service = Service::find()
            ->where(['code' => Service::TICKETS_GD])
            ->one();
        $service->color = '#c2b97e';
        $service->save();

        $service = Service::find()
            ->where(['code' => Service::TICKETS_AVIA])
            ->one();
        $service->color = '#a9b563';
        $service->save();

        $service = Service::find()
            ->where(['code' => Service::TICKETS_BUS])
            ->one();
        $service->color = '#8dc179';
        $service->save();

        $service = Service::find()
            ->where(['code' => Service::TICKETS_ENTERTAINMENTS])
            ->one();
        $service->color = '#7aa7ad';
        $service->save();

        $service = Service::find()
            ->where(['code' => Service::TICKETS_FOOTBALL])
            ->one();
        $service->color = '#77b3da';
        $service->save();

        $service = Service::find()
            ->where(['code' => Service::AIWA_ACCIDENTS5])
            ->one();
        $service->color = '#ba9cc5';
        $service->save();
    }

    public function safeDown()
    {
        $this->dropColumn($this->_tableName, 'color');
    }
}
