<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100015_partner_service extends Migration
{
    private $_tableName = '{{%partner_service}}';
    private $_partnerTableName = '{{%partner}}';
    private $_serviceTableName = '{{%service}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
                'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
                'partner_id' => "bigint(20) NOT NULL COMMENT 'Партнер'",
                'service_id' => "bigint(20) NOT NULL COMMENT 'Сервіс'",
                'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
                'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
                'PRIMARY KEY (`id`)',
            ], $this->_tableOptions);
        $this->createIndex('idx-partner_id-service_id', $this->_tableName, [
                'partner_id',
                'service_id'
            ], true);
        $this->addForeignKey('fk-partner_service-partner', $this->_tableName, 'partner_id', $this->_partnerTableName,
            'id');
        $this->addForeignKey('fk-partner_service-service', $this->_tableName, 'service_id', $this->_serviceTableName,
            'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk-partner_service-partner', $this->_tableName);
        $this->dropForeignKey('fk-partner_service-service', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
