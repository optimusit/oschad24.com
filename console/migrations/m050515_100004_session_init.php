<?php

use console\components\Migration;

class m050515_100004_session_init extends Migration
{
    private $_tableName = '{{%session}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
                'id' => 'CHAR(64) NOT NULL PRIMARY KEY',
                'expire' => 'INTEGER',
                'data' => 'LONGBLOB',
            ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
