<?php

use yii\db\Schema;
use console\components\Migration;

class m250515_100210_message extends Migration
{
    private $_tableName = '{{%message}}';
    private $_sourceMessageTableName = '{{%source_message}}';
    private $_partnerTableName = '{{%partner}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => Schema::TYPE_INTEGER,
            'partner_id' => "bigint(20) NOT NULL COMMENT 'Партнер'",
            'language' => Schema::TYPE_STRING . "(16) NOT NULL COMMENT 'Мова'",
            'translation' => Schema::TYPE_TEXT . " NOT NULL COMMENT 'Переклад'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (id, partner_id, language)',
        ], $this->_tableOptions);
        $this->addForeignKey('fk_message_source_message', $this->_tableName, 'id', $this->_sourceMessageTableName, 'id',
            'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_message_source_partner', $this->_tableName, 'partner_id', $this->_partnerTableName,
            'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('fk_message_source_partner', $this->_tableName);
        $this->dropForeignKey('fk_message_source_message', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
