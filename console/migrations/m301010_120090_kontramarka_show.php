<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120090_kontramarka_show extends Migration
{
    private $_tableName = '{{%kontramarka_show}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'siteId' => 'bigint(20) NOT NULL',
            'showId' => 'bigint(20) NOT NULL',
            'eventId' => 'bigint(20) NOT NULL',
            'hallId' => 'bigint(20) NOT NULL',
            'name' => "varchar(500) NOT NULL COMMENT 'Назва'",
            'description' => "varchar(10000) NOT NULL COMMENT 'Опис'",
            'date' => Schema::TYPE_DATETIME . " NOT NULL COMMENT 'Дата'",
            'poster' => "varchar(2000) NOT NULL COMMENT 'Постер'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`siteId`,`showId`,`eventId`,`hallId`)',
            'KEY `siteId` (`siteId`)',
            'KEY `showId` (`showId`)',
            'KEY `eventId` (`eventId`)',
            'KEY `hallId` (`hallId`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
