<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120050_karabas_partner_events_carousel extends Migration
{
    private $_tableName = '{{%karabas_partner_events_carousel}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'EventId' => "bigint(20) NOT NULL COMMENT 'ID заходу'",
            'PRIMARY KEY (`EventId`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
