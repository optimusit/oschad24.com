<?php

use yii\db\Schema;
use console\components\Migration;

class m050515_100006_gateway_response_description extends Migration
{
    private $_tableName = '{{%gateway_response_description}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'code' => 'tinyint(4) NOT NULL',
            'name_uk' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва Англійська'",
            'name_ru' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва Російська'",
            'name_en' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва Українська'",
            'description_uk' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Опис Англійська'",
            'description_ru' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Опис Російська'",
            'description_en' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Опис Українська'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (code)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
