<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120065_carousel extends Migration
{
    private $_tableName = '{{%carousel}}';
    private $_serviceTableName = '{{%service}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'service_id' => 'bigint(20) NOT NULL',
            'name' => "varchar(100) NOT NULL COMMENT 'Назва'",
            'url' => "varchar(2000) NOT NULL COMMENT 'URL Зображення'",
            'image' => "varchar(2000) NOT NULL COMMENT 'Зображення'",
            'image_file_name' => "varchar(200) NOT NULL COMMENT 'І`мя файла'",
            'show_begin' => Schema::TYPE_DATETIME . " NULL COMMENT 'Час початку показу'",
            'show_end' => Schema::TYPE_DATETIME . " NULL COMMENT 'Час припинення показу'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
        ], $this->_tableOptions);
        $this->addForeignKey('fk-carousel-service', $this->_tableName, 'service_id', $this->_serviceTableName, 'id',
            'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('fk-carousel-service', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
