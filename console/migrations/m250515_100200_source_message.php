<?php

use yii\db\Schema;
use console\components\Migration;

class m250515_100200_source_message extends Migration
{
    private $_tableName = '{{%source_message}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
                'id' => Schema::TYPE_PK,
                'category' => Schema::TYPE_STRING . "(64) NOT NULL COMMENT 'Категорія'",
                'message' => Schema::TYPE_TEXT . " NOT NULL COMMENT 'Повідомлення'",
                'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
                'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
