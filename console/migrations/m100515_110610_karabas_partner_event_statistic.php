<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110610_karabas_partner_event_statistic extends Migration
{
    private $_tableName = '{{%karabas_partner_event_statistic}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'EventId' => 'bigint(20) NOT NULL',
            'Price' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Стоимость'",
            'Quote' => Schema::TYPE_INTEGER . "(5) NOT NULL COMMENT 'Общее кол-во мест в данной ценовой группе.'",
            'Reserved' => Schema::TYPE_INTEGER
                . "(5) NOT NULL COMMENT 'Количество забронированных/купленых билетов в данной ценовой категории'",
            'Free' => Schema::TYPE_INTEGER . "(5) NOT NULL COMMENT 'Кол-во свободных для продажи мест.'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`EventId`, `Price`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
