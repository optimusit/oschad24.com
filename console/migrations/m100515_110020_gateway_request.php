<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110020_gateway_request extends Migration
{
    private $_tableName = '{{%gateway_request}}';
    private $_orderTableName = '{{%order}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'order_id' => "bigint(20) NOT NULL COMMENT 'Ордер'",
            'terminal' => Schema::TYPE_STRING . "(8) NOT NULL COMMENT 'Термінал'",
            'method' => Schema::TYPE_STRING . "(18) NOT NULL COMMENT 'Метод оплати'",
            'trtype' => "tinyint(4) NOT NULL COMMENT 'Тип запиту'",
            'amount' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Сума'",
            'currency' => Schema::TYPE_STRING . "(3) NOT NULL COMMENT 'Валюта'",
            'timestamp' => Schema::TYPE_STRING . "(14) NOT NULL COMMENT 'Відбиток часу'",
            'nonce' => Schema::TYPE_STRING . "(64) NOT NULL COMMENT 'nonce'",
            'action' => "tinyint(4) NULL COMMENT 'action'",
            'rc' => "tinyint(4) NULL COMMENT 'rc'",
            'rrn' => Schema::TYPE_STRING . "(12) NULL COMMENT 'rrn'",
            'int_ref' => Schema::TYPE_STRING . "(32) NULL COMMENT 'int_ref'",
            'auth_code' => Schema::TYPE_STRING . "(64) NULL COMMENT 'auth_code'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            //'KEY `order_id` (`order_id`)',
        ], $this->_tableOptions);

        $this->addForeignKey('gateway_request_ibfk_10', $this->_tableName, 'order_id', $this->_orderTableName, 'id',
            'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('gateway_request_ibfk_10', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
