<?php

use console\components\Migration;

class m200515_100200_dict_gateway_response_description extends Migration
{
    public function up()
    {

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -1;
        $gatewayResponseDescription->name_uk = 'Обов`язкове поле в запиті незаповнене';
        $gatewayResponseDescription->name_ru = 'Обов`язкове поле в запиті незаповнене';
        $gatewayResponseDescription->name_en = 'Mandatory field is empty';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -2;
        $gatewayResponseDescription->name_uk = 'Запит не відповідає вимогам специфікації';
        $gatewayResponseDescription->name_ru = 'Запит не відповідає вимогам специфікації';
        $gatewayResponseDescription->name_en = 'Bad CGI request';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -3;
        $gatewayResponseDescription->name_uk = 'Комунікаційний сервер не відповідає або невірний формат файла відповіді';
        $gatewayResponseDescription->name_ru = 'Комунікаційний сервер не відповідає або невірний формат файла відповіді';
        $gatewayResponseDescription->name_en = 'No or Invalid response received';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -4;
        $gatewayResponseDescription->name_uk = 'Відсутній зв`язок з комунікаційним сервером';
        $gatewayResponseDescription->name_ru = 'Відсутній зв`язок з комунікаційним сервером';
        $gatewayResponseDescription->name_en = 'Server is not responding';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -5;
        $gatewayResponseDescription->name_uk = 'Зв`язок з комунікаційним сервером не конфігуровано';
        $gatewayResponseDescription->name_ru = 'Зв`язок з комунікаційним сервером не конфігуровано';
        $gatewayResponseDescription->name_en = 'Connect failed';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -6;
        $gatewayResponseDescription->name_uk = 'Помилка конфігурації e-Gateway';
        $gatewayResponseDescription->name_ru = 'Помилка конфігурації e-Gateway';
        $gatewayResponseDescription->name_en = 'Configuration error';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -7;
        $gatewayResponseDescription->name_uk = 'Помилкова відповідь від комунікаційного сервера';
        $gatewayResponseDescription->name_ru = 'Помилкова відповідь від комунікаційного сервера';
        $gatewayResponseDescription->name_en = 'Invalid response';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -8;
        $gatewayResponseDescription->name_uk = 'Помилка в полі "Card number"';
        $gatewayResponseDescription->name_ru = 'Помилка в полі "Card number"';
        $gatewayResponseDescription->name_en = 'Error in card number field';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -9;
        $gatewayResponseDescription->name_uk = 'Помилка в полі "Card expiration date"';
        $gatewayResponseDescription->name_ru = 'Помилка в полі "Card expiration date"';
        $gatewayResponseDescription->name_en = 'Error in card expiration date field';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -10;
        $gatewayResponseDescription->name_uk = 'Помилка в полі "Amount"';
        $gatewayResponseDescription->name_ru = 'Помилка в полі "Amount"';
        $gatewayResponseDescription->name_en = 'Error in amount field';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -11;
        $gatewayResponseDescription->name_uk = 'Помилка в полі "Currency"';
        $gatewayResponseDescription->name_ru = 'Помилка в полі "Currency"';
        $gatewayResponseDescription->name_en = 'Error in currency field';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -12;
        $gatewayResponseDescription->name_uk = 'Помилка в полі "Merchant ID"';
        $gatewayResponseDescription->name_ru = 'Помилка в полі "Merchant ID"';
        $gatewayResponseDescription->name_en = 'Error in merchant terminal field';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -13;
        $gatewayResponseDescription->name_uk = 'IP-адреса не така як очікується';
        $gatewayResponseDescription->name_ru = 'IP-адреса не така як очікується';
        $gatewayResponseDescription->name_en = 'Unknown referrer';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -15;
        $gatewayResponseDescription->name_uk = 'Помилка в полі "RRN"';
        $gatewayResponseDescription->name_ru = 'Помилка в полі "RRN"';
        $gatewayResponseDescription->name_en = 'Invalid Retrieval reference number';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -16;
        $gatewayResponseDescription->name_uk = 'Термінал тимчасово заблоковано, спробуйте ще раз.';
        $gatewayResponseDescription->name_ru = 'Термінал тимчасово заблоковано, спробуйте ще раз.';
        $gatewayResponseDescription->name_en = 'Terminal is locked, please try again';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -17;
        $gatewayResponseDescription->name_uk = 'Доступ заборонено';
        $gatewayResponseDescription->name_ru = 'Доступ заборонено';
        $gatewayResponseDescription->name_en = 'Access denied';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -18;
        $gatewayResponseDescription->name_uk = 'Помилка в CVC2 або в описі CVC2';
        $gatewayResponseDescription->name_ru = 'Помилка в CVC2 або в описі CVC2';
        $gatewayResponseDescription->name_en = 'Error in CVC2 or CVC2 Description fields';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -19;
        $gatewayResponseDescription->name_uk = 'Помилка аутентифікації';
        $gatewayResponseDescription->name_ru = 'Помилка аутентифікації';
        $gatewayResponseDescription->name_en = 'Authentication failed';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -20;
        $gatewayResponseDescription->name_uk = 'Перевищено час проведення транзакції';
        $gatewayResponseDescription->name_ru = 'Перевищено час проведення транзакції';
        $gatewayResponseDescription->name_en = 'Expired transaction';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -21;
        $gatewayResponseDescription->name_uk = 'Дублікат транзакції';
        $gatewayResponseDescription->name_ru = 'Дублікат транзакції';
        $gatewayResponseDescription->name_en = 'Duplicate transaction';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = -22;
        $gatewayResponseDescription->name_uk = 'Помилка при аутентифікації клієнта';
        $gatewayResponseDescription->name_ru = 'Помилка при аутентифікації клієнта';
        $gatewayResponseDescription->name_en = 'Error in the request information required for client identification';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 0;
        $gatewayResponseDescription->name_uk = 'Підтвердити (успішне виконання)';
        $gatewayResponseDescription->name_ru = 'Підтвердити (успішне виконання)';
        $gatewayResponseDescription->name_en = 'Approved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 1;
        $gatewayResponseDescription->name_uk = 'Зверніться до емітента картки';
        $gatewayResponseDescription->name_ru = 'Зверніться до емітента картки';
        $gatewayResponseDescription->name_en = 'Call your bank';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 2;
        $gatewayResponseDescription->name_uk = 'Зверніться до емітента картки - спеціальні умови';
        $gatewayResponseDescription->name_ru = 'Зверніться до емітента картки - спеціальні умови';
        $gatewayResponseDescription->name_en = 'Call your bank';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 3;
        $gatewayResponseDescription->name_uk = 'Відмовити, підприємство не приймає даний вид карт';
        $gatewayResponseDescription->name_ru = 'Відмовити, підприємство не приймає даний вид карт';
        $gatewayResponseDescription->name_en = 'Invalid merchant';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 4;
        $gatewayResponseDescription->name_uk = 'Відмовити, картка заблокована';
        $gatewayResponseDescription->name_ru = 'Відмовити, картка заблокована';
        $gatewayResponseDescription->name_en = 'Your card is restricted';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 5;
        $gatewayResponseDescription->name_uk = 'Відмовити, операція відхилена';
        $gatewayResponseDescription->name_ru = 'Відмовити, операція відхилена';
        $gatewayResponseDescription->name_en = 'Transaction declined';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 6;
        $gatewayResponseDescription->name_uk = 'Помилка - повторіть запит';
        $gatewayResponseDescription->name_ru = 'Помилка - повторіть запит';
        $gatewayResponseDescription->name_en = 'Error - retry';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 7;
        $gatewayResponseDescription->name_uk = 'Відмовити, картка заблокована';
        $gatewayResponseDescription->name_ru = 'Відмовити, картка заблокована';
        $gatewayResponseDescription->name_en = 'Your card is disabled';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 8;
        $gatewayResponseDescription->name_uk = 'Необхідна додаткова ідентифікація';
        $gatewayResponseDescription->name_ru = 'Необхідна додаткова ідентифікація';
        $gatewayResponseDescription->name_en = 'Additional identification required';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 9;
        $gatewayResponseDescription->name_uk = 'Запит в процесі обробки';
        $gatewayResponseDescription->name_ru = 'Запит в процесі обробки';
        $gatewayResponseDescription->name_en = 'Request in progress';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 10;
        $gatewayResponseDescription->name_uk = 'Підтвердити для часткової суми операції';
        $gatewayResponseDescription->name_ru = 'Підтвердити для часткової суми операції';
        $gatewayResponseDescription->name_en = 'Partially approved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 11;
        $gatewayResponseDescription->name_uk = 'Підтвердити для особливо важливої персони (VIP)';
        $gatewayResponseDescription->name_ru = 'Підтвердити для особливо важливої персони (VIP)';
        $gatewayResponseDescription->name_en = 'Approved (VIP)';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 12;
        $gatewayResponseDescription->name_uk = 'Відмовити, невідомий тип операції';
        $gatewayResponseDescription->name_ru = 'Відмовити, невідомий тип операції';
        $gatewayResponseDescription->name_en = 'Invalid transaction';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 13;
        $gatewayResponseDescription->name_uk = 'Відмовити, некоректна сума операції';
        $gatewayResponseDescription->name_ru = 'Відмовити, некоректна сума операції';
        $gatewayResponseDescription->name_en = 'Invalid amount';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 14;
        $gatewayResponseDescription->name_uk = 'Відмовити, картку не знайдено';
        $gatewayResponseDescription->name_ru = 'Відмовити, картку не знайдено';
        $gatewayResponseDescription->name_en = 'No such card';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 15;
        $gatewayResponseDescription->name_uk = 'Відмовити, емітент не існує';
        $gatewayResponseDescription->name_ru = 'Відмовити, емітент не існує';
        $gatewayResponseDescription->name_en = 'No such card/issuer';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 16;
        $gatewayResponseDescription->name_uk = 'Підтвердити, поновити третю доріжку картки';
        $gatewayResponseDescription->name_ru = 'Підтвердити, поновити третю доріжку картки';
        $gatewayResponseDescription->name_en = 'Approved, update track 3';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 17;
        $gatewayResponseDescription->name_uk = 'Відмовити, відмова користувача';
        $gatewayResponseDescription->name_ru = 'Відмовити, відмова користувача';
        $gatewayResponseDescription->name_en = 'Customer cancellation';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 18;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Customer dispute';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 19;
        $gatewayResponseDescription->name_uk = 'Відмовити, повторити операцію';
        $gatewayResponseDescription->name_ru = 'Відмовити, повторити операцію';
        $gatewayResponseDescription->name_en = 'Re-enter transaction';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 20;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Invalid response';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 21;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'No action taken';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 22;
        $gatewayResponseDescription->name_uk = 'Помилка в роботі системи';
        $gatewayResponseDescription->name_ru = 'Помилка в роботі системи';
        $gatewayResponseDescription->name_en = 'Suspected malfunction';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 23;
        $gatewayResponseDescription->name_uk = 'Відмовити, неакцептовані витрати операції';
        $gatewayResponseDescription->name_ru = 'Відмовити, неакцептовані витрати операції';
        $gatewayResponseDescription->name_en = 'Unacceptable fee';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 24;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Update not supported';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 25;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'No such record';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 26;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Duplicate update/replaced';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 27;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Field update error';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 28;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'File locked out';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 29;
        $gatewayResponseDescription->name_uk = 'Помилка, зв`яжіться з центром обробки';
        $gatewayResponseDescription->name_ru = 'Помилка, зв`яжіться з центром обробки';
        $gatewayResponseDescription->name_en = 'Error, contact acquirer';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 30;
        $gatewayResponseDescription->name_uk = 'Відмовити, помилка в форматі запиту';
        $gatewayResponseDescription->name_ru = 'Відмовити, помилка в форматі запиту';
        $gatewayResponseDescription->name_en = 'Format error';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 31;
        $gatewayResponseDescription->name_uk = 'Відмовити, емітент тимчасово відключився';
        $gatewayResponseDescription->name_ru = 'Відмовити, емітент тимчасово відключився';
        $gatewayResponseDescription->name_en = 'Issuer signed-off';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 32;
        $gatewayResponseDescription->name_uk = 'Часткове закінчення';
        $gatewayResponseDescription->name_ru = 'Часткове закінчення';
        $gatewayResponseDescription->name_en = 'Completed partially';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 33;
        $gatewayResponseDescription->name_uk = 'Відмовити, термін дії картки вичерпано';
        $gatewayResponseDescription->name_ru = 'Відмовити, термін дії картки вичерпано';
        $gatewayResponseDescription->name_en = 'Expired card';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 34;
        $gatewayResponseDescription->name_uk = 'Відмовити, підозра у шахрайстві';
        $gatewayResponseDescription->name_ru = 'Відмовити, підозра у шахрайстві';
        $gatewayResponseDescription->name_en = 'Suspected fraud';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 35;
        $gatewayResponseDescription->name_uk = 'Відмовити, підприємству зв`язатись з емітентом';
        $gatewayResponseDescription->name_ru = 'Відмовити, підприємству зв`язатись з емітентом';
        $gatewayResponseDescription->name_en = 'Acceptor contact acquirer';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 36;
        $gatewayResponseDescription->name_uk = 'Відмовити, картка блокована';
        $gatewayResponseDescription->name_ru = 'Відмовити, картка блокована';
        $gatewayResponseDescription->name_en = 'Restricted card';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 37;
        $gatewayResponseDescription->name_uk = 'Відмовити, зв`яжіться зі своїм банком';
        $gatewayResponseDescription->name_ru = 'Відмовити, зв`яжіться зі своїм банком';
        $gatewayResponseDescription->name_en = 'Call your bank';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 38;
        $gatewayResponseDescription->name_uk = 'Відмовити, перевищено кількість спроб вводу ПІН';
        $gatewayResponseDescription->name_ru = 'Відмовити, перевищено кількість спроб вводу ПІН';
        $gatewayResponseDescription->name_en = 'PIN tries exceeded';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 39;
        $gatewayResponseDescription->name_uk = 'Відмовити, кредитного рахунку немає';
        $gatewayResponseDescription->name_ru = 'Відмовити, кредитного рахунку немає';
        $gatewayResponseDescription->name_en = 'No credit account';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 40;
        $gatewayResponseDescription->name_uk = 'Відмовити, функція не підтримується';
        $gatewayResponseDescription->name_ru = 'Відмовити, функція не підтримується';
        $gatewayResponseDescription->name_en = 'Function not supported';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 41;
        $gatewayResponseDescription->name_uk = 'Відмовити, картка загублена';
        $gatewayResponseDescription->name_ru = 'Відмовити, картка загублена';
        $gatewayResponseDescription->name_en = 'Lost card';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 42;
        $gatewayResponseDescription->name_uk = 'Відмовити, універсального рахунку немає';
        $gatewayResponseDescription->name_ru = 'Відмовити, універсального рахунку немає';
        $gatewayResponseDescription->name_en = 'No universal account';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 43;
        $gatewayResponseDescription->name_uk = 'Відмовити, картку викрадено';
        $gatewayResponseDescription->name_ru = 'Відмовити, картку викрадено';
        $gatewayResponseDescription->name_en = 'Stolen card';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 44;
        $gatewayResponseDescription->name_uk = 'Відмовити, інвестиційного рахунку немає';
        $gatewayResponseDescription->name_ru = 'Відмовити, інвестиційного рахунку немає';
        $gatewayResponseDescription->name_en = 'No investment account';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 45;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 46;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 47;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 48;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 49;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 50;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 51;
        $gatewayResponseDescription->name_uk = 'Відмовити, недостатньо коштів';
        $gatewayResponseDescription->name_ru = 'Відмовити, недостатньо коштів';
        $gatewayResponseDescription->name_en = 'Not sufficient funds';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 52;
        $gatewayResponseDescription->name_uk = 'Відмовити, чекового рахунку немає';
        $gatewayResponseDescription->name_ru = 'Відмовити, чекового рахунку немає';
        $gatewayResponseDescription->name_en = 'No chequing account';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 53;
        $gatewayResponseDescription->name_uk = 'Відмовити, ощадного рахунку немає';
        $gatewayResponseDescription->name_ru = 'Відмовити, ощадного рахунку немає';
        $gatewayResponseDescription->name_en = 'No savings account';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 54;
        $gatewayResponseDescription->name_uk = 'Відмовити, термін дії картки вичерпано';
        $gatewayResponseDescription->name_ru = 'Відмовити, термін дії картки вичерпано';
        $gatewayResponseDescription->name_en = 'Expired card';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 55;
        $gatewayResponseDescription->name_uk = 'Відмовити, некоректний ПІН';
        $gatewayResponseDescription->name_ru = 'Відмовити, некоректний ПІН';
        $gatewayResponseDescription->name_en = 'Incorrect PIN';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 56;
        $gatewayResponseDescription->name_uk = 'Відмовити, інформація про картку відсутня';
        $gatewayResponseDescription->name_ru = 'Відмовити, інформація про картку відсутня';
        $gatewayResponseDescription->name_en = 'No card record';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 57;
        $gatewayResponseDescription->name_uk = 'Відмовити, операцію не дозволено';
        $gatewayResponseDescription->name_ru = 'Відмовити, операцію не дозволено';
        $gatewayResponseDescription->name_en = 'Not permitted to client';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 58;
        $gatewayResponseDescription->name_uk = 'Відмовити, невідомий тип картки';
        $gatewayResponseDescription->name_ru = 'Відмовити, невідомий тип картки';
        $gatewayResponseDescription->name_en = 'Not permitted to merchant';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 59;
        $gatewayResponseDescription->name_uk = 'Відмовити, невірний CVC або термін дії картки';
        $gatewayResponseDescription->name_ru = 'Відмовити, невірний CVC або термін дії картки';
        $gatewayResponseDescription->name_en = 'Suspected fraud';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 60;
        $gatewayResponseDescription->name_uk = 'Відмовити, підприємству зв`язатись з центром обробки';
        $gatewayResponseDescription->name_ru = 'Відмовити, підприємству зв`язатись з центром обробки';
        $gatewayResponseDescription->name_en = 'Acceptor call acquirer';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 61;
        $gatewayResponseDescription->name_uk = 'Відмовити, перевищено ліміт суми операцій';
        $gatewayResponseDescription->name_ru = 'Відмовити, перевищено ліміт суми операцій';
        $gatewayResponseDescription->name_en = 'Exceeds amount limit';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 62;
        $gatewayResponseDescription->name_uk = 'Відмовити, картка блокована';
        $gatewayResponseDescription->name_ru = 'Відмовити, картка блокована';
        $gatewayResponseDescription->name_en = 'Restricted card';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 63;
        $gatewayResponseDescription->name_uk = 'Помилка, порушення безпеки системи';
        $gatewayResponseDescription->name_ru = 'Помилка, порушення безпеки системи';
        $gatewayResponseDescription->name_en = 'Security violation';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 64;
        $gatewayResponseDescription->name_uk = 'Відмовити, невірна оригінальна сума операції';
        $gatewayResponseDescription->name_ru = 'Відмовити, невірна оригінальна сума операції';
        $gatewayResponseDescription->name_en = 'Wrong original amount';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 65;
        $gatewayResponseDescription->name_uk = 'Відмовити, перевищено ліміт повторень операції';
        $gatewayResponseDescription->name_ru = 'Відмовити, перевищено ліміт повторень операції';
        $gatewayResponseDescription->name_en = 'Exceeds frequency limit';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 66;
        $gatewayResponseDescription->name_uk = 'Відмовити, підприємству зв`язатись з центром обробки';
        $gatewayResponseDescription->name_ru = 'Відмовити, підприємству зв`язатись з центром обробки';
        $gatewayResponseDescription->name_en = 'Acceptor call acquirer';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 67;
        $gatewayResponseDescription->name_uk = 'Відмовити, якщо операція в АТМ';
        $gatewayResponseDescription->name_ru = 'Відмовити, якщо операція в АТМ';
        $gatewayResponseDescription->name_en = 'Pick up at ATM';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 68;
        $gatewayResponseDescription->name_uk = 'Помилка, немає відповіді у відведений час';
        $gatewayResponseDescription->name_ru = 'Помилка, немає відповіді у відведений час';
        $gatewayResponseDescription->name_en = 'Reply received too late';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 69;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 70;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 71;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 72;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 73;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 74;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 75;
        $gatewayResponseDescription->name_uk = 'Відмовити, перевищено кількість спроб вводу ПІН';
        $gatewayResponseDescription->name_ru = 'Відмовити, перевищено кількість спроб вводу ПІН';
        $gatewayResponseDescription->name_en = 'PIN tries exceeded';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 76;
        $gatewayResponseDescription->name_uk = 'Відмовити, невірний ПІН, перевищено кількість спроб';
        $gatewayResponseDescription->name_ru = 'Відмовити, невірний ПІН, перевищено кількість спроб';
        $gatewayResponseDescription->name_en = 'Wrong PIN,tries exceeded';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 77;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Wrong Reference No.';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 78;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 79;
        $gatewayResponseDescription->name_uk = 'Помилка, вже відреверсовано';
        $gatewayResponseDescription->name_ru = 'Помилка, вже відреверсовано';
        $gatewayResponseDescription->name_en = 'Already reversed';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 80;
        $gatewayResponseDescription->name_uk = 'Відмовити, помилка авторизаційної мережі';
        $gatewayResponseDescription->name_ru = 'Відмовити, помилка авторизаційної мережі';
        $gatewayResponseDescription->name_en = 'Network error';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 81;
        $gatewayResponseDescription->name_uk = 'Відмовити, помилка зовнішньої мережі';
        $gatewayResponseDescription->name_ru = 'Відмовити, помилка зовнішньої мережі';
        $gatewayResponseDescription->name_en = 'Foreign network error';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 82;
        $gatewayResponseDescription->name_uk = 'Відмовити, тайм-аут мережі зв`язку / Невірний CVV';
        $gatewayResponseDescription->name_ru = 'Відмовити, тайм-аут мережі зв`язку / Невірний CVV';
        $gatewayResponseDescription->name_en = 'Time-out at issuer';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 83;
        $gatewayResponseDescription->name_uk = 'Відмовити, помилка операції';
        $gatewayResponseDescription->name_ru = 'Відмовити, помилка операції';
        $gatewayResponseDescription->name_en = 'Transaction failed';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 84;
        $gatewayResponseDescription->name_uk = 'Відмовити, перевищено час преавторизації';
        $gatewayResponseDescription->name_ru = 'Відмовити, перевищено час преавторизації';
        $gatewayResponseDescription->name_en = 'Pre-authorization timed out';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 85;
        $gatewayResponseDescription->name_uk = 'Відмовити, необхідна перевірка рахунку';
        $gatewayResponseDescription->name_ru = 'Відмовити, необхідна перевірка рахунку';
        $gatewayResponseDescription->name_en = 'Account verification required';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 86;
        $gatewayResponseDescription->name_uk = 'Відмовити, перевірка ПІН неможлива';
        $gatewayResponseDescription->name_ru = 'Відмовити, перевірка ПІН неможлива';
        $gatewayResponseDescription->name_en = 'Unable to verify PIN';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 87;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 88;
        $gatewayResponseDescription->name_uk = 'Відмовити, помилка криптографії';
        $gatewayResponseDescription->name_ru = 'Відмовити, помилка криптографії';
        $gatewayResponseDescription->name_en = 'Cryptographic failure';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 89;
        $gatewayResponseDescription->name_uk = 'Відмовити, помилка аутентифікації';
        $gatewayResponseDescription->name_ru = 'Відмовити, помилка аутентифікації';
        $gatewayResponseDescription->name_en = 'Authentication failure';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 90;
        $gatewayResponseDescription->name_uk = 'Відмовити, повторіть через деякий час';
        $gatewayResponseDescription->name_ru = 'Відмовити, повторіть через деякий час';
        $gatewayResponseDescription->name_en = 'Cutoff is in progress';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 91;
        $gatewayResponseDescription->name_uk = 'Відмовити, емітент чи вузол комутації недоступний';
        $gatewayResponseDescription->name_ru = 'Відмовити, емітент чи вузол комутації недоступний';
        $gatewayResponseDescription->name_en = 'Issuer unavailable';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 92;
        $gatewayResponseDescription->name_uk = 'Відмовити, неможлива адресація запиту';
        $gatewayResponseDescription->name_ru = 'Відмовити, неможлива адресація запиту';
        $gatewayResponseDescription->name_en = 'Router unavailable';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 93;
        $gatewayResponseDescription->name_uk = 'Відмовити, порушення закону';
        $gatewayResponseDescription->name_ru = 'Відмовити, порушення закону';
        $gatewayResponseDescription->name_en = 'Violation of law';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 94;
        $gatewayResponseDescription->name_uk = 'Відмовити, повторний запит';
        $gatewayResponseDescription->name_ru = 'Відмовити, повторний запит';
        $gatewayResponseDescription->name_en = 'Duplicate transmission';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 95;
        $gatewayResponseDescription->name_uk = 'Відмовити, помилка узгодження';
        $gatewayResponseDescription->name_ru = 'Відмовити, помилка узгодження';
        $gatewayResponseDescription->name_en = 'Reconcile error';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 96;
        $gatewayResponseDescription->name_uk = 'Відмовити, помилка в роботі системи';
        $gatewayResponseDescription->name_ru = 'Відмовити, помилка в роботі системи';
        $gatewayResponseDescription->name_en = 'System malfunction';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 97;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 98;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();

        $gatewayResponseDescription = new \common\models\GatewayResponseDescription();
        $gatewayResponseDescription->code = 99;
        $gatewayResponseDescription->name_uk = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_ru = 'Помилка, неприпустимий код відповіді';
        $gatewayResponseDescription->name_en = 'Reserved';
        $gatewayResponseDescription->description_uk = '-';
        $gatewayResponseDescription->description_ru = '-';
        $gatewayResponseDescription->description_en = '-';
        $gatewayResponseDescription->insert();
    }

    public function down()
    {
    }
}
