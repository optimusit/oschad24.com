<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_111200_aiwa_document extends Migration
{
    private $_tableName = '{{%aiwa_document}}';
    private $_orderTableName = '{{%order}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            // общая часть для всех документов
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'order_id' => "bigint(20) NOT NULL COMMENT 'Замовлення'",
            'doc_type' => Schema::TYPE_STRING . "(30) NOT NULL COMMENT 'Тип документу'",
            'system_id' => "bigint(20) NOT NULL COMMENT '№ в системі постачальника'",
            'amount' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Страховий платіж'",
            'author' => Schema::TYPE_STRING . "(200) NOT NULL COMMENT 'Електронна адреса'",
            'cover' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Страховая сумма'",
            'currency' => Schema::TYPE_SMALLINT . "(3) NULL DEFAULT '980' COMMENT 'Код валюты'",
            'date_from' => Schema::TYPE_DATE . " NOT NULL COMMENT 'Дата початку дії договору'",
            'date_period' => Schema::TYPE_STRING . "(10) NOT NULL COMMENT 'Період'",
            'date_to' => Schema::TYPE_DATE . " NOT NULL COMMENT 'Дата закінчення дії договору'",
            'doc_date' => Schema::TYPE_DATE . " NOT NULL COMMENT 'Дата створення документу'",
            'doc_number' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT '№ документу'",
            'draft' => "tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Чернетка'",
            // Несчастный случай + Здоровье
            'plan' => "char(1) NOT NULL DEFAULT 'A' COMMENT 'Варіант покриття'",
            'programm' => "tinyint(1) NOT NULL DEFAULT 5 COMMENT 'Код программы: 5'",
            'variant' => "tinyint(1) NOT NULL COMMENT 'Застраховані особи:1-родичі,2-дружина і діти'",

            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'KEY `order_id` (`order_id`,`system_id`)',
        ], $this->_tableOptions);

        $this->addForeignKey('aiwa_doc_order_ibfk_10', $this->_tableName, 'order_id', $this->_orderTableName, 'id',
            'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('aiwa_doc_order_ibfk_10', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
