<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110010_customer extends Migration
{
    private $_tableName = '{{%customer}}';
    private $_orderTableName = '{{%order}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'partner_id' => "bigint(20) NOT NULL COMMENT 'Партнер'",
            'email' => Schema::TYPE_STRING . "(200) NULL COMMENT 'Електронна адреса'",
            'phone' => Schema::TYPE_STRING . "(13) NOT NULL COMMENT 'Телефон'",
            'ipn' => Schema::TYPE_STRING . "(12) NULL COMMENT 'ІПН'",
            'name' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Ім`я'",
            'surname' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Прізвище'",
            'patronymic' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Побатькові'",
            'birthday' => Schema::TYPE_DATE . " NULL COMMENT 'Дата народження'",
            'gender' => "set('m','f') NULL COMMENT 'Стать'",
            'passport_series' => Schema::TYPE_STRING . "(2) NULL COMMENT 'Серія паспорту'",
            'passport_number' => Schema::TYPE_STRING . "(8) NULL COMMENT '№ паспорту'",
            'passport_date' => Schema::TYPE_DATE . " NULL COMMENT 'Дата видачі паспорту'",
            'passport_issuer' => Schema::TYPE_STRING . "(250) NULL COMMENT 'Орган що видав'",
            'address_city' => Schema::TYPE_STRING . "(250) NULL COMMENT 'Населений пункт'",
            'address_index' => Schema::TYPE_STRING . "(6) NULL COMMENT 'Індекс'",
            'address_address' => Schema::TYPE_STRING . "(250) NULL COMMENT 'Адреса'",
            'confirm' => "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Підтверджено'",
            'confirm_key' => Schema::TYPE_STRING . "(100) NOT NULL COMMENT 'Ключ підтвердження'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'UNIQUE INDEX `phone` (`partner_id`,`phone`)',
            'UNIQUE INDEX `ipn` (`partner_id`,`ipn`)',
        ], $this->_tableOptions);

        $this->addForeignKey('order_ibfk_10', $this->_orderTableName, 'customer_id', $this->_tableName, 'id',
            'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('order_ibfk_10', $this->_orderTableName);
        $this->dropTable($this->_tableName);
    }
}
