<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120080_kontramarka_site extends Migration
{
    private $_tableName = '{{%kontramarka_site}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'siteId' => 'bigint(20) NOT NULL',
            'name' => "varchar(500) NOT NULL COMMENT 'Назва'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`siteId`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
