<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100050_order_provider_history extends Migration
{
    private $_tableName = '{{%order_provider_history}}';
    private $_orderTableName = '{{%order}}';

    public function up()
    {
        $this->_tableOptions .= " comment = 'Історія запитів до провайдера'";
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'order_id' => "bigint(20) NOT NULL COMMENT 'Ордер'",
            'request' => Schema::TYPE_TEXT . " NOT NULL COMMENT 'Запит'",
            'response' => Schema::TYPE_TEXT . " NOT NULL COMMENT 'Відповідь'",
            'response_format' => Schema::TYPE_STRING . "(20) NOT NULL COMMENT 'Формат відповіді'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
        ], $this->_tableOptions);

        $this->addForeignKey('order_provider_history_ibfk_0', $this->_tableName, 'order_id', $this->_orderTableName,
            'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('order_provider_history_ibfk_0', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
