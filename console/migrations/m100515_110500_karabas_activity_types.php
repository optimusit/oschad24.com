<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110500_karabas_activity_types extends Migration
{
    private $_tableName = '{{%karabas_activity_types}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL',
            'name' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва типу події'",
            'color' => Schema::TYPE_STRING . "(6) NOT NULL COMMENT 'Колір типу події'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
