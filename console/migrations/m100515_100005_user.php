<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100005_user extends Migration
{
    private $_tableName = '{{%user}}';
    private $_partnerTableName = '{{%partner}}';

    public function up()
    {
        // это сделано для того, чтобы ИД пользователя системы и партнера не пересекались в RBAC
        // т.к. в одной таблице (auth_assignment) будут жить и партнеры и пользователи
        $this->_tableOptions .= ' AUTO_INCREMENT=100000';
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'partner_id' => "bigint(20) NOT NULL COMMENT 'Партнер'",
            'email' => Schema::TYPE_STRING . "(200) NOT NULL COMMENT 'Поштова адреса'",
            'change_email' => Schema::TYPE_STRING . "(20) NULL COMMENT 'Нова поштова адреса'",
            'phone' => Schema::TYPE_STRING . "(13) NOT NULL COMMENT 'Телефон'",
            'sms_code' => Schema::TYPE_STRING . "(32) NULL COMMENT 'SMS код'",
            'sms_time' => Schema::TYPE_DATETIME . " NULL COMMENT 'SMS час'",
            'ipn' => Schema::TYPE_STRING . "(12) NULL COMMENT 'ІПН'",
            'surname' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Прізвище'",
            'name' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Ім`я'",
            'patronymic' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Побатькові'",
            'birthday' => Schema::TYPE_DATE . " NULL COMMENT 'Дата народження'",
            'gender' => "set('m','f') NULL COMMENT 'Стать'",
            'passport_series' => Schema::TYPE_STRING . "(2) NULL COMMENT 'Серія паспорту'",
            'passport_number' => Schema::TYPE_STRING . "(8) NULL COMMENT '№ паспорту'",
            'passport_date' => Schema::TYPE_DATE . " NULL COMMENT 'Дата видачі паспорту'",
            'passport_issuer' => Schema::TYPE_STRING . "(250) NULL COMMENT 'Орган що видав'",
            'address_city' => Schema::TYPE_STRING . "(250) NULL COMMENT 'Населений пункт'",
            'address_index' => Schema::TYPE_STRING . "(6) NULL COMMENT 'Індекс'",
            'address_address' => Schema::TYPE_STRING . "(250) NULL COMMENT 'Адреса'",
            'auth_key' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Ключ доступу'",
            'auth_key_remember_forever' => "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Термін придатності ключа доступу ніколи не скінчується'",
            'auth_key_expiration' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Термін придатності ключа доступу'",
            'password_hash' => Schema::TYPE_STRING . " NOT NULL COMMENT 'Хеш паролю'",
            'password_reset_token' => Schema::TYPE_STRING . " COMMENT 'Маркер сбросу пароля'",
            'status' => Schema::TYPE_SMALLINT . " NOT NULL DEFAULT 10 COMMENT 'Стан'",
            'confirm' => "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Підтверджено'",
            'confirm_key' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Ключ підтвердження'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'UNIQUE INDEX `phone` (`partner_id`,`phone`)',
            'UNIQUE INDEX `ipn` (`partner_id`,`ipn`)',
        ], $this->_tableOptions);
        $this->createIndex('idx-partner_id', $this->_tableName, [
            'partner_id',
        ], false);
        $this->createIndex('ukey-partner-username', $this->_tableName, [
            'partner_id',
            'email',
        ], true);
        $this->addForeignKey('fk-user_partner', $this->_tableName, 'partner_id', $this->_partnerTableName, 'id',
            'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('fk-user_partner', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
