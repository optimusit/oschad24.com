<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100013_service_provider extends Migration
{
    private $_tableName = '{{%service_provider%}}';
    private $_serviceTableName = '{{%service%}}';
    private $_providerTableName = '{{%provider%}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
                'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
                'service_id' => "bigint(20) NOT NULL COMMENT 'Сервіс'",
                'provider_id' => "bigint(20) NOT NULL COMMENT 'Провайдер'",
                'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
                'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
                'PRIMARY KEY (`id`)',
            ], $this->_tableOptions);
        $this->createIndex('idx-service_id-provider_id', $this->_tableName, [
                'provider_id',
                'service_id'
            ], true);
        $this->addForeignKey('fk-service_provider-provider', $this->_tableName, 'provider_id',
            $this->_providerTableName, 'id');
        $this->addForeignKey('fk-service_provider-service', $this->_tableName, 'service_id', $this->_serviceTableName,
            'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk-service_provider-provider', $this->_tableName);
        $this->dropForeignKey('fk-service_provider-service', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
