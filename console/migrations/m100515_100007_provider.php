<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100007_provider extends Migration
{
    private $_tableName = '{{%provider}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
                'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
                'prefix' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Префікс в системі'",
                'name' => Schema::TYPE_STRING . " NOT NULL COMMENT 'Назва'",
                'email' => Schema::TYPE_STRING . " NOT NULL COMMENT 'Поштова адреса'",
                'phone' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Телефон'",
                'access_token' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Маркер доступу'",
                'secret_key' => Schema::TYPE_STRING . "(60) NOT NULL COMMENT 'Секретний ключ'",
                'status' => Schema::TYPE_BOOLEAN . " NOT NULL DEFAULT 1 COMMENT 'Стан'",
                'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
                'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
                'PRIMARY KEY (`id`)',
            ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
