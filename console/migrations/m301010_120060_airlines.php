<?php

use console\components\Migration;

class m301010_120060_airlines extends Migration
{
    private $_tableName = '{{%airlines}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'iata' => "char(2) NOT NULL COMMENT 'IATA код'",
            'name' => "varchar(100) NOT NULL COMMENT 'Назва'",
            'logo' => "char(2) NOT NULL DEFAULT '00' COMMENT 'Код картинки логотипа'",
            'PRIMARY KEY (`iata`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
