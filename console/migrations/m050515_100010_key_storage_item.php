<?php

use yii\db\Schema;
use console\components\Migration;

class m050515_100010_key_storage_item extends Migration
{
    public function up()
    {
        $this->createTable('{{%key_storage_item}}', [
                'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
                'key' => Schema::TYPE_STRING . '(128) NOT NULL',
                'value' => Schema::TYPE_TEXT . ' NOT NULL',
                'comment' => Schema::TYPE_TEXT,
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'PRIMARY KEY (`id`)',
            ], $this->_tableOptions);
        $this->createIndex('idx_key_storage_item_key', '{{%key_storage_item}}', 'key', true);
    }

    public function down()
    {
        $this->dropTable('{{%key_storage_item}}');
    }
}
