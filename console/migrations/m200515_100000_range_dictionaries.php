<?php

use yii\db\Schema;
use console\components\Migration;

/**
 * Class m150000_100000_range
 * Присутствует здесь только для отделения границы между созданием схемы БД и наполненим данными
 * при создании БД для тестов
 * ./yii migrate/to m150000_100000_range_dictionaries --interactive=0 --db=dbTest
 */
class m200515_100000_range_dictionaries extends Migration
{
    public function up()
    {
    }

    public function down()
    {
    }
}
