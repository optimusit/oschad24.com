<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100035_order extends Migration
{
    private $_tableName = '{{%order}}';
    private $_providerTableName = '{{%provider}}';
    private $_partnerTableName = '{{%partner}}';
    private $_serviceTableName = '{{%service}}';
    private $_userTableName = '{{%user}}';

    public function up()
    {
        $this->_tableOptions .= ' AUTO_INCREMENT=100000';
        $this->_tableOptions .= " comment = 'Замовлення'";
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'provider_id' => "bigint(20) NOT NULL COMMENT 'Провайдер'",
            'provider_order_id' => Schema::TYPE_STRING . "(100) NOT NULL COMMENT 'Id ордеру в системі постачальника'",
            'service_id' => "bigint(20) NOT NULL COMMENT 'Сервіс'",
            'customer_id' => "bigint(20) NULL COMMENT 'Покупець'",
            'user_id' => "bigint(20) NULL COMMENT 'Користувач системі'",
            'partner_id' => "bigint(20) NOT NULL COMMENT 'Партнер'",
            'phone' => Schema::TYPE_STRING . "(13) NOT NULL COMMENT 'Телефон покупця'",
            'email' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Поштова адреса покупця'",
            'name' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Ім`я покупця'",
            'surname' => Schema::TYPE_STRING . "(100) NULL COMMENT 'Прізвище покупця'",
            'patronymic' => Schema::TYPE_STRING . "(100) NULL COMMENT 'По батькові покупця'",
            'status_id' => "tinyint(4) NOT NULL COMMENT 'Стан'",
            'paid' => "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Ознака сплати'",
            'paid_time' => Schema::TYPE_DATETIME . " NOT NULL COMMENT 'Час сплати'",
            'expiration_time' => Schema::TYPE_DATETIME . " NOT NULL COMMENT 'Термін придатності до сплати'",
            'unit_count' => "tinyint(4) NOT NULL COMMENT 'Кількість одиниць в замовленні'",
            'income_amount' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Вхідна вартість замовлення'",
            'fee' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Комісія системи'",
            'order_amount' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Сума, яка включає комісію системи'",
            'partner_fee' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Комісія партнера'",
            'partner_amount' => Schema::TYPE_DECIMAL
                . "(7,2) NOT NULL COMMENT 'Сума, яка включає коміссію системи і комісію партнера'",
            'bank_fee' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Банківська комісія'",
            'e_fee' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Комісія еквайрингу'",
            'amount' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Підсумкова сума'",
            'custom' => "MEDIUMTEXT NOT NULL COMMENT 'Дані клієнта (серіалізовані)'",
            'commission_internal' => "tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ознака внутрішньої комісії'",
            'partner_commission_internal' => "tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ознака внутрішньої комісії партнера'",
            'language' => Schema::TYPE_STRING . "(2) DEFAULT NULL COMMENT 'Мова замовлення'",
            'viewed' => "tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ознака що вже переглянутий'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'UNIQUE INDEX `provider_order_id` (`provider_order_id`)',
            'KEY `phone` (`phone`)',
            'KEY `user_id` (`user_id`)',
            'KEY `partner_id` (`partner_id`)',
            'KEY `status_id` (`status_id`)',
        ], $this->_tableOptions);

        $this->addForeignKey('order_ibfk_0', $this->_tableName, 'provider_id', $this->_providerTableName, 'id',
            'RESTRICT', 'RESTRICT');
        $this->addForeignKey('order_ibfk_1', $this->_tableName, 'partner_id', $this->_partnerTableName, 'id',
            'RESTRICT', 'RESTRICT');
        $this->addForeignKey('order_ibfk_2', $this->_tableName, 'service_id', $this->_serviceTableName, 'id',
            'RESTRICT', 'RESTRICT');
        $this->addForeignKey('order_ibfk_3', $this->_tableName, 'user_id', $this->_userTableName, 'id', 'RESTRICT',
            'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('order_ibfk_0', $this->_tableName);
        $this->dropForeignKey('order_ibfk_1', $this->_tableName);
        $this->dropForeignKey('order_ibfk_2', $this->_tableName);
        $this->dropForeignKey('order_ibfk_3', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
