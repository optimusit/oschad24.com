<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110510_karabas_countries extends Migration
{
    private $_tableName = '{{%karabas_countries}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL',
            'name' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва країни'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
