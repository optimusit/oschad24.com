<?php

use console\components\Migration;

class m301010_120040_order_paid_time_index extends Migration
{
    private $_tableName = '{{%order}}';
    private $_column = 'paid_time';

    public function up()
    {
        $this->createIndex('idx_' . $this->_column, $this->_tableName, $this->_column);
    }

    public function down()
    {
        $this->dropIndex('idx_' . $this->_column, $this->_tableName);
    }
}
