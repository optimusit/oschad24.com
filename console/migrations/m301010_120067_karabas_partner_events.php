<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120067_karabas_partner_events extends Migration
{
    private $_tableName = '{{%karabas_partner_events}}';
    private $_columnSaleStatus = 'SaleStatus';
    private $_columnSaleStatusDescription = 'SaleStatusDescription';

    public function up()
    {
        $this->dropColumn($this->_tableName, $this->_columnSaleStatus);
        $this->dropColumn($this->_tableName, $this->_columnSaleStatusDescription);
    }

    public function down()
    {
        $this->addColumn($this->_tableName, $this->_columnSaleStatus,
            "tinyint(1) NOT NULL COMMENT 'Статус продажу (1 - дозволений продаж, 0 - заборонено продаж)' AFTER `OrganizerId`");
        $this->addColumn($this->_tableName, $this->_columnSaleStatusDescription,
            Schema::TYPE_STRING . "(255) NULL COMMENT 'Причина заборони' AFTER `ReplaceActivityDescription`");
    }
}
