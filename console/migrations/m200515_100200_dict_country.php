<?php

use console\components\Migration;

class m200515_100200_dict_country extends Migration
{
    public function up()
    {
        $country = new \common\models\Country();
        $country->id = 2;
        $country->name_en = 'Australia';
        $country->name_ru = 'Австралия';
        $country->name_uk = 'Австралия';
        $country->alpha2 = 'AU';
        $country->alpha3 = 'AUS';
        $country->iso = '036';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 3;
        $country->name_en = 'Austria';
        $country->name_ru = 'Австрия';
        $country->name_uk = 'Австрия';
        $country->alpha2 = 'AT';
        $country->alpha3 = 'AUT';
        $country->iso = '040';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 4;
        $country->name_en = 'Azerbaijan';
        $country->name_ru = 'Азербайджан';
        $country->name_uk = 'Азербайджан';
        $country->alpha2 = 'AZ';
        $country->alpha3 = 'AZE';
        $country->iso = '031';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 5;
        $country->name_en = 'Albania';
        $country->name_ru = 'Албания';
        $country->name_uk = 'Албания';
        $country->alpha2 = 'AL';
        $country->alpha3 = 'ALB';
        $country->iso = '008';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 6;
        $country->name_en = 'Algeria';
        $country->name_ru = 'Алжир';
        $country->name_uk = 'Алжир';
        $country->alpha2 = 'DZ';
        $country->alpha3 = 'DZA';
        $country->iso = '012';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 7;
        $country->name_en = 'American Samoa';
        $country->name_ru = 'Американское Самоа';
        $country->name_uk = 'Американское Самоа';
        $country->alpha2 = 'AS';
        $country->alpha3 = 'ASM';
        $country->iso = '016';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 8;
        $country->name_en = 'Anguilla';
        $country->name_ru = 'Ангилья';
        $country->name_uk = 'Ангилья';
        $country->alpha2 = 'AI';
        $country->alpha3 = 'AIA';
        $country->iso = '660';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 9;
        $country->name_en = 'Angola';
        $country->name_ru = 'Ангола';
        $country->name_uk = 'Ангола';
        $country->alpha2 = 'AO';
        $country->alpha3 = 'AGO';
        $country->iso = '024';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 10;
        $country->name_en = 'Andorra';
        $country->name_ru = 'Андорра';
        $country->name_uk = 'Андорра';
        $country->alpha2 = 'AD';
        $country->alpha3 = 'AND';
        $country->iso = '020';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 11;
        $country->name_en = 'Antarctica';
        $country->name_ru = 'Антарктида';
        $country->name_uk = 'Антарктида';
        $country->alpha2 = 'AQ';
        $country->alpha3 = 'ATA';
        $country->iso = '010';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 12;
        $country->name_en = 'Antigua and Barbuda';
        $country->name_ru = 'Антигуа и Барбуда';
        $country->name_uk = 'Антигуа и Барбуда';
        $country->alpha2 = 'AG';
        $country->alpha3 = 'ATG';
        $country->iso = '028';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 13;
        $country->name_en = 'Argentina';
        $country->name_ru = 'Аргентина';
        $country->name_uk = 'Аргентина';
        $country->alpha2 = 'AR';
        $country->alpha3 = 'ARG';
        $country->iso = '032';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 14;
        $country->name_en = 'Armenia';
        $country->name_ru = 'Армения';
        $country->name_uk = 'Армения';
        $country->alpha2 = 'AM';
        $country->alpha3 = 'ARM';
        $country->iso = '051';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 15;
        $country->name_en = 'Aruba';
        $country->name_ru = 'Аруба';
        $country->name_uk = 'Аруба';
        $country->alpha2 = 'AW';
        $country->alpha3 = 'ABW';
        $country->iso = '533';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 16;
        $country->name_en = 'Afghanistan';
        $country->name_ru = 'Афганистан';
        $country->name_uk = 'Афганистан';
        $country->alpha2 = 'AF';
        $country->alpha3 = 'AFG';
        $country->iso = '004';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 17;
        $country->name_en = 'Bahamas';
        $country->name_ru = 'Багамы';
        $country->name_uk = 'Багамы';
        $country->alpha2 = 'BS';
        $country->alpha3 = 'BHS';
        $country->iso = '044';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 18;
        $country->name_en = 'Bangladesh';
        $country->name_ru = 'Бангладеш';
        $country->name_uk = 'Бангладеш';
        $country->alpha2 = 'BD';
        $country->alpha3 = 'BGD';
        $country->iso = '050';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 19;
        $country->name_en = 'Barbados';
        $country->name_ru = 'Барбадос';
        $country->name_uk = 'Барбадос';
        $country->alpha2 = 'BB';
        $country->alpha3 = 'BRB';
        $country->iso = '052';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 20;
        $country->name_en = 'Bahrain';
        $country->name_ru = 'Бахрейн';
        $country->name_uk = 'Бахрейн';
        $country->alpha2 = 'BH';
        $country->alpha3 = 'BHR';
        $country->iso = '048';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 21;
        $country->name_en = 'Belarus';
        $country->name_ru = 'Беларусь';
        $country->name_uk = 'Беларусь';
        $country->alpha2 = 'BY';
        $country->alpha3 = 'BLR';
        $country->iso = '112';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 22;
        $country->name_en = 'Belize';
        $country->name_ru = 'Белиз';
        $country->name_uk = 'Белиз';
        $country->alpha2 = 'BZ';
        $country->alpha3 = 'BLZ';
        $country->iso = '084';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 23;
        $country->name_en = 'Belgium';
        $country->name_ru = 'Бельгия';
        $country->name_uk = 'Бельгия';
        $country->alpha2 = 'BE';
        $country->alpha3 = 'BEL';
        $country->iso = '056';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 24;
        $country->name_en = 'Benin';
        $country->name_ru = 'Бенин';
        $country->name_uk = 'Бенин';
        $country->alpha2 = 'BJ';
        $country->alpha3 = 'BEN';
        $country->iso = '204';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 25;
        $country->name_en = 'Bermuda';
        $country->name_ru = 'Бермуды';
        $country->name_uk = 'Бермуды';
        $country->alpha2 = 'BM';
        $country->alpha3 = 'BMU';
        $country->iso = '060';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 26;
        $country->name_en = 'Bulgaria';
        $country->name_ru = 'Болгария';
        $country->name_uk = 'Болгария';
        $country->alpha2 = 'BG';
        $country->alpha3 = 'BGR';
        $country->iso = '100';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 27;
        $country->name_en = 'Bolivia, plurinational state of';
        $country->name_ru = 'Боливия, Многонациональное Государство';
        $country->name_uk = 'Боливия, Многонациональное Государство';
        $country->alpha2 = 'BO';
        $country->alpha3 = 'BOL';
        $country->iso = '068';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 28;
        $country->name_en = 'Bonaire, Sint Eustatius and Saba';
        $country->name_ru = 'Бонайре, Саба и Синт-Эстатиус';
        $country->name_uk = 'Бонайре, Саба и Синт-Эстатиус';
        $country->alpha2 = 'BQ';
        $country->alpha3 = 'BES';
        $country->iso = '535';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 29;
        $country->name_en = 'Bosnia and Herzegovina';
        $country->name_ru = 'Босния и Герцеговина';
        $country->name_uk = 'Босния и Герцеговина';
        $country->alpha2 = 'BA';
        $country->alpha3 = 'BIH';
        $country->iso = '070';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 30;
        $country->name_en = 'Botswana';
        $country->name_ru = 'Ботсвана';
        $country->name_uk = 'Ботсвана';
        $country->alpha2 = 'BW';
        $country->alpha3 = 'BWA';
        $country->iso = '072';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 31;
        $country->name_en = 'Brazil';
        $country->name_ru = 'Бразилия';
        $country->name_uk = 'Бразилия';
        $country->alpha2 = 'BR';
        $country->alpha3 = 'BRA';
        $country->iso = '076';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 32;
        $country->name_en = 'British Indian Ocean Territory';
        $country->name_ru = 'Британская территория в Индийском океане';
        $country->name_uk = 'Британская территория в Индийском океане';
        $country->alpha2 = 'IO';
        $country->alpha3 = 'IOT';
        $country->iso = '086';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 33;
        $country->name_en = 'Brunei Darussalam';
        $country->name_ru = 'Бруней-Даруссалам';
        $country->name_uk = 'Бруней-Даруссалам';
        $country->alpha2 = 'BN';
        $country->alpha3 = 'BRN';
        $country->iso = '096';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 34;
        $country->name_en = 'Burkina Faso';
        $country->name_ru = 'Буркина-Фасо';
        $country->name_uk = 'Буркина-Фасо';
        $country->alpha2 = 'BF';
        $country->alpha3 = 'BFA';
        $country->iso = '854';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 35;
        $country->name_en = 'Burundi';
        $country->name_ru = 'Бурунди';
        $country->name_uk = 'Бурунди';
        $country->alpha2 = 'BI';
        $country->alpha3 = 'BDI';
        $country->iso = '108';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 36;
        $country->name_en = 'Bhutan';
        $country->name_ru = 'Бутан';
        $country->name_uk = 'Бутан';
        $country->alpha2 = 'BT';
        $country->alpha3 = 'BTN';
        $country->iso = '064';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 37;
        $country->name_en = 'Vanuatu';
        $country->name_ru = 'Вануату';
        $country->name_uk = 'Вануату';
        $country->alpha2 = 'VU';
        $country->alpha3 = 'VUT';
        $country->iso = '548';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 38;
        $country->name_en = 'Hungary';
        $country->name_ru = 'Венгрия';
        $country->name_uk = 'Венгрия';
        $country->alpha2 = 'HU';
        $country->alpha3 = 'HUN';
        $country->iso = '348';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 39;
        $country->name_en = 'Venezuela';
        $country->name_ru = 'Венесуэла Боливарианская Республика';
        $country->name_uk = 'Венесуэла Боливарианская Республика';
        $country->alpha2 = 'VE';
        $country->alpha3 = 'VEN';
        $country->iso = '862';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 40;
        $country->name_en = 'Virgin Islands, British';
        $country->name_ru = 'Виргинские острова, Британские';
        $country->name_uk = 'Виргинские острова, Британские';
        $country->alpha2 = 'VG';
        $country->alpha3 = 'VGB';
        $country->iso = '092';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 41;
        $country->name_en = 'Virgin Islands, U.S.';
        $country->name_ru = 'Виргинские острова, США';
        $country->name_uk = 'Виргинские острова, США';
        $country->alpha2 = 'VI';
        $country->alpha3 = 'VIR';
        $country->iso = '850';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 42;
        $country->name_en = 'Vietnam';
        $country->name_ru = 'Вьетнам';
        $country->name_uk = 'Вьетнам';
        $country->alpha2 = 'VN';
        $country->alpha3 = 'VNM';
        $country->iso = '704';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 43;
        $country->name_en = 'Gabon';
        $country->name_ru = 'Габон';
        $country->name_uk = 'Габон';
        $country->alpha2 = 'GA';
        $country->alpha3 = 'GAB';
        $country->iso = '266';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 44;
        $country->name_en = 'Haiti';
        $country->name_ru = 'Гаити';
        $country->name_uk = 'Гаити';
        $country->alpha2 = 'HT';
        $country->alpha3 = 'HTI';
        $country->iso = '332';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 45;
        $country->name_en = 'Guyana';
        $country->name_ru = 'Гайана';
        $country->name_uk = 'Гайана';
        $country->alpha2 = 'GY';
        $country->alpha3 = 'GUY';
        $country->iso = '328';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 46;
        $country->name_en = 'Gambia';
        $country->name_ru = 'Гамбия';
        $country->name_uk = 'Гамбия';
        $country->alpha2 = 'GM';
        $country->alpha3 = 'GMB';
        $country->iso = '270';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 47;
        $country->name_en = 'Ghana';
        $country->name_ru = 'Гана';
        $country->name_uk = 'Гана';
        $country->alpha2 = 'GH';
        $country->alpha3 = 'GHA';
        $country->iso = '288';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 48;
        $country->name_en = 'Guadeloupe';
        $country->name_ru = 'Гваделупа';
        $country->name_uk = 'Гваделупа';
        $country->alpha2 = 'GP';
        $country->alpha3 = 'GLP';
        $country->iso = '312';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 49;
        $country->name_en = 'Guatemala';
        $country->name_ru = 'Гватемала';
        $country->name_uk = 'Гватемала';
        $country->alpha2 = 'GT';
        $country->alpha3 = 'GTM';
        $country->iso = '320';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 50;
        $country->name_en = 'Guinea';
        $country->name_ru = 'Гвинея';
        $country->name_uk = 'Гвинея';
        $country->alpha2 = 'GN';
        $country->alpha3 = 'GIN';
        $country->iso = '324';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 51;
        $country->name_en = 'Guinea-Bissau';
        $country->name_ru = 'Гвинея-Бисау';
        $country->name_uk = 'Гвинея-Бисау';
        $country->alpha2 = 'GW';
        $country->alpha3 = 'GNB';
        $country->iso = '624';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 52;
        $country->name_en = 'Germany';
        $country->name_ru = 'Германия';
        $country->name_uk = 'Германия';
        $country->alpha2 = 'DE';
        $country->alpha3 = 'DEU';
        $country->iso = '276';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 53;
        $country->name_en = 'Guernsey';
        $country->name_ru = 'Гернси';
        $country->name_uk = 'Гернси';
        $country->alpha2 = 'GG';
        $country->alpha3 = 'GGY';
        $country->iso = '831';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 54;
        $country->name_en = 'Gibraltar';
        $country->name_ru = 'Гибралтар';
        $country->name_uk = 'Гибралтар';
        $country->alpha2 = 'GI';
        $country->alpha3 = 'GIB';
        $country->iso = '292';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 55;
        $country->name_en = 'Honduras';
        $country->name_ru = 'Гондурас';
        $country->name_uk = 'Гондурас';
        $country->alpha2 = 'HN';
        $country->alpha3 = 'HND';
        $country->iso = '340';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 56;
        $country->name_en = 'Hong Kong';
        $country->name_ru = 'Гонконг';
        $country->name_uk = 'Гонконг';
        $country->alpha2 = 'HK';
        $country->alpha3 = 'HKG';
        $country->iso = '344';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 57;
        $country->name_en = 'Grenada';
        $country->name_ru = 'Гренада';
        $country->name_uk = 'Гренада';
        $country->alpha2 = 'GD';
        $country->alpha3 = 'GRD';
        $country->iso = '308';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 58;
        $country->name_en = 'Greenland';
        $country->name_ru = 'Гренландия';
        $country->name_uk = 'Гренландия';
        $country->alpha2 = 'GL';
        $country->alpha3 = 'GRL';
        $country->iso = '304';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 59;
        $country->name_en = 'Greece';
        $country->name_ru = 'Греция';
        $country->name_uk = 'Греция';
        $country->alpha2 = 'GR';
        $country->alpha3 = 'GRC';
        $country->iso = '300';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 60;
        $country->name_en = 'Georgia';
        $country->name_ru = 'Грузия';
        $country->name_uk = 'Грузия';
        $country->alpha2 = 'GE';
        $country->alpha3 = 'GEO';
        $country->iso = '268';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 61;
        $country->name_en = 'Guam';
        $country->name_ru = 'Гуам';
        $country->name_uk = 'Гуам';
        $country->alpha2 = 'GU';
        $country->alpha3 = 'GUM';
        $country->iso = '316';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 62;
        $country->name_en = 'Denmark';
        $country->name_ru = 'Дания';
        $country->name_uk = 'Дания';
        $country->alpha2 = 'DK';
        $country->alpha3 = 'DNK';
        $country->iso = '208';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 63;
        $country->name_en = 'Jersey';
        $country->name_ru = 'Джерси';
        $country->name_uk = 'Джерси';
        $country->alpha2 = 'JE';
        $country->alpha3 = 'JEY';
        $country->iso = '832';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 64;
        $country->name_en = 'Djibouti';
        $country->name_ru = 'Джибути';
        $country->name_uk = 'Джибути';
        $country->alpha2 = 'DJ';
        $country->alpha3 = 'DJI';
        $country->iso = '262';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 65;
        $country->name_en = 'Dominica';
        $country->name_ru = 'Доминика';
        $country->name_uk = 'Доминика';
        $country->alpha2 = 'DM';
        $country->alpha3 = 'DMA';
        $country->iso = '212';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 66;
        $country->name_en = 'Dominican Republic';
        $country->name_ru = 'Доминиканская Республика';
        $country->name_uk = 'Доминиканская Республика';
        $country->alpha2 = 'DO';
        $country->alpha3 = 'DOM';
        $country->iso = '214';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 67;
        $country->name_en = 'Egypt';
        $country->name_ru = 'Египет';
        $country->name_uk = 'Египет';
        $country->alpha2 = 'EG';
        $country->alpha3 = 'EGY';
        $country->iso = '818';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 68;
        $country->name_en = 'Zambia';
        $country->name_ru = 'Замбия';
        $country->name_uk = 'Замбия';
        $country->alpha2 = 'ZM';
        $country->alpha3 = 'ZMB';
        $country->iso = '894';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 69;
        $country->name_en = 'Western Sahara';
        $country->name_ru = 'Западная Сахара';
        $country->name_uk = 'Западная Сахара';
        $country->alpha2 = 'EH';
        $country->alpha3 = 'ESH';
        $country->iso = '732';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 70;
        $country->name_en = 'Zimbabwe';
        $country->name_ru = 'Зимбабве';
        $country->name_uk = 'Зимбабве';
        $country->alpha2 = 'ZW';
        $country->alpha3 = 'ZWE';
        $country->iso = '716';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 71;
        $country->name_en = 'Israel';
        $country->name_ru = 'Израиль';
        $country->name_uk = 'Израиль';
        $country->alpha2 = 'IL';
        $country->alpha3 = 'ISR';
        $country->iso = '376';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 72;
        $country->name_en = 'India';
        $country->name_ru = 'Индия';
        $country->name_uk = 'Индия';
        $country->alpha2 = 'IN';
        $country->alpha3 = 'IND';
        $country->iso = '356';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 73;
        $country->name_en = 'Indonesia';
        $country->name_ru = 'Индонезия';
        $country->name_uk = 'Индонезия';
        $country->alpha2 = 'ID';
        $country->alpha3 = 'IDN';
        $country->iso = '360';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 74;
        $country->name_en = 'Jordan';
        $country->name_ru = 'Иордания';
        $country->name_uk = 'Иордания';
        $country->alpha2 = 'JO';
        $country->alpha3 = 'JOR';
        $country->iso = '400';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 75;
        $country->name_en = 'Iraq';
        $country->name_ru = 'Ирак';
        $country->name_uk = 'Ирак';
        $country->alpha2 = 'IQ';
        $country->alpha3 = 'IRQ';
        $country->iso = '368';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 76;
        $country->name_en = 'Iran, Islamic Republic of';
        $country->name_ru = 'Иран, Исламская Республика';
        $country->name_uk = 'Иран, Исламская Республика';
        $country->alpha2 = 'IR';
        $country->alpha3 = 'IRN';
        $country->iso = '364';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 77;
        $country->name_en = 'Ireland';
        $country->name_ru = 'Ирландия';
        $country->name_uk = 'Ирландия';
        $country->alpha2 = 'IE';
        $country->alpha3 = 'IRL';
        $country->iso = '372';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 78;
        $country->name_en = 'Iceland';
        $country->name_ru = 'Исландия';
        $country->name_uk = 'Исландия';
        $country->alpha2 = 'IS';
        $country->alpha3 = 'ISL';
        $country->iso = '352';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 79;
        $country->name_en = 'Spain';
        $country->name_ru = 'Испания';
        $country->name_uk = 'Испания';
        $country->alpha2 = 'ES';
        $country->alpha3 = 'ESP';
        $country->iso = '724';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 80;
        $country->name_en = 'Italy';
        $country->name_ru = 'Италия';
        $country->name_uk = 'Италия';
        $country->alpha2 = 'IT';
        $country->alpha3 = 'ITA';
        $country->iso = '380';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 81;
        $country->name_en = 'Yemen';
        $country->name_ru = 'Йемен';
        $country->name_uk = 'Йемен';
        $country->alpha2 = 'YE';
        $country->alpha3 = 'YEM';
        $country->iso = '887';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 82;
        $country->name_en = 'Cape Verde';
        $country->name_ru = 'Кабо-Верде';
        $country->name_uk = 'Кабо-Верде';
        $country->alpha2 = 'CV';
        $country->alpha3 = 'CPV';
        $country->iso = '132';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 83;
        $country->name_en = 'Kazakhstan';
        $country->name_ru = 'Казахстан';
        $country->name_uk = 'Казахстан';
        $country->alpha2 = 'KZ';
        $country->alpha3 = 'KAZ';
        $country->iso = '398';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 84;
        $country->name_en = 'Cambodia';
        $country->name_ru = 'Камбоджа';
        $country->name_uk = 'Камбоджа';
        $country->alpha2 = 'KH';
        $country->alpha3 = 'KHM';
        $country->iso = '116';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 85;
        $country->name_en = 'Cameroon';
        $country->name_ru = 'Камерун';
        $country->name_uk = 'Камерун';
        $country->alpha2 = 'CM';
        $country->alpha3 = 'CMR';
        $country->iso = '120';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 86;
        $country->name_en = 'Canada';
        $country->name_ru = 'Канада';
        $country->name_uk = 'Канада';
        $country->alpha2 = 'CA';
        $country->alpha3 = 'CAN';
        $country->iso = '124';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 87;
        $country->name_en = 'Qatar';
        $country->name_ru = 'Катар';
        $country->name_uk = 'Катар';
        $country->alpha2 = 'QA';
        $country->alpha3 = 'QAT';
        $country->iso = '634';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 88;
        $country->name_en = 'Kenya';
        $country->name_ru = 'Кения';
        $country->name_uk = 'Кения';
        $country->alpha2 = 'KE';
        $country->alpha3 = 'KEN';
        $country->iso = '404';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 89;
        $country->name_en = 'Cyprus';
        $country->name_ru = 'Кипр';
        $country->name_uk = 'Кипр';
        $country->alpha2 = 'CY';
        $country->alpha3 = 'CYP';
        $country->iso = '196';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 90;
        $country->name_en = 'Kyrgyzstan';
        $country->name_ru = 'Киргизия';
        $country->name_uk = 'Киргизия';
        $country->alpha2 = 'KG';
        $country->alpha3 = 'KGZ';
        $country->iso = '417';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 91;
        $country->name_en = 'Kiribati';
        $country->name_ru = 'Кирибати';
        $country->name_uk = 'Кирибати';
        $country->alpha2 = 'KI';
        $country->alpha3 = 'KIR';
        $country->iso = '296';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 92;
        $country->name_en = 'China';
        $country->name_ru = 'Китай';
        $country->name_uk = 'Китай';
        $country->alpha2 = 'CN';
        $country->alpha3 = 'CHN';
        $country->iso = '156';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 93;
        $country->name_en = 'Cocos (Keeling) Islands';
        $country->name_ru = 'Кокосовые (Килинг) острова';
        $country->name_uk = 'Кокосовые (Килинг) острова';
        $country->alpha2 = 'CC';
        $country->alpha3 = 'CCK';
        $country->iso = '166';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 94;
        $country->name_en = 'Colombia';
        $country->name_ru = 'Колумбия';
        $country->name_uk = 'Колумбия';
        $country->alpha2 = 'CO';
        $country->alpha3 = 'COL';
        $country->iso = '170';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 95;
        $country->name_en = 'Comoros';
        $country->name_ru = 'Коморы';
        $country->name_uk = 'Коморы';
        $country->alpha2 = 'KM';
        $country->alpha3 = 'COM';
        $country->iso = '174';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 96;
        $country->name_en = 'Congo';
        $country->name_ru = 'Конго';
        $country->name_uk = 'Конго';
        $country->alpha2 = 'CG';
        $country->alpha3 = 'COG';
        $country->iso = '178';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 97;
        $country->name_en = 'Congo, Democratic Republic of the';
        $country->name_ru = 'Конго, Демократическая Республика';
        $country->name_uk = 'Конго, Демократическая Республика';
        $country->alpha2 = 'CD';
        $country->alpha3 = 'COD';
        $country->iso = '180';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 98;
        $country->name_en = 'Korea, Democratic People\'s republic of';
        $country->name_ru = 'Корея, Народно-Демократическая Республика';
        $country->name_uk = 'Корея, Народно-Демократическая Республика';
        $country->alpha2 = 'KP';
        $country->alpha3 = 'PRK';
        $country->iso = '408';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 99;
        $country->name_en = 'Korea, Republic of';
        $country->name_ru = 'Корея, Республика';
        $country->name_uk = 'Корея, Республика';
        $country->alpha2 = 'KR';
        $country->alpha3 = 'KOR';
        $country->iso = '410';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 100;
        $country->name_en = 'Costa Rica';
        $country->name_ru = 'Коста-Рика';
        $country->name_uk = 'Коста-Рика';
        $country->alpha2 = 'CR';
        $country->alpha3 = 'CRI';
        $country->iso = '188';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 101;
        $country->name_en = 'Cote d\'Ivoire';
        $country->name_ru = 'Кот д\'Ивуар';
        $country->name_uk = 'Кот д\'Ивуар';
        $country->alpha2 = 'CI';
        $country->alpha3 = 'CIV';
        $country->iso = '384';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 102;
        $country->name_en = 'Cuba';
        $country->name_ru = 'Куба';
        $country->name_uk = 'Куба';
        $country->alpha2 = 'CU';
        $country->alpha3 = 'CUB';
        $country->iso = '192';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 103;
        $country->name_en = 'Kuwait';
        $country->name_ru = 'Кувейт';
        $country->name_uk = 'Кувейт';
        $country->alpha2 = 'KW';
        $country->alpha3 = 'KWT';
        $country->iso = '414';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 104;
        $country->name_en = 'Curaçao';
        $country->name_ru = 'Кюрасао';
        $country->name_uk = 'Кюрасао';
        $country->alpha2 = 'CW';
        $country->alpha3 = 'CUW';
        $country->iso = '531';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 105;
        $country->name_en = 'Lao People\'s Democratic Republic';
        $country->name_ru = 'Лаос';
        $country->name_uk = 'Лаос';
        $country->alpha2 = 'LA';
        $country->alpha3 = 'LAO';
        $country->iso = '418';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 106;
        $country->name_en = 'Latvia';
        $country->name_ru = 'Латвия';
        $country->name_uk = 'Латвия';
        $country->alpha2 = 'LV';
        $country->alpha3 = 'LVA';
        $country->iso = '428';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 107;
        $country->name_en = 'Lesotho';
        $country->name_ru = 'Лесото';
        $country->name_uk = 'Лесото';
        $country->alpha2 = 'LS';
        $country->alpha3 = 'LSO';
        $country->iso = '426';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 108;
        $country->name_en = 'Lebanon';
        $country->name_ru = 'Ливан';
        $country->name_uk = 'Ливан';
        $country->alpha2 = 'LB';
        $country->alpha3 = 'LBN';
        $country->iso = '422';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 109;
        $country->name_en = 'Libyan Arab Jamahiriya';
        $country->name_ru = 'Ливийская Арабская Джамахирия';
        $country->name_uk = 'Ливийская Арабская Джамахирия';
        $country->alpha2 = 'LY';
        $country->alpha3 = 'LBY';
        $country->iso = '434';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 110;
        $country->name_en = 'Liberia';
        $country->name_ru = 'Либерия';
        $country->name_uk = 'Либерия';
        $country->alpha2 = 'LR';
        $country->alpha3 = 'LBR';
        $country->iso = '430';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 111;
        $country->name_en = 'Liechtenstein';
        $country->name_ru = 'Лихтенштейн';
        $country->name_uk = 'Лихтенштейн';
        $country->alpha2 = 'LI';
        $country->alpha3 = 'LIE';
        $country->iso = '438';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 112;
        $country->name_en = 'Lithuania';
        $country->name_ru = 'Литва';
        $country->name_uk = 'Литва';
        $country->alpha2 = 'LT';
        $country->alpha3 = 'LTU';
        $country->iso = '440';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 113;
        $country->name_en = 'Luxembourg';
        $country->name_ru = 'Люксембург';
        $country->name_uk = 'Люксембург';
        $country->alpha2 = 'LU';
        $country->alpha3 = 'LUX';
        $country->iso = '442';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 114;
        $country->name_en = 'Mauritius';
        $country->name_ru = 'Маврикий';
        $country->name_uk = 'Маврикий';
        $country->alpha2 = 'MU';
        $country->alpha3 = 'MUS';
        $country->iso = '480';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 115;
        $country->name_en = 'Mauritania';
        $country->name_ru = 'Мавритания';
        $country->name_uk = 'Мавритания';
        $country->alpha2 = 'MR';
        $country->alpha3 = 'MRT';
        $country->iso = '478';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 116;
        $country->name_en = 'Madagascar';
        $country->name_ru = 'Мадагаскар';
        $country->name_uk = 'Мадагаскар';
        $country->alpha2 = 'MG';
        $country->alpha3 = 'MDG';
        $country->iso = '450';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 117;
        $country->name_en = 'Mayotte';
        $country->name_ru = 'Майотта';
        $country->name_uk = 'Майотта';
        $country->alpha2 = 'YT';
        $country->alpha3 = 'MYT';
        $country->iso = '175';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 118;
        $country->name_en = 'Macao';
        $country->name_ru = 'Макао';
        $country->name_uk = 'Макао';
        $country->alpha2 = 'MO';
        $country->alpha3 = 'MAC';
        $country->iso = '446';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 119;
        $country->name_en = 'Malawi';
        $country->name_ru = 'Малави';
        $country->name_uk = 'Малави';
        $country->alpha2 = 'MW';
        $country->alpha3 = 'MWI';
        $country->iso = '454';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 120;
        $country->name_en = 'Malaysia';
        $country->name_ru = 'Малайзия';
        $country->name_uk = 'Малайзия';
        $country->alpha2 = 'MY';
        $country->alpha3 = 'MYS';
        $country->iso = '458';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 121;
        $country->name_en = 'Mali';
        $country->name_ru = 'Мали';
        $country->name_uk = 'Мали';
        $country->alpha2 = 'ML';
        $country->alpha3 = 'MLI';
        $country->iso = '466';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 122;
        $country->name_en = 'United States Minor Outlying Islands';
        $country->name_ru = 'Малые Тихоокеанские отдаленные острова Соединенных Штатов';
        $country->name_uk = 'Малые Тихоокеанские отдаленные острова Соединенных Штатов';
        $country->alpha2 = 'UM';
        $country->alpha3 = 'UMI';
        $country->iso = '581';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 123;
        $country->name_en = 'Maldives';
        $country->name_ru = 'Мальдивы';
        $country->name_uk = 'Мальдивы';
        $country->alpha2 = 'MV';
        $country->alpha3 = 'MDV';
        $country->iso = '462';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 124;
        $country->name_en = 'Malta';
        $country->name_ru = 'Мальта';
        $country->name_uk = 'Мальта';
        $country->alpha2 = 'MT';
        $country->alpha3 = 'MLT';
        $country->iso = '470';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 125;
        $country->name_en = 'Morocco';
        $country->name_ru = 'Марокко';
        $country->name_uk = 'Марокко';
        $country->alpha2 = 'MA';
        $country->alpha3 = 'MAR';
        $country->iso = '504';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 126;
        $country->name_en = 'Martinique';
        $country->name_ru = 'Мартиника';
        $country->name_uk = 'Мартиника';
        $country->alpha2 = 'MQ';
        $country->alpha3 = 'MTQ';
        $country->iso = '474';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 127;
        $country->name_en = 'Marshall Islands';
        $country->name_ru = 'Маршалловы острова';
        $country->name_uk = 'Маршалловы острова';
        $country->alpha2 = 'MH';
        $country->alpha3 = 'MHL';
        $country->iso = '584';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 128;
        $country->name_en = 'Mexico';
        $country->name_ru = 'Мексика';
        $country->name_uk = 'Мексика';
        $country->alpha2 = 'MX';
        $country->alpha3 = 'MEX';
        $country->iso = '484';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 129;
        $country->name_en = 'Micronesia, Federated States of';
        $country->name_ru = 'Микронезия, Федеративные Штаты';
        $country->name_uk = 'Микронезия, Федеративные Штаты';
        $country->alpha2 = 'FM';
        $country->alpha3 = 'FSM';
        $country->iso = '583';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 130;
        $country->name_en = 'Mozambique';
        $country->name_ru = 'Мозамбик';
        $country->name_uk = 'Мозамбик';
        $country->alpha2 = 'MZ';
        $country->alpha3 = 'MOZ';
        $country->iso = '508';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 131;
        $country->name_en = 'Moldova';
        $country->name_ru = 'Молдова, Республика';
        $country->name_uk = 'Молдова, Республика';
        $country->alpha2 = 'MD';
        $country->alpha3 = 'MDA';
        $country->iso = '498';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 132;
        $country->name_en = 'Monaco';
        $country->name_ru = 'Монако';
        $country->name_uk = 'Монако';
        $country->alpha2 = 'MC';
        $country->alpha3 = 'MCO';
        $country->iso = '492';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 133;
        $country->name_en = 'Mongolia';
        $country->name_ru = 'Монголия';
        $country->name_uk = 'Монголия';
        $country->alpha2 = 'MN';
        $country->alpha3 = 'MNG';
        $country->iso = '496';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 134;
        $country->name_en = 'Montserrat';
        $country->name_ru = 'Монтсеррат';
        $country->name_uk = 'Монтсеррат';
        $country->alpha2 = 'MS';
        $country->alpha3 = 'MSR';
        $country->iso = '500';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 135;
        $country->name_en = 'Burma';
        $country->name_ru = 'Мьянма';
        $country->name_uk = 'Мьянма';
        $country->alpha2 = 'MM';
        $country->alpha3 = 'MMR';
        $country->iso = '104';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 136;
        $country->name_en = 'Namibia';
        $country->name_ru = 'Намибия';
        $country->name_uk = 'Намибия';
        $country->alpha2 = 'NA';
        $country->alpha3 = 'NAM';
        $country->iso = '516';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 137;
        $country->name_en = 'Nauru';
        $country->name_ru = 'Науру';
        $country->name_uk = 'Науру';
        $country->alpha2 = 'NR';
        $country->alpha3 = 'NRU';
        $country->iso = '520';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 138;
        $country->name_en = 'Nepal';
        $country->name_ru = 'Непал';
        $country->name_uk = 'Непал';
        $country->alpha2 = 'NP';
        $country->alpha3 = 'NPL';
        $country->iso = '524';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 139;
        $country->name_en = 'Niger';
        $country->name_ru = 'Нигер';
        $country->name_uk = 'Нигер';
        $country->alpha2 = 'NE';
        $country->alpha3 = 'NER';
        $country->iso = '562';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 140;
        $country->name_en = 'Nigeria';
        $country->name_ru = 'Нигерия';
        $country->name_uk = 'Нигерия';
        $country->alpha2 = 'NG';
        $country->alpha3 = 'NGA';
        $country->iso = '566';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 141;
        $country->name_en = 'Netherlands';
        $country->name_ru = 'Нидерланды';
        $country->name_uk = 'Нидерланды';
        $country->alpha2 = 'NL';
        $country->alpha3 = 'NLD';
        $country->iso = '528';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 142;
        $country->name_en = 'Nicaragua';
        $country->name_ru = 'Никарагуа';
        $country->name_uk = 'Никарагуа';
        $country->alpha2 = 'NI';
        $country->alpha3 = 'NIC';
        $country->iso = '558';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 143;
        $country->name_en = 'Niue';
        $country->name_ru = 'Ниуэ';
        $country->name_uk = 'Ниуэ';
        $country->alpha2 = 'NU';
        $country->alpha3 = 'NIU';
        $country->iso = '570';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 144;
        $country->name_en = 'New Zealand';
        $country->name_ru = 'Новая Зеландия';
        $country->name_uk = 'Новая Зеландия';
        $country->alpha2 = 'NZ';
        $country->alpha3 = 'NZL';
        $country->iso = '554';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 145;
        $country->name_en = 'New Caledonia';
        $country->name_ru = 'Новая Каледония';
        $country->name_uk = 'Новая Каледония';
        $country->alpha2 = 'NC';
        $country->alpha3 = 'NCL';
        $country->iso = '540';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 146;
        $country->name_en = 'Norway';
        $country->name_ru = 'Норвегия';
        $country->name_uk = 'Норвегия';
        $country->alpha2 = 'NO';
        $country->alpha3 = 'NOR';
        $country->iso = '578';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 147;
        $country->name_en = 'United Arab Emirates';
        $country->name_ru = 'Объединенные Арабские Эмираты';
        $country->name_uk = 'Объединенные Арабские Эмираты';
        $country->alpha2 = 'AE';
        $country->alpha3 = 'ARE';
        $country->iso = '784';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 148;
        $country->name_en = 'Oman';
        $country->name_ru = 'Оман';
        $country->name_uk = 'Оман';
        $country->alpha2 = 'OM';
        $country->alpha3 = 'OMN';
        $country->iso = '512';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 149;
        $country->name_en = 'Bouvet Island';
        $country->name_ru = 'Остров Буве';
        $country->name_uk = 'Остров Буве';
        $country->alpha2 = 'BV';
        $country->alpha3 = 'BVT';
        $country->iso = '074';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 150;
        $country->name_en = 'Isle of Man';
        $country->name_ru = 'Остров Мэн';
        $country->name_uk = 'Остров Мэн';
        $country->alpha2 = 'IM';
        $country->alpha3 = 'IMN';
        $country->iso = '833';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 151;
        $country->name_en = 'Norfolk Island';
        $country->name_ru = 'Остров Норфолк';
        $country->name_uk = 'Остров Норфолк';
        $country->alpha2 = 'NF';
        $country->alpha3 = 'NFK';
        $country->iso = '574';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 152;
        $country->name_en = 'Christmas Island';
        $country->name_ru = 'Остров Рождества';
        $country->name_uk = 'Остров Рождества';
        $country->alpha2 = 'CX';
        $country->alpha3 = 'CXR';
        $country->iso = '162';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 153;
        $country->name_en = 'Heard Island and McDonald Islands';
        $country->name_ru = 'Остров Херд и острова Макдональд';
        $country->name_uk = 'Остров Херд и острова Макдональд';
        $country->alpha2 = 'HM';
        $country->alpha3 = 'HMD';
        $country->iso = '334';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 154;
        $country->name_en = 'Cayman Islands';
        $country->name_ru = 'Острова Кайман';
        $country->name_uk = 'Острова Кайман';
        $country->alpha2 = 'KY';
        $country->alpha3 = 'CYM';
        $country->iso = '136';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 155;
        $country->name_en = 'Cook Islands';
        $country->name_ru = 'Острова Кука';
        $country->name_uk = 'Острова Кука';
        $country->alpha2 = 'CK';
        $country->alpha3 = 'COK';
        $country->iso = '184';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 156;
        $country->name_en = 'Turks and Caicos Islands';
        $country->name_ru = 'Острова Теркс и Кайкос';
        $country->name_uk = 'Острова Теркс и Кайкос';
        $country->alpha2 = 'TC';
        $country->alpha3 = 'TCA';
        $country->iso = '796';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 157;
        $country->name_en = 'Pakistan';
        $country->name_ru = 'Пакистан';
        $country->name_uk = 'Пакистан';
        $country->alpha2 = 'PK';
        $country->alpha3 = 'PAK';
        $country->iso = '586';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 158;
        $country->name_en = 'Palau';
        $country->name_ru = 'Палау';
        $country->name_uk = 'Палау';
        $country->alpha2 = 'PW';
        $country->alpha3 = 'PLW';
        $country->iso = '585';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 159;
        $country->name_en = 'Palestinian Territory, Occupied';
        $country->name_ru = 'Палестинская территория, оккупированная';
        $country->name_uk = 'Палестинская территория, оккупированная';
        $country->alpha2 = 'PS';
        $country->alpha3 = 'PSE';
        $country->iso = '275';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 160;
        $country->name_en = 'Panama';
        $country->name_ru = 'Панама';
        $country->name_uk = 'Панама';
        $country->alpha2 = 'PA';
        $country->alpha3 = 'PAN';
        $country->iso = '591';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 161;
        $country->name_en = 'Holy See (Vatican City State)';
        $country->name_ru = 'Папский Престол (Государство &mdash; город Ватикан)';
        $country->name_uk = 'Папский Престол (Государство &mdash; город Ватикан)';
        $country->alpha2 = 'VA';
        $country->alpha3 = 'VAT';
        $country->iso = '336';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 162;
        $country->name_en = 'Papua New Guinea';
        $country->name_ru = 'Папуа-Новая Гвинея';
        $country->name_uk = 'Папуа-Новая Гвинея';
        $country->alpha2 = 'PG';
        $country->alpha3 = 'PNG';
        $country->iso = '598';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 163;
        $country->name_en = 'Paraguay';
        $country->name_ru = 'Парагвай';
        $country->name_uk = 'Парагвай';
        $country->alpha2 = 'PY';
        $country->alpha3 = 'PRY';
        $country->iso = '600';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 164;
        $country->name_en = 'Peru';
        $country->name_ru = 'Перу';
        $country->name_uk = 'Перу';
        $country->alpha2 = 'PE';
        $country->alpha3 = 'PER';
        $country->iso = '604';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 165;
        $country->name_en = 'Pitcairn';
        $country->name_ru = 'Питкерн';
        $country->name_uk = 'Питкерн';
        $country->alpha2 = 'PN';
        $country->alpha3 = 'PCN';
        $country->iso = '612';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 166;
        $country->name_en = 'Poland';
        $country->name_ru = 'Польша';
        $country->name_uk = 'Польша';
        $country->alpha2 = 'PL';
        $country->alpha3 = 'POL';
        $country->iso = '616';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 167;
        $country->name_en = 'Portugal';
        $country->name_ru = 'Португалия';
        $country->name_uk = 'Португалия';
        $country->alpha2 = 'PT';
        $country->alpha3 = 'PRT';
        $country->iso = '620';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 168;
        $country->name_en = 'Puerto Rico';
        $country->name_ru = 'Пуэрто-Рико';
        $country->name_uk = 'Пуэрто-Рико';
        $country->alpha2 = 'PR';
        $country->alpha3 = 'PRI';
        $country->iso = '630';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 169;
        $country->name_en = 'Macedonia, The Former Yugoslab Republic Of';
        $country->name_ru = 'Республика Македония';
        $country->name_uk = 'Республика Македония';
        $country->alpha2 = 'MK';
        $country->alpha3 = 'MKD';
        $country->iso = '807';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 170;
        $country->name_en = 'Reunion';
        $country->name_ru = 'Реюньон';
        $country->name_uk = 'Реюньон';
        $country->alpha2 = 'RE';
        $country->alpha3 = 'REU';
        $country->iso = '638';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 171;
        $country->name_en = 'Russian Federation';
        $country->name_ru = 'Россия';
        $country->name_uk = 'Россия';
        $country->alpha2 = 'RU';
        $country->alpha3 = 'RUS';
        $country->iso = '643';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 172;
        $country->name_en = 'Rwanda';
        $country->name_ru = 'Руанда';
        $country->name_uk = 'Руанда';
        $country->alpha2 = 'RW';
        $country->alpha3 = 'RWA';
        $country->iso = '646';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 173;
        $country->name_en = 'Romania';
        $country->name_ru = 'Румыния';
        $country->name_uk = 'Румыния';
        $country->alpha2 = 'RO';
        $country->alpha3 = 'ROU';
        $country->iso = '642';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 174;
        $country->name_en = 'Samoa';
        $country->name_ru = 'Самоа';
        $country->name_uk = 'Самоа';
        $country->alpha2 = 'WS';
        $country->alpha3 = 'WSM';
        $country->iso = '882';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 175;
        $country->name_en = 'San Marino';
        $country->name_ru = 'Сан-Марино';
        $country->name_uk = 'Сан-Марино';
        $country->alpha2 = 'SM';
        $country->alpha3 = 'SMR';
        $country->iso = '674';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 176;
        $country->name_en = 'Sao Tome and Principe';
        $country->name_ru = 'Сан-Томе и Принсипи';
        $country->name_uk = 'Сан-Томе и Принсипи';
        $country->alpha2 = 'ST';
        $country->alpha3 = 'STP';
        $country->iso = '678';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 177;
        $country->name_en = 'Saudi Arabia';
        $country->name_ru = 'Саудовская Аравия';
        $country->name_uk = 'Саудовская Аравия';
        $country->alpha2 = 'SA';
        $country->alpha3 = 'SAU';
        $country->iso = '682';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 178;
        $country->name_en = 'Swaziland';
        $country->name_ru = 'Свазиленд';
        $country->name_uk = 'Свазиленд';
        $country->alpha2 = 'SZ';
        $country->alpha3 = 'SWZ';
        $country->iso = '748';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 179;
        $country->name_en = 'Saint Helena, Ascension And Tristan Da Cunha';
        $country->name_ru = 'Святая Елена, Остров вознесения, Тристан-да-Кунья';
        $country->name_uk = 'Святая Елена, Остров вознесения, Тристан-да-Кунья';
        $country->alpha2 = 'SH';
        $country->alpha3 = 'SHN';
        $country->iso = '654';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 180;
        $country->name_en = 'Northern Mariana Islands';
        $country->name_ru = 'Северные Марианские острова';
        $country->name_uk = 'Северные Марианские острова';
        $country->alpha2 = 'MP';
        $country->alpha3 = 'MNP';
        $country->iso = '580';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 181;
        $country->name_en = 'Saint Barthélemy';
        $country->name_ru = 'Сен-Бартельми';
        $country->name_uk = 'Сен-Бартельми';
        $country->alpha2 = 'BL';
        $country->alpha3 = 'BLM';
        $country->iso = '652';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 182;
        $country->name_en = 'Saint Martin (French Part)';
        $country->name_ru = 'Сен-Мартен';
        $country->name_uk = 'Сен-Мартен';
        $country->alpha2 = 'MF';
        $country->alpha3 = 'MAF';
        $country->iso = '663';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 183;
        $country->name_en = 'Senegal';
        $country->name_ru = 'Сенегал';
        $country->name_uk = 'Сенегал';
        $country->alpha2 = 'SN';
        $country->alpha3 = 'SEN';
        $country->iso = '686';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 184;
        $country->name_en = 'Saint Vincent and the Grenadines';
        $country->name_ru = 'Сент-Винсент и Гренадины';
        $country->name_uk = 'Сент-Винсент и Гренадины';
        $country->alpha2 = 'VC';
        $country->alpha3 = 'VCT';
        $country->iso = '670';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 185;
        $country->name_en = 'Saint Kitts and Nevis';
        $country->name_ru = 'Сент-Китс и Невис';
        $country->name_uk = 'Сент-Китс и Невис';
        $country->alpha2 = 'KN';
        $country->alpha3 = 'KNA';
        $country->iso = '659';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 186;
        $country->name_en = 'Saint Lucia';
        $country->name_ru = 'Сент-Люсия';
        $country->name_uk = 'Сент-Люсия';
        $country->alpha2 = 'LC';
        $country->alpha3 = 'LCA';
        $country->iso = '662';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 187;
        $country->name_en = 'Saint Pierre and Miquelon';
        $country->name_ru = 'Сент-Пьер и Микелон';
        $country->name_uk = 'Сент-Пьер и Микелон';
        $country->alpha2 = 'PM';
        $country->alpha3 = 'SPM';
        $country->iso = '666';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 188;
        $country->name_en = 'Serbia';
        $country->name_ru = 'Сербия';
        $country->name_uk = 'Сербия';
        $country->alpha2 = 'RS';
        $country->alpha3 = 'SRB';
        $country->iso = '688';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 189;
        $country->name_en = 'Seychelles';
        $country->name_ru = 'Сейшелы';
        $country->name_uk = 'Сейшелы';
        $country->alpha2 = 'SC';
        $country->alpha3 = 'SYC';
        $country->iso = '690';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 190;
        $country->name_en = 'Singapore';
        $country->name_ru = 'Сингапур';
        $country->name_uk = 'Сингапур';
        $country->alpha2 = 'SG';
        $country->alpha3 = 'SGP';
        $country->iso = '702';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 191;
        $country->name_en = 'Sint Maarten';
        $country->name_ru = 'Синт-Мартен';
        $country->name_uk = 'Синт-Мартен';
        $country->alpha2 = 'SX';
        $country->alpha3 = 'SXM';
        $country->iso = '534';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 192;
        $country->name_en = 'Syrian Arab Republic';
        $country->name_ru = 'Сирийская Арабская Республика';
        $country->name_uk = 'Сирийская Арабская Республика';
        $country->alpha2 = 'SY';
        $country->alpha3 = 'SYR';
        $country->iso = '760';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 193;
        $country->name_en = 'Slovakia';
        $country->name_ru = 'Словакия';
        $country->name_uk = 'Словакия';
        $country->alpha2 = 'SK';
        $country->alpha3 = 'SVK';
        $country->iso = '703';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 194;
        $country->name_en = 'Slovenia';
        $country->name_ru = 'Словения';
        $country->name_uk = 'Словения';
        $country->alpha2 = 'SI';
        $country->alpha3 = 'SVN';
        $country->iso = '705';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 195;
        $country->name_en = 'United Kingdom';
        $country->name_ru = 'Соединенное Королевство';
        $country->name_uk = 'Соединенное Королевство';
        $country->alpha2 = 'GB';
        $country->alpha3 = 'GBR';
        $country->iso = '826';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 196;
        $country->name_en = 'United States';
        $country->name_ru = 'Соединенные Штаты';
        $country->name_uk = 'Соединенные Штаты';
        $country->alpha2 = 'US';
        $country->alpha3 = 'USA';
        $country->iso = '840';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 197;
        $country->name_en = 'Solomon Islands';
        $country->name_ru = 'Соломоновы острова';
        $country->name_uk = 'Соломоновы острова';
        $country->alpha2 = 'SB';
        $country->alpha3 = 'SLB';
        $country->iso = '090';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 198;
        $country->name_en = 'Somalia';
        $country->name_ru = 'Сомали';
        $country->name_uk = 'Сомали';
        $country->alpha2 = 'SO';
        $country->alpha3 = 'SOM';
        $country->iso = '706';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 199;
        $country->name_en = 'Sudan';
        $country->name_ru = 'Судан';
        $country->name_uk = 'Судан';
        $country->alpha2 = 'SD';
        $country->alpha3 = 'SDN';
        $country->iso = '736';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 200;
        $country->name_en = 'Suriname';
        $country->name_ru = 'Суринам';
        $country->name_uk = 'Суринам';
        $country->alpha2 = 'SR';
        $country->alpha3 = 'SUR';
        $country->iso = '740';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 201;
        $country->name_en = 'Sierra Leone';
        $country->name_ru = 'Сьерра-Леоне';
        $country->name_uk = 'Сьерра-Леоне';
        $country->alpha2 = 'SL';
        $country->alpha3 = 'SLE';
        $country->iso = '694';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 202;
        $country->name_en = 'Tajikistan';
        $country->name_ru = 'Таджикистан';
        $country->name_uk = 'Таджикистан';
        $country->alpha2 = 'TJ';
        $country->alpha3 = 'TJK';
        $country->iso = '762';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 203;
        $country->name_en = 'Thailand';
        $country->name_ru = 'Таиланд';
        $country->name_uk = 'Таиланд';
        $country->alpha2 = 'TH';
        $country->alpha3 = 'THA';
        $country->iso = '764';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 204;
        $country->name_en = 'Taiwan, Province of China';
        $country->name_ru = 'Тайвань (Китай)';
        $country->name_uk = 'Тайвань (Китай)';
        $country->alpha2 = 'TW';
        $country->alpha3 = 'TWN';
        $country->iso = '158';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 205;
        $country->name_en = 'Tanzania, United Republic Of';
        $country->name_ru = 'Танзания, Объединенная Республика';
        $country->name_uk = 'Танзания, Объединенная Республика';
        $country->alpha2 = 'TZ';
        $country->alpha3 = 'TZA';
        $country->iso = '834';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 206;
        $country->name_en = 'Timor-Leste';
        $country->name_ru = 'Тимор-Лесте';
        $country->name_uk = 'Тимор-Лесте';
        $country->alpha2 = 'TL';
        $country->alpha3 = 'TLS';
        $country->iso = '626';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 207;
        $country->name_en = 'Togo';
        $country->name_ru = 'Того';
        $country->name_uk = 'Того';
        $country->alpha2 = 'TG';
        $country->alpha3 = 'TGO';
        $country->iso = '768';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 208;
        $country->name_en = 'Tokelau';
        $country->name_ru = 'Токелау';
        $country->name_uk = 'Токелау';
        $country->alpha2 = 'TK';
        $country->alpha3 = 'TKL';
        $country->iso = '772';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 209;
        $country->name_en = 'Tonga';
        $country->name_ru = 'Тонга';
        $country->name_uk = 'Тонга';
        $country->alpha2 = 'TO';
        $country->alpha3 = 'TON';
        $country->iso = '776';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 210;
        $country->name_en = 'Trinidad and Tobago';
        $country->name_ru = 'Тринидад и Тобаго';
        $country->name_uk = 'Тринидад и Тобаго';
        $country->alpha2 = 'TT';
        $country->alpha3 = 'TTO';
        $country->iso = '780';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 211;
        $country->name_en = 'Tuvalu';
        $country->name_ru = 'Тувалу';
        $country->name_uk = 'Тувалу';
        $country->alpha2 = 'TV';
        $country->alpha3 = 'TUV';
        $country->iso = '798';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 212;
        $country->name_en = 'Tunisia';
        $country->name_ru = 'Тунис';
        $country->name_uk = 'Тунис';
        $country->alpha2 = 'TN';
        $country->alpha3 = 'TUN';
        $country->iso = '788';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 213;
        $country->name_en = 'Turkmenistan';
        $country->name_ru = 'Туркмения';
        $country->name_uk = 'Туркмения';
        $country->alpha2 = 'TM';
        $country->alpha3 = 'TKM';
        $country->iso = '795';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 214;
        $country->name_en = 'Turkey';
        $country->name_ru = 'Турция';
        $country->name_uk = 'Турция';
        $country->alpha2 = 'TR';
        $country->alpha3 = 'TUR';
        $country->iso = '792';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 215;
        $country->name_en = 'Uganda';
        $country->name_ru = 'Уганда';
        $country->name_uk = 'Уганда';
        $country->alpha2 = 'UG';
        $country->alpha3 = 'UGA';
        $country->iso = '800';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 216;
        $country->name_en = 'Uzbekistan';
        $country->name_ru = 'Узбекистан';
        $country->name_uk = 'Узбекистан';
        $country->alpha2 = 'UZ';
        $country->alpha3 = 'UZB';
        $country->iso = '860';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 217;
        $country->name_en = 'Ukraine';
        $country->name_ru = 'Украина';
        $country->name_uk = 'Украина';
        $country->alpha2 = 'UA';
        $country->alpha3 = 'UKR';
        $country->iso = '804';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 218;
        $country->name_en = 'Wallis and Futuna';
        $country->name_ru = 'Уоллис и Футуна';
        $country->name_uk = 'Уоллис и Футуна';
        $country->alpha2 = 'WF';
        $country->alpha3 = 'WLF';
        $country->iso = '876';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 219;
        $country->name_en = 'Uruguay';
        $country->name_ru = 'Уругвай';
        $country->name_uk = 'Уругвай';
        $country->alpha2 = 'UY';
        $country->alpha3 = 'URY';
        $country->iso = '858';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 220;
        $country->name_en = 'Faroe Islands';
        $country->name_ru = 'Фарерские острова';
        $country->name_uk = 'Фарерские острова';
        $country->alpha2 = 'FO';
        $country->alpha3 = 'FRO';
        $country->iso = '234';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 221;
        $country->name_en = 'Fiji';
        $country->name_ru = 'Фиджи';
        $country->name_uk = 'Фиджи';
        $country->alpha2 = 'FJ';
        $country->alpha3 = 'FJI';
        $country->iso = '242';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 222;
        $country->name_en = 'Philippines';
        $country->name_ru = 'Филиппины';
        $country->name_uk = 'Филиппины';
        $country->alpha2 = 'PH';
        $country->alpha3 = 'PHL';
        $country->iso = '608';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 223;
        $country->name_en = 'Finland';
        $country->name_ru = 'Финляндия';
        $country->name_uk = 'Финляндия';
        $country->alpha2 = 'FI';
        $country->alpha3 = 'FIN';
        $country->iso = '246';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 224;
        $country->name_en = 'Falkland Islands (Malvinas)';
        $country->name_ru = 'Фолклендские острова (Мальвинские)';
        $country->name_uk = 'Фолклендские острова (Мальвинские)';
        $country->alpha2 = 'FK';
        $country->alpha3 = 'FLK';
        $country->iso = '238';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 225;
        $country->name_en = 'France';
        $country->name_ru = 'Франция';
        $country->name_uk = 'Франция';
        $country->alpha2 = 'FR';
        $country->alpha3 = 'FRA';
        $country->iso = '250';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 226;
        $country->name_en = 'French Guiana';
        $country->name_ru = 'Французская Гвиана';
        $country->name_uk = 'Французская Гвиана';
        $country->alpha2 = 'GF';
        $country->alpha3 = 'GUF';
        $country->iso = '254';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 227;
        $country->name_en = 'French Polynesia';
        $country->name_ru = 'Французская Полинезия';
        $country->name_uk = 'Французская Полинезия';
        $country->alpha2 = 'PF';
        $country->alpha3 = 'PYF';
        $country->iso = '258';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 228;
        $country->name_en = 'French Southern Territories';
        $country->name_ru = 'Французские Южные территории';
        $country->name_uk = 'Французские Южные территории';
        $country->alpha2 = 'TF';
        $country->alpha3 = 'ATF';
        $country->iso = '260';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 229;
        $country->name_en = 'Croatia';
        $country->name_ru = 'Хорватия';
        $country->name_uk = 'Хорватия';
        $country->alpha2 = 'HR';
        $country->alpha3 = 'HRV';
        $country->iso = '191';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 230;
        $country->name_en = 'Central African Republic';
        $country->name_ru = 'Центрально-Африканская Республика';
        $country->name_uk = 'Центрально-Африканская Республика';
        $country->alpha2 = 'CF';
        $country->alpha3 = 'CAF';
        $country->iso = '140';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 231;
        $country->name_en = 'Chad';
        $country->name_ru = 'Чад';
        $country->name_uk = 'Чад';
        $country->alpha2 = 'TD';
        $country->alpha3 = 'TCD';
        $country->iso = '148';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 232;
        $country->name_en = 'Montenegro';
        $country->name_ru = 'Черногория';
        $country->name_uk = 'Черногория';
        $country->alpha2 = 'ME';
        $country->alpha3 = 'MNE';
        $country->iso = '499';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 233;
        $country->name_en = 'Czech Republic';
        $country->name_ru = 'Чешская Республика';
        $country->name_uk = 'Чешская Республика';
        $country->alpha2 = 'CZ';
        $country->alpha3 = 'CZE';
        $country->iso = '203';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 234;
        $country->name_en = 'Chile';
        $country->name_ru = 'Чили';
        $country->name_uk = 'Чили';
        $country->alpha2 = 'CL';
        $country->alpha3 = 'CHL';
        $country->iso = '152';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 235;
        $country->name_en = 'Switzerland';
        $country->name_ru = 'Швейцария';
        $country->name_uk = 'Швейцария';
        $country->alpha2 = 'CH';
        $country->alpha3 = 'CHE';
        $country->iso = '756';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 236;
        $country->name_en = 'Sweden';
        $country->name_ru = 'Швеция';
        $country->name_uk = 'Швеция';
        $country->alpha2 = 'SE';
        $country->alpha3 = 'SWE';
        $country->iso = '752';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 237;
        $country->name_en = 'Svalbard and Jan Mayen';
        $country->name_ru = 'Шпицберген и Ян Майен';
        $country->name_uk = 'Шпицберген и Ян Майен';
        $country->alpha2 = 'SJ';
        $country->alpha3 = 'SJM';
        $country->iso = '744';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 238;
        $country->name_en = 'Sri Lanka';
        $country->name_ru = 'Шри-Ланка';
        $country->name_uk = 'Шри-Ланка';
        $country->alpha2 = 'LK';
        $country->alpha3 = 'LKA';
        $country->iso = '144';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 239;
        $country->name_en = 'Ecuador';
        $country->name_ru = 'Эквадор';
        $country->name_uk = 'Эквадор';
        $country->alpha2 = 'EC';
        $country->alpha3 = 'ECU';
        $country->iso = '218';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 240;
        $country->name_en = 'Equatorial Guinea';
        $country->name_ru = 'Экваториальная Гвинея';
        $country->name_uk = 'Экваториальная Гвинея';
        $country->alpha2 = 'GQ';
        $country->alpha3 = 'GNQ';
        $country->iso = '226';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 241;
        $country->name_en = 'Åland Islands';
        $country->name_ru = 'Эландские острова';
        $country->name_uk = 'Эландские острова';
        $country->alpha2 = 'AX';
        $country->alpha3 = 'ALA';
        $country->iso = '248';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 242;
        $country->name_en = 'El Salvador';
        $country->name_ru = 'Эль-Сальвадор';
        $country->name_uk = 'Эль-Сальвадор';
        $country->alpha2 = 'SV';
        $country->alpha3 = 'SLV';
        $country->iso = '222';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 243;
        $country->name_en = 'Eritrea';
        $country->name_ru = 'Эритрея';
        $country->name_uk = 'Эритрея';
        $country->alpha2 = 'ER';
        $country->alpha3 = 'ERI';
        $country->iso = '232';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 244;
        $country->name_en = 'Estonia';
        $country->name_ru = 'Эстония';
        $country->name_uk = 'Эстония';
        $country->alpha2 = 'EE';
        $country->alpha3 = 'EST';
        $country->iso = '233';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 245;
        $country->name_en = 'Ethiopia';
        $country->name_ru = 'Эфиопия';
        $country->name_uk = 'Эфиопия';
        $country->alpha2 = 'ET';
        $country->alpha3 = 'ETH';
        $country->iso = '231';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 246;
        $country->name_en = 'South Africa';
        $country->name_ru = 'Южная Африка';
        $country->name_uk = 'Южная Африка';
        $country->alpha2 = 'ZA';
        $country->alpha3 = 'ZAF';
        $country->iso = '710';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 247;
        $country->name_en = 'South Georgia and the South Sandwich Islands';
        $country->name_ru = 'Южная Джорджия и Южные Сандвичевы острова';
        $country->name_uk = 'Южная Джорджия и Южные Сандвичевы острова';
        $country->alpha2 = 'GS';
        $country->alpha3 = 'SGS';
        $country->iso = '239';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 249;
        $country->name_en = 'South Sudan';
        $country->name_ru = 'Южный Судан';
        $country->name_uk = 'Южный Судан';
        $country->alpha2 = 'SS';
        $country->alpha3 = 'SSD';
        $country->iso = '728';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 250;
        $country->name_en = 'Jamaica';
        $country->name_ru = 'Ямайка';
        $country->name_uk = 'Ямайка';
        $country->alpha2 = 'JM';
        $country->alpha3 = 'JAM';
        $country->iso = '388';
        $country->insert();

        $country = new \common\models\Country();
        $country->id = 251;
        $country->name_en = 'Japan';
        $country->name_ru = 'Япония';
        $country->name_uk = 'Япония';
        $country->alpha2 = 'JP';
        $country->alpha3 = 'JPN';
        $country->iso = '392';
        $country->insert();

    }

    public function down()
    {
    }
}
