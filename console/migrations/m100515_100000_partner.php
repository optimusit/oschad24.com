<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100000_partner extends Migration
{
    private $_tableName = '{{%partner}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
                'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
                'prefix' => Schema::TYPE_STRING . "(100) NOT NULL COMMENT 'Приставка у зоні .veb.ua'",
                'name' => Schema::TYPE_STRING . " NOT NULL COMMENT 'Назва'",
                'logo' => Schema::TYPE_STRING . " NULL COMMENT 'Логотип'",
                'email' => Schema::TYPE_STRING . " NOT NULL COMMENT 'Поштова адреса'",
                'phone' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Телефон'",
                'access_token' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Маркер доступу'",
                'secret_key' => Schema::TYPE_STRING . "(60) NOT NULL COMMENT 'Секретный ключ'",
                'balance' => Schema::TYPE_DECIMAL . "(10,2) NOT NULL COMMENT 'Баланс'",
                'credit_limit' => Schema::TYPE_DECIMAL . "(10,2) NOT NULL DEFAULT 0 COMMENT 'Кредитный ліміт'",
                'edrpou' => Schema::TYPE_STRING . "(12) NOT NULL COMMENT 'ЄДРПОУ'",
                'mfo' => "MEDIUMINT(8) NULL COMMENT 'МФО'",
                'rr' => Schema::TYPE_STRING . "(14) NOT NULL COMMENT 'Розрахунковий рахунок'",
                'bank' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Банк'",
                'internal_num' => Schema::TYPE_STRING . "(6) NULL COMMENT '(АРМ) Номер філії організації'",
                'transit_rr' => Schema::TYPE_STRING . "(14) NULL COMMENT '(АРМ) Транзитній рахунок'",
                'commission_rr' => Schema::TYPE_STRING . "(14) NULL COMMENT '(АРМ) Комісійний рахунок'",
                'return_rr' => Schema::TYPE_STRING . "(14) NULL COMMENT 'Рахунок повернення'",
                'const_file_mb' => Schema::TYPE_STRING . "(14) NULL COMMENT 'Константа файлуа міжбанку'",
                'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
                'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
                // все что ниже - исключительно для реализации дерева (работы компонента \kartik\tree\models\Tree)
                'root' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
                'lft' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
                'rgt' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
                'lvl' => Schema::TYPE_SMALLINT . ' DEFAULT NULL',
                'icon' => 'VARCHAR(255) DEFAULT NULL',
                'icon_type' => "TINYINT(1) NOT NULL DEFAULT '1'",
                'active' => 'TINYINT(1) NOT NULL DEFAULT TRUE',
                'selected' => 'TINYINT(1) NOT NULL DEFAULT FALSE',
                'disabled' => 'TINYINT(1) NOT NULL DEFAULT FALSE',
                'readonly' => 'TINYINT(1) NOT NULL DEFAULT FALSE',
                'visible' => 'TINYINT(1) NOT NULL DEFAULT TRUE',
                'collapsed' => 'TINYINT(1) NOT NULL DEFAULT FALSE',
                'movable_u' => 'TINYINT(1) NOT NULL DEFAULT TRUE',
                'movable_d' => 'TINYINT(1) NOT NULL DEFAULT TRUE',
                'movable_l' => 'TINYINT(1) NOT NULL DEFAULT TRUE',
                'movable_r' => 'TINYINT(1) NOT NULL DEFAULT TRUE',
                'removable' => 'TINYINT(1) NOT NULL DEFAULT TRUE',
                'removable_all' => 'TINYINT(1)   NOT NULL DEFAULT FALSE',
                'KEY tbl_product_NK1 (root)',
                'KEY tbl_product_NK2 (lft)',
                'KEY tbl_product_NK3 (rgt)',
                'KEY tbl_product_NK4 (lvl)',
                'KEY tbl_product_NK5 (active)',
                'PRIMARY KEY (`id`)',
            ], $this->_tableOptions);
        $this->createIndex('ukey-partnerprefix', $this->_tableName, [
            'prefix',
        ], true);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
