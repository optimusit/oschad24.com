<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110550_karabas_halls extends Migration
{
    private $_tableName = '{{%karabas_halls}}';
    private $_buildingsTableName = '{{%karabas_buildings}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL',
            'name' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва'",
            'hasPNG' => "tinyint(1) NOT NULL COMMENT 'Прапор наявності PNG-версії схеми залу'",
            'buildingId' => "bigint(20) NULL COMMENT 'ID місця проведення'",
            'description' => Schema::TYPE_STRING . "(512) NOT NULL COMMENT 'Опис'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'KEY `buildingId` (`buildingId`)',
        ], $this->_tableOptions);
//		$this->addForeignKey('building_ibfk_10', $this->_tableName, 'buildingId', $this->_buildingsTableName, 'id',
//			'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
//		$this->dropForeignKey('building_ibfk_10', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
