<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110620_karabas_partner_event_activity_type extends Migration
{
    private $_tableName = '{{%karabas_partner_event_activity_type}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'EventId' => 'bigint(20) NOT NULL',
            'ActivityTypeId' => 'bigint(20) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`EventId`, `ActivityTypeId`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
