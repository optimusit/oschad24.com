<?php

use yii\db\Schema;
use console\components\Migration;

class m050515_100005_country extends Migration
{
    private $countryTableName = '{{%country}}';

    public function up()
    {
        $this->createTable($this->countryTableName, [
                'id' => Schema::TYPE_PK,
                'name_en' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва Англійська'",
                'name_ru' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва Російська'",
                'name_uk' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва Українська'",
                'alpha2' => Schema::TYPE_STRING . "(2) NOT NULL COMMENT 'Двозначний код'",
                'alpha3' => Schema::TYPE_STRING . "(3) NOT NULL COMMENT 'Трьозначинй код'",
                'iso' => Schema::TYPE_STRING . "(3) NOT NULL COMMENT 'ISO'",
                'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
                'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->countryTableName);
    }
}
