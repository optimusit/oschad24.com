<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100020_commission extends Migration
{
    private $_tableName = '{{%commission}}';
    private $_partnerTableName = '{{%partner}}';
    private $_serviceTableName = '{{%service}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'partner_id' => "bigint(20) NOT NULL COMMENT 'Партнер'",
            'service_id' => "bigint(20) NOT NULL COMMENT 'Сервіс'",
            'kind' => "tinyint(1) NOT NULL COMMENT 'Тип'",
            'provider_id' => "bigint(20) NOT NULL COMMENT 'Провайдер'",
            'min' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Мінімальна сума'",
            'max' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Максимальна сума'",
            'value' => Schema::TYPE_DECIMAL . "(7,2) NOT NULL COMMENT 'Значення'",
            'internal' => "tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Внутрішня'",
            'round_type' => "tinyint(1) NOT NULL COMMENT 'Округлення'",
            'type' => "tinyint(4) NOT NULL COMMENT 'Тип2'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'KEY `partner_id` (`partner_id`)',
            'KEY `service_id` (`service_id`)',
        ], $this->_tableOptions);

        $this->addForeignKey('comission_ibfk_1', $this->_tableName, 'service_id', $this->_serviceTableName, 'id',
            'RESTRICT', 'RESTRICT');
        $this->addForeignKey('comission_ibfk_2', $this->_tableName, 'partner_id', $this->_partnerTableName, 'id',
            'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('comission_ibfk_1', $this->_tableName);
        $this->dropForeignKey('comission_ibfk_2', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
