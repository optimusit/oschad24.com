<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_100010_service extends Migration
{
    private $_tableName = '{{%service%}}';

    public function up()
    {
        $this->_tableOptions .= " comment = 'Сервіс'";
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'provider_code' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Код у провайдера'",
            'code' => Schema::TYPE_STRING . "(32) NOT NULL COMMENT 'Код в системі'",
            'name_uk' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Назва Українською'",
            'short_name_uk' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Коротка назва Українською'",
            'description_uk' => Schema::TYPE_STRING . "(1000) NOT NULL COMMENT 'Опис Українською'",
            'name_ru' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Назва Російською'",
            'short_name_ru' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Коротка назва Російською'",
            'description_ru' => Schema::TYPE_STRING . "(1000) NOT NULL COMMENT 'Опис Російською'",
            'name_en' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Назва Англійською'",
            'short_name_en' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Коротка назва Англійською'",
            'description_en' => Schema::TYPE_STRING . "(1000) NOT NULL COMMENT 'Опис Англійською'",
            'short_name_translit' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Коротка назва транслітом'",
            'script_url' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Шлях'",
            'controller' => Schema::TYPE_STRING . "(50) NOT NULL COMMENT 'Контролер'",
            'active' => "tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Активний'",
            'color' => Schema::TYPE_STRING . "(7) NOT NULL DEFAULT '#000000' COMMENT 'Колір'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'UNIQUE INDEX `controller` (`controller`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
