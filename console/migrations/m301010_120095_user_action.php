<?php

use yii\db\Schema;
use console\components\Migration;

class m301010_120095_user_action extends Migration
{
    private $_tableName = '{{%user_action}}';
    private $_partnerTableName = '{{%partner}}';
    private $_userTableName = '{{%user}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'user_id' => 'bigint(20) NOT NULL',
            'partner_id' => 'bigint(20) NOT NULL',
            'role' => "varchar(500) NOT NULL COMMENT 'Роль'",
            'action' => "varchar(500) NOT NULL COMMENT 'Дія'",
            'comment' => "varchar(10000) NOT NULL COMMENT 'Опис'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
            'KEY `partner_id` (`partner_id`)',
            'KEY `user_id` (`user_id`)',
        ], $this->_tableOptions);
        $this->addForeignKey('user_action_ibfk_1', $this->_tableName, 'partner_id', $this->_partnerTableName, 'id',
            'RESTRICT', 'RESTRICT');
        $this->addForeignKey('user_action_ibfk_2', $this->_tableName, 'user_id', $this->_userTableName, 'id',
            'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('user_action_ibfk_1', $this->_tableName);
        $this->dropForeignKey('user_action_ibfk_2', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}
