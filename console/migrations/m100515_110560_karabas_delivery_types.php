<?php

use yii\db\Schema;
use console\components\Migration;

class m100515_110560_karabas_delivery_types extends Migration
{
    private $_tableName = '{{%karabas_delivery_types}}';

    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => 'bigint(20) NOT NULL',
            'name' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Назва'",
            'code' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Код'",
            'addressRequired' => "tinyint(1) NOT NULL COMMENT 'Ознака необхідності адреси доставки'",
            'paymentTypeCodes' => Schema::TYPE_STRING
                . "(512) NOT NULL COMMENT 'Перелік типів оплати, які можуть бути використані разом з цим способом доставки'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Створено'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'Змінено'",
            'PRIMARY KEY (`id`)',
        ], $this->_tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
