<?php
namespace console\controllers;

use Yii;
use common\providers\shakhtar\Shakhtar;
use console\components\Controller;

class ShakhtarController extends Controller
{
    public function actionLoadmatches()
    {
        $shakhtar = new Shakhtar();
        $shakhtar->LoadMatches();
    }
}
