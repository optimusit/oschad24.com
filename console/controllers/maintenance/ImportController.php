<?php
namespace console\controllers\maintenance;

use Yii;
use common\components\AppHelper;
use common\models\User;
use common\models\Partner;
use console\components\Controller;
use yii\base\Exception;
use yii\db\Query;

class ImportController extends Controller
{
    /**
     * Перенос филиальной структуры и пользователей ощадбанка
     *
     * перед запуском поменять
     * 'log' 'targets'
     * с 'class' => 'yii\log\DbTarget',
     * на 'class' => 'yii\log\FileTarget',
     */
    public function actionAffiliate()
    {
        $partnerId = 7;
        $importedPartner = (new Query())->select('*')
            ->from('company')
            ->where(['id' => $partnerId])
            ->one(Yii::$app->olddb);
        $rootPartner = new Partner([
            'name' => $importedPartner['name'],
            'email' => $importedPartner['email'],
            'phone' => $importedPartner['phone'],
            'prefix' => $importedPartner['key'],
            'logo' => $importedPartner['logo'],
            'balance' => $importedPartner['balance'],
            'rr' => $importedPartner['rs'],
            'bank' => $importedPartner['bank'],
            'edrpou' => $importedPartner['egrpou'],
            'mfo' => $importedPartner['mfo'],
            'internal_num' => $importedPartner['internal_num'],
            'transit_rr' => $importedPartner['transit_rs'],
            'commission_rr' => $importedPartner['commission_rs'],
            'return_rr' => $importedPartner['return_rs'],
            'const_file_mb' => $importedPartner['const_file_mb'],
            'access_token' => 'oZUALqwqiEPXB9M5TLmB5AWCIPCzGsyT',
            'secret_key' => '$2y$13$uIvv/IQ0Ta5E4AxSS0RAVe3fbNRnVo/SAOA24pcL1fAFq6wREgCUm',
        ]);
        if (!$rootPartner->makeRoot()) {
            var_dump($rootPartner->getFirstErrors());
        }
        $allowedChars = '/[^a-zA-Z0-9]+/';
        $oldAffiliates = $this->getAffiliates($partnerId);
        $affiliate2total = count($oldAffiliates);
        $affiliate2step = 0;
        foreach ($oldAffiliates as $affiliate2) {
            $affiliate2step++;
            $prefix =
                preg_replace($allowedChars, '', mb_strtolower(AppHelper::transliteration($affiliate2['name']))) . '.'
                . $rootPartner->prefix;
            $partner2 = $this->createPartner($affiliate2, $prefix);
            if ($partner2->prependTo($rootPartner)) {
                $oldUsers = $this->getAffiliateUsers($affiliate2['id']);
                $this->createUsers($oldUsers, $partner2->id);
                $oldAffiliates3 = $this->getAffiliates($affiliate2['id']);
                $affiliate3total = count($oldAffiliates3);
                $affiliate3step = 0;
                foreach ($oldAffiliates3 as $affiliate3) {
                    $affiliate3step++;
                    $prefix =
                        preg_replace($allowedChars, '', mb_strtolower(AppHelper::transliteration($affiliate3['name'])))
                        . '.' . $rootPartner->prefix;
                    $partner3 = $this->createPartner($affiliate3, $prefix);
                    if ($partner3->prependTo($partner2)) {
                        $oldUsers = $this->getAffiliateUsers($affiliate3['id']);
                        $this->createUsers($oldUsers, $partner3->id);
                        echo "$affiliate2step ($affiliate2total) / $affiliate3step ($affiliate3total)" . PHP_EOL;
                    } else {
                        var_dump($partner3->getFirstErrors());
                        var_dump($partner3);
                        throw new Exception();
                    }
                }
            } else {
                var_dump($partner2->getFirstErrors());
                var_dump($partner2);
                throw new Exception();
            }
        }
    }

    private function createPartner($affiliate, $prefix)
    {
        return new Partner([
            'name' => $affiliate['name'],
            'email' => $affiliate['email'],
            'phone' => $affiliate['phone'],
            'prefix' => $prefix,
            'logo' => $affiliate['logo'],
            'balance' => $affiliate['balance'],
            'rr' => $affiliate['rs'],
            'bank' => $affiliate['bank'],
            'edrpou' => $affiliate['egrpou'],
            'mfo' => $affiliate['mfo'],
            'internal_num' => $affiliate['internal_num'],
            'transit_rr' => $affiliate['transit_rs'],
            'commission_rr' => $affiliate['commission_rs'],
            'return_rr' => $affiliate['return_rs'],
            'const_file_mb' => $affiliate['const_file_mb'],
            'access_token' => 'oZUALqwqiEPXB9M5TLmB5AWCIPCzGsyT',
            'secret_key' => '$2y$13$uIvv/IQ0Ta5E4AxSS0RAVe3fbNRnVo/SAOA24pcL1fAFq6wREgCUm',
        ]);
    }

    private function getAffiliates($parent)
    {
        $query = (new Query())->select([
            'c1.*',
            '`c2`.`id` is not null as `has_children`',
        ])
            ->from('company c1')
            ->leftJoin('company c2', 'c1.id=c2.parent')
            ->where([
                'c1.parent' => $parent,
                'c1.trash' => false
            ])
            ->groupBy('c1.id')
            ->orderBy('c1.name')
            ->limit(150);

        return $query->all(Yii::$app->olddb);
    }

    private function getAffiliateUsers($id)
    {
        $query = (new Query())->select('*')
            ->from('users as u')
            ->where([
                'company_id' => $id,
                'trash' => false
            ]);

        return $query->all(Yii::$app->olddb);
    }

    private function createUsers($oldUsers, $partnerId)
    {
        foreach ($oldUsers as $oldUser) {
            $userFIO = explode(' ', $oldUser['fio']);
            $newUser = new User([
                'email' => $oldUser['email'],
                'phone' => empty($oldUser['phone'])
                    ? '123'
                    : $oldUser['phone'],
                'first_name' => isset($userFIO[1])
                    ? $userFIO[1]
                    : '-',
                'middle_name' => isset($userFIO[2])
                    ? $userFIO[2]
                    : '-',
                'last_name' => $userFIO[0],
                'confirm' => 1,
                'confirm_key' => $oldUser['confirm_key'],
                'partner_id' => $partnerId,
                'auth_key' => '2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k',
            ]);
            $newUser->setPassword('user');
            if (!$newUser->save()) {
                var_dump($newUser->getFirstErrors());
                var_dump($newUser);
                throw new Exception();
            }
        }
    }
}
