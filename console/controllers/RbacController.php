<?php
namespace console\controllers;

use Yii;
use common\models\Partner;
use common\models\User;
use common\rbac\PartnerServiceAccess;
use common\rbac\UserServiceAccess;
use yii\base\InvalidParamException;
use console\components\Controller;

class RbacController extends Controller
{
    public function actionIndex()
    {
        echo PHP_EOL;
        echo 'You can use next commands:' . PHP_EOL;
        echo './yii rbac/init - to initilize permissions tree' . PHP_EOL;
        echo './yii rbac/assign role email partner_prefix - to assign roles to users' . PHP_EOL;
        echo PHP_EOL;
    }

    public function actionInit()
    {
        if (!$this->confirm('Are you sure? It will re-create permissions tree.')) {
            return self::EXIT_CODE_NORMAL;
        }

        $auth = Yii::$app->authManager;
        $auth->removeAll();

        // право управлять пользователями
        $adminUsers = $auth->createPermission('AdminUsers');
        $adminUsers->description = 'Право управлять пользователями';
        $auth->add($adminUsers);
        // право управлять сервисами
        $adminServices = $auth->createPermission('AdminServices');
        $adminServices->description = 'Право управлять сервисами';
        $auth->add($adminServices);
        // право производить манипуляции с ордерами, билетами и оплатами
        $ordersServicesManipulation = $auth->createPermission('OrdersServicesManipulation');
        $ordersServicesManipulation->description = 'право производить манипуляции с ордерами, билетами и оплатами';
        $auth->add($ordersServicesManipulation);
        // право производить продажу продуктов работниками партнера
        $saleServices = $auth->createPermission('saleServices');
        $saleServices->description = 'Право производить продажу продуктов работниками партнера';
        $auth->add($saleServices);

        // группа Администратор (ВСЕЙ) системы
        $superAdmin = $auth->createRole('admin');
        $superAdmin->description = 'Супер Администратор системы (ВСЕЙ)';
        $auth->add($superAdmin);
        $auth->addChild($superAdmin, $adminUsers);
        $auth->addChild($superAdmin, $adminServices);
        // пользователь admin
        Yii::$app->user->identityClass = Partner::className();
        $this->actionUserAssignRole('admin', 'admin@veb.ua', 'vebua');


        // группа Партнер
        $partner = $auth->createRole('partner');
        $partner->description = 'Партнер';
        $auth->add($partner);
        $auth->addChild($partner, $adminServices);
        // тестовые пользователи
        $this->actionPartnerAssignRole('partner', 'oschadbank');

        // группа Менеджер Партнера
        $manager = $auth->createRole('manager');
        $manager->description = 'Менеджер Партнера';
        $auth->add($manager);
        $auth->addChild($manager, $ordersServicesManipulation);
        // тестовые пользователи
        $this->actionUserAssignRole('manager', 'manager01@oschadbank.ua', 'oschadbank');

        // группа Торговая площадка (интернет-магазин) Партнера
        $marketplace = $auth->createRole('marketplace');
        $marketplace->description = 'интернет-магазин';
        $auth->add($marketplace);
        $auth->addChild($marketplace, $saleServices);
        // тестовые пользователи
        $this->actionUserAssignRole('marketplace', 'oschad24@oschadbank.ua', 'oschadbank');

        // группа Оператор (кассир) Партнера
        $cashier = $auth->createRole('cashier');
        $cashier->description = 'Кассир Партнера';
        $auth->add($cashier);
        $auth->addChild($cashier, $saleServices);
        // тестовые пользователи
        $this->actionUserAssignRole('cashier', '10007/0000_cashier01@oschadbank.ua', '10007/0000');


        // правило проверки партнера на доступ к элементам API (грубо говоря доступ партнера к конкретным сервисам)
        $rule = new PartnerServiceAccess();
        $auth->add($rule);
        $permissionPartnerUseService = $auth->createPermission('permissionPartnerUseService');
        $permissionPartnerUseService->description = 'Право Партнера использовать сервис';
        $permissionPartnerUseService->ruleName = $rule->name;
        $auth->add($permissionPartnerUseService);
        $auth->addChild($permissionPartnerUseService, $adminServices);
        $auth->addChild($superAdmin, $permissionPartnerUseService);
        $auth->addChild($partner, $permissionPartnerUseService);

        // правило проверки оператора (кассира) партнера на доступ к элементам API (грубо говоря доступ оператора (кассира) партнера к продаже продуктов)
        $rule = new UserServiceAccess();
        $auth->add($rule);
        $permissionUserUseService = $auth->createPermission('permissionUserUseService');
        $permissionUserUseService->description = 'Право Оператором Партнера использовать сервис';
        $permissionUserUseService->ruleName = $rule->name;
        $auth->add($permissionUserUseService);
        $auth->addChild($permissionUserUseService, $saleServices);
        $auth->addChild($cashier, $permissionUserUseService);
        $auth->addChild($marketplace, $permissionUserUseService);
    }

    /**
     * Назначить роль указаному пользователю
     *
     * @param string $role Имя роли
     * @param string $email Email пользователя
     * @param string $partnerPrefix Префикс партнера
     */
    public function actionUserAssignRole($role, $email, $partnerPrefix)
    {
        $user = Partner::findOne(['prefix' => $partnerPrefix])
            ->getUsers()
            ->where(['email' => $email])
            ->one();
        if (!$user) {
            throw new InvalidParamException("There is no user \"$email\".");
        }
        Yii::$app->user->identityClass = User::className();
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($role);
        if (!$role) {
            throw new InvalidParamException("There is no role \"$role\".");
        }
        $auth->assign($role, $user->id);
    }

    /**
     * Назначить роль указаному пользователю
     *
     * @param string $role Имя роли
     * @param string $partnerPrefix Префикс партнера
     */
    public function actionPartnerAssignRole($role, $partnerPrefix)
    {
        $partner = Partner::find()
            ->where(['prefix' => $partnerPrefix])
            ->one();
        if (!$partner) {
            throw new InvalidParamException("There is no partner with prefix \"$partnerPrefix\".");
        }
        Yii::$app->user->identityClass = Partner::className();
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($role);
        if (!$role) {
            throw new InvalidParamException("There is no role \"$role\".");
        }
        $auth->assign($role, $partner->id);
    }
}
