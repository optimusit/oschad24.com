<?php
namespace console\controllers;

use Yii;
use common\components\AppHelper;
use common\models\Order;
use common\models\Partner;
use console\components\Controller;
use yii\helpers\Html;
use common\models\GatewayRequest;
use api\modules\v1\models\BackGround;

class ApplicationController extends Controller
{
    public function actionReports()
    {
        // ежедневный
        $dateFrom = (new \DateTime())->sub(new \DateInterval('P1D'));
        $this->Report($dateFrom);

        // каждый понедельник за прошлую неделю
        if (date('w', time()) == 1) {
            $dateTo = (new \DateTime())->sub(new \DateInterval('P1D'));
            $dateFrom = clone($dateTo);
            $dateFrom->sub(new \DateInterval('P6D'));
            $this->Report($dateFrom, $dateTo);
        }
    }

    private function Report(\DateTime $dateFrom, \DateTime $dateTo = null)
    {
        $to = Yii::$app->params['email']['report'];
        $subject = 'Автозвіт ';
        if (is_null($dateTo)) {
            $period = Yii::$app->formatter->asDate($dateFrom);
        } else {
            $period = Yii::$app->formatter->asDate($dateFrom) . ' - ' . Yii::$app->formatter->asDate($dateTo);
        }
        $subject .= $period;
        $header = [
            [
                'Автозвіт за період',
                $period
            ],
            [
                'Продукт',
                'Кількість квитків оплачено',
                'На суму, грн',
            ]
        ];
        $storePath = Yii::$app->params['storage']['documents'] . 'reports' . DIRECTORY_SEPARATOR;
        $partners = Partner::find()
            ->roots()
            ->asArray()
            ->all();
        foreach ($partners as $partner) {
            if ($partner['prefix'] == 'vebua') {
                continue;
            }
            $report = Order::SalesReport($partner['id'], $dateFrom, $dateTo);
            $report = array_merge($header, $report);
            $filePath = sprintf('%s%s %s.xls', $storePath, $partner['name'], $period);
            if (file_put_contents($filePath, AppHelper::Array2xls($report)) !== false) {
                Yii::$app->core->SendMail($to, $subject, null, null, [$filePath]);
            }
        }
    }

    public function actionClearLog()
    {
        $tables = [
            'log',
            'log_providers',
        ];
        foreach ($tables as $table) {
            try {
                Yii::$app->db->createCommand()
                    ->delete($table, 'log_time < :log_time', [
                        'log_time' => (new \DateTime())->sub(new \DateInterval('P'
                            . Yii::$app->params['logs']['lifetime'] . 'D'))
                            ->getTimestamp()
                    ])
                    ->execute();
            } catch (\Exception $ex) {
            }
        }
    }

    public function actionGetnews($source)
    {
        if ($source == 'oschadbank') {
            $url = 'http://www.oschadbank.ua/ua/press-service/news/rss/index.php';
        } elseif ($source == 'oschad24') {
            $url = 'http://content.oschad24.com/?feed=rss2';
        } else {
            Yii::error('wrong source', 'news');

            return;
        }
        if (($xml = simplexml_load_file($url))) {
            $parent = $xml->children()->channel;
            $result = [
                'title' => Html::encode((string)$parent->title),
                'link' => Html::encode((string)$parent->link),
                'lastBuildDate' => Html::encode((string)$parent->lastBuildDate),
            ];
            foreach ($parent->item as $item) {
                $row = [];
                foreach ($item as $k => $v) {
                    $v = (string)$v;
                    $v = str_replace([
                        "\n",
                        "\r",
                        "\t",
                    ], '', $v);
                    $v = nl2br($v);
                    $v = strip_tags($v);
                    if ($k == 'description') {
                        $v = mb_substr($v, 0, 100);
                        $v .= '...';
                    }
                    $row[$k] = $v;
                }
                $result['item'][] = $row;
            }
            Yii::$app->cache->set('news.' . $source, json_encode($result));
        }
    }

    /**
     * Завершение продажи - окончательное списане денег с карточки
     */
    public function actionPaymentscomplete()
    {
        $orders = Order::find()
            ->where([
                'status_id' => Order::STATUS_COMPLETED,
            ])
            ->andWhere('paid = 1')
            ->innerJoinWith('gatewayRequest')
            ->andWhere([
                'trtype' => GatewayRequest::REQUEST_TYPE_PREAUTH,
                'rc' => 0,
                'action' => GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY
            ])
            ->orderBy('order.id');
        echo PHP_EOL;
        echo $orders->count();
        echo PHP_EOL;

        /** @var Order $order */
        foreach ($orders->all() as $order
        ) {
            echo PHP_EOL;
            echo $order->id;
            echo PHP_EOL;

            $order->paymentCommitTest();
            sleep(1);
        }
        //$sql = $orders->createCommand()->rawSql;
    }

    /**
     * Получение баланса у SMS провайдера
     * Запуск 2-3 раза в день
     */
    public function actionUpdatesmsbalance()
    {
        $smsBalance = Yii::$app->smsProvider->getBalance();
        if ($smsBalance) {
            $keyTurbosmsBalance = 'turbosmsBalance';
            Yii::$app->keyStorage->remove($keyTurbosmsBalance);
            Yii::$app->keyStorage->set($keyTurbosmsBalance, serialize([
                'time' => time(),
                'balance' => $smsBalance,
            ]));
        }
    }

    /**
     * Получение баланса у тикетса (на данный момент, я так понимаю, не имеет смысла)
     * Запуск 2-3 раза в день
     */
    public function actionUpdateticketsbalance()
    {
        $core = new BackGround();
        $ticketsbalance = $core->Balance();
        if ($ticketsbalance['success']) {
            Yii::$app->keyStorage->set('ticketsBalance', serialize([
                'time' => time(),
                'balance' => $ticketsbalance['amount'],
            ]));
        }
    }

    /**
     * Запустить доставку SMS из очереди
     * Запуск наверное 1 раз в минуту
     */
    public function actionRunsmsdelivery()
    {
        Yii::$app->sms->runDelivery();
    }
}
