<?php
namespace console\controllers;

use Yii;
use common\models\Order;
use console\components\Controller;

class OrderController extends Controller
{
    /*
     * Перевод неоплаченых ордеров в статус ПРОСРОЧЕН
     */
    public function actionCheckexpiration()
    {
        Order::CheckExpiration();
    }

    /*
     * Обработка оплаченных заказов покупки билетов (деньги были заблокированы на карточке пользователя,
     * необходимо выполнить запрос payment/commit и setCommission)
     */
    public function actionCheckpaymentcommit()
    {
        Order::CheckPaymentCommit();
    }

    /*
     * Заказ почти выполнен, необходимо пользователю прислать документы
     */
    public function actionSenddocuments()
    {
        $order = new Order();
        $order->SendDocuments();
    }
}
