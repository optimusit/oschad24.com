<?php
namespace console\controllers;

use Yii;
use common\providers\kontramarka\Kontramarka;
use console\components\Controller;

class KontramarkaController extends Controller
{
    public function actionLoaddictionaries()
    {
        $kontramarka = new Kontramarka();
        $kontramarka->LoadDictionaries();
    }

    public function actionLoadshows()
    {
        $kontramarka = new Kontramarka();
        $kontramarka->LoadShows();
    }
}
