<?php
namespace console\controllers;

use common\providers\webclever\TmsQuery;
use Yii;
use common\testing\v1\UZTester;
use common\testing\v1\Tester;
use console\components\Controller;

class TestproviderController extends Controller
{
    public function actionGd($accessToken, $urlAPI, $random = true, $lang = 'uk', $config = '')
    {
        parse_str($config, $config);
        $tester = new Tester();

        return $tester->Gd($accessToken, $urlAPI, $random, $lang, $config);
    }

    public function actionGdbookingshow($accessToken, $urlAPI, $orderId)
    {
        $tester = new Tester();

        return $tester->GdBookingShow($accessToken, $urlAPI, $orderId);
    }

    public function actionGdbookingslist($accessToken, $urlAPI)
    {
        $tester = new Tester();

        return $tester->GdBookingsList($accessToken, $urlAPI);
    }

    public function actionBus($accessToken, $urlAPI, $random = true, $lang = 'uk', $config = '')
    {
        parse_str($config, $config);
        $tester = new Tester();

        return $tester->Bus($accessToken, $urlAPI, $random, $lang, $config);
    }

    public function actionTicketscommit($orderId)
    {
        $tester = new Tester();

        return $tester->TicketsCommit($orderId);
    }

    public function actionTicketsgetdocument($orderId)
    {
        $tester = new Tester();

        return $tester->TicketsGetDocument($orderId)
            ? 'Успешно'
            : 'Провал';
    }

    public function actionAvia($accessToken, $urlAPI, $random = true, $lang = 'uk', $config = '')
    {
        parse_str($config, $config);
        $tester = new Tester();

        return $tester->Avia($accessToken, $urlAPI, $random, $lang, $config);
    }

    public function actionShakhtar($accessToken, $urlAPI)
    {
        $tester = new Tester();

        return $tester->Shakhtar($accessToken, $urlAPI);
    }

    public function actionAccidents5($accessToken, $urlAPI)
    {
        $tester = new Tester();

        return $tester->Accidents5($accessToken, $urlAPI);
    }

    public function actionUz()
    {
        $tester = new UZTester();

        return $tester->State();
    }

    public function actionKontramarka()
    {
        $tester = new \common\testing\v1\KontramarkaTester(74);
        $tester->Booking();

        return true;
    }

    public function actionWebclever()
    {
        $tester = new \common\testing\v1\WebCleverTester();
        $tester->Booking();
//		$webclever = new TmsQuery();
//		$webclever->token = 5345843;
//		$webclever->api_url = 'http://tms.webclever.in.ua/api';
//		$list = $webclever->getCountryList();

        return true;
    }
}
