<?php
namespace console\controllers;

use Yii;
use common\providers\karabas\Karabas;
use console\components\Controller;

class KarabasController extends Controller
{
    public function actionLoaddictionaries()
    {
        $karabas = new Karabas();
        $karabas->LoadDictionaries();
    }

    public function actionLoadpartnerevents()
    {
        $karabas = new Karabas();
        $karabas->LoadPartnerEvents();
    }
}
