<?php

namespace common\payments\CommerceGateway;

use Yii;
use api\modules\v1\components\ApiException;
use api\modules\v1\Module;
use common\components\AppHelper;
use yii\base\Exception;
use common\models\GatewayRequest;
use common\models\Order;
use yii\web\HttpException;

class CommerceGateway
{
    static private function getMerchant($for = '*')
    {
        if (!isset(Yii::$app->params['payment']['commerce_gateway']['merchant'][$for])) {
            return Yii::$app->params['payment']['commerce_gateway']['merchant']['*'];
        }

        return Yii::$app->params['payment']['commerce_gateway']['merchant'][$for];
    }

    static public function getMerchantId($for = '*')
    {
        $merchant = self::getMerchant($for);

        return $merchant['id'];
    }

    static public function getMerchantName($for = '*')
    {
        $merchant = self::getMerchant($for);

        return $merchant['name'];
    }

    static public function getMerchantUrl($for = '*')
    {
        $merchant = self::getMerchant($for);

        return $merchant['url'];
    }

    static public function getMerchantTerminalId($for = '*')
    {
        $merchant = self::getMerchant($for);

        return $merchant['terminal_id'];
    }

    static public function getMerchantSecretKey($for = '*')
    {
        $merchant = self::getMerchant($for);

        return $merchant['secret_key'];
    }

    static public function getMerchantIPNUrl($for = '*')
    {
        $url = Yii::$app->urlManagerAPI->createUrl([
            '/v1/front/egateway/notify',
        ], true);

        return $url;
    }

    static public function getMerchantIPNEmail($for = '*')
    {
        $merchant = self::getMerchant($for);

        return $merchant['ipn_email'];
    }

    static public function getGateWayUrl()
    {
        return Yii::$app->params['payment']['commerce_gateway']['gateway_url'];
    }

    static public function getCAFilePath()
    {
        return Yii::getAlias('@3dscafile');
    }

    static public function getGateWayIP()
    {
        return Yii::$app->params['payment']['commerce_gateway']['gateway_ip'];
    }

    static public function getMerchantGMT()
    {
        $timezoneOffset = date('Z');
        $timezoneOffset = ceil($timezoneOffset / 3600);
        if ($timezoneOffset > 0) {
            $timezoneOffset = '+' . $timezoneOffset;
        }

        return (string)$timezoneOffset;
    }

    static public function getMerchantTime($time = null)
    {
        $currentTimeZone = date_default_timezone_get();
        date_default_timezone_set("UTC");
        if (is_null($time)) {
            $time = time();
        }
        $result = date('YmdHis', $time);
        date_default_timezone_set($currentTimeZone);

        return $result;
    }

    static public function mergeMAC(array $data)
    {
        $string = '';
        foreach ($data as $field) {
            if ($field === '') {
                $field = '-';
            } else {
                $field = strlen($field) . $field;
            }
            $string .= $field;
        }

        return $string;
    }

    static public function genMAC($data, $key)
    {
        if (is_array($data)) {
            $data = self::mergeMAC($data);
        }
        $key = pack("H*", $key);

        return hash_hmac('sha1', $data, $key);
    }

    /**
     * @param Order $order
     * @return array
     * @throws HttpException
     * @throws ApiException
     */
    static public function createAuthorizationData(Order $order)
    {
        if ($order->isExpired()) {
            throw new ApiException(Module::t('app', 'Термін дії замовлення вичерпано'), 20);
        }
        if ($order->paid
            && GatewayRequest::find([
                'order_id' => $order->id,
                'trtype' => 0,
                'action' => (string)GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY,
                'rc' => '0'
            ])
                ->exists()
        ) {
            throw new ApiException(Module::t('app', 'Платіж вже здійснено'), 20);
        }
        $method = 'oschadnybank';
        $settings = '*';
        if ($order->isInsurance()) {
            $settings = $order->getService()
                ->one()->code;
        }
        $timestamp = self::getMerchantTime();
        $authorizationData = [
            'AMOUNT' => sprintf('%.2f', $order->amount),
            'CURRENCY' => 'UAH',
            'ORDER' => $order->id,
            'DESC' => mb_substr($order->getDescription(true), 0, 50),
            'MERCH_NAME' => self::getMerchantName($settings),
            'MERCH_URL' => Yii::$app->urlManagerFront->baseUrl,
            'MERCHANT' => self::getMerchantId($settings),
            'TERMINAL' => self::getMerchantTerminalId($settings),
            'EMAIL' => self::getMerchantIPNEmail($settings),
            'TRTYPE' => GatewayRequest::REQUEST_TYPE_PREAUTH,
            'COUNTRY' => 'UA',
            'MERCH_GMT' => self::getMerchantGMT(),
            'TIMESTAMP' => $timestamp,
            'NONCE' => AppHelper::hex(AppHelper::gen(16)),
            'BACKREF' => Yii::$app->urlManagerFront->createUrl([
                '',
                'orderId' => $order->id,
                'timestamp' => $timestamp,
                '#' => 'payment'
            ], true)
        ];

        $gatewayRequest = new GatewayRequest();
        $gatewayRequest->attributes = [
            'terminal' => $authorizationData['TERMINAL'],
            'order_id' => $authorizationData['ORDER'],
            'method' => $method,
            'trtype' => $authorizationData['TRTYPE'],
            'amount' => $order->amount,
            'currency' => $authorizationData['CURRENCY'],
            'timestamp' => $authorizationData['TIMESTAMP'],
            'nonce' => $authorizationData['NONCE']
        ];
        if (!$gatewayRequest->save(false)) {
            throw new HttpException(500);
        }
        $sign = self::genMAC($authorizationData, CommerceGateway::getMerchantSecretKey($order->getService()
            ->one()->code));
        $authorizationData['P_SIGN'] = $sign;
        $authorizationData = [
            'Form' => $authorizationData,
            'Action' => self::getGateWayUrl(),
        ];
        Yii::info($authorizationData, 'egatewayAuthorizationData');

        return $authorizationData;
    }

    static public function request($url, $data = [])
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CAINFO, self::getCAFilePath());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_USERAGENT, '');
        $result = curl_exec($curl);
        if (!curl_errno($curl)) {
            return $result;
        } else {
            throw new Exception(curl_error($curl));
        }
    }

    /**
     * @param GatewayRequest $gatewayRequest
     * @return bool
     */
    static public function salesCompletionRequest(GatewayRequest $gatewayRequest)
    {
//		throw new ApiException(Module::t('app', 'todo проверка на наличие оплаты'), 300);
//        if (TEST_SERVER) {
//            return true;
//        }
        /** @var Order $order */
        $order = $gatewayRequest->getOrder()
            ->one();
        $settings = '*';
        if ($order->isInsurance()) {
            $settings = $order->getService()
                ->one()->code;
        }
        $data = [
            'ORDER' => $order->id,
            'AMOUNT' => sprintf('%.2f', $gatewayRequest->amount),
            'CURRENCY' => $gatewayRequest->currency,
            'RRN' => $gatewayRequest->rrn,
            'INT_REF' => $gatewayRequest->int_ref,
            'TRTYPE' => GatewayRequest::REQUEST_TYPE_COMPLETE,
            'TERMINAL' => $gatewayRequest->terminal,
            'BACKREF' => self::getMerchantIPNUrl($order->getService()
                ->one()->code),
            'TIMESTAMP' => self::getMerchantTime(),
            'NONCE' => $gatewayRequest->nonce,
        ];
        $data['P_SIGN'] = self::genMAC($data, CommerceGateway::getMerchantSecretKey($order->getService()
            ->one()->code));
        $data['EMAIL'] = self::getMerchantIPNEmail($settings);
        $encoded = '';
        foreach ($data as $name => $value) {
            $encoded .= urlencode($name) . '=' . urlencode($value) . '&';
        }
        $encoded = rtrim($encoded, '&');
        $result = true;
        Yii::info(var_export($data, true), 'egatewaySalesCompletion');
        try {
            $output = self::request(self::getGateWayUrl(), $encoded);
            Yii::info(var_export($output, true), 'egatewaySalesCompletion');
        } catch (Exception $e) {
            $result = false;
            Yii::error($e->getCode() . ' ' . $e->getMessage(), 'egatewaySalesCompletion');
        }

        return $result;
    }

    static public function reversalRequest(GatewayRequest $gatewayRequest, $reversalAmount = null)
    {
//		if (TEST_SERVER) {
//			return true;
//		}
        /** @var Order $order */
        $order = $gatewayRequest->getOrder()
            ->one();
        $data = [
            'ORDER' => $order->id,
            'ORG_AMOUNT' => sprintf('%.2f', $gatewayRequest->order->amount),
            'AMOUNT' => sprintf('%.2f', $reversalAmount
                ? $reversalAmount
                : $gatewayRequest->amount),
            'CURRENCY' => $gatewayRequest->currency,
            'RRN' => $gatewayRequest->rrn,
            'INT_REF' => $gatewayRequest->int_ref,
            'TRTYPE' => GatewayRequest::REQUEST_TYPE_REVERSAL_ADVICE,
            'TERMINAL' => $gatewayRequest->terminal,
            'BACKREF' => CommerceGateway::getMerchantIPNUrl($gatewayRequest->order->getService()
                ->one()->code),
            'TIMESTAMP' => CommerceGateway::getMerchantTime(),
            'NONCE' => $gatewayRequest->nonce,
        ];
        $data['P_SIGN'] = self::genMAC($data, CommerceGateway::getMerchantSecretKey($order->getService()
            ->one()->code));
        $encoded = '';
        foreach ($data as $name => $value) {
            $encoded .= urlencode($name) . '=' . urlencode($value) . '&';
        }
        $encoded = rtrim($encoded, '&');
        $result = true;
        Yii::info(var_export($data, true), 'egatewayReversalRequest');
        try {
            $output = self::request(self::getGateWayUrl(), $encoded);
            Yii::info(var_export($output, true), 'egatewayReversalRequest');
        } catch (Exception $e) {
            $result = false;
            Yii::error($e->getCode() . ' ' . $e->getMessage(), 'egatewayReversalRequest');
        }

        return $result;
    }
}
