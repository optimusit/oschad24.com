<?php
namespace common\testing\v1;

use yii\base\Exception;

class Shakhtar extends ProviderTesting
{
    public function Events()
    {
        $serverResponse = $this->_restClient->get('shakhtar/events', [
            'accessToken' => $this->_accessToken,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['events'] = $data;

        return $data;
    }

    public function Sectors($sessionId, $eventId)
    {
        $serverResponse = $this->_restClient->get('shakhtar/sectors', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'eventId' => $eventId,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['events'] = $data;

        return $data;
    }

    public function Sector($sessionId, $eventId, $sectorId)
    {
        $serverResponse = $this->_restClient->get('shakhtar/sector', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'eventId' => $eventId,
            'sectorId' => $sectorId,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['events'] = $data;

        return $data;
    }

    public function Checkout($sessionId, $eventId, $sectorId, $seats, $phone, $email, $customerName, $customerSurname)
    {
        $serverResponse = $this->_restClient->get('shakhtar/checkout', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'eventId' => $eventId,
            'sectorId' => $sectorId,
            'seats' => json_encode($seats),
            'phone' => $phone,
            'email' => $email,
            'name' => $customerName,
            'surname' => $customerSurname,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['events'] = $data;

        return $data;
    }

    public function GetSector($sectors, $number = -1)
    {
        if ($number < 0) {
            $n = rand(0, count($sectors));
        } else {
            $n = $number;
        }
        if ($sectors[$n]->IsFree) {
            return $sectors[$n];
        } else {
            $number++;

            return $this->GetSector($sectors, $number);
        }
    }

    public function GetRandomSeats($seats, $number = 2)
    {
        $remain = $number;
        $result = [];
        foreach ($seats as &$seat) {
            if ($seat->IsFree) {
                $seat->IsFree = false;
                $result[] = $seat->SeatId;
                $remain--;
                if (!$remain) {
                    break;
                }
            }
        }

        return $result;
    }
}
