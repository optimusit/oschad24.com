<?php
namespace common\testing\v1;

use yii\base\Exception;

class Gd extends ProviderTesting
{
    protected $_destinations = [
        'запоріжжя 1',
        'київ-пасажирський',
        //		'харків-пас',
        //		'івано-франківськ південно-західна',
        //		'ковель південно-західна',
        //		'суми південно-західна',
        //		'шостка південно-західна',
    ];

    public function Searchstation($searchStationName, $sessionId = null, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('rail/searchstation', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'searchStationName' => $searchStationName,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['searchstation'] = $data;

        return $data;
    }

    public function Searchtrain($from, $to, $date, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('rail/searchtrain', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'from' => $from,
            'to' => $to,
            'date' => $date,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $date = $serverResponse->decode_response();
        $this->response['searchtrain'] = $date;

        return $date;
    }

    public function TrainRoute($from, $to, $date, $trainNumber, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('rail/trainroute', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'from' => $from,
            'to' => $to,
            'date' => $date,
            'trainNumber' => $trainNumber,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $date = $serverResponse->decode_response();
        $this->response['searchtrain'] = $date;

        return $date;
    }

    public function Traininfo($trainNumber, $carClass, $carSubclass, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('rail/traininfo', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'trainNumber' => $trainNumber,
            'carClass' => $carClass,
            'carSubclass' => $carSubclass,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['traininfo'] = $data;

        return $data;
    }

    public function Carinfo($carId, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('rail/carinfo', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'carId' => $carId,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['carinfo'] = $data;

        return $data;
    }

    public function Checkout($passengers, $operationType, $noClothes, $getCommissionOnly, $phone, $email, $customerName, $customerSurname, $customerPatronymic, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('rail/checkout', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'passengers' => json_encode($passengers),
            'operationType' => $operationType,
            'noClothes' => $noClothes,
            'getCommissionOnly' => $getCommissionOnly,
            'phone' => $phone,
            'email' => $email,
            'name' => $customerName,
            'surname' => $customerSurname,
            //'patronymic' => $customerPatronymic,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['reservation'] = $data;

        return $data;
    }

    public function BookingShow($orderId)
    {
        $serverResponse = $this->_restClient->get('rail/bookingshow', [
            'accessToken' => $this->_accessToken,
            'orderId' => $orderId,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['reservation'] = $data;

        return $data;
    }

    public function BookingsList()
    {
        $serverResponse = $this->_restClient->get('rail/bookingslist', [
            'accessToken' => $this->_accessToken,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['reservation'] = $data;

        return $data;
    }

    public function getFreeSeat(&$car)
    {
        foreach ($car->seats as &$seat) {
            // типы мест определены здесь - http://wiki.v2.api.tickets.ua/railway/search
            if ($seat->type < 11) {
                $seat->type += 10;

                return $seat->number;
            }
        }

        return null;
    }
}
