<?php
namespace common\testing\v1;

use yii\base\Exception;

class Accidents5 extends ProviderTesting
{
    public function Calculate($sessionId, $period, $cover, $plan)
    {
        $serverResponse = $this->_restClient->get('aiwa/accidents5/calculate', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'period' => $period,
            'cover' => $cover,
            'plan' => $plan
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['events'] = $data;

        return $data;
    }

    public function Document($sessionId, $form)
    {
        $serverResponse = $this->_restClient->get('aiwa/accidents5/document', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'form' => json_encode($form),
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['events'] = $data;

        return $data;
    }
}
