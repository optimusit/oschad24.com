<?php
namespace common\testing\v1;

use common\providers\webclever\WebClever;

class WebCleverTester
{
    private $_siteId;
    private $_provider;
    private $_name = 'Иван Иванов';
    private $_email = 'andrej.lola@gmail.com';
    private $_phone = '0000000000';

    public function __construct()
    {
        //$this->_siteId = $siteId;
        $this->_provider = new WebClever();
    }

    public function Countries()
    {
        $result = $this->_provider->getCountries();

        return $result['data'];
    }

    public function Cities()
    {
        $result = $this->_provider->getCities();

        return $result['data'];
    }

    public function Trees()
    {
        $result = $this->_provider->getTrees();

        return $result['data'];
    }

    public function Tags()
    {
        $result = $this->_provider->getEventTags();

        return $result['data'];
    }

    public function Events($city = 0, $limit = 100, $offset = 0)
    {
        $result = $this->_provider->getEventList([
            'city' => $city,
            'limit' => $limit,
            'offset' => $offset,
        ]);

        return $result['data'];
    }

    public function MapInfo($eventId)
    {
        $result = $this->_provider->getMapInfo([
            'event_id' => $eventId,
        ]);

        return $result['data'];
    }

    public function Sectors($eventId)
    {
        $result = $this->_provider->getSectors([
            'event_id' => $eventId,
        ]);

        return $result['data'];
    }

    public function Prices($eventId, $sectorId)
    {
        $result = $this->_provider->getPrices([
            'event_id' => $eventId,
            'sector_id' => $sectorId,
        ]);

        return $result['data'];
    }

    public function Places($eventId, $sectorId)
    {
        $result = $this->_provider->getPlaces([
            'event_id' => $eventId,
            'sector_id' => $sectorId,
        ]);

        return $result['data'];
    }

    public function SliderEvent($cityId)
    {
        $result = $this->_provider->getSliderEvent([
            'city_id' => $cityId,
        ]);

        return $result['data'];
    }

    public function SliderByCity($cityId)
    {
        $result = $this->_provider->getSliderByCity([
            'city_id' => $cityId,
        ]);

        return $result['data'];
    }

    public function Slider($sliderId = null)
    {
        $params = [];
        if (!is_null($sliderId)) {
            $params['city_id'] = $sliderId;
        }
        $result = $this->_provider->getSliderEvent($params);

        return $result['data'];
    }

    public function Orders()
    {
        $result = $this->_provider->getOrders();

        return $result['data'];
    }

    public function Booking()
    {
//		$countries = $this->Countries();
        $cities = $this->Cities();
//		$orders = $this->Orders();//?
        $events = $this->Events();
//		$trees = $this->Trees();//?
//		$tags = $this->Tags();
//		$map = $this->MapInfo($events[0]->id);//?
//		$sliderEvent = $this->SliderEvent($cities[1]->id);
//		$slider = $this->Slider();
//		$slider = $this->SliderByCity($cities[1]->id);
        $sectors = $this->Sectors($events[0]->id);
        //$prices = $this->Prices($events[0]->id, $sectors[0]->id);
        $places = $this->Places($events[0]->id, $sectors[0]->id);
    }
}
