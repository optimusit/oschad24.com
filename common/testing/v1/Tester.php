<?php
namespace common\testing\v1;

use common\models\Order;
use yii\helpers\Console;
use api\modules\v1\models\BackGround;
use common\providers\tickets\GdTickets;
use yii\helpers\VarDumper;

class Tester
{
    protected $_service;
    protected $phone = '+380957925818';
    protected $email = 'andrej.lola@gmail.com';
    protected $customerName = 'Иван';
    protected $customerSurname = 'Иванов';
    protected $customerPatronymic = 'Иванович';

    /**
     * @param string $accessToken
     * @param string $urlAPI
     * @param bool $random
     * @param string $lang
     * @param array $config
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function Gd($accessToken, $urlAPI, $random = true, $lang = 'uk', $config = [])
    {
        $this->_service = new Gd($accessToken, $urlAPI);

        if ($random === true) {
            $daysAdd = rand(20, 60);
            $date = (new \DateTime())->add(new \DateInterval('P' . $daysAdd . 'D'))
                ->format('d-m-Y');
            $fromName = $this->_service->GetRandomDestination();
            $toName = $this->_service->GetRandomDestination();
        } else {
            $date = $config['date'];
            $fromName = $config['from'];
            $toName = $config['to'];
        }
        echo 'Когда - ' . $date . PHP_EOL;

        echo 'Откуда - ';
        $result = $this->_service->Searchstation(mb_substr($fromName, 0, 3), null, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $sessionId = $result->sessionId;
        $from = $this->_service->GetStationObjectGd($result->data, $fromName);
        echo $from->value . PHP_EOL;

        echo 'Куда - ';
        $result = $this->_service->Searchstation(mb_substr($toName, 0, 3), $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $to = $this->_service->GetStationObjectGd($result->data, $toName);
        echo $to->value . PHP_EOL;

        echo 'Все поезда' . PHP_EOL;
        $result = $this->_service->Searchtrain($from->id, $to->id, $date, $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $searchtrain = $result->data;

        foreach ($searchtrain->trains as $train) {
            echo "\t" . $train->number;
            echo ' Отпр ' . $train->departure_date . ' ' . $train->departure_time;
            echo ', Приб ' . $train->arrival_date . ' ' . $train->arrival_time;
            echo PHP_EOL;
        }

        $train = $searchtrain->trains[rand(0, count($searchtrain->trains) - 1)];
        $trainNumber = $train->number;
        if (is_array($train->class_list)) {
            $class = $train->class_list[rand(0, count($train->class_list) - 1)];
        } else {
            $class = $train->class_list->class;
        }
        $carClass = $class->name;
        $carSubclass = $class->subclass;
        echo 'Информация по поезду' . PHP_EOL;
        $result = $this->_service->Traininfo($trainNumber, $carClass, $carSubclass, $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $traininfo = $result->data->trainInfo;
        $carinfo = $result->data->carInfo;

        echo $train->number;
        echo ', ' . $traininfo->train_class_label . ', ' . $traininfo->train_speed_label;
        echo ', Цена билета (грн) - ' . $traininfo->class->cost;
        echo PHP_EOL;


        echo 'Промежуточные станции следования' . PHP_EOL;
        $result = $this->_service->TrainRoute($from->id, $to->id, $date, $trainNumber, $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $trainRoute = $result->data;
        foreach ($trainRoute->stations as $item) {
            echo "\t" . $item->name;
            echo ', Приб ' . $item->time_arrival;
            echo ' Отпр ' . $item->time_departure;

            echo PHP_EOL;
        }

        $passengers = [
            [
                'name' => 'Иван',
                'surname' => 'Иванов',
                'carNumber' => $carinfo->number,
                'seat' => $this->_service->getFreeSeat($carinfo),
                'animal' => 1,
                'facilities' => 1,
                'excess_baggage' => 1,
            ],
        ];

        // типы операций определены здесь - http://wiki.v2.api.tickets.ua/railway/search
        $operationType = GdTickets::OPERATION_TYPE_E;
        //$operationType = $this->_service->getFreeSeat($carinfo);
        $noClothes = false;
        $getCommissionOnly = false;
        echo 'Бронирование' . PHP_EOL;
        $result = $this->_service->Checkout($passengers, $operationType, $noClothes, $getCommissionOnly, $this->phone,
            $this->email, $this->customerName, $this->customerSurname, $this->customerPatronymic, $sessionId, $lang);

        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $orderInfo = $result->data->OrderInfo;
        foreach ($orderInfo->passengers as $passenger) {
            echo 'Вагон №' . $passenger->car . ' (' . $passenger->class . ')';
            echo ', Место №' . $passenger->seat;
            echo ', ' . $passenger->passenger;
            echo ', ' . $passenger->type;
            echo PHP_EOL;
        }
        echo 'Сумма (грн) - ' . $orderInfo->cost;
        echo ', срок годности для оплаты - ' . $orderInfo->expiration_date . ' ' . $orderInfo->expiration_time;

        return true;
    }

    /**
     * @param string $accessToken
     * @param string $urlAPI
     * @param bool $random
     * @param string $lang
     * @param array $config
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function Bus($accessToken, $urlAPI, $random = true, $lang = 'uk', $config = [])
    {
        $this->_service = new Bus($accessToken, $urlAPI);

        if ($random === true) {
            $daysAdd = rand(20, 60);
            $date = (new \DateTime())->add(new \DateInterval('P' . $daysAdd . 'D'))
                ->format('d-m-Y');
            $fromName = $this->_service->GetRandomDestination();
            $toName = $this->_service->GetRandomDestination();
        } else {
            $date = $config['date'];
            $fromName = $config['from'];
            $toName = $config['to'];
        }
        echo 'Когда - ' . $date . PHP_EOL;

        echo 'Откуда - ';
        $result = $this->_service->Searchstation(mb_substr($fromName, 0, 3), null, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $sessionId = $result->sessionId;
        $from = $this->_service->GetStationObjectBus($result->data, $fromName);
        echo $from->value . PHP_EOL;

        echo 'Куда - ';
        $result = $this->_service->Searchstation(mb_substr($toName, 0, 3), $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $to = $this->_service->GetStationObjectBus($result->data, $toName);
        echo $to->value . PHP_EOL;

        echo 'Все автобусы' . PHP_EOL;
        $result = $this->_service->Searchbus($from->id, $to->id, $date, $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $searchbus = $result->data;

        foreach ($searchbus->buses as $bus) {
            echo "\t" . $bus->name;
            echo ' Отпр ' . $bus->station->departure_from_point_date . ' ' . $bus->station->departure_from_point_time;
            echo ', Приб ' . $bus->station->arrival_to_point_date . ' ' . $bus->station->arrival_to_point_time;
            echo ', Мест - ' . $bus->free_places;
            echo PHP_EOL;
            break;
        }

        echo 'Информация по рейсу' . PHP_EOL;
        $result = $this->_service->Tripinfo($bus->station->departure_code, $bus->station->arrival_code, $bus->number,
            $date, $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $tripinfo = $result->data;
        echo 'Рейс №' . $tripinfo->number;
        echo ', Перевозщик - ' . $tripinfo->carrier;
        echo ', Цена (грн) - ' . $tripinfo->amount;
        echo ', Мест - ' . count($tripinfo->places);
        echo PHP_EOL;

        $passengers = [
            [
                'name' => 'Иван',
                'surname' => 'Иванов',
                'seat' => $this->_service->getFreePlace($tripinfo),
            ],
        ];

        echo 'Информация по бронированию' . PHP_EOL;
        $result = $this->_service->Checkout($passengers, $this->phone, $this->email, $this->fullName, $sessionId,
            $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $orderInfo = $result->data->OrderInfo;
        echo 'Рейс №' . $orderInfo->number;
        echo ', ' . $orderInfo->voyage;
        echo ', цена (грн) - ' . $orderInfo->amount;
        echo PHP_EOL;
        foreach ($orderInfo->passengers as $passenger) {
            echo 'Место №' . $passenger->seat;
            echo ', ' . $passenger->passenger;
            echo PHP_EOL;
        }
    }

    /**
     * @param string $accessToken
     * @param string $urlAPI
     * @param bool $random
     * @param string $lang
     * @param array $config
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function Avia($accessToken, $urlAPI, $random = true, $lang = 'uk', $config = [])
    {
        $this->_service = new Avia($accessToken, $urlAPI);

        if ($random === true) {
            $daysAdd = rand(180, 200);
            $date = (new \DateTime())->add(new \DateInterval('P' . $daysAdd . 'D'))
                ->format('d-m-Y');
            $fromName = $this->_service->GetRandomDestination();
            $toName = $this->_service->GetRandomDestination();
        } else {
            $date = $config['date'];
            $fromName = $config['from'];
            $toName = $config['to'];
        }
        echo 'Когда - ' . $date . PHP_EOL;

        echo 'Откуда - ';
        $result = $this->_service->Searchstation(mb_substr($fromName, 0, 3), null, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $sessionId = $result->sessionId;
        $from = $this->_service->GetStationObjectAvia($result->data, $fromName);
        echo $from->value . PHP_EOL;

        echo 'Куда - ';
        $result = $this->_service->Searchstation(mb_substr($toName, 0, 3), $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $to = $this->_service->GetStationObjectAvia($result->data, $toName);
        echo $to->value . PHP_EOL;

        echo 'Все перелеты' . PHP_EOL;
        $destinations[] = [
            'departure' => $from->id,
            'arrival' => $to->id,
            'date' => $date,
        ];
        $adt = 1;
        $chd = 0;
        $inf = 0;
        $class = 'A';
        $result = $this->_service->Searchaircraft($destinations, $adt, $chd, $inf, $class, $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $aircrafts = $result->data;

        echo 'Кол-во - ' . count($aircrafts) . PHP_EOL;
        $aircraft = $aircrafts[array_rand($aircrafts)];
        echo 'Наш рейс - ' . $aircraft->parts[0]->flight_number . PHP_EOL;
        echo 'Самолет - ' . $aircraft->parts[0]->aircraft . PHP_EOL;
        echo 'Цена билета (грн) - ' . $aircraft->amount . PHP_EOL;

        //echo 'Условия перелета - ';
        //$result = $this->_service->Conditions($aircraft->id, $sessionId, $lang);
        //if ($result->code != 0) {
        //	$this->EchoError($result);

        //	return false;
        //}
        //$conditions = $result->data;
        //echo 'Ok';
        echo PHP_EOL;

        $passengers = [
            //взрослые
            [
                'type' => 'ADT',
                'name' => 'Ivan',
                'surname' => 'Ivanov',
                'patronymic' => 'Ivanovich',
                'birthday' => '20.02.2000',
                'gender' => 'M',
                'citizenship' => 'UA',
                'docnum' => 'ADT1234560',
                'doc_expire_date' => '01.01.2040'
            ],
            //			[
            //				'type' => 'ADT',
            //				'name' => 'adult1',
            //				'surname' => 'adult1',
            //				'patronymic' => 'adult1',
            //				'birthday' => '29.02.2004',
            //				'gender' => 'M',
            //				'citizenship' => 'UA',
            //				'docnum' => 'ADT1234560',
            //				'doc_expire_date' => '01.01.2040'
            //			],
            //			[
            //				'type' => 'ADT',
            //				'name' => 'adult2',
            //				'surname' => 'adult2',
            //				'patronymic' => 'adult2',
            //				'birthday' => '28.02.2004',
            //				'gender' => 'M',
            //				'citizenship' => 'UA',
            //				'docnum' => 'ADT1234561',
            //				'doc_expire_date' => '01.01.2040'
            //			],
            //
            //			//дети
            //			[
            //				'type' => 'CHD',
            //				'name' => 'child1',
            //				'surname' => 'child1',
            //				'patronymic' => 'child1',
            //				'birthday' => '01.03.2004',
            //				'gender' => 'M',
            //				'citizenship' => 'UA',
            //				'docnum' => 'CHD1234560',
            //				'doc_expire_date' => '01.01.2040'
            //			],
            //			[
            //				'type' => 'CHD',
            //				'name' => 'child2',
            //				'surname' => 'child2',
            //				'patronymic' => 'child2',
            //				'birthday' => '28.02.2014',
            //				'gender' => 'M',
            //				'citizenship' => 'UA',
            //				'docnum' => 'CHD1234560',
            //				'doc_expire_date' => '01.01.2040'
            //			],
            //
            //			//младенцы
            //			[
            //				'type' => 'INF',
            //				'name' => 'infant1',
            //				'surname' => 'infant1',
            //				'patronymic' => 'infant1',
            //				'birthday' => '01.03.2015',
            //				'gender' => 'M',
            //				'citizenship' => 'UA',
            //				'docnum' => 'INF1234560',
            //				'doc_expire_date' => '01.01.2040'
            //			],
            //			[
            //				'type' => 'INF',
            //				'name' => 'infant2',
            //				'surname' => 'infant2',
            //				'patronymic' => 'infant2',
            //				'birthday' => '28.02.2016',
            //				'gender' => 'M',
            //				'citizenship' => 'UA',
            //				'docnum' => 'INF1234560',
            //				'doc_expire_date' => '01.01.2040'
            //			],
        ];

        echo PHP_EOL;
        echo 'Информация по бронированию' . PHP_EOL;
        $result = $this->_service->Checkout($aircraft->id, $this->phone, $this->email, $this->customerName,
            $this->customerSurname, $passengers, $sessionId, $lang);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        $reservationInfo = $result->data;
        echo "\tСтоимость (грн) - " . $aircraft->amount . PHP_EOL;
        echo "\tПродолжительность - " . $aircraft->duration . PHP_EOL;
        echo "\tПерелеты:" . PHP_EOL;
        foreach ($reservationInfo->parts as $part) {
            echo "\t\t" . $part->departure_city;
            echo ' (' . $part->departure_airport . ')';
            echo ', ' . $part->departure_date . ' ' . $part->departure_time;
            echo ' - ' . $part->arrival_city;
            echo ' (' . $part->arrival_airport . ')';
            echo ', ' . $part->arrival_date . ' ' . $part->arrival_time;
            echo PHP_EOL;
            echo "\t\t\tПересадки:" . PHP_EOL;
            foreach ($part->segments as $segment) {
                echo "\t\t\t\t";
                echo $segment->departure_airport;
                echo ', ' . $segment->departure_city;
                echo ', ' . $segment->departure_country;
                echo ', ' . $segment->departure_date . ' ' . $segment->departure_time;
                echo ' - ' . $segment->arrival_airport;
                echo ', ' . $segment->arrival_city;
                echo ', ' . $segment->arrival_country;
                echo ', ' . $segment->arrival_date . ' ' . $segment->arrival_time;
                echo ', Перевозщик - ' . $segment->supplier;
                echo PHP_EOL;
            }
        }
        echo "\tПассажиры:" . PHP_EOL;
        foreach ($reservationInfo->passengers as $passenger) {
            echo "\t\t";
            echo $passenger->name . ' ' . $passenger->surname;
            echo PHP_EOL;
        }
    }

    public function TicketsCommit($orderId)
    {
        $backGround = new BackGround();
        /** @var Order $order */
        $order = Order::findOne($orderId);
        $result = $backGround->Commit($order);
        echo $result
            ? 'Успешно'
            : 'Провал';
    }

    public function TicketsGetDocument($orderId)
    {
        $backGround = new BackGround();
        /** @var Order $order */
        $order = Order::findOne($orderId);
        $result = $backGround->GetDocument($order);
        echo $result
            ? 'Успешно'
            : 'Провал';
    }

    public function GdBookingShow($accessToken, $urlAPI, $orderId)
    {
        $this->_service = new Gd($accessToken, $urlAPI);
        $result = $this->_service->BookingShow($orderId);

        VarDumper::dump($result);
    }

    public function GdBookingsList($accessToken, $urlAPI)
    {
        $this->_service = new Gd($accessToken, $urlAPI);
        $result = $this->_service->BookingsList();

        VarDumper::dump($result);
    }

    public function Shakhtar($accessToken, $urlAPI)
    {
        $this->_service = new Shakhtar($accessToken, $urlAPI);

        echo 'События' . PHP_EOL;
        $result = $this->_service->Events();
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }

        foreach ($result->data as $event) {
            echo $event->name . PHP_EOL;
            if (mb_strpos($event->name, 'Тест')) {
                $eventId = $event->id;
            }
        }

        $sessionId = $result->sessionId;

        echo 'Секторов - ';
        $result = $this->_service->Sectors($sessionId, $eventId);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        echo count($result->data) . PHP_EOL;
        $sector = $this->_service->GetSector($result->data, 10);
        echo 'Наш сектор - ' . $sector->Name . PHP_EOL;

        echo 'Цена места в секторе - ';
        $result = $this->_service->Sector($sessionId, $eventId, $sector->Id);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        echo $result->data->Price . PHP_EOL;
        $seats = $this->_service->GetRandomSeats($result->data->Seats, 1);
        echo 'Наши места - ' . var_export($seats, true);

        echo 'Резервирование' . PHP_EOL;
        $result = $this->_service->Checkout($sessionId, $eventId, $sector->Id, $seats, $this->phone, $this->email,
            $this->customerName, $this->customerSurname);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        var_export($result->data, true) . PHP_EOL;
    }

    public function Accidents5($accessToken, $urlAPI)
    {
        $this->_service = new Accidents5($accessToken, $urlAPI);

        $sessionId = 'll8fpaaJb1iB6X2HPMlk2';
        echo 'Калькуляция' . PHP_EOL;
        $period = 12;
        $cover = 3000;
        $plan = 'A';
        $result = $this->_service->Calculate($sessionId, $period, $cover, $plan);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
        //$sessionId = $result->sessionId;
        echo 'Месяцев - ' . $period . PHP_EOL;
        echo 'Сумма покрытия - ' . $cover . PHP_EOL;
        echo 'План - ' . $plan . PHP_EOL;
        echo 'Сумма - ' . $result->data->amount . PHP_EOL;

        echo PHP_EOL;
        echo PHP_EOL;
        echo 'Документ' . PHP_EOL;
        $form = [
            //'document_id',
            'date_from' => '2015-08-18',
            'date_period' => '+12m',

            'type' => 'vendor',
            'kind' => 'private',
            'phone' => '+380957925818',
            'email' => 'andrej.lola@gmail.com',
            'ipn' => '123456789012',
            'surname' => 'Иванов',
            'name' => 'Иван',
            'patronymic' => 'Иванович',
            'birthday' => '1980-02-23',
            'passport_series' => 'АЯ',
            'passport_number' => '12345678',
            'passport_date' => '2000-10-30',
            'passport_issuer' => 'Ленинским РОВД',
            'address_city' => 'Запорожье',
            'address_index' => '69000',
            'address_address' => 'пр.Ленина, д.123, кв.1',
        ];
        $result = $this->_service->Document($sessionId, $form);
        if ($result->code != 0) {
            $this->EchoError($result);

            return false;
        }
    }

    protected function EchoError($msg)
    {
        if (is_object($msg) || is_array($msg)) {
            $msg = var_export($msg);
        }
        Console::stdout(Console::ansiFormat($msg, [Console::FG_RED]));
    }
}
