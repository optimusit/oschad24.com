<?php
namespace common\testing\v1;

use common\components\RestClient;

class ProviderTesting
{
    /** @var RestClient $_restClient */
    protected $_restClient;
    /** @var array $response */
    public $response;

    protected $_accessToken;

    public function __construct($accessToken, $urlAPI)
    {
        $this->_accessToken = $accessToken;
        $this->_restClient = new RestClient([
            'base_url' => $urlAPI
        ]);
    }

    public function GetRandomDestination()
    {
        $index = rand(0, count($this->_destinations) - 1);
        $value = $this->_destinations[$index];
        unset($this->_destinations[$index]);
        $this->_destinations = array_values($this->_destinations);

        return $value;
    }

    public function GetStationObjectGd(array $list, $name)
    {
        foreach ($list as $item) {
            if (mb_strtolower($item->value) == mb_strtolower($name)) {
                return $item;
            }
        }

        return null;
    }

    public function GetStationObjectBus(array $list, $name)
    {
        foreach ($list as $item) {
            if (strpos(mb_strtolower($item->value), mb_strtolower($name)) == 0) {
                return $item;
            }
        }

        return null;
    }

    public function GetStationObjectAvia(array $list, $name)
    {
        foreach ($list as $item) {
            if (strpos(mb_strtolower($item->value), mb_strtolower($name)) == 0) {
                return $item;
            }
        }

        return null;
    }

    public function TicketsCommit($service, $orderId, $amount)
    {
        $serverResponse = $this->_restClient->get('rail/searchstation', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'name' => $name,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['searchstation'] = $data;

        return $data;
    }
}
