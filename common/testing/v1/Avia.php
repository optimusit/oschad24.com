<?php
namespace common\testing\v1;

use yii\base\Exception;

class Avia extends ProviderTesting
{
    protected $_destinations = [
        'Київ, Україна',
        'Москва, Росія',
    ];

    public function Searchstation($name, $sessionId = null, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('air/searchstation', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'searchStationName' => $name,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['searchstation'] = $data;

        return $data;
    }

    public function Searchaircraft(array $destinations, $adt, $chd, $inf, $class, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('air/searchaircraft', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'destinations' => json_encode($destinations),
            'adult' => $adt,
            'infant' => $inf,
            'child' => $chd,
            'class' => $class,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['searchaircraft'] = $data;

        return $data;
    }

    public function Conditions($recommendationId, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('air/conditions', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'recommendationId' => $recommendationId,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['aircraftinfo'] = $data;

        return $data;
    }

    public function Checkout($recommendationId, $phone, $email, $customerName, $customerSurname, array $passengers, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('air/checkout', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'recommendationId' => $recommendationId,
            'phone' => $phone,
            'email' => $email,
            'name' => $customerName,
            'surname' => $customerSurname,
            'passengers' => json_encode($passengers),
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['passenger'] = $data;

        return $data;
    }

    public function GetRandomVoyage(array $list)
    {
        return $list[array_rand($list)];
    }
}
