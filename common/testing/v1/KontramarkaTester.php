<?php
namespace common\testing\v1;

use common\providers\kontramarka\Kontramarka;

class KontramarkaTester
{
    private $_siteId;
    private $_provider;
    private $_name = 'Иван Иванов';
    private $_email = 'andrej.lola@gmail.com';
    private $_phone = '0000000000';

    public function __construct($siteId, $sessionId = null)
    {
        $this->_siteId = $siteId;
        $this->_provider = new Kontramarka($sessionId);
    }

    public function Sites()
    {
        $result = $this->_provider->Sites();

        return $result['data'];
    }

    public function Shows()
    {
        $result = $this->_provider->Shows(['siteId' => $this->_siteId]);

        return $result['data'];
    }

    public function EventMap($eventId)
    {
        $result = $this->_provider->EventMap([
            'siteId' => $this->_siteId,
            'eventId' => $eventId
        ]);

        return $result['data'][0];
    }

    public function Lock($eventId, $placeId, $priceId)
    {
        $result = $this->_provider->Lock([
            'siteId' => $this->_siteId,
            'eventId' => $eventId,
            'placeId' => $placeId,
            'priceId' => $priceId
        ]);

        return $result['data'];
    }

    public function Unlock($eventId, $placeId)
    {
        $result = $this->_provider->Unlock([
            'siteId' => $this->_siteId,
            'eventId' => $eventId,
            'placeId' => $placeId,
        ]);

        return $result['data'];
    }

    public function BasketList()
    {
        $result = $this->_provider->Basket_List();

        return $result['data'];
    }

    public function BasketReserve()
    {
        $result = $this->_provider->Basket_Reserve();

        return $result['data'];
    }

    public function BasketBuy($name, $email, $phone)
    {
        $result = $this->_provider->Basket_Buy([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        return $result['data'];
    }

    public function Booking()
    {
        $sites = $this->Sites();
        $shows = $this->Shows();
        $show = $this->getTestShow($shows);
        $eventmap = $this->EventMap($show->events[0]->eventId);
        $place = $this->getFirstFreePlace($eventmap);
        $price = $place->prices[0];
//		$lock = $this->Lock($show->events[0]->eventId, $place->placeId, $price->priceId);
//		$basketList = $this->BasketList();
//		$unlock = $this->Unlock($show->events[0]->eventId, $place->placeId);
//		$basketReserve = $this->BasketReserve();
//		$basketBuy = $this->BasketBuy($this->_name, $this->_email, $this->_phone);
//		$basketList = $this->BasketList();
    }

    private function getFirstShow($shows)
    {
        return $shows[0];
    }

    private function getTestShow($shows)
    {
        foreach ($shows as $show) {
            if ($show->name = 'test') {
                return $show;
            }
        }

        return false;
    }

    private function getFirstFreePlace($eventmap)
    {
        foreach ($eventmap->sectors as $sector) {
            foreach ($sector->rows as $row) {
                foreach ($row->places as $place) {
                    if ($place->status == Kontramarka::PLACE_STATUS_FREE) {
                        return $place;
                    }
                }
            }
        }

        return false;
    }
}
