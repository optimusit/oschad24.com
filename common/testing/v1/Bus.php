<?php
namespace common\testing\v1;

use yii\base\Exception;

class Bus extends ProviderTesting
{
    protected $_destinations = [
        'Запоріжжя, Украина',
        'Київ, Украина',
    ];

    public function Searchstation($name, $sessionId = null, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('bus/searchstation', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'name' => $name,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['searchstation'] = $data;

        return $data;
    }

    public function Searchbus($from, $to, $data, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('bus/searchbus', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'from' => $from,
            'to' => $to,
            'date' => $data,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['searchbus'] = $data;

        return $data;
    }

    public function Tripinfo($departureCode, $arrivalCode, $busNumber, $data, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('bus/tripinfo', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'departureCode' => $departureCode,
            'arrivalCode' => $arrivalCode,
            'busNumber' => $busNumber,
            'date' => $data,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['tripinfo'] = $data;

        return $data;
    }

    public function Checkout(array $passenger, $phone, $email, $fullName, $sessionId, $lang = 'uk')
    {
        $serverResponse = $this->_restClient->get('bus/checkout', [
            'accessToken' => $this->_accessToken,
            'sessionId' => $sessionId,
            'passengers' => json_encode($passenger),
            'phone' => $phone,
            'email' => $email,
            'fullName' => $fullName,
            'lang' => $lang,
        ]);
        if ($serverResponse->info->http_code != 200) {
            throw new Exception($serverResponse->response, $serverResponse->info->http_code);
        }
        $data = $serverResponse->decode_response();
        $this->response['passenger'] = $data;

        return $data;
    }

    public function getFreePlace(&$trip)
    {
        $index = rand(0, count($trip->places) - 1);
        $placeNumber = $trip->places[$index];
        unset($trip->places[$index]);
        $trip->places = array_values($trip->places);

        return $placeNumber;
    }
}
