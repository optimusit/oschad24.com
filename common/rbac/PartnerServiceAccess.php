<?php
namespace common\rbac;

use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;
use common\models\Partner;
use common\models\Service;

/**
 *
 */
class PartnerServiceAccess extends Rule
{
    public $name = 'isPartnerServiceAccess';

    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        // только если запрашиваемый контроллер является сервисом
        if (Service::find()
            ->where(['controller' => $params['service']])
            ->exists()
        ) {
            // проверить право его использовать
            return Partner::findOne(['id' => $user])
                ->haveAccessToService($params['service']);
        } else {
            return true;
        }
    }
}
