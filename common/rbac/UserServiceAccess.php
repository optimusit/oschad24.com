<?php

namespace common\rbac;

use Yii;
use common\models\Partner;
use yii\rbac\Rule;
use common\models\User;
use common\models\Service;

/**
 *
 */
class UserServiceAccess extends Rule
{
    public $name = 'isUserServiceAccess';

    /**
     * Проверка прав доступа оператора (кассира АРМ партнера) к операциям над сервисом.
     * Грубо говоря проверка права создания ...
     *
     * @param string|integer $userId the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($userId, $item, $params)
    {
        // только если запрашиваемый контроллер является сервисом
        if (Service::find()
            ->where(['controller' => $params['service']])
            ->exists()
        ) {
            // проверить право его использовать
            /** @var Partner $partnerRoot */
            $partnerRoot = User::find()
                ->where(['id' => $userId])
                ->one()
                ->getPartnerRoot()
                ->one();
            $partnerHaveAccessToService = false;
            if ($partnerRoot) {
                $partnerHaveAccessToService = $partnerRoot->haveAccessToService($params['service']);
            }

            $userRoles = [];
            foreach (Yii::$app->authManager->getRolesByUser($userId) as $role) {
                $userRoles[] = $role->name;
            }

            return $partnerHaveAccessToService
            && array_intersect([
                User::ROLE_CASHIER,
                User::ROLE_MARKETPLACE
            ], $userRoles);
        } else {
            return true;
        }
    }
}
