<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Carousel;

/**
 * CarouselQuery represents the model behind the search form about `common\models\Carousel`.
 */
class CarouselQuery extends Carousel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'service_id',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'url',
                    'image',
                    'show_begin',
                    'show_end'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Carousel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'service_id' => $this->service_id,
            'show_begin' => $this->show_begin,
            'show_end' => $this->show_end,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere([
            'like',
            'name',
            $this->name
        ])
            ->andFilterWhere([
                'like',
                'url',
                $this->url
            ])
            ->andFilterWhere([
                'like',
                'image',
                $this->image
            ]);

        return $dataProvider;
    }
}
