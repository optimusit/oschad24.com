<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%gateway_request_history}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $terminal
 * @property string $method
 * @property integer $trtype
 * @property string $amount
 * @property string $currency
 * @property string $timestamp
 * @property string $nonce
 * @property integer $action
 * @property integer $rc
 * @property string $rrn
 * @property string $int_ref
 * @property string $auth_code
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 */
class GatewayRequestHistory extends GatewayRequest
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gateway_request_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'terminal',
                    'method',
                    'trtype',
                    'amount',
                    'currency',
                    'timestamp',
                    'nonce',
                    'created_at',
                ],
                'required'
            ],
            [
                [
                    'updated_at',
                ],
                'safe'
            ],
            [
                [
                    'order_id',
                    'trtype',
                    'action',
                    'rc',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                ['amount'],
                'number'
            ],
            [
                ['terminal'],
                'string',
                'max' => 8
            ],
            [
                ['method'],
                'string',
                'max' => 18
            ],
            [
                ['currency'],
                'string',
                'max' => 3
            ],
            [
                ['timestamp'],
                'string',
                'max' => 14
            ],
            [
                [
                    'nonce',
                    'auth_code'
                ],
                'string',
                'max' => 64
            ],
            [
                ['rrn'],
                'string',
                'max' => 12
            ],
            [
                ['int_ref'],
                'string',
                'max' => 32
            ]
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
    }
}
