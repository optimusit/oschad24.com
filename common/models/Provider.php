<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "{{%provider}}".
 *
 * @property integer $id
 * @property string $prefix
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $email
 * @property string $phone
 * @property string $access_token
 * @property string $secret_key
 * @property integer $status
 *
 * @property Service[] $services
 */
class Provider extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'prefix',
                    'name',
                    'email',
                    'phone',
                    'access_token',
                    'secret_key'
                ],
                'required'
            ],
            [
                [
                    'created_at',
                    'updated_at',
                    'status'
                ],
                'integer'
            ],
            [
                [
                    'prefix',
                    'phone',
                    'access_token'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'name',
                    'email'
                ],
                'string',
                'max' => 255
            ],
            [
                ['secret_key'],
                'string',
                'max' => 60
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'prefix' => Yii::t('app', 'Префікс в системі'),
            'name' => Yii::t('app', 'Назва'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
            'email' => Yii::t('app', 'Поштова адреса'),
            'phone' => Yii::t('app', 'Телефон'),
            'access_token' => Yii::t('app', 'Маркер доступу'),
            'secret_key' => Yii::t('app', 'Секретний ключ'),
            'status' => Yii::t('app', 'Стан'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])
            ->viaTable('service_provider', ['provider_id' => 'id']);
    }
}
