<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "{{%gateway_response_description}}".
 *
 * @property integer $code
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_uk
 * @property string $description_en
 * @property string $description_ru
 * @property string $description_uk
 * @property integer $created_at
 * @property integer $updated_at
 */
class GatewayResponseDescription extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gateway_response_description}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'code',
                    'name_en',
                    'name_ru',
                    'name_uk',
                    'description_en',
                    'description_ru',
                    'description_uk',
                ],
                'required'
            ],
            [
                [
                    'code',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'name_en',
                    'name_ru',
                    'name_uk',
                    'description_en',
                    'description_ru',
                    'description_uk'
                ],
                'string',
                'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'name_en' => Yii::t('app', 'Назва Англійська'),
            'name_ru' => Yii::t('app', 'Назва Російська'),
            'name_uk' => Yii::t('app', 'Назва Українська'),
            'description_en' => Yii::t('app', 'Опис Англійська'),
            'description_ru' => Yii::t('app', 'Опис Російська'),
            'description_uk' => Yii::t('app', 'Опис Українська'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGatewayRequest()
    {
        return $this->hasMany(GatewayRequest::className(), ['rc' => 'code']);
    }

    public function getName($language = null)
    {
        return $this->{'name_' . (is_null($language)
            ? Yii::$app->language
            : $language)};
    }

    public function getDescription($language = null)
    {
        return explode(';', $this->{'description_' . (is_null($language)
            ? Yii::$app->language
            : $language)});
    }
}
