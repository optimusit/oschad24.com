<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $partner;
    public $email;
    public $password;
    public $rememberMe = true;

    protected $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'partner',
                    'email',
                    'password'
                ],
                'required'
            ],
            [
                'rememberMe',
                'boolean'
            ],
            [
                'password',
                'validatePassword'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
            'partner' => 'Партнер',
            'rememberMe' => 'Запомнить меня',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user) {
                /* @var $partner Partner */
                $partner = $user->getPartnerRoot()
                    ->one();
                if (!$user
                    || $this->partner != $partner->prefix
                    || !$user->validatePassword($this->password)
                ) {
                    $this->addError($attribute, 'Невірний email чи пароль.');
                }
            } else {
                $this->addError($attribute, 'Невірний email чи пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()
            && Yii::$app->user->login($this->getUser(), $this->rememberMe
                ? 3600 * 24 * 30
                : 0)
        ) {
            $user = $this->getUser();
            $user->generateAuthKey();
            $user->setAuthKeyExpiration();
            $user->save();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $partner = $this->getPartnerWithUser(Partner::find()
                ->where(['prefix' => $this->partner]));
            if (!$partner) {
                $partner = $this->getPartnerWithUser(Partner::find()
                    ->where(['prefix' => $this->partner])
                    ->one()
                    ->children());
            }

            if ($partner && $partner->users) {
                $this->_user = $partner->users[0];
            }
        }

        return $this->_user;
    }

    protected function getPartnerWithUser($partner)
    {
        return $partner->innerJoin(User::tableName() . ' as u',
            'u.partner_id=' . Partner::tableName() . '.id and u.email = :p1', [':p1' => $this->email])
            ->with('users')
            ->one();
    }
}
