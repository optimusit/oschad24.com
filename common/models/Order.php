<?php

namespace common\models;

use Yii;
use api\modules\v1\models\BackGround;
use common\components\Transliteration\Transliteration;
use common\components\ActiveRecord;
use common\payments\CommerceGateway\CommerceGateway;
use yii\base\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property string $provider_id
 * @property string $provider_order_id
 * @property integer $service_id
 * @property integer $customer_id
 * @property integer $user_id
 * @property integer $partner_id
 * @property string $phone
 * @property string $email
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $income_amount
 * @property string $fee
 * @property string $order_amount
 * @property string $partner_fee
 * @property string $partner_amount
 * @property string $bank_fee
 * @property string $e_fee
 * @property string $amount
 * @property string $custom
 * @property integer $status_id
 * @property integer $paid
 * @property string $paid_time
 * @property string $expiration_time
 * @property integer $unit_count
 * @property integer $commission_internal
 * @property integer $partner_commission_internal
 * @property string $language
 * @property integer $viewed
 * @property string $eticket
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property GatewayRequest[] $gatewayRequests
 * @property Provider $provider
 * @property Service $service
 * @property Partner $partner
 * @property OrderStatusHistory[] $orderStatusHistories
 */
class Order extends ActiveRecord
{
    const STATUS_PENDING = 10; // В ожидании оплаты, первичный статус заказа.
    const STATUS_PAID = 20; // Оплачен
    const STATUS_PROCESSING = 30;
    const STATUS_SALES_COMPLETION = 33; //тлько для интернен магазина
    const STATUS_REVERSAL = 35; //тлько для интернен магазина
    const STATUS_REVERSAL_COMPLETED = 37; //тлько для интернен магазина
    const STATUS_PRINTED = 40; // Только для "Корпоративный АРМ". Устанавливается как только будет открыта квитанция на печать, при этом теперь станет доступна кнопка "Оплатить".
    const STATUS_COMPLETED = 50;
    const STATUS_EXPIRED = 60;
    const STATUS_CANCELED = 70;
    const STATUS_BROKEN = 80;
    const STATUS_ERROR = 90;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'provider_id',
                    'provider_order_id',
                    'service_id',
                    'partner_id',
                    'income_amount',
                    'fee',
                    'order_amount',
                    'e_fee',
                    'amount',
                    'custom',
                    'status_id',
                    'unit_count',
                    'name',
                    'surname',
                ],
                'required',
                'on' => 'default'
            ],
            [
                [
                    'fee',
                    'income_amount',
                ],
                'required',
                'on' => 'commission'
            ],
            [
                [
                    'provider_id',
                    'service_id',
                    'customer_id',
                    'user_id',
                    'partner_id',
                    'created_at',
                    'updated_at',
                    'status_id',
                    'unit_count',
                    'commission_internal',
                    'partner_commission_internal',
                    'viewed',
                    'paid',
                ],
                'integer'
            ],
            [
                [
                    'income_amount',
                    'fee',
                    'order_amount',
                    'partner_fee',
                    'partner_amount',
                    'bank_fee',
                    'e_fee',
                    'amount'
                ],
                'number'
            ],
            [
                [
                    'custom',
                    'eticket',
                ],
                'string'
            ],
            [
                [
                    'paid_time',
                    'expiration_time'
                ],
                'safe'
            ],
            [
                ['provider_order_id'],
                'string',
                'max' => 100
            ],
            [
                ['phone'],
                'string',
                'max' => 13
            ],
            [
                ['email'],
                'string',
                'max' => 100
            ],
            [
                [
                    'name',
                    'surname',
                    //'patronymic',
                ],
                'string',
                'max' => 100
            ],
            [
                ['language'],
                'string',
                'max' => 2
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Провайдер'),
            'provider_order_id' => Yii::t('app', 'Id провайдера'),
            'service_id' => Yii::t('app', 'Сервіс'),
            'customer_id' => Yii::t('app', 'Покупець'),
            'user_id' => Yii::t('app', 'Користувач системі'),
            'partner_id' => Yii::t('app', 'Партнер'),
            'phone' => Yii::t('app', 'Телефон покупця'),
            'email' => Yii::t('app', 'Поштова адреса покупця'),
            'name' => Yii::t('app', 'Ім`я покупця'),
            'surname' => Yii::t('app', 'Прізвище покупця'),
            'patronymic' => Yii::t('app', 'По батькові покупця'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
            'income_amount' => Yii::t('app', 'Вхідна вартість замовлення'),
            'fee' => Yii::t('app', 'Комісія системи'),
            'order_amount' => Yii::t('app', 'Сума, яка включає комісію системи'),
            'partner_fee' => Yii::t('app', 'Комісія партнера'),
            'partner_amount' => Yii::t('app', 'Сума, яка включає коміссію системи і комісію партнера'),
            'bank_fee' => Yii::t('app', 'Банківська комісія'),
            'e_fee' => Yii::t('app', 'Комісія еквайрингу'),
            'amount' => Yii::t('app', 'Підсумкова сума'),
            'custom' => Yii::t('app', 'Дані клієнта (серіалізовані)'),
            'status_id' => Yii::t('app', 'Стан'),
            'paid' => Yii::t('app', 'Ознака сплати'),
            'paid_time' => Yii::t('app', 'Час сплати'),
            'expiration_time' => Yii::t('app', 'Термін придатності до сплати'),
            'language' => Yii::t('app', 'Мова замовлення'),
            'unit_count' => Yii::t('app', 'Кількість одиниць в замовленні'),
            'commission_internal' => Yii::t('app', 'Ознака внутрішньої комісії'),
            'partner_commission_internal' => Yii::t('app', 'Ознака внутрішньої комісії партнера'),
            'viewed' => Yii::t('app', 'Ознака що вже переглянутий'),
        ];
    }

    public function getAmount()
    {
        return Yii::$app->formatter->asDecimal($this->amount);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        try {
            if (is_array($this->eticket)) {
                $this->eticket = json_encode($this->eticket);
            }
        } catch (\Exception $e) {
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->language = Yii::$app->language;

                return true;
            }
        }

        return true;
    }

    public function getEticket()
    {
        $result = null;
        try {
            $result = json_decode($this->eticket, true);
        } catch (\Exception $e) {

        }

        return $result;
    }

    public function getServiceName()
    {
        return self::getService()
            ->one()
            ->getName($this->language);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Provider::className(), ['id' => 'provider_id']);
    }

    /**
     * @return string
     */
    public function getProviderName()
    {
        /** @var Provider $provider */
        $provider = $this->getProvider()
            ->one();

        return $provider
            ? $provider->name
            : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusHistories()
    {
        return $this->hasMany(OrderStatusHistory::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGatewayRequest()
    {
        return $this->hasMany(GatewayRequest::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGatewayRequestHistory()
    {
        return $this->hasMany(GatewayRequestHistory::className(), ['order_id' => 'id']);
    }

    public function getCustomerFullName()
    {
        return $this->surname . ' ' . $this->name;
    }

    public function calculateCommission()
    {
        $this->order_amount = (double)$this->income_amount + ((double)$this->fee * (int)$this->unit_count);
        $this->amount = round($this->order_amount / (1 - (double)$this->e_fee / 100), 2);
    }

    public function getServiceAmount()
    {
        if ($this->isRelationPopulated('service')) {
            $service = $this->relatedRecords['service'];
        } else {
            $service = $this->getService()
                ->one();
        }
        $result = 0;
        if ($service->code == Service::TICKETS_ENTERTAINMENTS) {
            $custom = unserialize($this->custom);
            foreach ($custom['orderInfo']['tickets'] as $ticket) {
                $result += (double)$ticket['amount'] * 0.05;
            }
        } elseif ($service->code == Service::TICKETS_FOOTBALL) {
            $custom = unserialize($this->custom);
            // минимальная сумма
            $min = 2;
            foreach ($custom['orderInfo']['reservation']['ReservedSeats']['ReservedSeat'] as $ticket) {
                if (($serviceAmount = (double)$ticket['Price'] * 0.02) < $min) {
                    $serviceAmount = $min;
                }
                $result += $serviceAmount;
            }
        } else {
            $result = (double)$this->fee * (int)$this->unit_count;
        }

        return round($result, 2);
    }

    public function getAcquiringAmount()
    {
        return round((double)$this->e_fee / 100 * (double)$this->amount, 2);
    }

    public function getBankAmount()
    {
        return round((double)$this->amount - $this->getAcquiringAmount(), 2);
    }

    public function getToProviderAmount()
    {
        return round((double)$this->amount - $this->getServiceAmount() - $this->getAcquiringAmount(), 2);
    }

    public function getCustomData()
    {
        if ($this->custom) {
            $data = unserialize($this->custom);
            $data = $data['form'];
            if ($data === false) {
                Yii::error('Failed to decode. Data: ' . var_export($this->custom, true), 'admin');
                $data = [];
            }
        } else {
            $data = [];
        }

        return $data;
    }

    public function isGdTicket()
    {
        return ($this->getService()
                ->one()->code == Service::TICKETS_GD);
    }

    public function isBusTicket()
    {
        return ($this->getService()
                ->one()->code == Service::TICKETS_BUS);
    }

    public function isAviaTicket()
    {
        return ($this->getService()
                ->one()->code == Service::TICKETS_AVIA);
    }

    public function isInsurance()
    {
        return ($this->getService()
                ->one()->code == Service::AIWA_ACCIDENTS5);
    }

    public function isAmusement()
    {
        return ($this->getService()
                ->one()->code == Service::TICKETS_ENTERTAINMENTS);
    }

    public function isFootballTicket()
    {
        return ($this->getService()
                ->one()->code == Service::TICKETS_FOOTBALL);
    }

    public function setStatusBroken()
    {
        return $this->setStatus(self::STATUS_BROKEN);
    }

    public function setStatusProcessing()
    {
        return $this->setStatus(self::STATUS_PROCESSING);
    }

    public function setStatusSalesCompletion()
    {
        return $this->setStatus(self::STATUS_SALES_COMPLETION);
    }

    public function setStatusReversal()
    {
        return $this->setStatus(self::STATUS_REVERSAL);
    }

    public function setStatusCanceled()
    {
        return $this->setStatus(self::STATUS_CANCELED);
    }

    public function setStatusError($errorNo = null)
    {
        $errorMsg = "Error" . (is_null($errorNo)
                ? ''
                : ' #' . $errorNo) . " while order #{$this->id} processing";
        Yii::error($errorMsg, 'app');

        //CSite::sendErrorSMS(CTransliteration::text($errorMsg));
        return $this->setStatus(self::STATUS_ERROR);
    }

    public function setStatusExpired()
    {
        $result = $this->setStatus(self::STATUS_EXPIRED);
        if ($result === true) {
            if ($this->isAuthorized()) {
                // Если оплата прошла успешно то нам нужно вернуть деньги
                $this->PaymentCancel();
            }
        }

        return $result;
    }

    /**
     * Запрос на отмену предавторизационного запроса (потом обязательно переименовать функцию на более правильное завание)
     * @return bool
     */
    public function PaymentCancel($amount = null)
    {
        $result = false;
        if ($this->isAuthorized()) {
            /** @var GatewayRequest $gatewayRequest */
            $gatewayRequest = $this->getGatewayRequest()
                ->where([
                    'action' => GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY,
                    'rc' => 0
                ])
                ->one();
            if ($gatewayRequest) {
                $result = CommerceGateway::reversalRequest($gatewayRequest, $amount);
                if ($result) {
                    $this->setStatusReversal();
                }
            }
        }

        return $result;
    }

    public function getTransaction($type)
    {
        return $this->getGatewayRequestHistory()
            ->where([
                'trtype' => $type,
                'action' => GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY,
                'rc' => 0
            ])
            ->one();
    }

    /**
     * Можно ли вернуть деньги на карту
     *
     * @param bool $partially Частично
     * @return bool
     */
    public function PaymentCanBeCanceled($partially = false)
    {
        $result = false;
        if ($this->isAuthorized()) {
            /** @var GatewayRequestHistory $transactionComplete */
            /** @var GatewayRequestHistory $transactionPreauth */
            if (is_null($this->getTransaction(GatewayRequest::REQUEST_TYPE_REVERSAL_ADVICE))) {
                if ($partially) {
                    $transactionComplete = $this->getTransaction(GatewayRequest::REQUEST_TYPE_COMPLETE);
                    if ($transactionComplete && !$transactionComplete->isExpired()) {
                        $result = true;
                    }
                } else {
                    $transactionComplete = $this->getTransaction(GatewayRequest::REQUEST_TYPE_COMPLETE);
                    $transactionPreauth = $this->getTransaction(GatewayRequest::REQUEST_TYPE_PREAUTH);
                    if ($transactionComplete) {
                        if (!$transactionComplete->isExpired()) {
                            $result = true;
                        }
                    } elseif ($transactionPreauth) {
                        if (!$transactionPreauth->isExpired()) {
                            $result = true;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Можно ли вручную завершить платеж
     *
     * @return bool
     */
    public function PaymentCanBeCompleted()
    {
        $result = false;
        if ($this->isAuthorized()) {
            /** @var GatewayRequestHistory $transactionComplete */
            /** @var GatewayRequestHistory $transactionPreauth */
            if (is_null($this->getTransaction(GatewayRequest::REQUEST_TYPE_REVERSAL_ADVICE))) {
                $transactionComplete = $this->getTransaction(GatewayRequest::REQUEST_TYPE_COMPLETE);
                $transactionPreauth = $this->getTransaction(GatewayRequest::REQUEST_TYPE_PREAUTH);
                if ($transactionPreauth && !$transactionComplete) {
                    $result = true;
                }
            }
        }

        return $result;
    }

    /**
     * Запрос на отмену завершенного запроса
     * @return bool
     */
    public function CompletePaymentCancel()
    {
        $result = false;
        /** @var GatewayRequest $gatewayRequest */
        $gatewayRequest = $this->getGatewayRequest()
            ->where([
                'trtype' => GatewayRequest::REQUEST_TYPE_COMPLETE,
                'action' => GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY,
                'rc' => 0
            ])
            ->one();
        if ($gatewayRequest) {
            $result = CommerceGateway::reversalRequest($gatewayRequest);
            if ($result) {
                $this->setStatusReversal();
            }
        }

        return $result;
    }

    public function setStatusCompleted()
    {
        //todo sms
        return $this->setStatus(self::STATUS_COMPLETED);
    }

    public function setStatus($status)
    {
        if ($this->status_id != $status) {
            $this->status_id = $status;
            $result = $this->save(false);
        } else {
            $result = -1;
        }

        return $result;
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_PENDING => Yii::t('app', 'В очікуванні'),
            self::STATUS_PAID => Yii::t('app', 'Сплачен'),
            self::STATUS_PROCESSING => Yii::t('app', 'В обробці'),
            self::STATUS_SALES_COMPLETION => Yii::t('app', 'Передача коштів'),
            self::STATUS_REVERSAL => Yii::t('app', 'Повернення коштів'),
            self::STATUS_REVERSAL_COMPLETED => Yii::t('app', 'Кошти повернуті'),
            self::STATUS_PRINTED => Yii::t('app', 'Роздруковано'),
            self::STATUS_COMPLETED => Yii::t('app', 'Завершено'),
            self::STATUS_EXPIRED => Yii::t('app', 'Скінчився'),
            self::STATUS_CANCELED => Yii::t('app', 'Скасовано'),
            self::STATUS_BROKEN => Yii::t('app', 'Невдалий'),
            self::STATUS_ERROR => Yii::t('app', 'Помилка')
        ];
    }

    public function getStatus()
    {
        $statuses = self::getStatuses();

        return $statuses[$this->status_id];
    }

    public function isStatusCompleted()
    {
        return ($this->status === (string)self::STATUS_COMPLETED);
    }

    public function isStatusExpired()
    {
        return ($this->status === (string)self::STATUS_EXPIRED);
    }

    public function isStatusError()
    {
        return ($this->status === (string)self::STATUS_ERROR);
    }

    public function hasLifeTime()
    {
        if (!$this->expiration_time || $this->expiration_time == '0000-00-00 00:00:00') {
            return false;
        }

        return true;
    }

    public function isExpired()
    {
        if ($this->hasLifeTime()) {
            $expiration_time = strtotime($this->expiration_time);
            if ($expiration_time <= time()) {
                return true;
            }
        }

        return false;
    }

    public function getAuthorizedRequest()
    {
        $result = null;
        $gatewayRequests = $this->getGatewayRequestHistory()
            ->all();
        if ($gatewayRequests) {
            /** @var GatewayRequest $gatewayRequest */
            foreach ($gatewayRequests as $gatewayRequest) {
                if ($gatewayRequest->trtype == GatewayRequest::REQUEST_TYPE_PREAUTH
                    && (string)GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY === (string)$gatewayRequest->action
                    && '0' === (string)$gatewayRequest->rc
                ) {
                    $result = $gatewayRequest;
                }
            }
        }

        return $result;
    }

    public function isAuthorized()
    {
        $result = false;
        $request = $this->getAuthorizedRequest();
        if ($request !== null) {
            $result = true;
        }

        return $result;
    }

    public function getAuthorizedMethod()
    {
        $result = null;
        $request = $this->getAuthorizedRequest();
        if ($request !== null) {
            $result = $request->method;
        }

        return $result;
    }

    public function isReversed()
    {
        $result = false;
        if ($this->requests) {
            foreach ($this->requests as $row) {
                if ($row->trtype == 24 && $row->action === (string)GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY
                    && $row->rc === '0'
                ) {
                    $result = true;
                }
            }
        }

        return $result;
    }

    public function getProviderStatus()
    {
        if ($this->isGdTicket()) {
            $statuses = [
                0 => Yii::t('app', 'Очікування оплати'),
                1 => Yii::t('app', 'Оплачено'),
                3 => Yii::t('app', 'Аннульовано оплачене'),
                4 => Yii::t('app', 'Аннулбовано неоплачене'),
                7 => Yii::t('app', 'Оплачено після аннулювання'),
                8 => Yii::t('app', 'Повернуті кошти'),
                10 => Yii::t('app', 'В процесі повернення'),
            ];
            if (isset($statuses[$this->status_id])) {
                return $statuses[$this->status_id];
            }
        } elseif ($this->isBusTicket()) {
            $statuses = [
                0 => Yii::t('app', 'Неоплачено'),
                1 => Yii::t('app', 'Ожидается оплата'),
                2 => Yii::t('app', 'Оплачено'),
                3 => Yii::t('app', 'В процессе возврата'),
                4 => Yii::t('app', 'Отменено пользователем'),
                5 => Yii::t('app', 'Отменено системой'),
                6 => Yii::t('app', 'Средства возвращены'),
                7 => Yii::t('app', 'Запрос на возврат средств'),
                8 => Yii::t('app', 'Отменено системой'),
                9 => Yii::t('app', 'Оплачено после отмены'),
            ];
            if (isset($statuses[$this->status_id])) {
                return $statuses[$this->status_id];
            }
        } elseif ($this->isAviaTicket()) {
            $statuses = [
                0 => Yii::t('app', 'Неоплачено'),
                1 => Yii::t('app', 'Оплачен и выписан'),
                2 => Yii::t('app', 'Оплачен, не выписан'),
                3 => Yii::t('app', 'Оплачен после аннулирования'),
                4 => Yii::t('app', 'Аннулирован'),
                5 => Yii::t('app', 'В процессе возврата'),
                6 => Yii::t('app', 'Заявка на возврат'),
                7 => Yii::t('app', 'Возвращён'),
                8 => Yii::t('app', 'Аннулирован'),
                9 => Yii::t('app', 'Аннулирован'),
                10 => Yii::t('app', 'Оплата не подтвердилась'),
            ];
            if (isset($statuses[$this->status_id])) {
                return $statuses[$this->status_id];
            }
        }

        return $this->status_id;
    }

    public function canBeCancelled()
    {
        if ($this->isGdTicket()
            || $this->isBusTicket()
        ) {
            if (in_array($this->status_id, [0])) {
                return true;
            }
        }

        return false;
    }

    public function Cancel()
    {
        if ($this->isGdTicket() && $this->canBeCancelled()) {
            $this->status_id = 4;
        }
    }

    public function getDocId()
    {
        return $this->provider_order_id;
    }

    public function getType($short = false)
    {
        $short = $short
            ? 1
            : 0;
        $label[0] = self::getTypes();
        $label[1] = self::getTypes(true);

        return (string)@$label[$short][$this->getService()
            ->one()->code];
    }

    static public function getTypes($short = false)
    {
        if ($short) {
            return [
                Service::TICKETS_GD => 'Ж/д бил.',
                Service::TICKETS_BUS => 'Автоб. бил.',
                Service::TICKETS_AVIA => 'Авиабил.',
                Service::TICKETS_FOOTBALL => 'Футб. бил.',
                Service::AIWA_ACCIDENTS5 => Yii::t('aiwa', 'НС+здоровье'),
                Service::TICKETS_ENTERTAINMENTS => Yii::t('amusement', 'Развлечения'),
            ];
        } else {
            return [
                Service::TICKETS_GD => 'Ж/д билеты',
                Service::TICKETS_BUS => 'Автобусные билеты',
                Service::TICKETS_AVIA => 'Авиабилеты',
                Service::TICKETS_FOOTBALL => 'Футбольные билеты',
                Service::AIWA_ACCIDENTS5 => Yii::t('aiwa', 'Страховка: НС+здоровье'),
                Service::TICKETS_ENTERTAINMENTS => Yii::t('amusement', 'Развлекательные мероприятия'),
            ];
        }
    }

    public function getDescription($short = false)
    {
        $description = '';
        switch ($this->getService()
            ->one()->code) {
            case Service::TICKETS_GD:
                $data = $this->getCustomData();
                $train = $data['selected_train_info'];
                $seats = count($data['selected_seats']);
                if ($short) {
                    $description = Transliteration::text($this->getType($short) . ' x' . intval($seats) . ' '
                        . $train['departure_date'] . ' в ' . $train['passenger_arrival_name']);
                } else {
                    $description =
                        'Покупка ' . $this->getType() . ' (' . intval($seats) . ') на ' . $train['departure_date']
                        . ' в ' . $train['passenger_arrival_name'];
                }
                break;
            case Service::TICKETS_BUS:
                $data = $this->getCustomData();
                $trip = $data['trip'];
                $seats = count($data['passengers']);
                //$departure = CDateTimeParser::parse($trip['station']['departure_from_point'], 'dd.MM.yyyy HH:mm');
                if ($short) {
                    $description = Transliteration::text($this->getType($short) . ' x' . intval($seats) . ' '
                    //. Yii::app()->dateFormatter->format('d-MM-yy', $departure) . ' в '
                    //. $trip['ticket']['arrival_name']);
                    );
                } else {
                    $description = 'Покупка ' . $this->getType() . ' (' . intval($seats) . ') на '
                        //. Yii::app()->dateFormatter->format('d MMMM yyyy', $departure) . ' в '
                        //. $trip['ticket']['arrival_name']
                    ;
                }
                break;
            case Service::TICKETS_AVIA:
                $data = $this->getCustomData();
                $trip = $data['aircraft']['parts'][0];
                $seats = count($data['passengers']);
                if ($short) {
                    $description = Transliteration::text($this->getType($short) . ' x' . intval($seats) . ' '
                        . $trip['departure_date'] . ' ' . $trip['departure_time'] . ' в '
                        . $trip['departure_city_name']);
                } else {
                    $description = 'Покупка ' . $this->getType() . ' (' . intval($seats)
                        . ') на '//. Yii::app()->dateFormatter->format('d MMMM yyyy', $departure) . ' в ' . $data['end_title']
                    ;
                }
                break;
            case Service::TICKETS_ENTERTAINMENTS:
                $data = unserialize($this->custom);
                if ($short) {
                } else {
                }
                $event = $data['orderInfo']['event'];
                $description = $event['EventName'] . ', ' . $event['DetailsDate'];
                break;
            case Service::TICKETS_FOOTBALL:
                $data = unserialize($this->custom);
                if ($short) {
                } else {
                }
                $event = $data['orderInfo']['event'];
                $description = $event['name'] . ', ' . $event['date_start'];
                break;
        }

        return trim($description);
    }

    protected function paid()
    {
        $result = false;
        /** @var GatewayRequest $gatewayRequest */
//		$gatewayRequest = $this->getGatewayRequest()
//			->where([
//				'action' => GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY,
//				'rc' => 0
//			])
//			->one();
//		if ($gatewayRequest) {
//			$result = CommerceGateway::salesCompletionRequest($gatewayRequest);
//			if ($result) {
//				$this->paid = true;
//				$this->paid_time = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd HH:mm:ss');
//				$result = $this->update();
//			}
//		}
        $this->paid = true;
        $this->paid_time = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd HH:mm:ss');
        $result = $this->update();

        return $result;
    }

    public function paymentCommit()
    {
        return $this->paid();
    }

    public function paymentCommitTest()
    {
        $result = false;
        /** @var GatewayRequest $gatewayRequest */
        $gatewayRequest = $this->getGatewayRequestHistory()
            ->where([
                'trtype' => GatewayRequest::REQUEST_TYPE_PREAUTH,
                'action' => GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY,
                'rc' => 0
            ])
            ->one();
        if ($gatewayRequest) {
            $result = CommerceGateway::salesCompletionRequest($gatewayRequest);
//			if ($result) {
//				$this->paid = true;
//				$this->paid_time = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd HH:mm:ss');
//				$result = $this->update();
//			}
        }

        return $result;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getMailSubject()
    {
        $custom = unserialize($this->custom);
        $orderInfo = $custom['orderInfo'];
        //$subject = sprintf('%s %s', Yii::t('tickets', 'Замовлення'), $this->id);
        /** @var Service $service */
        $service = $this->getService()
            ->one();
        if (in_array($service->code, [
            Service::TICKETS_GD,
            Service::TICKETS_BUS,
        ])) {
            return sprintf('%s %s %s-%s %s', Yii::t('tickets', 'Замовлення'), $this->id,
                $orderInfo['departure']['name'], $orderInfo['arrival']['name'], $orderInfo['departure']['date_time']);
        } elseif (in_array($service->code, [
            Service::TICKETS_AVIA,
        ])) {
//			$trip = $orderInfo['aircraft']['parts'][0];
//			return sprintf('%s %s %s-%s %s', Yii::t('tickets', 'Замовлення'), $this->id,
//				$orderInfo['departure']['name'], $orderInfo['arrival']['name'], $orderInfo['departure']['date_time']);
            return sprintf('%s %s ', Yii::t('tickets', 'Замовлення'), $this->id, 'Квитки на літак');
        } elseif (in_array($service->code, [
            Service::TICKETS_ENTERTAINMENTS,
        ])) {
            return sprintf('%s %s', $this->id, $orderInfo['event']['EventName']);
        } elseif (in_array($service->code, [
            Service::TICKETS_FOOTBALL,
        ])) {
            return sprintf('%s %s', $this->id, $orderInfo['event']['name']);
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getDocumentFileName()
    {
        $service = $this->getService()
            ->one();
        $storePath = Yii::$app->params['storage']['documents'] . $service->provider_code . DIRECTORY_SEPARATOR;
        /** @var Service $service */
        $service = $this->getService()
            ->one();
        $files = [];
        $filePathPattern = '%s%s_%s.pdf';
        if (in_array($service->code, [
            Service::TICKETS_GD,
            Service::TICKETS_AVIA,
        ])) {
            $files[] = sprintf($filePathPattern, $storePath, $this->id, $this->provider_order_id);
        } elseif (in_array($service->code, [
            Service::TICKETS_BUS,
        ])) {
            for ($c = 0; $c < $this->unit_count; $c++) {
                $files[] = sprintf($filePathPattern, $storePath, $this->id, $c);
            }
        } elseif (in_array($service->code, [
            Service::TICKETS_ENTERTAINMENTS,
        ])) {
            if (!is_null(($eticket = $this->getEticket()))) {
                foreach ($eticket['tickets'] as $ticket) {
                    $files[] = sprintf($filePathPattern, $storePath, $this->id, $ticket['Barcode']);
                }
            }
        } elseif (in_array($service->code, [
            Service::TICKETS_FOOTBALL,
        ])) {
            if (!is_null(($eticket = $this->getEticket()))) {
                $files[] = sprintf($filePathPattern, $storePath, $this->id, $this->provider_order_id);
            }
        } else {
            throw new \Exception('Невідомий сервіс ' . $service->code);
        }

        return $files;
    }

    /*
     * Перевод неоплаченых ордеров в статус ПРОСРОЧЕН
     */
    public static function CheckExpiration()
    {
        $orders = self::find()
            ->where([
                'status_id' => [
                    Order::STATUS_PENDING,
                    Order::STATUS_PROCESSING,
                ],
            ])
            ->andWhere([
                '!=',
                'expiration_time',
                '0000-00-00 00:00:00'
            ])
            ->andWhere('expiration_time < NOW()')
            ->andWhere('paid = 0')
            ->all();
        /** @var Order $order */
        foreach ($orders as $order) {
            if ($order->setStatusExpired()) {
                Yii::$app->core->reservationExpired($order);
                BackGround::Cancel($order);
            }
        }
    }

    /*
     * Обработка оплаченных заказов покупки билетов (деньги были заблокированы на карточке пользователя,
     * необходимо выполнить запрос payment/commit)
     */
    public static function CheckPaymentCommit()
    {
        $orders = self::find()
            ->where([
                'status_id' => Order::STATUS_PROCESSING,
            ])
            ->andWhere('paid = 1')
            ->andWhere('expiration_time > NOW()')
            ->innerJoinWith('gatewayRequest')
            ->andWhere([
                'trtype' => GatewayRequest::REQUEST_TYPE_PREAUTH,
                'rc' => 0,
                'action' => GatewayRequest::ACTION_TRANSACTION_SUCCESSFULLY
            ])
            ->all();
        /** @var Order $order */
        foreach ($orders as $order) {
            if ($order->isAuthorized()) {
                if ($order->paid) {
                    if (BackGround::Commit($order)) {
                        //todo отметка комита у провайдера (а нужна-ли?)
                        if ($order->paymentCommit()) {
                            if ($order->setStatusSalesCompletion()) {
                                Yii::$app->core->reservationPaymentSuccess($order);
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * Заказ почти выполнен, необходимо пользователю прислать документы
     */
    public function SendDocuments()
    {
        $orders = self::find()
            ->where([
                'status_id' => Order::STATUS_SALES_COMPLETION,
            ])
            ->andWhere('paid = 1')
            ->all();
        /** @var Order $order */
        foreach ($orders as $order) {
            try {
                if (($documentPath = $order->getDocuments()) && $documentPath['code'] == 0) {
                    Yii::$app->core->sendDocumentsToCustomer($order);
                    $order->setStatusCompleted();
                }
            } catch (Exception $e) {
                Yii::error($e->getMessage(), __METHOD__);
                $order->setStatusError();
            }
        }
    }

    public function getDocuments()
    {
        return BackGround::GetDocument($this);
    }

    public function sendDocument()
    {
        if ($this->status_id == self::STATUS_COMPLETED && $this->paid) {
            $documentPath = $this->getDocuments();
            if ($documentPath['code'] == 0) {
                Yii::$app->core->sendDocumentsToCustomer($this);
            }
        }
    }

    public static function SalesByType($period = 12)
    {
        $now = new \DateTime();
        $from = $now->add(new \DateInterval('P1D'));
        $labels = [];
        $c = 0;
        while ($c <= $period) {
            $labels[] = $from->sub(new \DateInterval('P1D'))
                ->format('Y-m-d');
            $c++;
        }
        $labels = array_reverse($labels);
        $services = Service::find()
            ->select([
                'id',
                'name_' . Yii::$app->language . ' as name',
                'color'
            ])
            ->where(['active' => 1])
            ->asArray()
            ->all();
        $orders = Order::find()
            ->select([
                'count(*) as `count`',
                'date(paid_time) as `date`',
                'service_id'
            ])
            ->where(['paid' => 1])
            ->andWhere([
                'status_id' => Order::STATUS_COMPLETED
            ])
            ->andWhere([
                '>=',
                'paid_time',
                $from->format('Y-m-d')
            ])
            ->groupBy([
                'date(paid_time)',
                'service_id'
            ])
            ->asArray()
            ->all();
        $datasets = [];
        $today = [];
        foreach ($services as $service) {
            $dataset = [];
            foreach ($labels as $label) {
                $value = 0;
                foreach ($orders as $order) {
                    if ($order['service_id'] == $service['id'] && strcmp($order['date'], $label) == 0) {
                        $value = $order['count'];
                        break;
                    }
                }
                $dataset[] = $value;
            }
            $datasets[] = [
                'scaleShowLabels' => false,
                'fillColor' => 'transparent',
                'strokeColor' => $service['color'],
                'pointColor' => $service['color'],
                'pointStrokeColor' => '#fff',
                'data' => $dataset,
            ];
        }

        $now = (new \DateTime())->format('Y-m-d');
        foreach ($services as $service) {
            $item = [
                'service' => $service['name'],
                'color' => $service['color'],
            ];
            foreach ($orders as $order) {
                if ($order['date'] == $now && $order['service_id'] == $service['id']) {
                    $item['count'] = $order['count'];
                    break;
                }
            }
            if (!array_key_exists('count', $item)) {
                $item['count'] = 0;
            }
            $today[] = $item;
        }
        $result = [
            'widget' => [
                'labels' => $labels,
                'datasets' => $datasets,
            ],
            'today' => $today,
        ];

        return $result;
    }

    public static function SalesReport($partnerId, \DateTime $dateFrom, \DateTime $dateTo = null)
    {
        if (is_null($dateTo)) {
            $dateTo = clone($dateFrom);
        }
        $dateFrom = $dateFrom->format('Y-m-d');
        $dateTo = $dateTo->format('Y-m-d');

        $query = self::find()
            ->select([
                's.name_uk as `service`',
                'sum(order.unit_count) as `count`',
                'sum(order.amount) as `amount`'
            ])
            ->innerJoin(Service::tableName() . ' as s', 's.id=order.service_id')
            ->where([
                'partner_id' => $partnerId,
                'status_id' => [
                    self::STATUS_COMPLETED,
                    self::STATUS_PAID,
                    self::STATUS_PENDING,
                    self::STATUS_PROCESSING
                ],
            ]);
        if ($dateFrom == $dateTo) {
            $query = $query->andWhere('date(paid_time)=:paid_time', ['paid_time' => $dateFrom]);
        } else {
            $query = $query->andWhere('date(paid_time)>=:paid_time_from', [':paid_time_from' => $dateFrom])
                ->andWhere('date(paid_time)<=:paid_time_to', [':paid_time_to' => $dateTo]);
        }
        $result = $query->groupBy('order.service_id')
            ->asArray()
            ->all();

        return $result;
    }
}
