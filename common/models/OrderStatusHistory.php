<?php

namespace common\models;

use common\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%order_status_history}}".
 *
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order_id
 * @property integer $status
 *
 * @property Order $order
 */
class OrderStatusHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_status_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'status'
                ],
                'required'
            ],
            [
                [
                    'created_at',
                    'updated_at',
                    'order_id',
                    'status'
                ],
                'integer'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
            'order_id' => Yii::t('app', 'Замовлення'),
            'status' => Yii::t('app', 'Стан'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
