<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "{{%karabas_cities}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $countryId
 * @property string $countryName
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Buildings[] $buildings
 * @property Countries $country
 */
class Cities extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%karabas_cities}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'countryName',
                ],
                'required'
            ],
            [
                [
                    'id',
                    'countryId',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'countryName'
                ],
                'string',
                'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Назва міста'),
            'countryId' => Yii::t('app', 'ID країни'),
            'countryName' => Yii::t('app', 'Назва країни'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildings()
    {
        return $this->hasMany(Buildings::className(), ['cityId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'countryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(PartnerEvents::className(), ['CityId' => 'id']);
    }
}
