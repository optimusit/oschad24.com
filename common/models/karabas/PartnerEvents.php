<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "{{%karabas_partner_events}}".
 *
 * @property integer $EventId
 * @property string $EventName
 * @property string $EventDate
 * @property string $EventDescription
 * @property integer $ActivityId
 * @property string $ActivityName
 * @property string $ActivityDescription
 * @property string $ActivityDescriptionLong
 * @property string $CategoriesIDs
 * @property string $Categories
 * @property integer $IsETicketPassportRequired
 * @property integer $IsThermoTicketPassportRequired
 * @property integer $ClientInfoRequired
 * @property integer $ETicketEnabled
 * @property integer $PrintEnabled
 * @property integer $HasPNG
 * @property integer $HasSVG
 * @property string $SVG
 * @property string $City
 * @property integer $CityId
 * @property string $HallName
 * @property integer $HallId
 * @property string $BuildingName
 * @property integer $BuildingId
 * @property string $Organizer
 * @property string $OrganizerShortName
 * @property integer $OrganizerId
 * @property integer $ReplaceAvailabeTicketsCount
 * @property integer $ReplaceAvailabeTicketsCountWith
 * @property integer $ReplaceActivityDescription
 * @property string $SalesEndTime
 * @property string $ReserveEndTime
 * @property string $Percent
 * @property string $ActivityImageSmall
 * @property string $ActivityImageMedium
 * @property string $ActivityImageBig
 * @property string $ActivityImageLarge
 * @property string $EventImageSmall
 * @property string $EventImageMedium
 * @property string $EventImageBig
 * @property string $EventImageLarge
 * @property string $EventType
 * @property string $LineImage
 * @property string $LinePriceRange
 * @property string $LinePlace
 * @property string $LineDateTime
 * @property string $DetailsDate
 * @property string $DetailsTime
 * @property string $DetailsPlace
 * @property string $DetailsAddress
 * @property string $DetailsImage
 * @property string $DetailsDescription
 * @property string $DetailsType
 * @property integer $created_at
 * @property integer $updated_at
 */
class PartnerEvents extends \common\components\ActiveRecord
{
    public $provider = 'Karabas';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%karabas_partner_events}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'EventId',
                    'EventDate',
                    'ActivityId',
                    //'ActivityName',
                    'IsETicketPassportRequired',
                    'IsThermoTicketPassportRequired',
                    'ClientInfoRequired',
                    'ETicketEnabled',
                    'PrintEnabled',
                    'HasPNG',
                    'HasSVG',
                    'City',
                    'CityId',
                    'HallName',
                    'HallId',
                    'BuildingName',
                    'BuildingId',
                    'Organizer',
                    'OrganizerId',
                    'ReplaceAvailabeTicketsCount',
                    'ReplaceAvailabeTicketsCountWith',
                    'ReplaceActivityDescription',
                    'SalesEndTime',
                    'ReserveEndTime',
                    'EventType',
                    'LinePriceRange',
                    'LinePlace',
                    'LineDateTime',
                    'DetailsDate',
                    'DetailsTime',
                    'DetailsPlace',
                ],
                'required'
            ],
            [
                [
                    'EventId',
                    'ActivityId',
                    'IsETicketPassportRequired',
                    'IsThermoTicketPassportRequired',
                    'ClientInfoRequired',
                    'ETicketEnabled',
                    'PrintEnabled',
                    'HasPNG',
                    'HasSVG',
                    'CityId',
                    'HallId',
                    'BuildingId',
                    'OrganizerId',
                    'ReplaceAvailabeTicketsCount',
                    'ReplaceAvailabeTicketsCountWith',
                    'ReplaceActivityDescription',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'EventDate',
                    'SalesEndTime',
                    'ReserveEndTime'
                ],
                'safe'
            ],
            [
                [
                    'EventDescription',
                    'ActivityDescriptionLong',
                    'DetailsDescription',
                    'DetailsType',
                    'Percent'
                ],
                'string'
            ],
            [
                [
                    'EventName',
                    'ActivityName',
                    'CategoriesIDs',
                    'City',
                    'HallName',
                    'BuildingName',
                    'Organizer',
                    'OrganizerShortName',
                    'EventType',
                    'LinePriceRange',
                    'LineDateTime'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'ActivityDescription',
                    'Categories',
                    'SVG',
                    'ActivityImageSmall',
                    'ActivityImageMedium',
                    'ActivityImageBig',
                    'ActivityImageLarge',
                    'EventImageSmall',
                    'EventImageMedium',
                    'EventImageBig',
                    'EventImageLarge',
                    'DetailsPlace',
                    'DetailsAddress',
                    'LineImage',
                    'DetailsImage',
                ],
                'string',
                'max' => 1024
            ],
            [
                ['LinePlace'],
                'string',
                'max' => 500
            ],
            [
                ['DetailsDate'],
                'string',
                'max' => 100
            ],
            [
                ['DetailsTime'],
                'string',
                'max' => 20
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ActivityId' => Yii::t('app', 'Activity ID'),
            'ActivityName' => Yii::t('app', 'Найменування події'),
            'ActivityDescription' => Yii::t('app', 'Опис події'),
            'ActivityDescriptionLong' => Yii::t('app', 'Описание події'),
            'EventDescription' => Yii::t('app', 'Опис заходу'),
            'CategoriesIDs' => Yii::t('app', 'Перелік категорій заходу, розділені комою'),
            'Categories' => Yii::t('app', 'Перелік найменувань категорій заходи, розділені комою'),
            'IsETicketPassportRequired' => Yii::t('app', 'Паспортні дані на електронному квитку обов`язкові'),
            'IsThermoTicketPassportRequired' => Yii::t('app', 'Паспортні дані на термобілеті обов`язкові'),
            'ClientInfoRequired' => Yii::t('app', 'Інформація про клієнта обов`язкове для електронного квитка'),
            'ETicketEnabled' => Yii::t('app', 'дозволена / заборонено продаж електронних квитків на цей захід'),
            'PrintEnabled' => Yii::t('app', 'дозволена / заборонена друк квитків на захід'),
            'HasPNG' => Yii::t('app', 'є / немає ПНГ-версія залу. Якщо ні - необхідно вивести клієнту флешевих зал.'),
            'HasSVG' => Yii::t('app',
                'є / немає SVG-версія залу. Якщо ні - необхідно вивести клієнту PNG або флеш-версію залу.'),
            'SVG' => Yii::t('app', 'Посилання на SVG-файл схеми залу'),
            'City' => Yii::t('app', 'Назва міста'),
            'CityId' => Yii::t('app', 'ID міста'),
            'HallName' => Yii::t('app', 'Найменування залу'),
            'HallId' => Yii::t('app', 'ID залу'),
            'BuildingName' => Yii::t('app', 'Найменування місця проведення'),
            'BuildingId' => Yii::t('app', 'ID місця проведення'),
            'EventName' => Yii::t('app', 'Найменування заходу'),
            'EventId' => Yii::t('app', 'ID заходу'),
            'Organizer' => Yii::t('app', 'Назва компанії-організатора'),
            'OrganizerShortName' => Yii::t('app', 'Коротка назва компанії-організатора'),
            'OrganizerId' => Yii::t('app', 'ID організатора'),
            'ReplaceAvailabeTicketsCount' => Yii::t('app', 'Обмежити кількість доступних квитків в легенді'),
            'ReplaceAvailabeTicketsCountWith' => Yii::t('app',
                'Максимальна кількість квитків відображаються в легенді, по кожній ціновій категорії'),
            'ReplaceActivityDescription' => Yii::t('app',
                'Замінити опис події описом заходу. Використовується, коли у заході використовується альтернативна версія опису, а не доповнення.'),
            'EventDate' => Yii::t('app', 'Дата заходу'),
            'SalesEndTime' => Yii::t('app', 'Час закінчення продажів'),
            'ReserveEndTime' => Yii::t('app', 'Время окончания резервування'),
            'Percent' => Yii::t('app', 'Відсоток реалізатора від продажу заходи'),
            'ActivityImageSmall' => Yii::t('app', 'Маленька картинка події'),
            'ActivityImageMedium' => Yii::t('app', 'Середня картинка події'),
            'ActivityImageBig' => Yii::t('app', 'Велика картинка події'),
            'ActivityImageLarge' => Yii::t('app', 'Величезна картинка події'),
            'EventImageSmall' => Yii::t('app', 'Маленька картинка заходу'),
            'EventImageMedium' => Yii::t('app', 'Середня картинка заходу'),
            'EventImageBig' => Yii::t('app', 'Велика картинка заходу'),
            'EventImageLarge' => Yii::t('app', 'Величезна картинка заходу'),
            'EventType' => Yii::t('app', 'Тип події (Default - стандартне, AllTimeEvent - постійне)'),
            'LineImage' => Yii::t('app', 'Картинка (Стрічка)'),
            'LinePriceRange' => Yii::t('app', 'Ціновий діапазон'),
            'LinePlace' => Yii::t('app', 'Місце проведення'),
            'LineDateTime' => Yii::t('app', 'Час проведення'),
            'DetailsDate' => Yii::t('app', 'Дата проведення'),
            'DetailsTime' => Yii::t('app', 'Час проведення'),
            'DetailsPlace' => Yii::t('app', 'Місце проведення'),
            'DetailsAddress' => Yii::t('app', 'Адреса проведення'),
            'DetailsImage' => Yii::t('app', 'Картинка'),
            'DetailsDescription' => Yii::t('app', 'Опис події'),
            'DetailsType' => Yii::t('app', 'Тип події (серіализований масив - назва-колір)'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityTypes()
    {
        return $this->hasMany(ActivityTypes::className(), ['id' => 'ActivityTypeId'])
            ->select('id, name, color')
            ->viaTable(PartnerEventActivityType::tableName(), ['EventId' => 'EventId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarousel()
    {
        return $this->hasMany(PartnerEventsCarousel::className(), ['EventId' => 'EventId']);
    }
}
