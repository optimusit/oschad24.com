<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "{{%karabas_partner_event_statistic}}".
 *
 * @property integer $EventId
 * @property string $Price
 * @property integer $Quote
 * @property integer $Reserved
 * @property integer $Free
 * @property integer $created_at
 * @property integer $updated_at
 */
class PartnerEventStatistic extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%karabas_partner_event_statistic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'EventId',
                    'Price',
                    'Quote',
                    'Reserved',
                    'Free',
                ],
                'required'
            ],
            [
                [
                    'EventId',
                    'Quote',
                    'Reserved',
                    'Free',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                ['Price'],
                'number'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'EventId' => Yii::t('app', 'Event ID'),
            'Price' => Yii::t('app', 'Стоимость'),
            'Quote' => Yii::t('app', 'Общее кол-во мест в данной ценовой группе.'),
            'Reserved' => Yii::t('app', 'Количество забронированных/купленых билетов в данной ценовой категории'),
            'Free' => Yii::t('app', 'Кол-во свободных для продажи мест.'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(PartnerEvents::className(), ['id' => 'EventId']);
    }
}
