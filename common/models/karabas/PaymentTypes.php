<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "karabas_payment_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $isOnline
 * @property integer $created_at
 * @property integer $updated_at
 */
class PaymentTypes extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'karabas_payment_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'code',
                    'isOnline',
                ],
                'required'
            ],
            [
                [
                    'id',
                    'isOnline',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'code'
                ],
                'string',
                'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Назва'),
            'code' => Yii::t('app', 'Код'),
            'isOnline' => Yii::t('app', 'Ознака способу оплати через online-системи'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }
}
