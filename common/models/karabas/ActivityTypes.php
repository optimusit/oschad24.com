<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "karabas_activity_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class ActivityTypes extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'karabas_activity_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'color',
                ],
                'required'
            ],
            [
                [
                    'id',
                    'integer',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                ['name'],
                'string',
                'max' => 255
            ],
            [
                ['color'],
                'string',
                'max' => 6
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Назва типу події'),
            'color' => Yii::t('app', 'Колір типу події'),
            'integer' => Yii::t('app', 'Активний'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerEvents()
    {
        return $this->hasMany(PartnerEvents::className(), ['EventId' => 'EventId'])
            ->viaTable(PartnerEventActivityType::tableName(), ['ActivityTypeId' => 'id']);
    }
}
