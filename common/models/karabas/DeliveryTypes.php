<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "karabas_delivery_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $addressRequired
 * @property string $paymentTypeCodes
 * @property integer $created_at
 * @property integer $updated_at
 */
class DeliveryTypes extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'karabas_delivery_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'code',
                    'addressRequired',
                    'paymentTypeCodes',
                ],
                'required'
            ],
            [
                [
                    'id',
                    'addressRequired',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'code'
                ],
                'string',
                'max' => 255
            ],
            [
                ['paymentTypeCodes'],
                'string',
                'max' => 512
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Назва'),
            'code' => Yii::t('app', 'Код'),
            'addressRequired' => Yii::t('app', 'Ознака необхідності адреси доставки'),
            'paymentTypeCodes' => Yii::t('app',
                'Перелік типів оплати, які можуть бути використані разом з цим способом доставки'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }
}
