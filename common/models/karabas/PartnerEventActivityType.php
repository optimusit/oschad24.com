<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "{{%karabas_partner_event_activity_type}}".
 *
 * @property integer $EventId
 * @property integer $ActivityTypeId
 * @property integer $created_at
 * @property integer $updated_at
 */
class PartnerEventActivityType extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%karabas_partner_event_activity_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'EventId',
                    'ActivityTypeId',
                ],
                'required'
            ],
            [
                [
                    'EventId',
                    'ActivityTypeId',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'EventId' => Yii::t('app', 'Event ID'),
            'ActivityTypeId' => Yii::t('app', 'ActivityTypeId'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }
}
