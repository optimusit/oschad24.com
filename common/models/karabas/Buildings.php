<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "karabas_buildings".
 *
 * @property integer $id
 * @property string $name
 * @property string $shortName
 * @property string $address
 * @property integer $buildingTypeId
 * @property integer $cityId
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Cities $city
 * @property BuildingTypes $buildingType
 * @property Halls[] $halls
 */
class Buildings extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'karabas_buildings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'shortName',
                    'address',
                    'description',
                ],
                'required'
            ],
            [
                [
                    'id',
                    'buildingTypeId',
                    'cityId',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'address'
                ],
                'string',
                'max' => 255
            ],
            [
                ['shortName'],
                'string',
                'max' => 50
            ],
            [
                ['description'],
                'string',
                'max' => 512
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Назва місця проведення'),
            'shortName' => Yii::t('app', 'Коротка назва'),
            'address' => Yii::t('app', 'Адреса'),
            'buildingTypeId' => Yii::t('app', 'ID типу місця проведення'),
            'cityId' => Yii::t('app', 'ID міста'),
            'description' => Yii::t('app', 'Опис'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildingType()
    {
        return $this->hasOne(BuildingTypes::className(), ['id' => 'buildingTypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHalls()
    {
        return $this->hasMany(Halls::className(), ['buildingId' => 'id']);
    }
}
