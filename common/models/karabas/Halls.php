<?php

namespace common\models\karabas;

use Yii;

/**
 * This is the model class for table "{{%karabas_halls}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $hasPNG
 * @property integer $buildingId
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Buildings $building
 */
class Halls extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%karabas_halls}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'hasPNG',
                    'description',
                ],
                'required'
            ],
            [
                [
                    'id',
                    'hasPNG',
                    'buildingId',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                ['name'],
                'string',
                'max' => 255
            ],
            [
                ['description'],
                'string',
                'max' => 512
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Назва'),
            'hasPNG' => Yii::t('app', 'Прапор наявності PNG-версії схеми залу'),
            'buildingId' => Yii::t('app', 'ID місця проведення'),
            'description' => Yii::t('app', 'Опис'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'buildingId']);
    }
}
