<?php

namespace common\models\karabas;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%karabas_partner_events_carousel}}".
 *
 * @property integer $EventId
 */
class PartnerEventsCarousel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%karabas_partner_events_carousel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'EventId',
                ],
                'required'
            ],
            [
                [
                    'EventId',
                ],
                'integer'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'EventId' => Yii::t('app', 'ID заходу'),
        ];
    }
}
