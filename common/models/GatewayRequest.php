<?php

namespace common\models;

use Yii;
use api\modules\v1\Module;
use common\components\AppHelper;

/**
 * This is the model class for table "{{%gateway_request}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order_id
 * @property string $terminal
 * @property string $method
 * @property integer $trtype
 * @property string $amount
 * @property string $currency
 * @property string $timestamp
 * @property string $nonce
 * @property integer $action
 * @property integer $rc
 * @property string $rrn
 * @property string $int_ref
 * @property string $auth_code
 *
 * @property Order $order
 */
class GatewayRequest extends \common\components\ActiveRecord
{
    const REQUEST_TYPE_PREAUTH = 0;
    const REQUEST_TYPE_AUTH = 1;
    const REQUEST_TYPE_COMPLETE = 21;
    const REQUEST_TYPE_REVERSAL_ADVICE = 24;

    const ACTION_TRANSACTION_SUCCESSFULLY = 0;
    const ACTION_DUPLICATE_TRANSACTION = 1;
    const ACTION_TRANSACTION_DECLINED = 2;
    const ACTION_TRANSACTION_FAILED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gateway_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'terminal',
                    'method',
                    'trtype',
                    'amount',
                    'currency',
                    'timestamp',
                    'nonce'
                ],
                'required'
            ],
            [
                [
                    'created_at',
                    'updated_at',
                    'order_id',
                    'trtype',
                    'action',
                    'rc'
                ],
                'integer'
            ],
            [
                ['amount'],
                'number'
            ],
            [
                ['terminal'],
                'string',
                'max' => 8
            ],
            [
                ['method'],
                'string',
                'max' => 18
            ],
            [
                ['currency'],
                'string',
                'max' => 3
            ],
            [
                [
                    'timestamp',
                    'nonce'
                ],
                'string',
                'max' => 14
            ],
            [
                ['rrn'],
                'string',
                'max' => 12
            ],
            [
                ['int_ref'],
                'string',
                'max' => 32
            ],
            [
                ['auth_code'],
                'string',
                'max' => 64
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
            'order_id' => Yii::t('app', 'Ордер'),
            'terminal' => Yii::t('app', 'Термінал'),
            'method' => Yii::t('app', 'Метод оплати'),
            'trtype' => Yii::t('app', 'Тип запиту'),
            'amount' => Yii::t('app', 'Сума'),
            'currency' => Yii::t('app', 'Валюта'),
            'timestamp' => Yii::t('app', 'Відбиток часу'),
            'nonce' => Yii::t('app', 'nonce'),
            'action' => Yii::t('app', 'action'),
            'rc' => Yii::t('app', 'rc'),
            'rrn' => Yii::t('app', 'rrn'),
            'int_ref' => Yii::t('app', 'int_ref'),
            'auth_code' => Yii::t('app', 'auth_code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getType()
    {
        if ($this->trtype == self::REQUEST_TYPE_PREAUTH) {
            return 'PreAuth';
        } elseif ($this->trtype == self::REQUEST_TYPE_AUTH) {
            return 'Auth';
        } elseif ($this->trtype == self::REQUEST_TYPE_COMPLETE) {
            return 'Complet';
        } elseif ($this->trtype == self::REQUEST_TYPE_REVERSAL_ADVICE) {
            return 'RevAdv';
        }

        return '?';
    }

    public function getStatus()
    {
        if (is_null($this->action) && is_null($this->rc)) {
            return 'Без відповіді';
        } elseif ('0' === (string)$this->action && '0' === (string)$this->rc) {
            return 'Виконан';
        } else {
            return 'Помилка';
        }
    }

    public function getAmount($currency = true)
    {
        if ($currency) {
            $amount = AppHelper::currency($this->amount);
        } else {
            $amount = sprintf('%.2f', $this->amount);
        }

        return $amount;
    }

    public function getRequestId()
    {
        return $this->id;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (!$this->isNewRecord) {
            if ($this->trtype == self::REQUEST_TYPE_PREAUTH
                && (string)self::ACTION_TRANSACTION_SUCCESSFULLY === (string)$this->action
                && '0' === (string)$this->rc
            ) {
                $this->order->paymentCommit();
                $this->order->setStatusProcessing();
            }
            if ($this->trtype == self::REQUEST_TYPE_COMPLETE
                && (string)self::ACTION_TRANSACTION_SUCCESSFULLY === (string)$this->action
                && '0' === (string)$this->rc
            ) {
                $this->order->setStatusCompleted();
            }
            if ($this->trtype == self::REQUEST_TYPE_REVERSAL_ADVICE
                && (string)self::ACTION_TRANSACTION_SUCCESSFULLY === (string)$this->action
                && '0' === (string)$this->rc
            ) {
                $this->order->setStatusCanceled();
            }
        }
        parent::afterSave($insert, $changedAttributes);
        $gatewayRequestHistory = new GatewayRequestHistory();
        $gatewayRequestHistory->attributes = $this->attributes;
        $gatewayRequestHistory->insert();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGatewayResponseDescription()
    {
        return $this->hasOne(GatewayResponseDescription::className(), ['code' => 'rc']);
    }

    public function getName()
    {
        $gatewayResponseDescription = self::getGatewayResponseDescription()
            ->one();
        if ($gatewayResponseDescription) {
            $name = $gatewayResponseDescription->getName(self::getOrder()
                ->one()->language);
        } else {
            $name = Module::t('app', 'Без відповіді');
        }

        return $name;
    }

    /**
     * @return string
     */
    public function getTypeDescription()
    {
        switch ($this->trtype) {
            case self::REQUEST_TYPE_PREAUTH :
                $result = Module::t('tickets', 'Предавторизаційний запит');
                break;
            case self::REQUEST_TYPE_AUTH :
                $result = Module::t('tickets', 'Авторизаційний запит');
                break;
            case self::REQUEST_TYPE_COMPLETE :
                $result = Module::t('tickets', 'Завершення продажу');
                break;
            case self::REQUEST_TYPE_REVERSAL_ADVICE :
                $result = Module::t('tickets', 'Скасування продажу');
                break;
            default :
                $result = Module::t('tickets', 'Невідомий');
                break;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getTransactionDescription()
    {
        switch ($this->action) {
            case self::ACTION_TRANSACTION_SUCCESSFULLY :
                $result = Module::t('tickets', 'Успішна');
                break;
            case self::ACTION_DUPLICATE_TRANSACTION :
                $result = Module::t('tickets', 'Подвійна');
                break;
            case self::ACTION_TRANSACTION_DECLINED :
                $result = Module::t('tickets', 'Відмінюванна');
                break;
            case self::ACTION_TRANSACTION_FAILED :
                $result = Module::t('tickets', 'Невдала');
                break;
            default :
                $result = Module::t('tickets', 'Невідома');
                break;
        }

        return $result;
    }

    /**
     * @return null|static GatewayRequestHistory
     */
    public function getPreauth()
    {
        $result = self::findOne([
            'order_id' => $this->order_id,
            'trtype' => self::REQUEST_TYPE_PREAUTH,
            'action' => self::ACTION_TRANSACTION_SUCCESSFULLY,
            'rc' => 0
        ]);

        return $result;
    }

    public function isExpired()
    {
        $expirationAfter = Yii::$app->params['payment']['commerce_gateway']['expirationAfter'];
        $expirationDate = (new \DateTime())->setTimestamp($this->created_at)
            ->add(new \DateInterval('P' . $expirationAfter . 'D'));
        $expirationDate = \DateTime::createFromFormat('Y-m-d H:i:s', $expirationDate->format('Y-m-d') . ' 00:00:00');
        if (time() > $expirationDate->getTimestamp()) {
            return true;
        }

        return false;
    }
}
