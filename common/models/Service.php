<?php
namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "{{%service}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $provider_code
 * @property string $code
 * @property string $name_uk
 * @property string $short_name_uk
 * @property string $name_ru
 * @property string $short_name_ru
 * @property string $name_en
 * @property string $short_name_en
 * @property string $short_name_translit
 * @property string $description_uk
 * @property string $description_ru
 * @property string $description_en
 * @property string $script_url
 * @property string $controller
 * @property integer $active
 * @property integer $color
 *
 * @property Commission[] $commissions
 * @property Order[] $orders
 * @property PartnerService[] $partnerServices
 * @property Provider $provider
 */
class Service extends ActiveRecord
{
    const TICKETS_GD = 'TICKETS_GD';
    const TICKETS_BUS = 'TICKETS_BUS';
    const TICKETS_AVIA = 'TICKETS_AVIA';
    const TICKETS_FOOTBALL = 'TICKETS_FOOTBALL';
    const AIWA_ACCIDENTS5 = 'AIWA_ACCIDENTS5';
    const TICKETS_ENTERTAINMENTS = 'TICKETS_ENTERTAINMENTS';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'provider_code',
                    'code',
                    'name_uk',
                    'short_name_uk',
                    'name_ru',
                    'short_name_ru',
                    'name_en',
                    'short_name_en',
                    'short_name_translit',
                    'description_uk',
                    'script_url',
                    'controller',
                    'color',
                ],
                'required'
            ],
            [
                [
                    'created_at',
                    'updated_at',
                    'active'
                ],
                'integer'
            ],
            [
                [
                    'provider_code',
                    'code'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'name_uk',
                    'short_name_uk',
                    'name_ru',
                    'short_name_ru',
                    'name_en',
                    'short_name_en',
                    'short_name_translit',
                    'script_url',
                    'controller'
                ],
                'string',
                'max' => 50
            ],
            [
                ['description_uk'],
                'string',
                'max' => 1000
            ],
            [
                ['color'],
                'string',
                'max' => 7
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_code' => Yii::t('app', 'Код у провайдера'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
            'code' => Yii::t('app', 'Код в системі'),
            'name_uk' => Yii::t('app', 'Назва Українською'),
            'short_name_uk' => Yii::t('app', 'Коротка назва Українською'),
            'description_uk' => Yii::t('app', 'Опис Українською'),
            'name_ru' => Yii::t('app', 'Назва Російською'),
            'short_name_ru' => Yii::t('app', 'Коротка назва Російською'),
            'description_ru' => Yii::t('app', 'Опис Російською'),
            'name_en' => Yii::t('app', 'Назва Англійською'),
            'short_name_en' => Yii::t('app', 'Коротка назва Англійською'),
            'short_name_translit' => Yii::t('app', 'Коротка назва транслітом'),
            'description_en' => Yii::t('app', 'Опис Англійською'),
            'script_url' => Yii::t('app', 'Шлях'),
            'controller' => Yii::t('app', 'Контролер'),
            'active' => Yii::t('app', 'Активний'),
            'color' => Yii::t('app', 'Колір'),
        ];
    }

    public function getName($language = null)
    {
        return $this->{'name_' . (is_null($language)
            ? Yii::$app->language
            : $language)};
    }

    public function getDescription($language = null)
    {
        return $this->{'description_' . (is_null($language)
            ? Yii::$app->language
            : $language)};
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerServices()
    {
        return $this->hasMany(PartnerService::className(), ['service_id' => 'id']);
    }

//	/**
//	 * @return \yii\db\ActiveQuery
//	 */
//	public function getProvider()
//	{
//		return $this->hasOne(Provider::className(), ['id' => 'partner_id']);
//	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommissions()
    {
        return $this->hasMany(Commission::className(), ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviders()
    {
        return $this->hasMany(Provider::className(), ['id' => 'provider_id'])
            ->viaTable('service_provider', ['service_id' => 'id']);
    }
}
