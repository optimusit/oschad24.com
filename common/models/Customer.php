<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%customer}}".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property string $email
 * @property string $phone
 * @property string $ipn
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property string $birthday
 * @property string $gender
 * @property string $passport_series
 * @property string $passport_number
 * @property string $passport_date
 * @property string $passport_issuer
 * @property string $address_city
 * @property string $address_index
 * @property string $address_address
 * @property integer $confirm
 * @property string $confirm_key
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order[] $orders
 */
class Customer extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'partner_id',
                    'phone',
                ],
                'required'
            ],
            [
                [
                    'partner_id',
                    'confirm',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'birthday',
                    'passport_date'
                ],
                'safe'
            ],
            [
                ['gender'],
                'string'
            ],
            [
                ['email'],
                'string',
                'max' => 200
            ],
            [
                ['phone'],
                'string',
                'max' => 13
            ],
            [
                ['ipn'],
                'string',
                'max' => 12
            ],
            [
                [
                    'surname',
                    'name',
                    'patronymic',
                    'confirm_key'
                ],
                'string',
                'max' => 100
            ],
            [
                ['passport_series'],
                'string',
                'max' => 2
            ],
            [
                ['passport_number'],
                'string',
                'max' => 8
            ],
            [
                [
                    'passport_issuer',
                    'address_city',
                    'address_address'
                ],
                'string',
                'max' => 250
            ],
            [
                ['address_index'],
                'string',
                'max' => 6
            ],
            [
                [
                    'partner_id',
                    'phone'
                ],
                'unique',
                'targetAttribute' => [
                    'partner_id',
                    'phone'
                ],
                'message' => 'The combination of Партнер and Телефон has already been taken.'
            ],
            [
                [
                    'partner_id',
                    'ipn'
                ],
                'unique',
                'targetAttribute' => [
                    'partner_id',
                    'ipn'
                ],
                'message' => 'The combination of Партнер and ІПН has already been taken.'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'partner_id' => Yii::t('app', 'Партнер'),
            'email' => Yii::t('app', 'Електронна адреса'),
            'phone' => Yii::t('app', 'Телефон'),
            'ipn' => Yii::t('app', 'ІПН'),
            'surname' => Yii::t('app', 'Прізвище'),
            'name' => Yii::t('app', 'Ім`я'),
            'patronymic' => Yii::t('app', 'Побатькові'),
            'birthday' => Yii::t('app', 'Дата народження'),
            'gender' => Yii::t('app', 'Стать'),
            'passport_series' => Yii::t('app', 'Серія паспорту'),
            'passport_number' => Yii::t('app', '№ паспорту'),
            'passport_date' => Yii::t('app', 'Дата видачі паспорту'),
            'passport_issuer' => Yii::t('app', 'Орган що видав'),
            'address_city' => Yii::t('app', 'Населений пункт'),
            'address_index' => Yii::t('app', 'Індекс'),
            'address_address' => Yii::t('app', 'Адреса'),
            'confirm' => Yii::t('app', 'Підтверджено'),
            'confirm_key' => Yii::t('app', 'Ключ підтвердження'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->confirm_key = Yii::$app->security->generateRandomString();

                return true;
            }
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['customer_id' => 'id']);
    }

    public function getFullName()
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
    }
}
