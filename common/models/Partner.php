<?php

namespace common\models;

use Yii;
use common\components\TreeManager\TreeView;
use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "partner".
 *
 * @property integer $id
 * @property string $prefix
 * @property string $name
 * @property string $logo
 * @property integer $primary
 * @property integer $parent
 * @property integer $level
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $email
 * @property string $phone
 * @property string $access_token
 * @property string $secret_key
 * @property string $balance
 * @property string $credit_limit
 * @property string $edrpou
 * @property integer $mfo
 * @property string $rr
 * @property string $bank
 * @property string $internal_num
 * @property string $transit_rr
 * @property string $commission_rr
 * @property string $return_rr
 * @property string $const_file_mb
 *
 * @property Commission[] $commissions
 * @property Order[] $orders
 * @property PartnerService[] $partnerServices
 * @property User[] $users
 */
class Partner extends \common\components\TreeManager\Tree implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    public $encodeNodeNames = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['timestamp'] = [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
            ],
            'value' => function () {
                return date('U');
            }
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        // взято из родителя для того, чтобы подменить TreeView (см. components\TreeManager\readme.md)
        $module = TreeView::module();
        $nameAttribute = $iconAttribute = $iconTypeAttribute = null;
        extract($module->dataStructure);
        $attribs = array_merge([
            $nameAttribute,
            $iconAttribute,
            $iconTypeAttribute
        ], static::$boolAttribs);
        $rules = [
            [
                [$nameAttribute],
                'required'
            ],
            [
                $attribs,
                'safe'
            ]
        ];
        if ($this->encodeNodeNames) {
            $rules[] = [
                $nameAttribute,
                'filter',
                'filter' => function ($value) {
                    return Html::encode($value);
                }
            ];
        }
        if ($this->purifyNodeIcons) {
            $rules[] = [
                $iconAttribute,
                'filter',
                'filter' => function ($value) {
                    return HtmlPurifier::process($value);
                }
            ];
        }

        return array_merge($rules, [
            [
                [
                    'prefix',
                    'name',
                    //'email',
                    //'phone',
                    'access_token',
                    'secret_key',
                    'balance',
                    'edrpou',
                    'rr',
                    'bank'
                ],
                'required'
            ],
            [
                [
                    'created_at',
                    'updated_at',
                    'mfo',
                ],
                'integer'
            ],
            [
                [
                    'balance',
                    'credit_limit'
                ],
                'number'
            ],
            [
                [
                    'phone',
                    'access_token'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'prefix',
                ],
                'string',
                'max' => 100
            ],
            [
                [
                    'name',
                    'logo',
                    'email',
                    'bank'
                ],
                'string',
                'max' => 255
            ],
            [
                ['secret_key'],
                'string',
                'max' => 60
            ],
            [
                ['edrpou'],
                'string',
                'max' => 12
            ],
            [
                [
                    'rr',
                    'transit_rr',
                    'commission_rr',
                    'return_rr',
                    'const_file_mb'
                ],
                'string',
                'max' => 14
            ],
            [
                ['internal_num'],
                'string',
                'max' => 6
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('app', 'ID'),
            'prefix' => Yii::t('app', 'Приставка у зоні .veb.ua'),
            'name' => Yii::t('app', 'Назва'),
            'logo' => Yii::t('app', 'Логотип'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
            'email' => Yii::t('app', 'Поштова адреса'),
            'phone' => Yii::t('app', 'Телефон'),
            'access_token' => Yii::t('app', 'Маркер доступу'),
            'secret_key' => Yii::t('app', 'Секретный ключ'),
            'balance' => Yii::t('app', 'Баланс'),
            'credit_limit' => Yii::t('app', 'Кредитный ліміт'),
            'edrpou' => Yii::t('app', 'ЄДРПОУ'),
            'mfo' => Yii::t('app', 'МФО'),
            'rr' => Yii::t('app', 'Розрахунковий рахунок'),
            'bank' => Yii::t('app', 'Банк'),
            'internal_num' => Yii::t('app', '(АРМ) Номер філії організації'),
            'transit_rr' => Yii::t('app', '(АРМ) Транзитній рахунок'),
            'commission_rr' => Yii::t('app', '(АРМ) Комісійний рахунок'),
            'return_rr' => Yii::t('app', 'Рахунок повернення'),
            'const_file_mb' => Yii::t('app', 'Константа файлуа міжбанку'),
            'active' => Yii::t('app', 'Активний'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'active' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])
            ->viaTable('partner_service', ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommission()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])
            ->viaTable('partner_service', ['partner_id' => 'id']);
    }

    /**
     * Проверяет наличие прав доступа партнера к сервису
     *
     * @param $name string Название контроллера
     * @return bool
     */
    public function haveAccessToService($name)
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])
            ->andWhere(['controller' => $name])
            ->viaTable('partner_service', ['partner_id' => 'id'])
            ->exists();
    }

    public function setAuthKeyExpiration()
    {
    }
}
