<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "{{%order_provider_history}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $request
 * @property string $response
 * @property string $response_format
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 */
class OrderProviderHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_provider_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'request',
                    'response',
                    'response_format',
                ],
                'required'
            ],
            [
                [
                    'order_id',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'request',
                    'response'
                ],
                'string'
            ],
            [
                [
                    'response_format'
                ],
                'string',
                'max' => 20
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Ордер'),
            'request' => Yii::t('app', 'Запит'),
            'response' => Yii::t('app', 'Відповідь'),
            'response_format' => Yii::t('app', 'Формат відповіді'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
