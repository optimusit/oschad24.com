<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%sms_queue}}".
 *
 * @property integer $id
 * @property string $phone
 * @property string $text
 * @property string $hash
 * @property integer $number_tries
 * @property integer $created_at
 * @property integer $updated_at
 */
class SmsQueue extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_queue}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'phone',
                    'text',
                    'hash',
                    'number_tries',
                ],
                'required'
            ],
            [
                [
                    'number_tries',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                ['phone'],
                'string',
                'max' => 13
            ],
            [
                ['text'],
                'string',
                'max' => 300
            ],
            [
                ['hash'],
                'string',
                'max' => 32
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'phone' => Yii::t('app', 'Телефон'),
            'text' => Yii::t('app', 'Текст'),
            'hash' => Yii::t('app', 'hash'),
            'number_tries' => Yii::t('app', 'Кількість спроб'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }
}
