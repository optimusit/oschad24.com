<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "{{%commission}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $partner_id
 * @property integer $service_id
 * @property integer $kind
 * @property integer $provider_id
 * @property string $min
 * @property string $max
 * @property string $value
 * @property integer $internal
 * @property integer $round_type
 * @property integer $type
 *
 * @property Partner $partner
 * @property Service $service
 */
class Commission extends ActiveRecord
{
    const TYPE_STATIC = 0;
    const TYPE_PERCENTAGE = 1;
    const TYPE_EXPRESSION = 9;

    const KIND_SYSTEM = 0;
    const KIND_PARTNER = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%commission}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'partner_id',
                    'service_id',
                    'kind',
                    'provider_id',
                    'min',
                    'max',
                    'value',
                    'round_type',
                    'type'
                ],
                'required'
            ],
            [
                [
                    'created_at',
                    'updated_at',
                    'partner_id',
                    'service_id',
                    'kind',
                    'provider_id',
                    'internal',
                    'round_type',
                    'type'
                ],
                'integer'
            ],
            [
                [
                    'min',
                    'max',
                    'value'
                ],
                'number'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
            'partner_id' => Yii::t('app', 'Партнер'),
            'service_id' => Yii::t('app', 'Сервіс'),
            'kind' => Yii::t('app', 'Тип'),
            'provider_id' => Yii::t('app', 'Провайдер'),
            'min' => Yii::t('app', 'Мінімальна сума'),
            'max' => Yii::t('app', 'Максимальна сума'),
            'value' => Yii::t('app', 'Значення'),
            'internal' => Yii::t('app', 'Внутрішня'),
            'round_type' => Yii::t('app', 'Округлення'),
            'type' => Yii::t('app', 'Тип2'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    static function extractCommission($amount, $min, $max, $commission, $roundType = 0, $type = self::TYPE_STATIC)
    {
        $roundType = intval($roundType);
        $return = 0;
        if ($type == self::TYPE_STATIC) {
            return $commission;
        }
        if ($type == self::TYPE_PERCENTAGE) {
            if ($commission > 0) {
                $return = $amount * ($commission / 100);
                if ($roundType == 0) {
                    $return = round($return, 2);
                } else {
                    $return = round($return, 0);
                }
                if ($min > 0 && $return < $min) {
                    $return = $min;
                }
                if ($max > 0 && $return > $max) {
                    $return = $max;
                }
            }
        }

        return $return;
    }
}
