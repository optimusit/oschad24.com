<?php

namespace common\models\kontramarka;

use Yii;

/**
 * This is the model class for table "{{%kontramarka_show}}".
 *
 * @property integer $siteId
 * @property integer $showId
 * @property integer $eventId
 * @property integer $hallId
 * @property string $name
 * @property string $description
 * @property string $date
 * @property string $poster
 * @property integer $created_at
 * @property integer $updated_at
 */
class Show extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kontramarka_show}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'siteId',
                    'showId',
                    'eventId',
                    'hallId',
                    'name',
                    'description',
                    'date',
                    'poster',
                ],
                'required'
            ],
            [
                [
                    'siteId',
                    'showId',
                    'eventId',
                    'hallId',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                'date',
                'safe',
            ],
            [
                ['name'],
                'string',
                'max' => 500
            ],
            [
                ['description'],
                'string',
                'max' => 10000
            ],
            [
                ['poster'],
                'string',
                'max' => 2000
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'siteId' => Yii::t('app', 'Site ID'),
            'showId' => Yii::t('app', 'Show ID'),
            'eventId' => Yii::t('app', 'Event ID'),
            'hallId' => Yii::t('app', 'Hall ID'),
            'name' => Yii::t('app', 'Назва'),
            'description' => Yii::t('app', 'Опис'),
            'date' => Yii::t('app', 'Дата'),
            'poster' => Yii::t('app', 'Постер'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }
}
