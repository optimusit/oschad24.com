<?php

namespace common\models\kontramarka;

use Yii;

/**
 * This is the model class for table "{{%kontramarka_site}}".
 *
 * @property integer $siteId
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 */
class Sites extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kontramarka_site}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'siteId',
                    'name',
                ],
                'required'
            ],
            [
                [
                    'siteId',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                ['name'],
                'string',
                'max' => 500
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'siteId' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Назва'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }
}
