<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "{{%service_provider}}".
 *
 * @property integer $id
 * @property integer $service_id
 * @property integer $provider_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Service $service
 * @property Provider $provider
 */
class ServiceProvider extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service_provider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'service_id',
                    'provider_id',
                ],
                'required'
            ],
            [
                [
                    'service_id',
                    'provider_id',
                ],
                'integer'
            ],
            [
                [
                    'provider_id',
                    'service_id'
                ],
                'unique',
                'targetAttribute' => [
                    'provider_id',
                    'service_id'
                ],
                'message' => 'The combination of Сервіс and Провайдер has already been taken.'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Сервіс',
            'provider_id' => 'Провайдер',
            'created_at' => 'Створено',
            'updated_at' => 'Змінено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasMany(Provider::className(), ['id' => 'provider_id']);
    }
}
