<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "{{%airlines}}".
 *
 * @property string $iata
 * @property string $name
 * @property string $logo
 */
class Airlines extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%airlines}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'iata',
                    'name'
                ],
                'required'
            ],
            [
                [
                    'iata',
                    'logo'
                ],
                'string',
                'max' => 2
            ],
            [
                ['name'],
                'string',
                'max' => 100
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iata' => Yii::t('app', 'IATA код'),
            'name' => Yii::t('app', 'Назва'),
            'logo' => Yii::t('app', 'Код картинки логотипа'),
        ];
    }

    static public function getByIATA($code)
    {
        $result = Yii::$app->db->createCommand('select * from airlines where iata=:iata')
            ->bindValues([':iata' => $code])
            ->cache()
            ->queryOne();
        if (!$result) {
            Yii::warning('Unknown supplier ' . $code);
            $result = Yii::$app->db->createCommand('select * from airlines where iata="00"')
                ->cache(60 * 60 * 24)
                ->queryOne();
        }

        return $result;
    }
}
