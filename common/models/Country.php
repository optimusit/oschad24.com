<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_uk
 * @property string $alpha2
 * @property string $alpha3
 * @property string $iso
 */
class Country extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name_en',
                    'name_ru',
                    'name_uk',
                    'alpha2',
                    'alpha3',
                    'iso',
                ],
                'required'
            ],
            [
                [
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'name_en',
                    'name_ru',
                    'name_uk'
                ],
                'string',
                'max' => 255
            ],
            [
                ['alpha2'],
                'string',
                'max' => 2
            ],
            [
                [
                    'alpha3',
                    'iso'
                ],
                'string',
                'max' => 3
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Назва Англійська'),
            'name_ru' => Yii::t('app', 'Назва Російська'),
            'name_uk' => Yii::t('app', 'Назва Українська'),
            'alpha2' => Yii::t('app', 'Двозначний код'),
            'alpha3' => Yii::t('app', 'Трьозначинй код'),
            'iso' => Yii::t('app', 'ISO'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    static public function getList()
    {
        return self::find()
            ->orderBy('name_' . Yii::$app->language)
            ->asArray()
            ->all();
    }
}
