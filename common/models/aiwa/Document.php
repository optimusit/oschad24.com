<?php

namespace common\models\aiwa;

use Yii;
use common\models\Order;

/**
 * This is the model class for table "{{%aiwa_document}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $doc_type
 * @property integer $system_id
 * @property string $amount
 * @property string $author
 * @property string $cover
 * @property integer $currency
 * @property string $date_from
 * @property string $date_period
 * @property string $date_to
 * @property string $doc_date
 * @property string $doc_number
 * @property integer $draft
 * @property string $plan
 * @property integer $programm
 * @property integer $variant
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 * @property Participant[] $Participants
 */
class Document extends \common\components\ActiveRecord
{
    // Застраховані особи:1-родичі,2-дружина і діти
    CONST VARIANT_FAMILY = 1;
    CONST VARIANT_WIFE_AND_CHILDREN = 2;

    CONST TYPE_ACCIDENTS5 = 'accidents5';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%aiwa_document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    //'order_id',
                    'doc_type',
                    //'system_id',
                    'amount',
                    'author',
                    'cover',
                    'date_from',
                    'date_period',
                    //'date_to',
                    //'doc_date',
                    //'doc_number',
                    //'variant',
                ],
                'required'
            ],
            [
                [
                    'order_id',
                    'system_id',
                    'currency',
                    'draft',
                    'programm',
                    'variant',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'amount',
                    'cover'
                ],
                'number'
            ],
            [
                [
                    'date_from',
                    'date_to',
                    'doc_date'
                ],
                'safe'
            ],
            [
                ['doc_type'],
                'string',
                'max' => 30
            ],
            [
                ['author'],
                'string',
                'max' => 200
            ],
            [
                ['date_period'],
                'string',
                'max' => 10
            ],
            [
                ['doc_number'],
                'string',
                'max' => 255
            ],
            [
                ['plan'],
                'string',
                'max' => 1
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Замовлення'),
            'doc_type' => Yii::t('app', 'Тип документу'),
            'system_id' => Yii::t('app', '№ в системі постачальника'),
            'amount' => Yii::t('app', 'Страховий платіж'),
            'author' => Yii::t('app', 'Електронна адреса'),
            'cover' => Yii::t('app', 'Страховая сумма'),
            'currency' => Yii::t('app', 'Код валюты'),
            'date_from' => Yii::t('app', 'Дата початку дії договору'),
            'date_period' => Yii::t('app', 'Період'),
            'date_to' => Yii::t('app', 'Дата закінчення дії договору'),
            'doc_date' => Yii::t('app', 'Дата створення документу'),
            'doc_number' => Yii::t('app', '№ документу'),
            'draft' => Yii::t('app', 'Чернетка'),
            'plan' => Yii::t('app', 'Варіант покриття'),
            'programm' => Yii::t('app', 'Код программы: 5'),
            'variant' => Yii::t('app', 'Застраховані особи:1-родичі,2-дружина і діти'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $dateTo = new \DateTime($this->date_from);
            $period = mb_strtoupper(str_replace('+', 'p', $this->date_period));
            $dateTo->add(new \DateInterval($period));
            $this->date_to = $dateTo->format('Y-m-d');
            $this->draft = 1;
            $this->doc_date = (new \DateTime())->format('Y-m-d');
            $this->variant = self::VARIANT_FAMILY;
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants()
    {
        return $this->hasMany(Participant::className(), ['document_id' => 'id']);
    }

    /**
     * @return Participant
     */
    public function getVendor()
    {
        return $this->getParticipants()
            ->andWhere(['type' => Participant::TYPE_VENDOR])
            ->one();
    }

    /**
     * @return Participant[]
     */
    public function getOtherVendors()
    {
        return $this->getParticipants()
            ->andWhere(['type' => Participant::TYPE_TPL])
            ->all();
    }

    public function getAccidents5DataForProvider()
    {
        return $this->getDataForProvider(self::TYPE_ACCIDENTS5);
    }

    protected function getDataForProvider($type)
    {
        // общие поля для всех типов документов
        $commonData = [
            'amount' => $this->amount,
            'cover' => $this->cover,
            'date-from' => Yii::$app->formatter->asDate($this->date_from),
            'date-period' => $this->date_period,
            'date-to' => Yii::$app->formatter->asDate($this->date_to),
            'doc-date' => Yii::$app->formatter->asDate($this->doc_date),
            'doc-number' => $this->doc_number,
            'draft' => 1,
        ];
        $productData = [];
        if ($type == self::TYPE_ACCIDENTS5) {
            $vendor = $this->getVendor();
            $vendorData = $vendor->getVendorData();
            $otherVendorsData = [];
            $otherVendors = $this->getOtherVendors();
            if ($otherVendors) {
                /** @var Participant $vendor */
                foreach ($otherVendors as $vendor) {
                    $otherVendorsData[] = $vendor->getOtherVendorData();
                }
            }
            $productData = array_merge([
                'plan' => $this->plan,
                'programm' => $this->programm,
                'variant' => $this->variant,
                'vendorCount' => 0
            ], $vendorData, $otherVendorsData);
        }
        $data = array_merge($commonData, $productData);

        return $data;
    }
}
