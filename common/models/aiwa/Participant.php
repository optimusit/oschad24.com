<?php

namespace common\models\aiwa;

use Yii;

/**
 * This is the model class for table "{{%aiwa_participant}}".
 *
 * @property integer $id
 * @property integer $document_id
 * @property string $type
 * @property string $kind
 * @property string $phone
 * @property string $ipn
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property string $title
 * @property string $birthday
 * @property string $passport_series
 * @property string $passport_number
 * @property string $passport_date
 * @property string $passport_issuer
 * @property string $address_city
 * @property string $address_index
 * @property string $address_address
 * @property string $address_title
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Document $document
 */
class Participant extends \common\components\ActiveRecord
{
    // Тип учасника
    const TYPE_VENDOR = 'vendor';
    const TYPE_TPL = 'tpl';
    const TYPE_BENEFICIARY = 'beneficiary';

    // Вид контрагента-страхувальника
    const KIND_PRIVATE = 'private';
    const KIND_ORGANIZATION = 'organization';

    const DATA_FIELDS_MAP = [
        'address_address' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'address-address',
        ],
        'address_city' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'address-city'
        ],
        'address_index' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'address-index'
        ],
        'address_title' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'address-title'
        ],
        'birthday' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'birthday'
        ],
        'ipn' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'inn'
        ],
        'kind' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'kind'
        ],
        'surname' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'name1'
        ],
        'name' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'name2'
        ],
        'patronymic' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'name3'
        ],
        'passport_date' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'passport-date'
        ],
        'passport_issuer' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'passport-issuer'
        ],
        'passport_number' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'passport-number'
        ],
        'passport_series' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'passport-series'
        ],
        'phone' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'phone'
        ],
        'title' => [
            'types' => [
                self::TYPE_VENDOR,
                self::TYPE_TPL,
                self::TYPE_BENEFICIARY,
            ],
            'value' => 'title'
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%aiwa_participant}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    //'document_id',
                    'type',
                    'kind',
                    'phone',
                    'ipn',
                    'surname',
                    'name',
                    'patronymic',
                    'birthday',
                    'passport_series',
                    'passport_number',
                    'passport_date',
                    'passport_issuer',
                    'address_city',
                    'address_index',
                    'address_address',
                ],
                'required'
            ],
            [
                [
                    'document_id',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'birthday',
                    'passport_date'
                ],
                'safe'
            ],
            [
                ['type'],
                'string',
                'max' => 11
            ],
            [
                [
                    'kind',
                    'phone'
                ],
                'string',
                'max' => 13
            ],
            [
                ['ipn'],
                'string',
                'max' => 12
            ],
            [
                [
                    'surname',
                    'name',
                    'patronymic'
                ],
                'string',
                'max' => 100
            ],
            [
                ['passport_series'],
                'string',
                'max' => 2
            ],
            [
                ['passport_number'],
                'string',
                'max' => 8
            ],
            [
                [
                    'passport_issuer',
                    'address_city',
                    'address_address',
                    'address_title'
                ],
                'string',
                'max' => 250
            ],
            [
                ['address_index'],
                'string',
                'max' => 6
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'document_id' => Yii::t('app', 'ID документу'),
            'type' => Yii::t('app', 'Тип учасника'),
            'kind' => Yii::t('app', 'Вид контрагента-страхувальника'),
            'phone' => Yii::t('app', 'Телефон'),
            'ipn' => Yii::t('app', 'ІПН'),
            'surname' => Yii::t('app', 'Прізвище'),
            'name' => Yii::t('app', 'Ім`я'),
            'patronymic' => Yii::t('app', 'Побатькові'),
            'title' => Yii::t('app', 'Подання (Прізвище Ім`я Побатькові)'),
            'birthday' => Yii::t('app', 'Дата народження'),
            'passport_series' => Yii::t('app', 'Серія паспорту'),
            'passport_number' => Yii::t('app', '№ паспорту'),
            'passport_date' => Yii::t('app', 'Дата видачі паспорту'),
            'passport_issuer' => Yii::t('app', 'Орган що видав'),
            'address_city' => Yii::t('app', 'Населений пункт'),
            'address_index' => Yii::t('app', 'Індекс'),
            'address_address' => Yii::t('app', 'Адреса'),
            'address_title' => Yii::t('app', 'Повна адреса'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->title = $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
            $this->address_title = $this->address_index . ' ' . $this->address_city . ' ' . $this->address_address;
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    public function getVendorData()
    {
        return $this->getData(self::TYPE_VENDOR);
    }

    public function getOtherVendorData()
    {
        return $this->getData(self::TYPE_TPL);
    }

    protected function getData($type)
    {
        $data = [];
        foreach (self::DATA_FIELDS_MAP as $key => $value) {
            if (in_array($type, $value['types'])) {
                $data[$type . '-' . $value['value']] = $this->$key;
            }
        }

        return $data;
    }
}
