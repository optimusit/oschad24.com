<?php
namespace common\models;

use Yii;
use common\components\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $auth_key
 * @property integer $auth_key_remember_forever
 * @property integer $auth_key_expiration
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $change_email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $phone
 * @property string $sms_code
 * @property string $sms_time
 * @property string $name
 * @property string $patronymic
 * @property string $surname
 * @property integer $confirm
 * @property string $confirm_key
 * @property integer $partner_id
 *
 * @property Partner $partner
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGER = 'manager';
    const ROLE_CASHIER = 'cashier';
    const ROLE_MARKETPLACE = 'marketplace';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'auth_key',
                    'password_hash',
                    'email',
                    'phone',
                    'name',
                    'patronymic',
                    'surname',
                    'confirm',
                    'confirm_key',
                    'partner_id'
                ],
                'required'
            ],
            [
                [
                    'auth_key_remember_forever',
                ],
                'safe'
            ],
            [
                [
                    'status',
                    'auth_key_expiration',
                    'auth_key_remember_forever',
                    'created_at',
                    'updated_at',
                    'confirm',
                    'partner_id'
                ],
                'integer'
            ],
            [
                ['sms_time'],
                'safe'
            ],
            [
                [
                    'password_hash',
                    'password_reset_token',
                    'email'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'auth_key',
                    'phone',
                    'sms_code',
                    'confirm_key'
                ],
                'string',
                'max' => 32
            ],
            [
                ['change_email'],
                'string',
                'max' => 60
            ],
            [
                [
                    'name',
                    'patronymic',
                    'surname'
                ],
                'string',
                'max' => 100
            ],
            [
                [
                    'partner_id',
                    'email',
                ],
                'unique',
                'targetAttribute' => [
                    'partner_id',
                    'email',
                ],
                'message' => 'The combination of Ім`я входу (логін) and Партнер has already been taken.'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'auth_key' => Yii::t('app', 'Ключ доступу'),
            'auth_key_expiration' => Yii::t('app', 'Термін придатності ключа доступу'),
            'password_hash' => Yii::t('app', 'Хеш паролю'),
            'password_reset_token' => Yii::t('app', 'Маркер сбросу пароля'),
            'email' => Yii::t('app', 'Поштова адреса'),
            'change_email' => Yii::t('app', 'Нова поштова адреса'),
            'status' => Yii::t('app', 'Стан'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
            'phone' => Yii::t('app', 'Телефон'),
            'sms_code' => Yii::t('app', 'SMS код'),
            'sms_time' => Yii::t('app', 'SMS час'),
            'name' => Yii::t('app', 'Им`я'),
            'patronymic' => Yii::t('app', 'По батькові'),
            'surname' => Yii::t('app', 'Прізвище'),
            'confirm' => Yii::t('app', 'Підтверджено'),
            'confirm_key' => Yii::t('app', 'Ключ підтвердження'),
            'partner_id' => Yii::t('app', 'Партнер'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

        return static::find()
            ->where(['auth_key' => $token])
            ->andWhere([
                '>',
                'auth_key_expiration',
                date('U')
            ])
            ->one();
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function hash($password)
    {
        $hash = crypt($password, '$2a$10$' . Yii::$app->security->generateRandomKey());
        if ($hash == '' || $hash == '*0' || $hash == '*1') {
            throw new \Exception('Cryptography error');
        }

        return $hash;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerRoot()
    {
        $partner = $this->hasOne(Partner::className(), ['id' => 'partner_id'])
            ->one();
        if ($partner->isRoot()) {
            return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
        } else {
            return $partner->parents();
        }
    }

    public function getName()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function setAuthKeyExpiration()
    {
        if (!$this->auth_key_remember_forever) {
            $this->auth_key_expiration = (new \DateTime())->modify('+' . Yii::$app->session->timeout . ' second')
                ->getTimestamp();
            $this->save();
        }
    }
}
