<?php

namespace common\models;

use common\components\AppHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\jui\DatePicker;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order
{
    /** @var  ActiveDataProvider */
    public $dataProvider;
    public $applyCreateDateFilter = true;
    public $createdFrom;
    public $createdTo;
    public $paidFrom;
    public $paidTo;
    public $expiredFrom;
    public $expiredTo;
    public $totalUnitCount = 0;
    public $totalAmount = 0;
    public $totalServiceAmount = 0;
    public $totalAcquiringAmount = 0;
    public $totalBankAmount = 0;
    public $totalToProviderAmount = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'createdFrom',
                    'createdTo',
                ],
                'required'
            ],
            [
                [
                    'id',
                    'service_id',
                    'provider_id',
                    'customer_id',
                    'user_id',
                    'partner_id',
                    'created_at',
                    'updated_at',
                    'status_id',
                    'unit_count',
                    'commission_internal',
                    'partner_commission_internal',
                    'viewed'
                ],
                'integer'
            ],
            [
                [
                    'phone',
                    'custom',
                    'expiration_time',
                    'paidFrom',
                    'paidTo',
                    'expiredFrom',
                    'expiredTo',
                    'applyCreateDateFilter',
                ],
                'safe'
            ],
            [
                [
                    'income_amount',
                    'fee',
                    'order_amount',
                    'partner_fee',
                    'partner_amount',
                    'bank_fee',
                    'e_fee',
                    'amount'
                ],
                'number'
            ],
            [
                [
                    'provider_order_id',
                    'name',
                    'surname',
                ],
                'string'
            ],
            [
                [
                    'createdFrom',
                    'createdTo',
                    'paidFrom',
                    'paidTo',
                    'expiredFrom',
                    'expiredTo',
                ],
                'default',
                'value' => null,
            ],
            [
                ['applyCreateDateFilter'],
                'boolean'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (is_null($this->createdFrom)) {
            $this->createdFrom = (new \DateTime())->sub(new \DateInterval('P30D'))
                ->getTimestamp();
        }
        if (is_null($this->createdTo)) {
            $this->createdTo = (new \DateTime())->getTimestamp();
        }
        if (floatval($this->income_amount) > 0) {
            $this->income_amount = str_replace(',', '.', $this->income_amount);
        }
        if (floatval($this->amount) > 0) {
            $this->amount = str_replace(',', '.', $this->amount);
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
        if (($createdFrom = \DateTime::createFromFormat(AppHelper::GetDateFormat() . ' H:i',
                $this->createdFrom . ' 00:00')) != false
        ) {
            $this->createdFrom = $createdFrom->getTimestamp();
        }
        if (($createdTo = \DateTime::createFromFormat(AppHelper::GetDateFormat() . ' H:i', $this->createdTo . ' 00:00'))
            != false
        ) {
            $createdTo->add(new \DateInterval('P1D'))
                ->sub(new \DateInterval('PT1S'));
            $this->createdTo = $createdTo->getTimestamp();
        }
        if (($paidFrom = \DateTime::createFromFormat(AppHelper::GetDateFormat() . ' H:i', $this->paidFrom . ' 00:00'))
            != false
        ) {
            $this->paidFrom = AppHelper::DateTimeForMySQL($paidFrom);
        }
        if (($paidTo = \DateTime::createFromFormat(AppHelper::GetDateFormat() . ' H:i', $this->paidTo . ' 00:00'))
            != false
        ) {
            $this->paidTo = AppHelper::DateTimeForMySQL($paidTo);
        }
        if (($expiredFrom = \DateTime::createFromFormat(AppHelper::GetDateFormat() . ' H:i',
                $this->expiredFrom . ' 00:00')) != false
        ) {
            $this->expiredFrom = AppHelper::DateTimeForMySQL($expiredFrom);
        }
        if (($expiredTo = \DateTime::createFromFormat(AppHelper::GetDateFormat() . ' H:i', $this->expiredTo . ' 00:00'))
            != false
        ) {
            $this->expiredTo = AppHelper::DateTimeForMySQL($expiredTo);
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applyCreateDateFilter' => Yii::t('app', 'Урахувати'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $extraParams = [], $calculateTotal = false)
    {
        $query = $this::find()
            ->where($extraParams);
        if (is_array($params) && array_key_exists('sort', $params)) {
        } else {
            $query->orderBy(['id' => SORT_DESC]);
        }
        $this->load($params);
        if (!$this->validate()) {
            return new ActiveDataProvider([
                'query' => self::find()
                    ->where('1=0'),
            ]);
        }
        $query->andFilterWhere([
            'order.id' => $this->id,
            'order.provider_order_id' => $this->provider_order_id,
            'order.service_id' => $this->service_id,
            'order.provider_id' => $this->provider_id,
            'order.customer_id' => $this->customer_id,
            'order.user_id' => $this->user_id,
            'order.partner_id' => $this->partner_id,
            'order.name' => $this->name,
            'order.surname' => $this->surname,
            'order.status_id' => $this->status_id,
            'order.unit_count' => $this->unit_count,
            'order.income_amount' => $this->income_amount,
            'order.amount' => $this->amount,
        ]);

        $query->andFilterWhere([
            'like',
            'order.phone',
            $this->phone
        ])
            ->andFilterWhere([
                'like',
                'order.custom',
                $this->custom
            ]);
        if ($this->applyCreateDateFilter) {
            if (!is_null($this->createdFrom)) {
                $query->andFilterWhere([
                    '>=',
                    'order.created_at',
                    $this->createdFrom
                ]);
            }
            if (!is_null($this->createdTo)) {
                $query->andFilterWhere([
                    '<=',
                    'order.created_at',
                    $this->createdTo
                ]);
            }
        }
        if (!is_null($this->paidFrom)) {
            $query->andFilterWhere([
                '>=',
                'order.paid_time',
                $this->paidFrom
            ]);
        }
        if (!is_null($this->paidTo)) {
            $query->andFilterWhere([
                '<=',
                'order.paid_time',
                $this->paidTo
            ]);
        }
        if (!is_null($this->expiredFrom)) {
            $query->andFilterWhere([
                '>=',
                'order.expiration_time',
                $this->expiredFrom
            ]);
        }
        if (!is_null($this->expiredTo)) {
            $query->andFilterWhere([
                '<=',
                'order.expiration_time',
                $this->expiredTo
            ]);
        }
        $query->innerJoinWith('service');
        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if ($calculateTotal) {
            $this->CalculateTotal();
        }

        return $this->dataProvider;
    }

    public function CalculateTotal()
    {
        if ($this->dataProvider) {
            $orders = $this->dataProvider->query->all();
            /** @var Order $order */
            foreach ($orders as $order) {
                $this->totalUnitCount += (int)$order->unit_count;
                $this->totalAmount += (float)$order->amount;
                $this->totalServiceAmount += (float)$order->getServiceAmount();
                $this->totalAcquiringAmount += (float)$order->getAcquiringAmount();
                $this->totalBankAmount += (float)$order->getBankAmount();
                $this->totalToProviderAmount += (float)$order->getToProviderAmount();
            }
        }
    }

    public function getCreateDateFilter()
    {
        $result = Html::activeCheckbox($this, 'applyCreateDateFilter') . DatePicker::widget([
                'model' => $this,
                'attribute' => 'createdFrom',
            ]) . DatePicker::widget([
                'model' => $this,
                'attribute' => 'createdTo',
            ]);

        return $result;
    }

    public function getPaidDateFilter()
    {
        $result = DatePicker::widget([
                'model' => $this,
                'attribute' => 'paidFrom',
            ]) . DatePicker::widget([
                'model' => $this,
                'attribute' => 'paidTo',
            ]);

        return $result;
    }

    public function getExpiredDateFilter()
    {
        $result = DatePicker::widget([
                'model' => $this,
                'attribute' => 'expiredFrom',
            ]) . DatePicker::widget([
                'model' => $this,
                'attribute' => 'expiredTo',
            ]);

        return $result;
    }
}
