<?php

namespace common\models\back\ticketsua;

use Yii;

class GdBooking extends Booking
{
    const STATUSES = [
        0 => 'ожидание оплаты',
        1 => 'оплачено',
        3 => 'аннулировано оплаченое',
        4 => 'аннулировано неоплаченое',
        7 => 'оплачено после аннулирования',
        8 => 'возвращены средства',
        10 => 'заказ в процессе возврата',
    ];

    public $reservation_id;
    public $booking_id;
    public $passenger_departure_code;
    public $passenger_arrival_code;
    public $passenger_departure_name;
    public $passenger_arrival_name;
    public $departure_date;
    public $departure_time;
    public $arrival_date;
    public $arrival_time;
    public $currency;
    public $cost;
    public $status;
    public $train_number;
    public $car_number;
    public $car_type;
    public $bedclothes;
    public $operation_type;
    public $train_speed;
    public $train_class;
    public $seats;
    public $travel_time;
    public $expiration_time;
    public $payment_date;

    /**
     * Аннулирование билетов
     *
     * @param string $orderId Номер заказа
     * @return array
     */
    public function Cancel($orderId)
    {
        /** @var Order $order */
        $order = Order::findOne([
            'id' => $orderId,
            //todo
            //'user_id' => CSession::_()->userId,
        ]);
        if ($order) {
            if ($order->canBeCancelled()) {
                $result = GdTickets::cancel($order->provider_order_id);
                if ($result['success']) {
                    $this->_response['data'] = $result['data'];
                } else {
                    $this->_response['code'] = $result['code'];
                    $this->_response['message'] = $result['msg'];
                }
            } else {
                $this->_response['code'] = -1;
                $this->_response['message'] = 'Не може буте скасоване';
            }
        } else {
            $this->_response['code'] = -1;
            $this->_response['message'] = 'Бронювання не знайдене';
        }


        return $this->_response;
    }
}
