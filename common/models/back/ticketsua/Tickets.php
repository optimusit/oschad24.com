<?php

namespace common\models\back\ticketsua;

use Yii;
use common\components\AppHelper;
use common\providers\tickets\GdTickets;
use yii\base\Exception;
use yii\base\Model;

class Tickets extends Model
{
    const GD = 'gd';
    const BUS = 'bus';
    const AVIA = 'avia';

    public $service;
    public $orderId;
    public $limit = 50;
    public $page;
    public $status;
    public $dateFrom;
    public $dateTo;
    public $reservationId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['service'],
                'in',
                'range' => $this->getServices(),
                'on' => 'default'
            ],
            [
                ['service'],
                'trim',
                'on' => 'default'
            ],
            [
                [
                    'limit',
                    'page',
                    'status',
                    'dateFrom',
                    'dateTo',
                ],
                'safe',
                //'on' => 'bookingslist',
            ],
            [
                'limit',
                'integer',
            ],
            [
                'limit',
                'default',
                'value' => 10,
            ],
            [
                'page',
                'integer',
            ],
            [
                'page',
                'default',
                'value' => 0,
            ],
            [
                'status',
                'in',
                'range' => $this->getServiceStatuses(),
            ],
            [
                [
                    'dateFrom',
                    'dateTo',
                ],
                'default',
                'value' => null,
            ],
            [
                ['reservationId'],
                'safe',
                'on' => 'default'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
//		if (isset($this->status)) {
//			$this->status = explode(',', $this->status);
//		}
        if (isset($this->page) && $this->page > 0) {
            $this->page--;
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
        if (($dateFrom = \DateTime::createFromFormat(AppHelper::GetDateFormat(), $this->dateFrom)) != false) {
            $this->dateFrom = $dateFrom;
        }
        if (($dateTo = \DateTime::createFromFormat(AppHelper::GetDateFormat(), $this->dateTo)) != false) {
            $this->dateTo = $dateTo;
        }
    }

    /**
     * Запрос получения списка бронирований
     *
     * @return array
     * @throws Exception
     */
    public function BookingsList()
    {
        /** @var Tickets $service */
        $serviceName = 'common\providers\tickets\\' . ucfirst(strtolower($this->service)) . 'Tickets';
        $service = new $serviceName;
        $result = $service->bookingsList($this->limit, $this->page, $this->status, $this->dateFrom, $this->dateTo);
        $list = [];
        $total = 0;
        if ($result['success'] == true) {
            $serviceName = 'common\models\back\ticketsua\\' . ucfirst(strtolower($this->service)) . 'Booking';
            $service = new $serviceName;
            $total = $result['data']['total'];
            foreach ($result['data']['items'] as $id => $booking) {
                $model = new $service;
                $model->setAttributes($booking, false);
                $list[] = $model;
            }
        }

        return [
            'items' => $list,
            'total' => $total,
        ];
    }

    /**
     * Запрос информации по одному бронированию
     *
     * @return RailBooking
     * @throws Exception
     */
    public function BookingShow()
    {
        $gdTickets = new GdTickets();
        $result = $gdTickets->bookingShow($this->reservationId);
        if ($result['success'] !== true) {
            throw new Exception();
        }
        $model = new RailBooking();
        $model->setAttributes($result['data'], false);

        return $model;
    }

    public function BookingRefund()
    {
        $gdTickets = new GdTickets();
        $result = $gdTickets->bookingShow($this->reservationId);
        if ($result['success'] !== true) {
            throw new Exception();
        }
        $booking = new RailBooking();
        $booking->setAttributes($result['data'], false);
        $ids = $booking->getDocumentsIds();
        $result = $gdTickets->getRefundAmount($this->reservationId, $ids);
        if ($result['success'] !== true) {
            throw new Exception();
        }
    }

    public function getServices()
    {
        return [
            self::GD,
            self::BUS,
            self::AVIA,
        ];
    }

    public function getServiceStatuses()
    {
        $service = 'common\models\back\ticketsua\\' . ucfirst(strtolower($this->service)) . 'Booking';
        $model = new $service;

        return array_keys($model->getStatuses());
    }
}
