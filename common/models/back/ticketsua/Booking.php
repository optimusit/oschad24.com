<?php

namespace common\models\back\ticketsua;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class Booking extends Model
{
    public $documents;

    public function getStatuses()
    {
        return $this::STATUSES;
    }

    public function getStatusName()
    {
        $statuses = $this::STATUSES;
        $result = 'Неизвестно';
        if (array_key_exists($this->status, $statuses)) {
            $result = $statuses[$this->status];
        }

        return $result;
    }

    public function getDocumentsIds()
    {
        return ArrayHelper::getColumn($this->documents, 'id');
    }

//	/**
//	 * Аннулирование билетов
//	 *
//	 * @param string $orderId Номер заказа
//	 * @return array
//	 */
//	public function Cancel($orderId)
//	{
//		/** @var Order $order */
//		$order = Order::findOne([
//			'id' => $orderId,
//			//todo
//			//'user_id' => CSession::_()->userId,
//		]);
//		if ($order) {
//			if ($order->canBeCancelled()) {
//				$result = GdTickets::cancel($order->provider_order_id);
//				if ($result['success']) {
//					$this->_response['data'] = $result['data'];
//				} else {
//					$this->_response['code'] = $result['code'];
//					$this->_response['message'] = $result['msg'];
//				}
//			} else {
//				$this->_response['code'] = -1;
//				$this->_response['message'] = 'Не може буте скасоване';
//			}
//		} else {
//			$this->_response['code'] = -1;
//			$this->_response['message'] = 'Бронювання не знайдене';
//		}
//
//
//		return $this->_response;
//	}
}
