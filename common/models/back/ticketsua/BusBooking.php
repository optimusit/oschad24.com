<?php

namespace common\models\back\ticketsua;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class BusBooking extends Booking
{
    const STATUSES = [
        '0' => 'неоплачено',
        '1' => 'ожидается оплата',
        '2' => 'оплачено',
        '3' => 'в процесе возврата',
        '4' => 'отменено пользователем',
        '5' => 'отменено системой',
        '6' => 'средства возвращены',
        '7' => 'запрос на возврат средств',
        '8' => 'отменено системой',
        '9' => 'оплачено после отмены',
    ];

    public $reservation_id;
    public $departure_address;
    public $server_address;
    public $price_without_reward;
    public $ticket_in_bus;
    public $carrier;
    public $arrival;
    public $bus_number;
    public $insurance;
    public $departure;
    public $transaction_commission;
    public $server_code;
    public $departure_date;
    public $arrival_date;
    public $status;
    public $created_at;
    public $updated_at;
    public $comment;
    public $payment_date;
    public $payment_system;
    public $transaction_total;
    public $amount;
    public $name;
    public $base;
    public $uid;
}
