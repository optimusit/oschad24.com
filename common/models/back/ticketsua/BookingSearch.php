<?php

namespace common\models\back\ticketsua;

use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

class BookingSearch extends Tickets
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $result = array_merge(parent::rules(), []);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ArrayDataProvider
     * @throws Exception
     */
    public function search($params)
    {
        $this->service = $params['service'];
        $this->page = array_key_exists('page', $params)
            ? $params['page']
            : 0;
        if (array_key_exists('BookingSearch', $params)) {
            $this->setAttributes($params['BookingSearch']);
        }
        if (!$this->validate()) {
            throw new Exception(implode(',', $this->firstErrors));
        }
        $data = $this->BookingsList();

        $dataProvider = new ArrayDataProvider([
            'pagination' => [
                'pageSize' => $this->limit,
                'totalCount' => $data['total'],
            ],
        ]);
        //$dataProvider->setKeys(ArrayHelper::getColumn($data['items'], 'reservation_id'));
        $dataProvider->setModels($data['items']);
        $dataProvider->setTotalCount($data['total']);

        return $dataProvider;
    }

    public function getModel()
    {
        $model = 'common\models\back\ticketsua\\' . ucfirst(strtolower($this->service)) . 'Booking';

        return new $model;
    }

    public function getDateFilter()
    {
        $result = DatePicker::widget([
                'model' => $this,
                'attribute' => 'dateFrom',
            ]) . DatePicker::widget([
                'model' => $this,
                'attribute' => 'dateTo',
            ]);

        return $result;
    }
}
