<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%shakhtar}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_start
 * @property string $date_end
 * @property integer $limit
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Shakhtar extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shakhtar}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'date_start',
                    'date_end',
                    'limit',
                ],
                'required'
            ],
            [
                [
                    'id',
                    'limit',
                    'active',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'date_start',
                    'date_end'
                ],
                'safe'
            ],
            [
                ['name'],
                'string',
                'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id матчу'),
            'name' => Yii::t('app', 'Назва матчу'),
            'date_start' => Yii::t('app', 'Час початку'),
            'date_end' => Yii::t('app', 'Час закінчення'),
            'limit' => Yii::t('app', 'Ліміт на продаж в 1 руки'),
            'active' => Yii::t('app', 'Активний'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }
}
