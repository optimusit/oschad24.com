<?php
namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "{{%partner_service}}".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property integer $service_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Service $service
 * @property Partner $partner
 */
class PartnerService extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'partner_id',
                    'service_id',
                ],
                'required'
            ],
            [
                [
                    'partner_id',
                    'service_id',
                ],
                'integer'
            ],
            [
                [
                    'partner_id',
                    'service_id'
                ],
                'unique',
                'targetAttribute' => [
                    'partner_id',
                    'service_id'
                ],
                'message' => 'The combination of Партнер and Сервіс has already been taken.'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'partner_id' => Yii::t('app', 'Партнер'),
            'service_id' => Yii::t('app', 'Сервіс'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }
}
