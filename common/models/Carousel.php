<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%carousel}}".
 *
 * @property integer $id
 * @property integer $service_id
 * @property string $name
 * @property string $url
 * @property string $image
 * @property string $image_file
 * @property string $image_file_name
 * @property string $show_begin
 * @property string $show_end
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Service $service
 */
class Carousel extends ActiveRecord
{
    const FOLDER = 'carousel';

    /**
     * @var UploadedFile
     */
    public $image_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%carousel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'service_id',
                    'name',
                    'url',
                    'image_file',
                ],
                'required',
                'on' => 'create'
            ],
            [
                [
                    'service_id',
                    'name',
                    'url',
                    'image_file',
                ],
                'safe',
                'on' => 'update'
            ],
            [
                [
                    'service_id',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'show_begin',
                    'show_end'
                ],
                'safe'
            ],
            [
                ['name'],
                'string',
                'max' => 100
            ],
            [
                ['image_file'],
                'image',
                'extensions' => 'png,jpg',
                'maxSize' => 1024 * 1024 * 3,
            ],
            [
                [
                    'url',
                    'image'
                ],
                'string',
                'max' => 2000
            ],
            [
                [
                    'url',
                ],
                'url',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service_id' => Yii::t('app', 'Сервіс'),
            'name' => Yii::t('app', 'Назва'),
            'url' => Yii::t('app', 'URL'),
            'image' => Yii::t('app', 'URL Зображення'),
            'image_file' => Yii::t('app', 'Зображення'),
            'image_file_name' => Yii::t('app', 'І`мя файла'),
            'show_begin' => Yii::t('app', 'Час початку показу'),
            'show_end' => Yii::t('app', 'Час припинення показу'),
            'created_at' => Yii::t('app', 'Створено'),
            'updated_at' => Yii::t('app', 'Змінено'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->image_file instanceof UploadedFile) {
                $this->deleteFile();
                $fileName = time() . '.' . $this->image_file->extension;
                $this->image_file_name = $fileName;
                $this->image = Yii::$app->urlManagerAPI->createAbsoluteUrl(Yii::$app->assetManagerAPI->getPublishedUrl(Yii::getAlias('@apiAssets'))
                    . '/' . self::FOLDER . '/' . $fileName);
                $this->image_file->saveAs($this->getStoragePath() . $fileName);
            }
            if ($insert) {

                return true;
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->clearCache();
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $this->clearCache();
        parent::afterDelete();
    }

    protected function deleteFile()
    {
        if (!is_null($this->image_file_name)) {
            @unlink($this->getStoragePath() . $this->image_file_name);
        }
    }

    protected function clearCache()
    {
        $cacheKey = 'carousel.' . $this->getService()
                ->one()->id;
        Yii::$app->cache->delete($cacheKey);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    public static function getData($serviceId)
    {
        $cacheKey = 'carousel.' . $serviceId;
        $result = Yii::$app->cache->get($cacheKey);
        if ($result === false) {
            $result = self::find()
                ->select('name,url,image')
                ->where(['service_id' => $serviceId])
                ->andWhere('(show_begin <= now() and show_end >= now()) or (show_begin <= now() and show_end is null) or (show_begin is null and show_end >= now()) or (show_begin is null and show_end is null)')
                ->asArray()
                ->all();
            Yii::$app->cache->set($cacheKey, $result, 86400);
        }

        return $result;
    }

    protected function getStoragePath()
    {
        return Yii::getAlias('@apiAssets') . self::FOLDER . '/';
    }
}
