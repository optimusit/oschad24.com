<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;

class TurboSMS extends Component
{
    public $url;
    public $sender;
    public $login;
    public $password;
    public $dryRun = true;

    private $_soapClient;
    private $_authorized = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->auth();
    }

    private function auth()
    {
        try {
            $this->_soapClient = new \SoapClient($this->url);
            $result = $this->_soapClient->Auth([
                'login' => $this->login,
                'password' => $this->password
            ]);
            if ($result->AuthResult == 'Вы успешно авторизировались') {
                $this->_authorized = true;
            } else {
                Yii::error("SMS: {$result->AuthResult}", 'user_notification_sms');
            }
        } catch (Exception $e) {
            Yii::error("SMS: " . $e->getMessage(), 'user_notification_sms');
        }
    }

    public function getBalance()
    {
        if ($this->_soapClient instanceof \SoapClient && $this->_authorized) {
            try {
                $result = $this->_soapClient->GetCreditBalance();
                if (isset($result->GetCreditBalanceResult) && is_numeric($result->GetCreditBalanceResult)) {
                    return floatval($result->GetCreditBalanceResult);
                } else {
                    Yii::error("Get SMS Balance : " . (isset($result->GetCreditBalanceResult)
                            ? $result->GetCreditBalanceResult
                            : var_export($result, true)), 'user_notification_sms');
                }
            } catch (Exception $e) {
                Yii::error("Get SMS Balance : " . $e->getMessage(), 'user_notification_sms');
            }
        }

        return false;
    }

    /**
     * @param $phone
     * @param $text
     * @return bool
     */
    public function send($phone, $text)
    {
        Yii::info("$phone $text", 'user_notification_sms');
        if ($this->dryRun) {

            return true;
        }
        if ($this->_soapClient instanceof \SoapClient && $this->_authorized) {
            try {
                $sms = [
                    'sender' => $this->sender,
                    'destination' => $phone,
                    'text' => $text
                ];
                $result = $this->_soapClient->SendSMS($sms);
                if (is_array($result->SendSMSResult->ResultArray)) {
                    $result = $result->SendSMSResult->ResultArray[0];
                } else {
                    $result = $result->SendSMSResult->ResultArray;
                }

                if ($result == 'Сообщения успешно отправлены') {
                    return true;
                } else {
                    Yii::error("$phone $text $result", 'user_notification_sms');
                }
            } catch (Exception $e) {
                Yii::error("$phone $text " . $e->getMessage(), 'user_notification_sms');
            }

            return false;
        }

        return false;
    }
}
