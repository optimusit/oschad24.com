<?php
namespace common\components;

use Yii;
use common\models\Service;
use common\models\Order;
use yii\base\Component;

class Core extends Component
{
    public function getDashBoardData()
    {
        $salesByType = Order::SalesByType();
        $data = [
            'smsBalance' => unserialize(Yii::$app->keyStorage->get('turbosmsBalance')),
            'stuckSMS' => Yii::$app->sms->getStuckMessages(),
            'enqueuedSMS' => Yii::$app->sms->getEnqueuedMessages(),
            'ticketsBalance' => unserialize(Yii::$app->keyStorage->get('ticketsBalance')),
            'salesByType' => $salesByType,
        ];

        return $data;
    }

    public function getEmailNoReply()
    {
        return Yii::$app->params['email']['noreply'];
    }

    /**
     * Уведомление покупателя об успешном бронировании перед оплатой
     *
     * @param Order $order
     */
    public function reservationCreated($order)
    {
        $smsPattern = 'oschad24.com Broniuvannia zamovlennia {orderId}, {serviceName} na sumu {amount} grn. - USPISHNO. Ochikuietsia oplata do {expirationTime} Sluzhba pidtrymky {phoneSupport}';
        $this->sendSMS($order, $smsPattern);
        $this->send($order, ['html' => 'reservationCreated-html']);
    }

    /**
     * Уведомление покупателя о истекшем заказе
     *
     * @param Order $order
     */
    public function reservationExpired($order)
    {
        $smsPattern = 'oschad24.com Broniuvannia zamovlennia {orderId}, {serviceName} na sumu {amount} grn. - PROSTROCHENO. Sluzhba pidtrymky {phoneSupport}';
        $this->sendSMS($order, $smsPattern);
        $this->send($order, ['html' => 'reservationExpired-html']);
    }

    /**
     * Уведомление покупателя об успешной оплате
     *
     * @param Order $order
     */
    public function reservationPaymentSuccess($order)
    {
        $smsPattern = 'oschad24.com Oplata zamovlennia {orderId}, {serviceName} na sumu {amount} grn. - USPISHNO. Sluzhba pidtrymky {phoneSupport}';
        $this->sendSMS($order, $smsPattern);
        $this->send($order, ['html' => 'reservationPaymentSuccess-html']);
    }

    /**
     * Уведомление покупателя о неуспешной оплате
     *
     * @param Order $order
     */
    public function reservationPaymentError($order)
    {
        $smsPattern = 'oschad24.com Oplata zamovlennya {orderId}, {serviceName} na sumu {amount} grn. - POMYLKA. Sluzhba pidtrymky {phoneSupport}';
        $this->sendSMS($order, $smsPattern);
        $this->send($order, ['html' => 'reservationPaymentError-html']);
    }

    /**
     * Отправка документа покупателю
     *
     * @param Order $order
     */
    public function sendDocumentsToCustomer($order)
    {
        $files = $order->getDocumentFileName();
        //$smsPattern = 'oschad24.com Oplata zamovlennya {orderId}, {serviceName} na sumu {amount} grn. - POMYLKA. Sluzhba pidtrymky {phoneSupport}';
        //$this->sendSMS($order, $smsPattern);
        $this->send($order, ['html' => 'sendDocumentsToCustomer-html'], $files);
    }

    /**
     * @param Order $order
     * @param $pattern
     */
    public function sendSMS($order, $pattern)
    {
        if ($order->phone) {
            /** @var Service $service */
            $service = $order->getService()
                ->one();
            $msg = Yii::t('tickets', $pattern, [
                'orderId' => $order->id,
                'serviceName' => $service->short_name_translit,
                'amount' => sprintf('%.2f', $order->amount),
                'expirationTime' => $order->expiration_time,
                'phoneSupport' => AppHelper::GetSupportPhones()[0],
            ]);
            Yii::$app->sms->enqueue($order->phone, $msg);
        }
    }

    /**
     * @param Order $order
     * @param $pattern
     * @param null $attaches
     */
    protected function send($order, $pattern, $attaches = null)
    {
        if (is_null($order->email)) {
            return;
        }
        $custom = unserialize($order->custom);
        $subject = $order->getMailSubject();
        /** @var Service $service */
        $service = $order->getService()
            ->one();
        $params = [
            'order' => $order,
            'service' => $service,
            'custom' => $custom
        ];
        $patternNew = [];
        foreach ($pattern as $key => $value) {
            $patternNew[$key] = $order->language . DIRECTORY_SEPARATOR . $value;
        }
        Yii::$app->params['.']['logo'] = Yii::getAlias('@common/mail/layouts/images/logo.png');
        $this->SendMail($order->email, $subject, $patternNew, $params, $attaches);
    }

    public function SendMail($to, $subject, $pattern, $params, $attaches = null)
    {
        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $message = $mailer->compose($pattern, ['params' => $params])
            ->setFrom($this->getEmailNoReply())
            ->setTo($to)
            ->setSubject($subject);
        if (is_array($attaches)) {
            foreach ($attaches as $attach) {
                $message->attach($attach);
            }
        }
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer->getSwiftMailer()
            ->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
        Yii::info($message->toString(), 'user_notification_email');
        try {
            if (!$message->send()) {
                Yii::error($logger->dump(), 'user_notification_email');
            }
        } catch (\Exception $e) {
            Yii::error($e, 'user_notification_email');
        }
    }
}
