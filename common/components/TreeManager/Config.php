<?php
namespace common\components\TreeManager;

use Yii;
use kartik\tree\Module;

class Config extends \kartik\base\Config
{
    /**
     * Gets the module
     *
     * @param string $m the module name
     *
     * @return Module
     */
    public static function getModule($m)
    {
        $mod = is_null(Yii::$app->controller)
            ? null
            : Yii::$app->controller->module;

        return $mod && $mod->getModule($m)
            ? $mod->getModule($m)
            : Yii::$app->getModule($m);
    }
}
