<?php
namespace common\components\TreeManager;

use kartik\tree\Module;

class TreeView extends \kartik\tree\TreeView
{
    /**
     * Returns the tree view module
     *
     * @return Module
     */
    public static function module()
    {
        return Config::getModule(Module::MODULE);
    }
}
