<?php
namespace common\components\TreeManager;

use creocoder\nestedsets\NestedSetsBehavior;

class Tree extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $module = TreeView::module();
        $settings = ['class' => NestedSetsBehavior::className()] + $module->treeStructure;

        return [$settings];
    }
}
