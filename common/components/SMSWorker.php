<?php
namespace common\components;

use Yii;
use common\models\SmsQueue;
use yii\base\Component;
use yii\base\Exception;

class SMSWorker extends Component
{
    public $maxNumberTries;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (intval($this->maxNumberTries) == 0 || intval($this->maxNumberTries) < 50) {

        }
    }

    public function enqueue($phone, $text)
    {
        $hash = md5($phone . $text);
        $model = new SmsQueue();
        $model->attributes = [
            'phone' => $phone,
            'text' => $text,
            'hash' => $hash,
            'number_tries' => 0,
        ];
        try {
            $model->save();
        } catch (Exception $e) {
        }
    }

    public function runDelivery()
    {
        /** @var SmsQueue $sms */
        $queue = SmsQueue::find()
            ->where([
                '<',
                'number_tries',
                $this->maxNumberTries
            ])
            ->orderBy('created_at desc')
            ->all();
        foreach ($queue as $sms) {
            if (Yii::$app->smsProvider->send($sms->phone, $sms->text)) {
                $sms->delete();
            } else {
                $sms->number_tries++;
                $sms->update();
            }
        }
    }

    public function getEnqueuedMessages()
    {
        $count = SmsQueue::find()
            ->where([
                '<',
                'number_tries',
                $this->maxNumberTries
            ])
            ->count();

        return $count;
    }

    public function getStuckMessages()
    {
        $count = SmsQueue::find()
            ->where([
                '>=',
                'number_tries',
                $this->maxNumberTries
            ])
            ->count();

        return $count;
    }
}
