<?php
namespace common\components\log;

use Yii;
use yii\web\Request;

class DbTarget extends \yii\log\DbTarget
{
    /**
     * @inheritdoc
     */
    public function getMessagePrefix($message)
    {
        if ($this->prefix !== null) {
            return call_user_func($this->prefix, $message);
        }

        if (Yii::$app === null) {
            return '';
        }

        $request = Yii::$app->getRequest();
        $ip = $request instanceof Request
            ? $request->getUserIP()
            : '-';

        /* @var $user \yii\web\User */
        $user = Yii::$app->has('user', true)
            ? Yii::$app->get('user')
            : null;
        if ($user && ($identity = $user->getIdentity(false))) {
            $userID = $identity->getId();
        } else {
            $userID = '-';
        }

        /* @var $session \yii\web\Session */
        $session = Yii::$app->has('session', true)
            ? Yii::$app->get('session')
            : null;
        $session = Yii::$app->has('session', true)
            ? Yii::$app->get('session')
            : null;
        $sessionID = $session
            ? $session->getId()
            : '-';

        return "[$ip][$userID][$sessionID]";
    }
}
