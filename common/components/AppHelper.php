<?php
namespace common\components;

use Yii;

class AppHelper
{
    static function recognizeLanguage($value)
    {
        if (preg_match('/^[ ,.a-z-]*$/i', $value)) {
            return 'en';
        } elseif (preg_match('/^[^іїґє]*$/i', $value)) {
            return 'ru';
        } else {
            return 'uk';
        }
    }

    static function reverseTransliteration($value)
    {
        if (self::recognizeLanguage($value) == 'en') {
            $trans = [];
            $trans['ru'] = [
                'q' => 'й',
                'w' => 'ц',
                'e' => 'у',
                'r' => 'к',
                't' => 'е',
                'y' => 'н',
                'u' => 'г',
                'i' => 'ш',
                'o' => 'щ',
                'p' => 'з',
                '[' => 'х',
                ']' => 'ъ',
                'a' => 'ф',
                's' => 'ы',
                'd' => 'в',
                'f' => 'а',
                'g' => 'п',
                'h' => 'р',
                'j' => 'о',
                'k' => 'л',
                'l' => 'д',
                ';' => 'ж',
                '\'' => 'э',
                'z' => 'я',
                'x' => 'ч',
                'c' => 'с',
                'v' => 'м',
                'b' => 'и',
                'n' => 'т',
                'm' => 'ь',
                ',' => 'б',
                '.' => 'ю',
            ];
            $trans['uk'] = [
                'q' => 'й',
                'w' => 'ц',
                'e' => 'у',
                'r' => 'к',
                't' => 'е',
                'y' => 'н',
                'u' => 'г',
                'i' => 'ш',
                'o' => 'щ',
                'p' => 'з',
                '[' => 'х',
                ']' => 'ї',
                'a' => 'ф',
                's' => 'і',
                'd' => 'в',
                'f' => 'а',
                'g' => 'п',
                'h' => 'р',
                'j' => 'о',
                'k' => 'л',
                'l' => 'д',
                ';' => 'ж',
                '\'' => 'є',
                'z' => 'я',
                'x' => 'ч',
                'c' => 'с',
                'v' => 'м',
                'b' => 'и',
                'n' => 'т',
                'm' => 'ь',
                ',' => 'б',
                '.' => 'ю',
            ];
            $lang = Yii::$app->language;
            if (isset($trans[$lang])) {
                $patterns = [];
                $replacements = [];
                foreach ($trans[$lang] as $s1 => $s2) {
                    $patterns[] = '/' . preg_quote($s1) . '/iu';
                    $replacements[] = $s2;
                }
                $value = preg_replace($patterns, $replacements, $value);
            }
        }

        return $value;
    }

    static function transliteration($value)
    {
        $sourceLanguage = self::recognizeLanguage($value);
        if ($sourceLanguage == 'uk') {
            $trans = [
                'а' => 'a',
                'б' => 'b',
                'в' => 'v',
                'г' => 'h',
                'ґ' => 'g',
                'д' => 'd',
                'е' => 'e',
                'є' => 'ie',
                'ж' => 'zh',
                'з' => 'z',
                'и' => 'y',
                'і' => 'i',
                'ї' => 'i',
                'й' => 'i',
                'к' => 'k',
                'л' => 'l',
                'м' => 'm',
                'н' => 'n',
                'о' => 'o',
                'п' => 'p',
                'р' => 'r',
                'с' => 's',
                'т' => 't',
                'у' => 'u',
                'ф' => 'f',
                'х' => 'kh',
                'ц' => 'ts',
                'ч' => 'ch',
                'ш' => 'sh',
                'щ' => 'shch',
                'ь' => '',
                'ю' => 'iu',
                'я' => 'ya',

                'А' => 'A',
                'Б' => 'B',
                'В' => 'V',
                'Г' => 'H',
                'Ґ' => 'G',
                'Д' => 'D',
                'Е' => 'E',
                'Є' => 'IE',
                'Ж' => 'ZH',
                'З' => 'Z',
                'И' => 'Y',
                'І' => 'I',
                'Ї' => 'I',
                'Й' => 'I',
                'К' => 'K',
                'Л' => 'L',
                'М' => 'M',
                'Н' => 'N',
                'О' => 'O',
                'П' => 'P',
                'Р' => 'R',
                'С' => 'S',
                'Т' => 'T',
                'У' => 'U',
                'Ф' => 'F',
                'Х' => 'KH',
                'Ц' => 'TS',
                'Ч' => 'CH',
                'Ш' => 'SH',
                'Щ' => 'SHCH',
                'Ь' => '',
                'Ю' => 'IU',
                'Я' => 'YA',
            ];
            $value = strtr($value, $trans);
        } elseif ($sourceLanguage == 'ru') {
            $trans = [
                'а' => 'a',
                'б' => 'b',
                'в' => 'v',
                'г' => 'g',
                'д' => 'd',
                'е' => 'e',
                'ё' => 'e',
                'ж' => 'zh',
                'з' => 'z',
                'и' => 'i',
                'й' => 'y',
                'к' => 'k',
                'л' => 'l',
                'м' => 'm',
                'н' => 'n',
                'о' => 'o',
                'п' => 'p',
                'р' => 'r',
                'с' => 's',
                'т' => 't',
                'у' => 'u',
                'ф' => 'f',
                'х' => 'h',
                'ц' => 'c',
                'ч' => 'ch',
                'ш' => 'sh',
                'щ' => 'sch',
                'ь' => '\'',
                'ы' => 'y',
                'ъ' => '\'',
                'э' => 'e',
                'ю' => 'yu',
                'я' => 'ya',

                'А' => 'A',
                'Б' => 'B',
                'В' => 'V',
                'Г' => 'G',
                'Д' => 'D',
                'Е' => 'E',
                'Ё' => 'E',
                'Ж' => 'Zh',
                'З' => 'Z',
                'И' => 'I',
                'Й' => 'Y',
                'К' => 'K',
                'Л' => 'L',
                'М' => 'M',
                'Н' => 'N',
                'О' => 'O',
                'П' => 'P',
                'Р' => 'R',
                'С' => 'S',
                'Т' => 'T',
                'У' => 'U',
                'Ф' => 'F',
                'Х' => 'H',
                'Ц' => 'C',
                'Ч' => 'Ch',
                'Ш' => 'Sh',
                'Щ' => 'Sch',
                'Ь' => '\'',
                'Ы' => 'Y',
                'Ъ' => '\'',
                'Э' => 'E',
                'Ю' => 'Yu',
                'Я' => 'Ya',
            ];
            $value = strtr($value, $trans);
        }

        return $value;
    }

    static function mb_ucfirst($str, $enc = 'utf-8')
    {
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc) . mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }

    static function currency($value)
    {
        return Yii::$app->formatter->asCurrency($value);
    }

    static function hex($string)
    {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $hex .= dechex(ord($string[$i]));
        }

        return strtoupper($hex);
    }

    static function gen($length)
    {
        $string = "";
        $possible = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        $maxlength = strlen($possible);
        if ($length > $maxlength) {
            $length = $maxlength;
        }
        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
            if (!strstr($string, $char)) {
                $string .= $char;
                $i++;
            }
        }

        return $string;
    }

//	static function isMobilePhoneValid($phone)
//	{
//		return preg_match('/\+[0-9]{12}$/', trim($phone));
//	}

    static function isEmailValid($email)
    {
        return preg_match('/(.*<?)(.*)@(.*?)(>?)/', trim($email));
    }

    static function getDateTimeNowMidNight()
    {
        return (new \DateTime())->setTimestamp(strtotime('today midnight'));
    }

    /**
     * Создает дату-время из строки, используя общепринятый в системе формат. Используется для входных параметров.
     *
     * @param $string
     * @return \DateTime
     */
    static function CreateDateTimeFromString($string)
    {
        return \DateTime::createFromFormat(str_replace('php:', '', Yii::$app->formatter->dateFormat) . ' H:i',
            $string . ' 00:00');
    }

    static function CreateDateNow()
    {
        return \DateTime::createFromFormat('Y-m-d H:i:s', (new \DateTime())->format('Y-m-d') . ' 00:00:00');
    }

    static function CreateDateTimeFromMySQL($string)
    {
        return \DateTime::createFromFormat('Y-m-d H:i:s', $string);
    }

    static function GetDateFormat()
    {
        return str_replace('php:', '', Yii::$app->formatter->dateFormat);
    }

    static function GetTimeFormat()
    {
        return str_replace('php:', '', Yii::$app->formatter->timeFormat);
    }

    static function GetDateTimeFormat()
    {
        return str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
    }

    static function GetTimeDateFormat()
    {
        return 'php:H:i d.m.Y';
    }

    static function GetDayOfWeek(\DateTime $date)
    {
        return Yii::$app->formatter->asDate($date->getTimestamp(), 'php:l');
    }

    static function GetDayOfWeekShort(\DateTime $date)
    {
        return Yii::$app->formatter->asDate($date->getTimestamp(), 'php:D');
    }

    static function GetMonth(\DateTime $date)
    {
        return Yii::$app->formatter->asDate($date->getTimestamp(), 'php:F');
    }

    static function FormatCurrency($value)
    {
        return Yii::$app->formatter->asDecimal($value, 2);
    }

    static function DateTimeForMySQL(\DateTime $datetime)
    {
        return $datetime->format('Y-m-d H:i');
    }

    static function GetSupportPhones()
    {
        return Yii::$app->params['phones']['support'];
    }

    static function Array2xls(array &$array)
    {
        $xls = new \PHPExcel();
        $sheet = $xls->getActiveSheet();
        $rc = 1;
        foreach ($array as $row) {
            $cc = 0;
            foreach ($row as $col) {
                $sheet->setCellValueByColumnAndRow($cc, $rc, $col);
                $cc++;
            }
            $rc++;
        }
        $writer = \PHPExcel_IOFactory::createWriter($xls, 'Excel5');
        ob_start();
        $writer->save('php://output');

        return ob_get_clean();
    }
}
