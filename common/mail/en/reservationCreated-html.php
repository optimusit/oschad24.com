<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var \common\models\Order $order */
$order = $params['order'];
?>
<div>
    <p>Hello, <?= Html::encode($order->getCustomerFullName()) ?>,</p>

    <p>Your order №<?= $order->id ?> successfully booked.</p>

    <p>Order details:</p>

    <p><?= Html::encode($order->getServiceName()) ?></p>

    <p>Amount: <?= $order->amount ?></p>
</div>
