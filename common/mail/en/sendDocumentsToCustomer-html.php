<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var \common\models\Order $order */
$order = $params['order'];
?>
<div>
    <p>Hello, <?= Html::encode($order->getCustomerFullName()) ?>,</p>

    <p>Your order №<?= $order->id ?> successful.</p>

    <p>Ticket attached to the message as a separate file. Please download and print it yourself.</p>
</div>
