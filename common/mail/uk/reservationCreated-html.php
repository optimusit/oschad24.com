<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var \common\models\Service $service */
/* @var \common\models\Order $order */
$order = $params['order'];
$service = $params['service'];
?>
<div>
    <p>Доброго дня, <?= Html::encode($order->getCustomerFullName()) ?>.</p>

    <p><b>Ваше замовлення №<?= $order->id ?> успішно заброньовано. Термін оплати
            до <?= Yii::$app->formatter->asDatetime($order->expiration_time) ?></b></p>

    <p>Деталі замовлення:</p>

    <p><?= $this->render('_' . $service->provider_code . 'Details', ['custom' => $params['custom']]) ?></p>

    <p>Сума до cплати: <?= $order->getAmount() ?></p>
</div>
