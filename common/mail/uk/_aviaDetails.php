<?php
use api\modules\v1\Module;

$orderInfo = $custom['orderInfo'];
$form = $custom['form'];
?>
<div><?= Module::t('app', 'Авіаквитки') ?></div>
<div><?= Module::t('app', 'Маршрут') ?></div>
<table>
    <?php foreach ($form['aircraft']['parts'] as $part): ?>
        <tr>
            <td><?= $part['departure_city_name'] ?> (<?= $part['departure_airport_name'] ?>)</td>
            <td><?= $part['arrival_city_name'] ?> (<?= $part['arrival_airport_name'] ?>)</td>
        </tr>
        <tr>
            <td><?= $part['departure_date'] ?> <?= $part['departure_time'] ?></td>
            <td><?= $part['arrival_date'] ?> <?= $part['arrival_time'] ?></td>
        </tr>
        <tr>
            <td colspan="2">Пересадки</td>
        </tr>
        <?php foreach ($part['segments'] as $segment): ?>
            <tr>
                <td><?= $segment['departure_country_name'] ?>, <?= $segment['departure_city_name'] ?>
                    (<?= $segment['departure_airport_name'] ?>)
                </td>
                <td><?= $segment['arrival_country_name'] ?>, <?= $segment['arrival_city_name'] ?>
                    (<?= $segment['arrival_airport_name'] ?>)
                </td>
            </tr>
            <tr>
                <td><?= $segment['departure_date'] ?> <?= $segment['departure_time'] ?></td>
                <td><?= $segment['arrival_date'] ?> <?= $segment['arrival_time'] ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="2"></td>
        </tr>
    <?php endforeach; ?>
</table>
<br>
<div><?= Module::t('app', 'Документ(и)') ?></div>
<table>
    <thead>
    <tr>
        <td><?= Module::t('app', 'Прізвище та ім’я') ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orderInfo['passengers'] as $passenger): ?>
        <tr>
            <td><?= $passenger->getFullName() ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
