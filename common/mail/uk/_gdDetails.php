<?php
use api\modules\v1\Module;

$orderInfo = $custom['orderInfo'];
$form = $custom['form'];
?>
<div><?= Module::t('app', 'Поїзд') ?></div>
<div><?= $orderInfo['train'] ?> <?= $orderInfo['voyage'] ?></div>
<div><?= Module::t('app', 'Маршрут') ?></div>
<div>
    <div><?= Module::t('app',
            'Відправлення') ?> <?= $orderInfo['departure']['name'] ?> <?= $orderInfo['departure']['date_time'] ?></div>
    <div><?= Module::t('app',
            'Прибуття') ?> <?= $orderInfo['arrival']['name'] ?> <?= $orderInfo['arrival']['date_time'] ?></div>
    <div><?= Module::t('app', 'В дорозі') ?> <?= $orderInfo['travel_time'] ?></div>
</div>
<div><?= Module::t('app', 'Документ(и)') ?></div>
<table>
    <thead>
    <tr>
        <td><?= Module::t('app', 'Прізвище та ім’я') ?></td>
        <td><?= Module::t('app', 'Вагон') ?></td>
        <td><?= Module::t('app', 'Місце') ?></td>
        <td><?= Module::t('app', 'Тип пассажира') ?></td>
        <td><?= Module::t('app', 'Постільна білизна') ?></td>
        <td><?= Module::t('app', 'Додаткові послуги') ?></td>
        <td><?= Module::t('app', 'Додатковий багаж') ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orderInfo['passengers'] as $passenger): ?>
        <tr>
            <td><?= $passenger['passenger'] ?></td>
            <td><?= $passenger['car'] ?> (<?= $passenger['train_class_label'] ?>)</td>
            <td><?= $passenger['seat'] ?> (<?= $passenger['class'] ?>)</td>
            <td><?= $passenger['type'] ?></td>
            <td><?= Yii::$app->formatter->asBoolean(!$form['no_clothes']) ?></td>
            <td><?= count($passenger['services']) > 0
                    ? implode(', ', $passenger['services'])
                    : Module::t('app', 'Відсутні') ?></td>
            <td><?= count($passenger['luggage']) > 0
                    ? implode(', ', $passenger['luggage'])
                    : Module::t('app', 'Відсутні') ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
