<?php
use api\modules\v1\Module;

$orderInfo = $custom['orderInfo'];
$form = $custom['form'];
?>
<div><?= Module::t('app', 'Автобус') ?></div>
<div><?= $orderInfo['number'] ?> <?= $orderInfo['voyage'] ?></div>
<div><?= Module::t('app', 'Маршрут') ?></div>
<div>
    <div><?= Module::t('app',
            'Відправлення') ?> <?= $orderInfo['departure']['name'] ?> <?= $orderInfo['departure']['date_time'] ?></div>
    <div><?= Module::t('app',
            'Прибуття') ?> <?= $orderInfo['arrival']['name'] ?> <?= $orderInfo['arrival']['date_time'] ?></div>
</div>
<div><?= Module::t('app', 'Документ(и)') ?></div>
<table>
    <thead>
    <tr>
        <td><?= Module::t('app', 'Прізвище та ім’я') ?></td>
        <td><?= Module::t('app', 'Місце') ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orderInfo['passengers'] as $passenger): ?>
        <tr>
            <td><?= $passenger['passenger'] ?></td>
            <td><?= $passenger['seat'] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
