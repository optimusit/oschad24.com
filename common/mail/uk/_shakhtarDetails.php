<?php
use api\modules\v1\Module;

$orderInfo = $custom['orderInfo'];
$form = $custom['form'];
?>
<div><?= Module::t('app', 'Подія') ?></div>
<div><?= $orderInfo['event']['name'] ?></div>
<div><?= Module::t('app', 'Час') ?></div>
<div><?= $orderInfo['event']['date_start'] ?></div>
<div><?= Module::t('app', 'Документ(и)') ?></div>
<table>
    <thead>
    <tr>
        <td><?= Module::t('app', 'Зона') ?></td>
        <td><?= Module::t('app', 'Рівень') ?></td>
        <td><?= Module::t('app', 'Сектор') ?></td>
        <td><?= Module::t('app', 'Ряд') ?></td>
        <td><?= Module::t('app', 'Місце') ?></td>
        <td><?= Module::t('app', 'Вартість') ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orderInfo['reservation']['ReservedSeats']['ReservedSeat'] as $ticket): ?>
        <tr>
            <td><?= $orderInfo['sectorInfo']['Zone'] ?></td>
            <td><?= $orderInfo['sectorInfo']['Level'] ?></td>
            <td><?= $orderInfo['sectorInfo']['Name'] ?></td>
            <td><?= $ticket['RowText'] ?></td>
            <td><?= $ticket['SeatText'] ?></td>
            <td><?= $ticket['Price'] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
