<?php
use api\modules\v1\Module;

$orderInfo = $custom['orderInfo'];
$form = $custom['form'];
?>
<div><?= Module::t('app', 'Подія') ?></div>
<div><?= $orderInfo['event']['EventName'] ?></div>
<div><?= Module::t('app', 'Документ(и)') ?></div>
<table>
    <thead>
    <tr>
        <td><?= Module::t('app', 'Зона') ?></td>
        <td><?= Module::t('app', 'Ряд') ?></td>
        <td><?= Module::t('app', 'Місце') ?></td>
        <td><?= Module::t('app', 'Вартість') ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orderInfo['tickets'] as $ticket): ?>
        <tr>
            <td><?= $ticket['zone'] ?></td>
            <td><?= $ticket['row'] ?></td>
            <td><?= $ticket['number'] ?></td>
            <td><?= $ticket['amount'] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
