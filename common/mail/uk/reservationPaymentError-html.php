<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var \common\models\Order $order */
$order = $params['order'];
?>
<div>
    <p>Доброго дня, <?= Html::encode($order->getCustomerFullName()) ?>,</p>

    <p>Ваше замовлення №<?= $order->id ?> не оплачено.</p>

    <p>Виникла помилка <?= $order->getGatewayRequest()
            ->one()->rc ?>: <b><?= $order->getGatewayRequest()
                ->one()
                ->getName() ?></b></p>

    <p>Деталі замовленя:</p>

    <p><?= Html::encode($order->getServiceName()) ?></p>

    <p>Сума до cплати: <?= $order->getAmount() ?></p>
</div>
