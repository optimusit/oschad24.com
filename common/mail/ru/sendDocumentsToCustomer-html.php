<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var \common\models\Order $order */
$order = $params['order'];
?>
<div>
    <p>Здравствуйте, <?= Html::encode($order->getCustomerFullName()) ?>,</p>

    <p>Ваш заказ №<?= $order->id ?> успешно выполнен.</p>

    <p>Билет прикреплен к письму отдельным файлом. Пожалуйста, загрузите и распечатайте его самостоятельно.</p>
</div>
