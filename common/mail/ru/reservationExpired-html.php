<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var \common\models\Order $order */
$order = $params['order'];
?>
<div>
    <p>Здравствуйте, <?= Html::encode($order->getCustomerFullName()) ?>,</p>

    <p>Ваш заказ №<?= $order->id ?> Просрочено(?).</p>

    <p>Детали заказа:</p>

    <p><?= Html::encode($order->getServiceName()) ?></p>

    <p>Сумма: <?= $order->amount ?></p>
</div>
