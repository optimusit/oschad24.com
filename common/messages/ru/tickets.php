<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'oschad24.com Bronyuvannya zamovlennya {orderId}, {serviceName} na sumu {amount} grn. - PROSROCHENE. Telefon {phoneSupport}' => '',
    'oschad24.com Bronyuvannya zamovlennya {orderId}, {serviceName} na sumu {amount} grn. - USPISHNO. Ochukuyet`sya oplata do {expirationTime} Telefon {phoneSupport}' => '',
    'oschad24.com Oplata zamovlennya {orderId}, {serviceName} na sumu {amount} grn. - POMYLKA. Telefon {phoneSupport}' => '',
    'oschad24.com Oplata zamovlennya {orderId}, {serviceName} na sumu {amount} grn. - USPISHNO. Telefon {phoneSupport}' => '',
    '{price} грн' => '',
    'Ваучер' => '',
    'Взрослый' => '',
    'Детский' => '',
    'Детский (Младенец)' => '',
    'Експрес' => '',
    'Електронний квиток' => '',
    'Загальний' => '',
    'Замовлення' => '',
    'Запит не може бути виконаний із-за заборони на оформлення місць, встановленого Залізницею відправлення поїзда для заданого напряму на дану дату.' => '',
    'Купе' => '',
    'Купівля електронних квитків. Друкувати електронний квиток потрібно обов\'язково.' => '',
    'Купівля квитків з подальшим обміном на паперові в квитковій касі. Роздруковувати ваучер не обов\'язково, досить повідомити касиру номер квитка.' => '',
    'Люкс' => '',
    'М\'який' => '',
    'Не вибрані посадочні місця' => '',
    'Невірний ключ авторизації' => '',
    'Нефірмовий' => '',
    'Пасажирський' => '',
    'Плацкарт' => '',
    'Сесія скінчилась' => '',
    'Сидячий' => '',
    'Фірмовий' => '',
    'Швидкий' => '',
    'Швидкісний' => '',
];
