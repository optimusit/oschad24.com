<?php

namespace common\providers\UZ;

use \Yii;
//use api\modules\v1\components\ApiException;
use \SoapClient;
use \SoapFault;
use yii\base\Component;

class UZ extends Component
{
    public $url;
    public $privateKeyPath;
    public $privateKeyPassword = '';
    private $_privateKeyData;
    private $_privateKey;
    protected $_soapClient;
    protected $_options = [
//		'connection_timeout' => 10,
//		'exceptions' => 1,
//		'keep_alive' => 1,
//		'trace' => 1,
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //$this->_soapClient = new SoapClient($this->url, $this->_options);
        if (!file_exists($this->privateKeyPath)) {
            throw new \Exception('Приватний ключ не знайдено, шлях: ' . $this->privateKeyPath);
        }
        $fp = fopen($this->privateKeyPath, 'r');
        $this->_privateKeyData = fread($fp, 8192);
        fclose($fp);
        $this->_privateKey = openssl_pkey_get_private($this->_privateKeyData, $this->privateKeyPassword);

//		try {
//
//		} catch (\Exception $e) {
//			throw new \Exception($this->_errorCode);
//		}
//		if (!$this->getSessionId()) {
//			$this->login();
//		}
    }

    private function Sign($data)
    {

        openssl_sign($data, $signature, $pkeyid);
        openssl_free_key($pkeyid);

        return base64_encode($signature);
    }

    public function getState()
    {
        return $this->url;
    }
}
