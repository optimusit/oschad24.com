<?php

namespace common\providers\shakhtar;

use \Yii;
use api\modules\v1\components\ApiException;
use \SoapClient;
use \SoapFault;
use common\models\Shakhtar as ShakhtarModel;

class Shakhtar
{
    const PREFIX = 'SHAKHTAR';
    protected $_url;
    protected $_login;
    protected $_password;
    protected $_ns;
    private $_options = [
        'connection_timeout' => 10,
        'exceptions' => 1,
        'keep_alive' => 1,
        'trace' => 1
    ];
    protected $_soapClient;
    protected $_sessionId;
    protected $_sessionLifeTime = 15;
    protected $_errorCode = 500;

    public function __construct()
    {
        $this->_url = $this->getAPIUrl();
        $this->_login = $this->getLogin();
        $this->_password = $this->getPassword();
        $this->_ns = $this->getNs();
        try {
            $this->_soapClient = new SoapClient($this->_url, $this->_options);
        } catch (ApiException $e) {
            throw new ApiException($this->_errorCode);
        }
        if (!$this->getSessionId()) {
            $this->login();
        }
    }

    protected function getAPIUrl()
    {
        return Yii::$app->params['provider']['fk_shakhtar']['api_url'];
    }

    protected function getLogin()
    {
        return Yii::$app->params['provider']['fk_shakhtar']['login'];
    }

    protected function getPassword()
    {
        return Yii::$app->params['provider']['fk_shakhtar']['password'];
    }

    protected function getNs()
    {
        return Yii::$app->params['provider']['fk_shakhtar']['ns'];
    }

    protected function getSessionId()
    {
        if (Yii::$app->session['ShakhtarSession'] && time() < Yii::$app->session['ShakhtarSessionTimeOut']) {
            return Yii::$app->session['ShakhtarSession'];
        }

        return false;
    }

    protected function setSessionId($sessionId)
    {
        $this->_sessionId = $sessionId;
        Yii::$app->session['ShakhtarSession'] = $this->_sessionId;
        Yii::$app->session['ShakhtarSessionTimeOut'] = (new \DateTime())->add(new \DateInterval('PT'
            . $this->_sessionLifeTime . 'M'))
            ->getTimestamp();
    }

    protected function setSessionHeader()
    {
        $header = new \SoapHeader($this->_ns, 'SessionHeader', ['SessionId' => $this->getSessionId()]);
        $this->_soapClient->__setSoapHeaders($header);
    }

    protected function Login()
    {
        $result = false;
        try {
            $result = $this->_soapClient->Login([
                'login' => $this->_login,
                'password' => $this->_password
            ]);
        } catch (SoapFault $soapFault) {
            $error = $this->ExtractError($soapFault);
        }
        if ($result && $result->LoginResult) {
            $this->setSessionId($result->LoginResult);

            return true;
        }

        return false;
    }

    protected function Logout()
    {
        if (!empty($this->getSessionId())) {
            $this->setSessionId(null);
            $this->setSessionHeader();
            try {
                $this->_soapClient->Logout();
            } catch (SoapFault $soapFault) {
                $data = $this->ExtractError($soapFault);
            }
        }

        return true;
    }

    public function __call($name, $arguments)
    {
        $data = [
            'success' => false,
            'dialog' => null,
        ];
        if (in_array($name, [
            //'Events',
            'SectorsList',
            'GetSectorStructure',
            'SeatsAvailability',
            'Reservation',
            'ReservPayment',
            'SellReservation',
        ])) {
            $this->setSessionHeader();
            if (count($arguments) > 0) {
                $params = $arguments[0];
            } else {
                $params = null;
            }
            try {
                $result = $this->_soapClient->$name($params);
                $resultName = $name . 'Result';
                if ($result->{$resultName}) {
                    $data = [
                        'success' => true,
                        'data' => json_decode(json_encode($result->{$resultName}), true),
                    ];
                }
            } catch (SoapFault $soapFault) {
                $data = $this->ExtractError($soapFault);
            }
            $logItem = [
                'request' => [
                    'url' => $name,
                    'params' => $params
                ],
                'response' => json_encode($data),
                'response_format' => 'json',
            ];
            Yii::info($logItem, __METHOD__);
            $data['dialog'] = $logItem;
        } elseif (method_exists($this, $name)) {
            $this->{$name}($arguments);
        } else {
            throw new \Exception('Недопустимый метод');
        }

        return $data;
    }

    public function ClearReservation($reservationNumber)
    {
        $data = [];
        $this->setSessionHeader();
        try {
            $result = $this->_soapClient->CleareReservation([
                'reservationNumber' => $reservationNumber,
            ]);
            if ($result instanceof \stdClass) {
                $data = $result->CleareReservationResult;
            }
        } catch (SoapFault $soapFault) {
            $data = $this->ExtractError($soapFault);
        }

        return $data;
    }

    private function ExtractError(SoapFault $soapFault)
    {
        $data['success'] = false;
        $code = 100;
        $message = 'Невідома помилка';
        if (isset($soapFault->detail)) {
            $error = $soapFault->detail->Error;
            if ($error->ErrorCode == '3001') {
                $code = 100;
                $message = 'Сесія скінчилась';
            }
        } else {
            $message = $soapFault->getMessage();
        }
        $data['message'] = $message;
        $data['code'] = $code;

        return $data;
    }

    public function LoadMatches()
    {
        $data = [];
        $this->setSessionHeader();
        try {
            $result = $this->_soapClient->Events();
            if ($result->EventsResult) {
                Yii::info(var_export($result, true), __METHOD__);
                $this->SetMatches($result);
            }
        } catch (SoapFault $soapFault) {
            $data = $this->ExtractError($soapFault);
            Yii::error(var_export($data, true), __METHOD__);
        }

        return $data;
    }

    private function SetMatches($matches)
    {
        foreach ($matches->EventsResult->Event as $match) {
            if (!($shakhtar = ShakhtarModel::find()
                ->where(['id' => $match->EventId])
                ->one())
            ) {
                $shakhtar = new ShakhtarModel();
            }
            $shakhtar->id = $match->EventId;
            $shakhtar->name = $match->EventName;
            $shakhtar->date_start = $match->EventStart;
            $shakhtar->date_end = $match->EventEnd;
            $shakhtar->limit = $match->Limit;
            $shakhtar->active = ($match->IsActiv == true)
                ? 1
                : 0;
            if (!$shakhtar->save()) {
                Yii::error(var_export($shakhtar->errors, true), __METHOD__);
            }
        }
    }
}
