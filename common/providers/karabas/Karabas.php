<?php
namespace common\providers\karabas;

use Yii;
use common\models\karabas\ActivityTypes;
use common\models\karabas\Buildings;
use common\models\karabas\PartnerEventActivityType;
use common\models\karabas\PartnerEvents;
use common\models\karabas\PartnerEventStatistic;
use yii\base\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Karabas
{
    const PREFIX = 'KARABAS';
    const STATUS_CONFIRM_RESERVATION = 0;
    const STATUS_CONFIRM_PAYMENT = 1;
    const STATUS_CANCEL_ORDER = 2;
    const STATUS_SEND_DELIVERY = 3;

    protected $_apiUrl = null;
    protected $_sessionId = null;
    protected $_instance;
    protected $_authKey = null;
    protected $_dictionaries = [
        'ActivityTypes',
        'Countries',
        'BuildingTypes',
        'DeliveryTypes',
        'PaymentTypes',
        'Cities',
        'Buildings',
        'Halls',
    ];
    protected $_images = [
        'ActivityImageLarge',
        'ActivityImageBig',
        'ActivityImageMedium',
        'ActivityImageSmall',
        'EventImageLarge',
        'EventImageBig',
        'EventImageMedium',
        'EventImageSmall',
    ];

    protected $_data = [];

    public function __construct($auth = null)
    {
        $this->_authKey = $auth;
        if (is_null($this->_authKey)) {
            $this->_authKey = self::Login();
        }
    }

    public function getAPIUrl()
    {
        if (is_null($this->_apiUrl)) {
            $this->_apiUrl = Yii::$app->params['provider']['karabas']['api_url'];
            if ($this->_apiUrl[strlen($this->_apiUrl) - 1] != '/') {
                $this->_apiUrl .= '/';
            }
        }

        return $this->_apiUrl;
    }

    public function getUID()
    {
        return Yii::$app->params['provider']['karabas']['__uid'];
    }

    public function getPID()
    {
        return Yii::$app->params['provider']['karabas']['__pid'];
    }

    protected function getLogin()
    {
        return Yii::$app->params['provider']['karabas']['login'];
    }

    protected function getPassword()
    {
        return Yii::$app->params['provider']['karabas']['password'];
    }

    protected function errorMsg($code, $description)
    {
        $data = [
            'response' => [
                'result' => [
                    'code' => $code,
                    'description' => $description,
                    'execution_time' => null
                ],
                'data' => []
            ]
        ];

        return $data;
    }

    protected function getAuthKey()
    {
        return $this->_authKey;
    }

    protected function GetDictionary($method, $params = [])
    {
        return $this->get($method, $params, 'dict');
    }

    protected function Command($method, $params = [])
    {
        return $this->get($method, $params);
    }

    private function get($method, $params = [], $type = '')
    {
        $method .= ($type == 'dict'
            ? ''
            : 'Command');
        $method .= '.cmd?';
        $url = $this->getAPIUrl() . $method;
        $params['__pid'] = $this->getPID();
        $params['__auth'] = $this->getAuthKey();
        $params['compress'] = 0;
//		$requestParams = [];
//		if (is_array($params) && $params) {
//			foreach ($params as $key => $value) {
//				$requestParams[$key] = rawurlencode($value);
//			}
//		}
        if (is_array($params) && $params) {
            foreach ($params as $key => $value) {
                $url .= '&' . $key . '=' . rawurlencode($value);
            }
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
//		curl_setopt($curl, CURLOPT_POST, 1);
//		curl_setopt($curl, CURLOPT_POSTFIELDS, $requestParams);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);

        $data = curl_exec($curl);
        $logItem = [
            'request' => [
                'url' => $url,
                'params' => $params
            ],
            'response' => $data,
            'response_format' => 'json',
        ];
        Yii::info($logItem, __METHOD__);
        if ($data === false) {
            $data = $this->errorMsg(500,
                'Connection error ' . curl_errno($curl) . '. ' . curl_error($curl) . ' ' . $url);
            Yii::error($data, __METHOD__);
        } else {
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($httpCode == 200) {
                $result = $data;
                $data = json_decode($result);
                if (!$data) {
                    $data = $this->errorMsg(500, 'Decoding error. Data: ' . var_export($result, true));
                }
            } else {
                $data = $this->errorMsg(500, 'Connection error. HTTP server returns an error code ' . $httpCode);
            }
        }
        curl_close($curl);
//		if (!isset($data->CommandStatus) || (isset($data->CommandStatus) && $data->CommandStatus != 0)) {
//			Yii::error('Response code not found. Data: ' . var_export($data, true), __METHOD__);
//		}
        $this->_data['dialog'] = $logItem;

        return new KarabasResponse($method, $data, $url);
    }

    public function Login()
    {
        $data = null;
        $pid = $this->getPID();
        $uid = $this->getUID();
        $result = $this->Command('Login', [
            '__pid' => $pid,
            '__uid' => $uid,
        ]);
        if ($result->isSuccess()) {
            $data = $result->getData()->__auth;
            $this->_authKey = $data;
        }

        return $data;
    }

    public function LoadDictionaries()
    {
        foreach ($this->_dictionaries as $dictionary) {
            $result = $this->GetDictionary('Get' . $dictionary);
            if ($result->isSuccess()) {
                $this->SetDictionary($dictionary, $result->getData()->list);
            }
        }
    }

    public function LoadPartnerEvents()
    {
        $this->Login();
        $dictionary = 'PartnerEvents';
        $result = $this->Command('Get' . $dictionary);
        if ($result->isSuccess()) {
            $this->SetEvents($result->getData()->Events);
        }
    }

    public function GetOrder($id = null)
    {
        $params = [];
        if (!is_null($id)) {
            $params['id'] = $id;
        }
        $result = $this->Command('GetOrder', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = $result->getData();
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }

        return $this->_data;
    }

    public function DeletePlaceFromCart($itemId)
    {
        $result = $this->Command('DeletePlaceFromCart', [
            'orderItem' => $itemId,
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = $result->getData();
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }

        return $this->_data;
    }

    public function ClearCurrentOrder()
    {
        $result = $this->Command('ClearCurrentOrder');
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = $result->getData();
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }

        return $this->_data;
    }

    public function ClearProductCart()
    {
        $result = $this->Command('ClearProductCart');
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = $result->getData();
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }

        return $this->_data;
    }

    public function ConfirmOrder($id, $email, $clientName, $status = self::STATUS_CONFIRM_RESERVATION, $deliveryType = '', $paymentType = '')
    {
        $result = $this->Command('ConfirmOrder', [
            'login' => $this->getLogin(),
            'password' => $this->getLogin(),
            'order' => $id,
            'deliveryType' => $deliveryType,
            'paymentType' => $paymentType,
            'email' => $email,
            'client_name' => $clientName,
            'comment' => '',
            'status' => $status,
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = $result->getData();
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = $result->getCode();
            $this->_data['message'] = $result->getDescription();
        }

        return $this->_data;
    }

    private function SetDictionary($dictionaryName, $data)
    {
        $c = 0;
        $total = count($data);
        foreach ($data as $item) {
            /** @var \yii\db\ActiveRecord $model */
            $modelName = '\common\models\karabas\\' . $dictionaryName;
            $model = new $modelName();
            $model = $model::findOne(['id' => $item->id]);
            if (is_null($model)) {
                $model = new $modelName();
            }
            $model->setAttributes(get_object_vars($item));
            if (!$model->save(false)) {
                Yii::error(var_export($model->errors, true), 'karabas');
                throw new \Exception(var_export($model->errors, true));
            }
            $c++;
            echo $dictionaryName . ' ' . $total . '/' . $c . PHP_EOL;
        }
    }

    private function SetEvents($events)
    {
        $c = 0;
        $total = count($events);
        foreach ($events as $event) {
//			if (!in_array($event->EventId, [
//				432716198,
//				393939969,
//				398705615,
//				398705765
//			])
//			) {
//				continue;
//			}
            if (!($eventDate = \DateTime::createFromFormat('d.m.Y H:i', $event->EventDate))) {
                continue;
            }
            $eventStatistic = $event->Statistic;
            unset($event->Statistic);
            $event->ETicketEnabled = strtolower($event->ETicketEnabled) == 'true'
                ? 1
                : 0;
            $event->PrintEnabled = strtolower($event->PrintEnabled) == 'true'
                ? 1
                : 0;
            $event->HasPNG = strtolower($event->HasPNG) == 'true'
                ? 1
                : 0;
            $event->HasSVG = strtolower($event->HasSVG) == 'true'
                ? 1
                : 0;
            $event->ReplaceActivityDescription = strtolower($event->ReplaceActivityDescription) == 'true'
                ? 1
                : 0;

            if (!empty($event->EventImageBig)) {
                $event->LineImage = $event->EventImageBig;
            } elseif (!empty($event->ActivityImageBig)) {
                $event->LineImage = $event->ActivityImageBig;
            } elseif (!empty($event->EventImageMedium)) {
                $event->LineImage = $event->EventImageMedium;
            } elseif (!empty($event->ActivityImageMedium)) {
                $event->LineImage = $event->ActivityImageMedium;
            } elseif (!empty($event->EventImageSmall)) {
                $event->LineImage = $event->EventImageSmall;
            } elseif (!empty($event->ActivityImageSmall)) {
                $event->LineImage = $event->ActivityImageSmall;
            } else {
                $event->LineImage = '';
            }

            $minPrice = PHP_INT_MAX;
            $maxPrice = 0;
            $priceRange = [];
            foreach ($eventStatistic as $item) {
                if ($item->Price <= $minPrice) {
                    $minPrice = $item->Price;
                }
                if ($item->Price >= $maxPrice) {
                    $maxPrice = $item->Price;
                }
            }
            $priceRange[$minPrice] = '';
            $priceRange[$maxPrice] = '';
            $event->LinePriceRange = Yii::t('tickets', '{price} грн',
                ['price' => implode(' - ', array_keys($priceRange))]);

            $place = [];
            $place[$event->City] = '';
            $place[$event->BuildingName] = '';
            $place[$event->HallName] = '';
            $event->LinePlace = implode('. ', array_keys($place));

            $event->EventDate = $eventDate->format('Y-m-d H:i');
            $event->LineDateTime =
                $eventDate->format('d') . ' ' . Yii::$app->formatter->asDate($eventDate, 'php:F') . ' '
                . $eventDate->format('H:i');

            $eventDate = \DateTime::createFromFormat('Y-m-d H:i', $event->EventDate);
            $eventDateStr = $eventDate->format('d') . ' ' . Yii::$app->formatter->asDate($eventDate, 'php:F') . ', '
                . Yii::$app->formatter->asDate($eventDate, 'php:l');
            $event->DetailsDate = $eventDateStr;
            $event->DetailsTime = $eventDate->format('H:i');

            $place[$event->BuildingName] = '';
            $place[$event->HallName] = '';
            $event->DetailsPlace = implode('. ', array_keys($place));

            $address = '';
            /** @var Buildings $buildigModel */
            $buildigModel = Buildings::findOne(['id' => $event->BuildingId]);
            if ($buildigModel) {
                $address = $buildigModel->address;
            }
            $event->DetailsAddress = $address;

            $imageTypesPriority = [
                'EventImageLarge',
                'EventImageBig',
                'EventImageMedium',
                'ActivityImageLarge',
                'ActivityImageBig',
                'ActivityImageMedium',
                'EventImageSmall',
                'ActivityImageSmall',
            ];
            $image = '';
            foreach ($imageTypesPriority as $item) {
                if (!empty($event->$item)) {
                    $image = $event->$item;
                    break;
                }
            }
            $event->DetailsImage = $image;

            $description = '';
            if (!empty($event->EventDescription)) {
                $description = $event->EventDescription;
            } elseif (!empty($event->ActivityDescriptionLong)) {
                $description = $event->ActivityDescriptionLong;
            }
            $event->DetailsDescription = $description;

            $partnerEvent = PartnerEvents::findOne(['EventId' => $event->EventId]);
            if (!$partnerEvent) {
                $partnerEvent = new PartnerEvents();
            }
            $partnerEvent->setAttributes(get_object_vars($event));
            if ($partnerEvent->save()) {
                foreach ($eventStatistic as $item) {
                    $eventStatisticModel = PartnerEventStatistic::findOne([
                        'EventId' => $event->EventId,
                        'Price' => $item->Price
                    ]);
                    if (!$eventStatisticModel) {
                        $eventStatisticModel = new PartnerEventStatistic();
                        $eventStatisticModel->EventId = $event->EventId;
                    }
                    $eventStatisticModel->setAttributes(get_object_vars($item));
                    $eventStatisticModel->save();
                }

                $eventTypes = explode(',', $event->CategoriesIDs);
                foreach ($eventTypes as $eventType) {
                    $params = [
                        'EventId' => $event->EventId,
                        'ActivityTypeId' => $eventType,
                    ];
                    $partnerEventActivityType = PartnerEventActivityType::findOne($params);
                    if (!$partnerEventActivityType) {
                        $partnerEventActivityType = new PartnerEventActivityType();
                        $partnerEventActivityType->setAttributes($params);
                        $partnerEventActivityType->save();
                    }
                }
            } else {
                //Yii::error(var_export($partnerEvent->errors, true), 'karabas');
                continue;
                //throw new Exception(var_export($partnerEvent->errors, true));
            }

            $eventType = serialize(ArrayHelper::map($partnerEvent->getActivityTypes()
                ->asArray()
                ->all(), 'name', 'color'));
            $partnerEvent->DetailsType = $eventType;
            $partnerEvent->save();
            $c++;
            echo $total . '/' . $c . PHP_EOL;
        }
        // отсеим неактуальные типы событий
        $partnerEvents = PartnerEvents::tableName();
        $partnerEventActivityType = PartnerEventActivityType::tableName();
        $activityTypes = ActivityTypes::tableName();
        $types = (new Query())->select([
                $activityTypes . '.id',
            ])
            ->from($partnerEvents)
            ->distinct()
            ->innerJoin($partnerEventActivityType,
                $partnerEventActivityType . '.EventId=' . $partnerEvents . '.EventId')
            ->innerJoin($activityTypes, $activityTypes . '.id=' . $partnerEventActivityType . '.ActivityTypeId')
            ->andWhere($partnerEvents . '.EventDate>=now()')
            ->all();
        if (count($types)) {
            $types = ArrayHelper::getColumn($types, 'id');
            ActivityTypes::updateAll(['active' => 0]);
            ActivityTypes::updateAll(['active' => 1], [
                'in',
                'id',
                $types
            ]);
        }
    }
}
