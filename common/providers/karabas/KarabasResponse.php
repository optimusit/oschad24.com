<?php
namespace common\providers\karabas;

use Yii;

class KarabasResponse
{
    const ERROR_NONE = 0;

    private $_method = null;
    private $_code = 0;
    private $_description = '';
    private $_data;

    function __construct($method, $response, $url)
    {
        $this->_method = $method;
        $this->_code = $response->CommandStatus;
        if ($response->CommandStatus != 0) {
            $this->_description = $response->errormessage;
        } else {
            unset($response->CommandStatus);
            $this->_data = $response;
        }
    }

    public function isSuccess()
    {
        return ($this->_code == self::ERROR_NONE);
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function setDescription($description)
    {
        return $this->_description = $description;
    }
}
