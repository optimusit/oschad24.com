<?php
namespace common\providers\tickets;

use Yii;
use api\modules\v1\Module;

class BusTickets extends Tickets
{
    const MAX_PASSENGERS = 5;

    const DOCUMENT_TYPE = [
        1 => 'загранпаспорт',
        2 => 'внутренний паспорт',
        3 => 'свидетельство о рождении',
    ];

    public function __construct()
    {
        parent::__construct();
        if (isset(Yii::$app->session)) {
            $this->sessionId = Yii::$app->session['busSession'];
        } else {
            $this->sessionId = '';
        }
    }

    public function getAuthKey()
    {
        return $this->signin();
    }

    static function getCountryLabel($key)
    {
        $countryLabel = [
            'UA' => Module::t('tickets', 'Украина'),
            'RU' => Module::t('tickets', 'Россия'),
        ];
        if (array_key_exists($key, $countryLabel)) {
            return $countryLabel[$key];
        }

        return $key;
    }

    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
        Yii::$app->session['busSession'] = $this->sessionId;
    }

    public function get($service, $method, $params = [])
    {
        if ($this->sessionId) {
            $params['session_id'] = $this->sessionId;
        }
        $result = parent::get($service, $method, $params);
        if ($result->isSuccess()) {
            if ($result->session) {
                $this->setSessionId($result->session['id']);
            }
        } else {
            $code = $result->getCode();
            if ($code == 213) {
                $result->setDescription('Нет свободных мест. Выберите другой рейс или повторите попытку позже.');
            }
        }

        return $result;
    }

    function stations()
    {
        $result = $this->get('bus', 'stations', []);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = [];
            if (isset($result->stations['@children']) && $result->stations['@children']) {
                $stations = [];
                foreach ($result->stations['@children'] as $row) {
                    if (empty($row['name'])) {
                        continue;
                    }
                    $stations[] = [
                        'id' => $row['code'],
                        'value' => $row['name'] . ', ' . self::getCountryLabel($row['country'])
                    ];
                }
                $this->_data['data'] = $stations;
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    function searchStation($q, $code = '')
    {
        $params = [
            'name' => $q,
            'lang' => Yii::$app->language
        ];
        if ($code) {
            $params['code'] = $code;
        }
        $result = $this->get('bus', 'station', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = [];
            if (isset($result->stations['@children']) && $result->stations['@children']) {
                $stations = [];
                foreach ($result->stations['@children'] as $row) {
                    if (empty($row['name'])) {
                        continue;
                    }
                    $stations[] = [
                        'id' => $row['code'],
                        'value' => $row['name'] . ', ' . self::getCountryLabel($row['country'])
                    ];
                }
                $this->_data['data'] = $stations;
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    function searchBus($params, $sortBy = '', $sortDir = 1)
    {
        $result = $this->get('bus', 'search', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $buses = [];
            $fieldsSortAvailed = [
                'amount'
            ];
            if (empty($sortBy)
                || (!empty($sortBy) && !in_array($fieldsSortAvailed, $sortBy))
            ) {
                $sortBy = 'amount';
            }
            foreach ($result->trips['@children'] as $row) {
                static::cleanArray($row, '@name');
                //unset($row['free_place_numbers']);
                $amount = 0;
                foreach ($row['@children']['exchanges']['@children'] as $exchange) {
                    if (isset($exchange['base'])) {
                        $amount = $exchange['amount'];
                        break;
                    }
                }
//				$station = $row['@children']['station'];
//				unset($row['@children']);
//				//$row['free_place_numbers'] = explode(',', $row['free_place_numbers']);
//				$departureFromPoint = \DateTime::createFromFormat('d.m.Y H:i', $station['departure_from_point']);
//				$arrivalToPoint = \DateTime::createFromFormat('d.m.Y H:i', $station['arrival_to_point']);
//				$row['departure_address'] = $station['departure_address'];
//				$row['departure_code'] = $station['departure_code'];
//				$row['arrival_code'] = $station['arrival_code'];
//				$row['departure_from_point_date'] = Yii::$app->formatter->asDate($departureFromPoint);
//				$row['departure_from_point_time'] = Yii::$app->formatter->asTime($departureFromPoint);
//				$row['departure_from_point_date_time'] = Yii::$app->formatter->asDatetime($departureFromPoint);
//				$row['arrival_to_point_date'] = Yii::$app->formatter->asDate($arrivalToPoint);
//				$row['arrival_to_point_time'] = Yii::$app->formatter->asTime($arrivalToPoint);
//				$row['arrival_to_point_date_time'] = Yii::$app->formatter->asDatetime($arrivalToPoint);
                if ($amount > 0) {
                    $row['amount'] = $amount;
                    $buses[${$sortBy}][] = $row;
                }
            }

            if (isset($sortDir)) {
                if ($sortDir > 0) {
                    ksort($buses);
                } else {
                    krsort($buses);
                }
            } else {
                ksort($buses);
            }

            $temp = array_values($buses);
            $buses = [];
            foreach ($temp as $list) {
                foreach ($list as $row) {
                    $buses[] = $row;
                }
            }
            static::cleanArray($buses, 'amount');

            $this->_data['data'] = $buses;
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    function tripInfo($params)
    {
        $result = $this->get('bus', 'info', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $info = $result->ticket;
            $places = [];
            $amount = 0;
            foreach ($info['@children']['exchanges']['@children'] as $exchange) {
                if (isset($exchange['base'])) {
                    $amount = $exchange['amount'];
                    break;
                }
            }
            foreach ($info['@children']['places']['@children'] as $place) {
                $places[] = $place['number'];
            }
            static::cleanArray($info, '@name');
            static::cleanArray($info, '@children');
            $info['amount'] = $amount;
            $info['places'] = $places;
            $this->_data['data'] = $info;
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function reservation($form)
    {
        ignore_user_abort(true);
        set_time_limit(0);
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'server_code' => $form['bus']['server_code'],
                'from' => $form['bus']['departure_code'],
                'to' => $form['bus']['arrival_code'],
                'date' => $form['departureDate'],
                'bus_number' => $form['bus']['number'],
                'provider' => $form['bus']['provider'],
                'passengers' => json_encode($form['passengersForTickets']),
                'user_token' => $authKey,
                'ticket_in_bus' => 0,
            ];
            $result = $this->get('bus', 'booking', $params);
            if ($result->isSuccess()) {
                $this->_data['reservation'] = $result->booking;
                static::cleanArray($this->_data['reservation'], '@name');
            } else {
                $this->_data['reservation'] = false;
                $this->_data['code'] = $result->getCode();
                $this->_data['message'] = $result->getDescription();
                $this->_data['back_param'] = $params;
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['message'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function bookingsList($limit = 10, $offset = 0, $status = null, \DateTime $timeFrom = null, \DateTime $timeTo = null)
    {
        $this->_data = [
            'success' => true,
            'code' => 0,
            'message' => ''
        ];
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'user_token' => $authKey,
                'limit' => $limit,
                'offset' => $offset,
            ];
            if ($status) {
                $params['status'] = $status;
            }
            if ($timeFrom) {
                $params['date_from'] = $timeFrom->format('d-m-Y');
            }
            if ($timeTo) {
                $params['date_to'] = $timeTo->format('d-m-Y');
            }
            $result = $this->get('bus', 'booking_list', $params);
            if ($result->isSuccess()) {
                $this->_data['data'] = $result->bookings;
                $this->_data['data']['total'] = $result->total['@text'];
                $this->_data['data']['items'] = $this->_data['data']['@children'];
                unset($this->_data['data']['@name']);
                static::cleanArray($this->_data['data'], '@children');
                static::cleanArray($this->_data['data']['items'], '@name');
                static::cleanArray($this->_data['data']['total'], '@name');
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode();
                $this->_data['message'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['message'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function bookingShow($reservationId)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $this->_data['message'] = '';
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'user_token' => $authKey,
                'reservation_id' => $reservationId,
            ];
            $result = $this->get('bus', 'booking_show', $params);
            if ($result->isSuccess()) {
                $this->_data['data'] = $result->booking;
                $this->_data['data']['exchanges'] = $this->_data['data']['@children']['exchanges']['@children'];
                unset($this->_data['data']['@children']);
                static::cleanArray($this->_data['data'], '@name');
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode();
                $this->_data['message'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['message'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function paymentCommit($id, $amount)
    {
        return $this->commit('bus', $id, $amount, $this->getShopApiKeyFor('bus'), $this->getShopSecretKeyFor('bus'));
    }

    public function cancel($reservationId)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'user_token' => $authKey,
                'reservation_id' => $reservationId,
            ];
            $result = $this->get('bus', 'booking_canceled', $params);
            if ($result->isSuccess()) {
                $this->_data['success'] = true;
                $this->_data['data']['cancelled'] = $result->booking['cancelled'];
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode() == 0
                    ? 500
                    : $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function eticket($reservationId)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'user_token' => $authKey,
                'reservation_id' => $reservationId,
            ];
            $result = $this->get('bus', 'ticket_pdf', $params);
            if ($result->isSuccess() && isset($result->files) && $result->files['@children']) {
                $files = [];
                if (count($result->files['@children']) > 1) {
                    foreach ($result->files['@children'] as $file) {
                        $files[] = $file['base64_pdf'];
                    }
                } else {
                    $files[] = $result->files['@children']['file']['base64_pdf'];
                }
                $this->_data['data'] = $files;
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode() == 0
                    ? 500
                    : $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }
}
