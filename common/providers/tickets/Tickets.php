<?php
namespace common\providers\tickets;

use Yii;

class Tickets
{
    const PREFIX = 'TICKETSUA';

    private $apiUrl = null;
    protected $sessionId = null;
    protected $authKey = null;
    protected $_data = [];

    public function __construct()
    {
        $this->authKey = Yii::$app->keyStorage->get('ticketsua_token');
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function getAPIUrl()
    {
        if (is_null($this->apiUrl)) {
            $this->apiUrl = Yii::$app->params['provider']['tickets']['api_url'];
            if ($this->apiUrl[strlen($this->apiUrl) - 1] != '/') {
                $this->apiUrl .= '/';
            }
        }

        return $this->apiUrl;
    }

    public function getKey()
    {
        return Yii::$app->params['provider']['tickets']['key'];
    }

    protected static function getUID()
    {
        return Yii::$app->params['provider']['tickets']['uuid'];
    }

    protected function getShopApiKeyFor($service)
    {
        return Yii::$app->params['provider']['tickets']['shop_api_key'][$service];
    }

    protected function getShopSecretKeyFor($service)
    {
        return Yii::$app->params['provider']['tickets']['shop_secret_key'][$service];
    }

    protected function getEmail()
    {
        return Yii::$app->params['provider']['tickets']['user_email'];
    }

    protected function getPassword()
    {
        return Yii::$app->params['provider']['tickets']['user_password'];
    }

    static function toArray($obj)
    {
        $array = [];
        $children = $obj->children();
        $tags = [];
        foreach ($children as $elementName => $node) {
            $elementName = strtolower((string)$elementName);
            if (isset($tags[$elementName])) {
                $tags[$elementName]++;
            } else {
                $tags[$elementName] = 1;
            }
        }
        foreach ($children as $elementName => $node) {
            $elementName = strtolower((string)$elementName);
            if ($tags[$elementName] == 1) {
                $index = $elementName;
            } else {
                $index = count($array);
            }
            if ($index === 'fare-conditions') {
                $index = 'fare_conditions';
            }
            $array[$index] = ['@name' => $elementName];
            $attributes = $node->attributes();
            foreach ($attributes as $attributeName => $attributeValue) {
                $attribName = strtolower(trim((string)$attributeName));
                $attribVal = trim((string)$attributeValue);
                $array[$index][$attribName] = $attribVal;
            }
            $text = trim((string)$node);
            if (strlen($text) > 0) {
                $array[$index]['@text'] = $text;
            }
            $inner = self::toArray($node);
            if ($inner) {
                $array[$index]['@children'] = $inner;
            }
        }

        return $array;
    }

    protected function decode($data)
    {
        $array = [];
        $obj = false;
        try {
            $obj = simplexml_load_string($data);
        } catch (\Exception $ex) {
        }

        if (!$obj) {
            return false;
        }
        $elementName = strtolower($obj->getName());
        $array[$elementName] = [];
        $array[$elementName] = self::toArray($obj, $array[$elementName]);

        return $array;
    }

    protected function errorMsg($code, $description)
    {
        $data = [
            'response' => [
                'result' => [
                    'code' => $code,
                    'description' => $description,
                    'execution_time' => null
                ],
                'data' => []
            ]
        ];

        return $data;
    }

    public function get($service, $method, $params = [])
    {
        $url = $this->getAPIUrl() . $service . '/' . $method . ($service == 'avia'
                ? '.xml'
                : '') . '?key=' . $this->getKey();
        if ($service != 'payment' && !isset($params['lang'])) {
            //$url .= '&lang=' . Yii::$app->language;
            $params['lang'] = Yii::$app->language;
        }
        if (is_array($params) && $params) {
            foreach ($params as $key => $value) {
                if ($service == 'payment' && $key == 'session_id') {
                    continue;
                }
                $url .= '&' . $key . '=' . rawurlencode($value);
            }
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);

        $data = curl_exec($curl);
        if ($service == 'bus'
            && in_array($method, [
                'booking_list',
                'booking_show'
            ])
        ) {
            $data = str_replace([
                "&amp;",
                "&",
                "<br/>",
            ], [
                "&",
                "&amp;",
                "",
            ], $data);
        }
        $logItem = [
            'request' => [
                'url' => $url,
                'params' => $params
            ],
            'response' => $data,
            'response_format' => 'xml',
        ];
        Yii::info($logItem, __METHOD__);
        if ($data === false) {
            $error = curl_errno($curl) . ' ' . curl_error($curl);
            $data = $this->errorMsg(500, $error);
            $logItem['error'] = $error;
            Yii::error($logItem, __METHOD__);
        } else {
            //Yii::info($url, __METHOD__);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($httpCode == 200) {
                $result = $data;
                //Yii::info($result, __METHOD__);
                $data = $this->decode($result);
                if (!$data) {
                    $data = $this->errorMsg(500, 'Decoding error. Data: ' . var_export($result, true));
                }
            } else {
                $data = $this->errorMsg(500, 'Connection error. HTTP server returns an error code ' . $httpCode);
            }
        }
        curl_close($curl);
        if (!isset($data['response']['result']['code'])) {
            $data['response']['result']['code'] = 500;
            Yii::error('Response code not found. Data: ' . var_export($result, true), __METHOD__);
        }
        $this->_data['dialog'] = $logItem;

        return new TicketsResponse($service, $method, $data['response'], $url);
    }

    public function signin($reset = false)
    {
        if ($this->authKey && !$reset) {
            return $this->authKey;
        } else {
            $result = $this->get('user', 'signin', [
                'email' => self::getEmail(),
                'password' => self::getPassword()
            ]);
            if ($result->isSuccess()) {
                $this->authKey = $result->auth_key['@text'];
                Yii::$app->keyStorage->set('ticketsua_token', $this->authKey);

                return $this->authKey;
            } else {
                sleep(1);
                $result = $this->get('user', 'signin', [
                    'email' => self::getEmail(),
                    'password' => self::getPassword()
                ]);
                if ($result->isSuccess()) {
                    $this->authKey = $result->auth_key['@text'];
                    Yii::$app->keyStorage->set('ticketsua_token', $this->authKey);

                    return $this->authKey;
                } else {
                    Yii::error('Authorization failed (second attempt). Error #' . $result->getCode() . ' '
                        . $result->getDescription(), __METHOD__);
                }
            }

            return null;
        }
    }

    public function getBalance($service = 'gd')
    {
        $result = $this->get('payment', 'balance', ['service' => $service]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['amount'] = $result->balance['amount'];
        } else {
            $this->_data['success'] = false;
        }

        return $this->_data;
    }

    public function commit($service, $orderId, $amount)
    {
//		if (TEST_SERVER) {
//			return true;
//		}
        $shopApiKey = self::getShopApiKeyFor($service);
        $shopSecretKey = self::getShopSecretKeyFor($service);
        $amount = sprintf('%.2f', $amount);
        $result = $this->get('payment', 'commit', [
            'service' => $service,
            'order_id' => $orderId,
            'amount' => sprintf('%.2f', $amount),
            'signature' => md5($shopApiKey . $service . $orderId . $amount . $shopSecretKey),
        ]);
        $this->_data['success'] = false;
        if ($result->isSuccess()) {
            if ($result->order['paid'] == 'true') {
                $this->_data['success'] = true;
            }
        } elseif ($result->getCode() == 68) {
            $this->_data['success'] = true;
        }

        return $this->_data;
    }

    /**
     * Получение списка попыток оплаты заказа
     *
     * @param string $service Название сервиса (gd|avia|bus)
     * @param string $order_id Идентификатор заказа
     * @param string $status Статус транзакции (через запятую) (created,success,failure,refunded)
     * @return array
     */
    public function transactions($service, $order_id, $status)
    {
        $result = $this->get('payment', 'transactions', [
            'service' => $service,
            'order_id' => $order_id,
            'status' => $status,
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = $result->transactions;
            unset($this->_data['data']['@name']);
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function getUser()
    {
        $authKey = $this->signin();
        if ($authKey) {
            $result = $this->get('user', 'card', [
                'auth_key' => $authKey,
            ]);
            if ($result->isSuccess()) {
                $this->_data['email'] = $result->card['@children']['email']['@text'];
                $this->_data['phone'] = $result->card['@children']['phone']['@text'];
                $this->_data['name'] = $result->card['@children']['name']['@text'];

                return $this->_data;
            }
        }

        return false;
    }

    public function changeUser($name, $phone)
    {
        $authKey = $this->signin();
        if ($authKey) {
            $result = $this->get('user', 'change', [
                'auth_key' => $authKey,
                'name' => $name,
                'phone' => $phone,
            ]);

            return $result;
        }

        return false;
    }

    public function CheckAvailability()
    {
        $user = $this->getUser();

        return $user !== false;
    }

    static public function cleanArray(&$data, $param)
    {
        if (is_array($data)) {
            if (array_key_exists($param, $data)) {
                unset($data[$param]);
            }
            foreach ($data as $key => $item) {
                if (is_array($data[$key])) {
                    self::cleanArray($data[$key], $param);
                }
            }
        }
    }
}
