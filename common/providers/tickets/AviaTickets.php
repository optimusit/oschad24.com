<?php
namespace common\providers\tickets;

use Yii;

class AviaTickets extends Tickets
{
    const MAX_PASSENGERS = 9;

    const CLASS_A = 'A';
    const CLASS_E = 'E';
    const CLASS_B = 'B';
    const CLASSES = [
        self::CLASS_A => 'Всі',
        self::CLASS_E => 'Економ',
        self::CLASS_B => 'Бізнес',
    ];

    const PASSENGER_TYPE_ADULT = 'ADT';
    const PASSENGER_TYPE_CHILD = 'CHD';
    const PASSENGER_TYPE_INFANT = 'INF';
    const PASSENGER_TYPES = [
        self::PASSENGER_TYPE_ADULT => 'Дорослий',
        self::PASSENGER_TYPE_CHILD => 'Дитина',
        self::PASSENGER_TYPE_INFANT => 'Немовля',
    ];
    const PASSENGER_AGE_RANGES = [
        self::PASSENGER_TYPE_ADULT => [
            'min' => 12,
            'max' => 200
        ],
        self::PASSENGER_TYPE_CHILD => [
            'min' => 2,
            'max' => 11
        ],
        self::PASSENGER_TYPE_INFANT => [
            'min' => 0,
            'max' => 1
        ],
    ];

    public $statuses = [
        'W' => 'Забронирован. Ожидает оплаты',
        'P' => 'Оплачен и выписан',
        'PN' => 'Оплачен',
        'PC' => 'Оплачен после аннулирования',
        'C' => 'Аннулирован',
        'CR' => 'В процессе возврата',
        'OR' => 'Заявка на возврат',
        'R' => 'Возвращен',
        'CU' => 'Аннулирован',
        'CO' => 'Аннулирован',
        'NP' => 'Оплата не подтвердилась',
    ];

    public function __construct()
    {
        parent::__construct();
        if (isset(Yii::$app->session)) {
            $this->sessionId = Yii::$app->session['aviaSession'];
        } else {
            $this->sessionId = '';
        }
    }

    public function getAuthKey()
    {
        return $this->signin();
    }

    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
        Yii::$app->session['aviaSession'] = $this->sessionId;
    }

//	static function getTypeLabel($type)
//	{
//		$typeLabel = [
//			'ADT' => Yii::t('tickets', 'Взрослый'),
//			'CHD' => Yii::t('tickets', 'Детский'),
//			'INF' => Yii::t('tickets', 'Детский (Младенец)'),
//		];
//		if (array_key_exists($type, $typeLabel)) {
//			return $typeLabel[$type];
//		}
//
//		return $type;
//	}

    static function status2int($status)
    {
        switch ($status) {
            case 'P':
                $number = 1;
                break;
            case 'PN':
                $number = 2;
                break;
            case 'PC':
                $number = 3;
                break;
            case 'C':
                $number = 4;
                break;
            case 'CR':
                $number = 5;
                break;
            case 'OR':
                $number = 6;
                break;
            case 'R':
                $number = 7;
                break;
            case 'CU':
                $number = 8;
                break;
            case 'CO':
                $number = 9;
                break;
            case 'NP':
                $number = 10;
                break;
            default:
                $number = 0;
        }

        return $number;
    }

    public function get($service, $method, $params = [])
    {
        if ($this->sessionId) {
            $params['session_id'] = $this->sessionId;
        }
        $result = parent::get($service, $method, $params);
        if ($result->isSuccess()) {
            if ($result->session) {
                $this->setSessionId($result->session['@children']['id']['@text']);
            }
        } else {
            $code = $result->getCode();
            if ($code == 213) {
                $result->setDescription('Нет свободных мест. Выберите другой рейс или повторите попытку позже.');
            }
        }

        return $result;
    }

    public function getOld($service, $method, $params = [])
    {
        if ($this->sessionId) {
            $params['session_id'] = $this->sessionId;
        }
        // Custom get request
        $url = 'https://api.tickets.ua/' . $method . '.php?key=' . $this->getKey();
        if ($service != 'payment' && !isset($params['lang'])) {
            //$url .= '&lang=' . Yii::$app->language;
            $params['lang'] = Yii::$app->language;
        }
        if (is_array($params) && $params) {
            foreach ($params as $key => $value) {
                if ($service == 'payment' && $key == 'session_id') {
                    continue;
                }
                $url .= '&' . $key . '=' . rawurlencode($value);
            }
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);

        $data = curl_exec($curl);
        $logItem = [
            'request' => [
                'url' => $url,
                'params' => $params
            ],
            'response' => $data
        ];
        Yii::info($logItem, __METHOD__);
        if ($data === false) {
            $data = $this->errorMsg(500, 'Connection error ' . curl_errno($curl) . '. ' . curl_error($curl));
            Yii::error($data, __METHOD__);
        } else {
            //Yii::info($url, __METHOD__);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($httpCode == 200) {
                $result = $data;
                //Yii::info($result, __METHOD__);
                $data = $this->decode($result);
                if ($data) {
                    $data = ['response' => $data['results']];
                    if (isset($data['response']['session_id'])) {
                        $data['response']['session'] = ['id' => $data['response']['session_id']['@text']];
                        unset($data['response']['session_id']);
                    }
                } else {
                    $data = $this->errorMsg(500, 'Decoding error. Data: ' . var_export($result, true));
                }
            } else {
                $data = $this->errorMsg(500, 'Connection error. HTTP server returns an error code ' . $httpCode);
            }
        }
        curl_close($curl);
//		if (!isset($data['response']['result']['code'])) {
//			$data['response']['result']['code'] = 500;
//			Yii::warning('Response code not found. Data: ' . var_export($result, true), 'tickets');
//		}
        $this->_data['dialog'] = $logItem;

        return new TicketsResponse($service, $method, $data['response'], $url);
    }

    public function getAirline($value)
    {
        $result = $this->getOld('avia', 'vocabulary', [
            'value' => $value,
            'lang' => Yii::$app->language,
            'action' => 'airline'
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = [];
            if (isset($result->airline['@children']) && $result->airline['@children']) {
                $item = [];
                foreach ($result->airline['@children'] as $key => $row) {
                    if (isset($row['@text'])) {
                        $item[$key] = @$row['@text'];
                    }
                }
                $this->_data['data'] = $item;
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function getCountry($code)
    {
        $result = $this->getOld('avia', 'vocabulary', [
            'value' => $code,
            'lang' => Yii::$app->language,
            'action' => 'country'
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = [];
            if (isset($result->country['@children']) && $result->country['@children']) {
                $item = [];
                foreach ($result->country['@children'] as $key => $row) {
                    if (isset($row['@text'])) {
                        $item[$key] = @$row['@text'];
                    }
                }
                $this->_data['data'] = $item;
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function getCity($code)
    {
        $result = $this->getOld('avia', 'vocabulary', [
            'value' => $code,
            'lang' => Yii::$app->language,
            'action' => 'city_by_airport'
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = [];
            if (isset($result->city['@children']) && $result->city['@children']) {
                $item = [];
                foreach ($result->city['@children'] as $key => $row) {
                    if (isset($row['@text'])) {
                        $item[$key] = @$row['@text'];
                    }
                }
                $this->_data['data'] = $item;
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function getAirport($code)
    {
        $result = $this->getOld('avia', 'vocabulary', [
            'value' => $code,
            'lang' => Yii::$app->language,
            'action' => 'airport'
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = [];
            if (isset($result->airport['@children']) && $result->airport['@children']) {
                $item = [];
                foreach ($result->airport['@children'] as $key => $row) {
                    if (isset($row['@text'])) {
                        $item[$key] = @$row['@text'];
                    }
                }
                $this->_data['data'] = $item;
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function searchStation($name)
    {
        $result = $this->getOld('avia', 'vocabulary', [
            'value' => $name,
            'lang' => Yii::$app->language,
            'action' => 'airports_list'
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = [];
            if (isset($result->airports['@children']) && $result->airports['@children']) {
                $cities = [];
                foreach ($result->airports['@children'] as $row) {
                    $city_code = $row['@children']['city_code']['@text'];
                    if (!isset($cities[$city_code])) {
                        $this->_data['data'][] = [
                            'id' => $city_code,
                            'value' => $row['@children']['city_name']['@text'] . ', '
                                . $row['@children']['country_name']['@text'],
                        ];
                        $cities[$city_code] = true;
                    }
                }
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function searchAircraft($params)
    {
        $this->_data['data'] = [];
        $result = $this->get('avia', 'search', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['code'] = 0;
            $this->_data['message'] = '';
            if (isset($result->recommendations['@children'])) {
                // варианты
                foreach ($result->recommendations['@children'] as $row) {
                    $departure = null;
                    $arrival = null;
                    $duration = 0;
                    $parts = [];
                    $totalSegments = 0;
                    $maxSegmentsPerParts = 0;
                    // части перелетов между начальной и конечной точками
                    foreach ($row['@children']['routes']['@children'] as $route) {
                        $segments = $route['@children']['segments']['@children'];
                        reset($segments);
                        $segment1 = current($segments);
                        $segment2 = end($segments);
                        $departureDateTime = \DateTime::createFromFormat('d.m.Y H:i',
                            $segment1['@children']['departure-time']['@text']);
                        $arrivalDateTime = \DateTime::createFromFormat('d.m.Y H:i',
                            $segment2['@children']['arrival-time']['@text']);
                        $part = [
                            'flight_number' => $segment1['@children']['flight-number']['@text'],
                            'aircraft' => $segment1['@children']['aircraft']['@text'],
                            'departure_terminal' => isset($segment1['@children']['departure-terminal']['@text'])
                                ? $segment1['@children']['departure-terminal']['@text']
                                : '',
                            'departure_airport' => $segment1['@children']['departure-airport']['@text'],
                            'departure_city' => isset($segment1['@children']['departure-city']['@text'])
                                ? $segment1['@children']['departure-city']['@text']
                                : '',
                            'departure_date' => Yii::$app->formatter->asDate($departureDateTime),
                            'departure_time' => Yii::$app->formatter->asTime($departureDateTime),
                            'arrival_terminal' => isset($segment2['@children']['arrival-terminal']['@text'])
                                ? $segment2['@children']['arrival-terminal']['@text']
                                : '',
                            'arrival_airport' => $segment2['@children']['arrival-airport']['@text'],
                            'arrival_city' => isset($segment2['@children']['arrival-city']['@text'])
                                ? $segment2['@children']['arrival-city']['@text']
                                : '',
                            'arrival_date' => Yii::$app->formatter->asDate($arrivalDateTime),
                            'arrival_time' => Yii::$app->formatter->asTime($arrivalDateTime),
                            'segments' => [],
                        ];
                        // сегменты в пределах перелета между начальной и конечной точками
                        if ($segments) {
                            if (count($segments) > $maxSegmentsPerParts) {
                                $maxSegmentsPerParts = count($segments);
                            }
                            foreach ($segments as $segment) {
                                $departureDateTime = \DateTime::createFromFormat('d.m.Y H:i',
                                    @$segment['@children']['departure-time']['@text']);
                                if ($departureDateTime) {
                                    $departureDate = Yii::$app->formatter->asDate($departureDateTime);
                                    $departureTime = Yii::$app->formatter->asTime($departureDateTime);
                                } else {
                                    $departureDate = '';
                                    $departureTime = '';
                                }
                                $arrivalDateTime = \DateTime::createFromFormat('d.m.Y H:i',
                                    @$segment['@children']['arrival-time']['@text']);
                                if ($arrivalDateTime) {
                                    $arrivalDate = Yii::$app->formatter->asDate($arrivalDateTime);
                                    $arrivalTime = Yii::$app->formatter->asTime($arrivalDateTime);
                                } else {
                                    $arrivalDate = '';
                                    $arrivalTime = '';
                                }
                                $part['segments'][] = [
                                    'segment_index' => @$segment['@children']['segment-index']['@text'],
                                    'departure_country' => @$segment['@children']['departure-country']['@text'],
                                    'departure_city' => @$segment['@children']['departure-city']['@text'],
                                    'departure_airport' => @$segment['@children']['departure-airport']['@text'],
                                    'departure_terminal' => @$segment['@children']['departure-terminal']['@text'],
                                    'departure_date' => $departureDate,
                                    'departure_time' => $departureTime,
                                    'arrival_country' => @$segment['@children']['arrival-country']['@text'],
                                    'arrival_city' => @$segment['@children']['arrival-city']['@text'],
                                    'arrival_airport' => @$segment['@children']['arrival-airport']['@text'],
                                    'arrival_terminal' => @$segment['@children']['arrival-terminal']['@text'],
                                    'arrival_date' => $arrivalDate,
                                    'arrival_time' => $arrivalTime,
                                    'supplier' => @$segment['@children']['supplier']['@text'],
                                    'flight_number' => @$segment['@children']['flight-number']['@text'],
                                    'aircraft_code' => @$segment['@children']['aircraft-code']['@text'],
                                    'aircraft' => @$segment['@children']['aircraft']['@text'],
                                    'service_class_type' => @$segment['@children']['service-class-type']['@text'],
                                    'service_class' => @$segment['@children']['service-class']['@text'],
                                    'fare_code' => @$segment['@children']['fare-code']['@text'],
                                ];
                                $totalSegments++;
                            }
                        }
                        $parts[] = $part;
                        $duration += intval(@$route['@children']['route-duration']['@text']);
                    }
                    $item = [
                        'id' => $row['@children']['id']['@text'],
                        'gds_id' => $row['@children']['gds-id']['@text'],
                        'passportRequired' => $row['@children']['passport-required']['@text'] == 1
                            ? true
                            : false,
                        'amount' => floatval($row['@children']['amount']['@children']['uah']['@text']),
                        'duration' => $duration,
                        'totalPartsSegments' => $totalSegments,
                        'maxSegmentsPerParts' => $maxSegmentsPerParts,
                        'parts' => $parts
                    ];
//					$this->_data['data'][preg_replace('/[^a-zA-Z]+/', '',
//						md5($row['@children']['id']['@text']))] = $item;
                    $this->_data['data'][] = $item;
                }
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = $result->getCode();
            $this->_data['message'] = $result->getDescription();
        }

        return $this->_data;
    }

    public function fareConditions($recommendationId)
    {
        $this->_data['success'] = false;
        $this->_data['code'] = 0;
        $this->_data['message'] = '';
        $this->_data['data'] = null;
        $params = ['recommendation_id' => $recommendationId];
        $result = $this->get('avia', 'fare_conditions', $params);
        if ($result->isSuccess()) {
            $rows = [];
            if (isset($result->fare_conditions['@children']) && $result->fare_conditions['@children']) {
                foreach ($result->fare_conditions['@children'] as $row) {
                    foreach ($row['@children'] as $fieldKey => $field) {
                        if (isset($field['@children'])) {
                            if ($field['@children']) {
                                $fields2 = [];
                                foreach ($field['@children'] as $fieldKey2 => $field2) {
                                    $fields2[$fieldKey2] = $field2['@text'];
                                }
                                $row['@children'][$fieldKey] = $fields2;
                            } else {
                                $row['@children'][$fieldKey] = [];
                            }
                        } else {
                            $row['@children'][$fieldKey] = array_key_exists('@text', $field)
                                ? $field['@text']
                                : '';
                        }
                    }
                    $rows[] = $row['@children'];
                }
            }
            $this->_data['success'] = true;
            $this->_data['data'] = $rows;
        } else {
            $this->_data['code'] = $result->getCode();
            $this->_data['message'] = $result->getDescription();
        }

        return $this->_data;
    }

    public function reservation($form)
    {
        ignore_user_abort(true);
        set_time_limit(0);
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $this->_data['message'] = '';
        $authKey = self::getAuthKey();
        if ($authKey) {
            $aircraft = $form['aircraft'];
            if ($form['passengers']) {
                $params = [
                    'action' => 'booking',
                    'recommendation_id' => $aircraft['id'],
                    'auth_key' => $authKey,
                ];
                $key = 0;
                foreach ($form['passengers'] as $passenger) {
                    $params['passengers[' . $key . '][type]'] = $passenger->type;
                    $params['passengers[' . $key . '][firstname]'] = $passenger->name;
                    $params['passengers[' . $key . '][lastname]'] = $passenger->surname;
                    $params['passengers[' . $key . '][birthday]'] = \DateTime::createFromFormat('d.m.Y',
                        $passenger->birthday)
                        ->format('d-m-Y');
                    $params['passengers[' . $key . '][gender]'] = $passenger->gender;
                    $params['passengers[' . $key . '][citizenship]'] = isset($passenger->citizenship)
                        ? $passenger->citizenship
                        : '';
                    $params['passengers[' . $key . '][docnum]'] = isset($passenger->docnum)
                        ? $passenger->docnum
                        : '';
                    if ($passenger->doc_expire_date) {
                        $params['passengers[' . $key . '][doc_expire_date]'] = isset($passenger->doc_expire_date)
                            ? \DateTime::createFromFormat('d.m.Y', $passenger->doc_expire_date)
                                ->format('d-m-Y')
                            : '';
                    }
                    $key++;
                }
                $result = $this->get('avia', 'book', $params);
                if ($result->isSuccess()) {
                    $reservation = [
                        'locator' => $result->booking['@children']['locator']['@text'],
                        'ticketing_time_limit' => $result->booking['@children']['ticketing-time-limit']['@text'],
                    ];
                    $this->_data['reservation'] = $reservation;
                } else {
                    $this->_data['reservation'] = false;
                    if (isset($result->errors['@children']) && $result->errors['@children']) {
                        $errors = $result->errors['@children'];
                        reset($errors);
                        $error = current($errors);
                        $this->_data['code'] = $error['code'];
                    } else {
                        $this->_data['code'] = $result->getCode();
                        $this->_data['message'] = $result->getDescription();
                    }
                }
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = 500;
                $this->_data['message'] = 'Не выбраны места';
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['message'] = 'Неверный ключ авторизации';
        }

        return $this->_data;
    }

    public function bookingsList($limit = 10, $offset = 0, $status = null, \DateTime $timeFrom = null, \DateTime $timeTo = null)
    {
        if ($offset > 0) {
            $offset++;
        }
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $this->_data['message'] = '';
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'limit' => $limit,
                'page' => $offset,
            ];
            if ($status) {
                $params['statuses'] = $status;
            }
            if ($timeFrom) {
                $params['date_from'] = $timeFrom->format('d.m.Y');
            }
            if ($timeTo) {
                $params['date_until'] = $timeTo->format('d.m.Y');
            }
            $result = $this->get('avia', 'bookings_list', $params);
            if ($result->isSuccess()) {
                $bookings = $result->bookings;
                foreach ($bookings['@children'] as $booking) {
                    $locations = [];
                    foreach ($booking['@children']['locations']['@children'] as $location) {
                        $locations[] = [
                            'other_supplier_iata' => $location['@children']['other-supplier-iata']['@text'],
                            'arrival_time' => $location['@children']['arrival-time']['@text'],
                            'booking_class' => $location['@children']['booking-class']['@text'],
                            'aircraft' => $location['@children']['aircraft']['@text'],
                            'supplier_iata' => $location['@children']['supplier-iata']['@text'],
                            'arrival_country' => $location['@children']['arrival-country']['@text'],
                            'departure_city' => $location['@children']['departure-city']['@text'],
                            'carrier' => $location['@children']['carrier']['@text'],
                            'departure_country' => $location['@children']['departure-country']['@text'],
                            'arrival_terminal' => $location['@children']['arrival-terminal']['@text'],
                            'supplier_name' => $location['@children']['supplier-name']['@text'],
                            'flight_duration' => $location['@children']['flight-duration']['@text'],
                            'departure_terminal' => $location['@children']['departure-terminal']['@text'],
                            'segment_id' => $location['@children']['segment-id']['@text'],
                            'trip_part' => $location['@children']['trip-part']['@text'],
                            'baggage' => $location['@children']['baggage']['@text'],
                            'departure_location' => $location['@children']['departure-location']['@text'],
                            'departure_time' => $location['@children']['departure-time']['@text'],
                            'arrival_city' => $location['@children']['arrival-city']['@text'],
                            'arrival_location' => $location['@children']['arrival-location']['@text'],
                            'other_supplier_name' => $location['@children']['other-supplier-name']['@text'],
                            'aircraft_code' => $location['@children']['aircraft-code']['@text'],
                            'booking_class_type' => $location['@children']['booking-class-type']['@text'],
                        ];
                    }
                    $passengers = [];
                    foreach ($booking['@children']['passengers']['@children'] as $passenger) {
                        $passengers[] = [
                            'age_type' => $passenger['@children']['age-type']['@text'],
                            'docnum' => $passenger['@children']['docnum']['@text'],
                            'doc_expire_date' => $passenger['@children']['doc-expire-date']['@text'],
                            'ticket_number' => $passenger['@children']['ticket-number']['@text'],
                            'citizenship' => $passenger['@children']['citizenship']['@text'],
                            'lastname' => $passenger['@children']['lastname']['@text'],
                            'firstname' => $passenger['@children']['firstname']['@text'],
                            'patronymic' => @$passenger['@children']['patronymic']['@text'],
                            'birthday' => $passenger['@children']['birthday']['@text'],
                            'gender' => $passenger['@children']['gender']['@text'],
                            'amounts' => $passenger['@children']['amounts']['@text'],
                        ];
                    }
                    $item = [
                        'locator' => $booking['@children']['locator']['@text'],
                        'status' => $booking['@children']['status']['@text'],
                        'final_price' => $booking['@children']['final-price']['@children']['uah']['@text'],
                        'booking_date' => $booking['@children']['booking-date']['@text'],
                        'expiration_date' => $booking['@children']['expiration-date']['@text'],
                        'vendor_locators' => $booking['@children']['vendor-locators']['@text'],
                        'reward' => $booking['@children']['reward']['@text'],
                        'transaction_total' => $booking['@children']['transaction-total']['@text'],
                        'payed_currency' => $booking['@children']['payed-currency']['@text'],
                        'reservation_id' => @$booking['@children']['reservation-id']['@text'],
                        'validating_supplier' => $booking['@children']['validating-supplier']['@text'],
                        'source_locator' => @$booking['@children']['source-locator']['@text'],
                        'is_lcc' => @$booking['@children']['is-lcc']['@text'],
                        'is_multi' => @$booking['@children']['is-multi']['@text'],
                    ];
                    $item['passengers'] = $passengers;
                    $item['locations'] = $locations;
                    $this->_data['data']['items'][] = $item;
                }
                $this->_data['data']['total'] = $result->count['@children']['items']['@text'];
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode();
                $this->_data['message'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['message'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function bookingCancel($locatorId)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'locator' => $locatorId,
            ];
            $result = $this->get('avia', 'cancel_pnr', $params);
            if ($result->isSuccess()) {
                $status = $result->status;
                if (isset($status['@children']['success']['@text'])
                    && $status['@children']['success']['@text'] === 'true'
                ) {
                    $this->_data['data'] = ['confirm' => 'true'];
                }
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = '';
        }

        return $this->_data;
    }

    public function eticket($locatorId)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'locator' => $locatorId,
                'type' => 'pdf',
            ];
            $result = $this->get('avia', 'eticket', $params);
            if ($result->isSuccess() && isset($result->file['@children']['content']['@text'])
                && $result->file['@children']['content']['@text']
            ) {
                $this->_data['data'] = $result->file['@children']['content']['@text'];
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode() == 0
                    ? 500
                    : $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = '';
        }

        return $this->_data;
    }

    public static function getPassengerTypeByAge($age)
    {
        foreach (self::PASSENGER_AGE_RANGES as $key => $range) {
            if ($age >= $range['min'] && $age <= $range['max']) {
                return $key;
            }
        }

        return false;
    }
}
