<?php
namespace common\providers\tickets;

use Yii;

class TicketsResponse
{
    const ERROR_NONE = 0;
    const OPERATION_TYPE_RESERVATION_ONLY = 1;
    const OPERATION_TYPE_EXCHANGE = 2;
    const OPERATION_TYPE_E_TICKET = 3;

    private $service = null;
    private $method = null;
    private $code = 0;
    private $description = '';
    private $execution_time = 0;
    private $_data = [];

    function __construct($service, $method, array $response, $url)
    {
        $this->service = $service;
        $this->method = $method;
        $executionTime = -1;
        if ($service == 'avia') {
            if (isset($response['errors']['@children'])) {
                $code = 500;
                $errors = [];
                if ($response['errors']['@children']) {
                    foreach ($response['errors']['@children'] as $row) {
                        $errors[] = $row['@text'];
                    }
                }
                $description = implode(' | ', $errors);
            } elseif (isset($response['result']['code']) && $response['result']['code'] != '0') {
                $code = -1;
                $description = isset($response['result']['description'])
                    ? $response['result']['description']
                    : '';
            } else {
                $code = isset($response['result']['code'])
                    ? $response['result']['code']
                    : 0;
                $description = isset($response['result']['description'])
                    ? $response['result']['description']
                    : '';
            }
        } else {
            $code = '';
            $response['result']['code'] = (string)$response['result']['code'];
            if (strlen($response['result']['code']) > 0) {
                for ($i = 0; $i < strlen($response['result']['code']); $i++) {
                    $char = $response['result']['code'][$i];
                    if (is_numeric($char)) {
                        $code .= $char;
                    } else {
                        $code .= ord($char);
                    }
                }
                if ($code != '0') {
                    $code = intval($code);
                    if ($code == 0) {
                        $code = 2147483647;
                    }
                }
            } else {
                $code = 500;
            }
            $description = (string)$response['result']['description'];
            $executionTime = floatval($response['result']['execution_time']);
        }
        $this->code = intval($code);
        $this->description = $description;
        $this->execution_time = $executionTime;
        foreach ($response as $key => $item) {
            if ($key == 'result') {
                continue;
            }
            $this->_data[$key] = $item;
        }
        if ($this->code != self::ERROR_NONE) {
            if (in_array($this->code, [68])) {
                Yii::warning('#' . $this->code . ' ' . $this->description . PHP_EOL . 'REQUEST_URI=' . $url,
                    __METHOD__);
            } else {
                Yii::error('#' . $this->code . ' ' . $this->description . PHP_EOL . 'REQUEST_URI=' . $url, __METHOD__);
            }
        }

//		$logItem = [
//			'url' => $url,
//			'response' => $response
//		];
//		Yii::info($logItem, __METHOD__);
    }

    public function isSuccess()
    {
        return ($this->code == self::ERROR_NONE);
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        return $this->description = $description;
    }

    function __get($name)
    {
        if (array_key_exists($name, $this->_data)) {
            return $this->_data[$name];
        }

        return null;
    }

    public function __set($name, $value)
    {

    }

    public function __isset($name)
    {
        if (array_key_exists($name, $this->_data)) {
            return true;
        }

        return false;
    }
}
