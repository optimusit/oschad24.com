<?php
namespace common\providers\tickets;

use Yii;

class GdTickets extends Tickets
{
    const MAX_PASSENGERS = 4;
    const OPERATION_TYPE_E = 3; // Покупка электронных билетов
    const OPERATION_TYPE_V = 2; // Покупка билетов с последующим обменом на бумажные в билетной кассе
    const OPERATION_TYPE_B = 1; // Бронирование билетов с последующей оплатой стоимости в билетной кассе (Не используется)

    const LUGGAGE_CODE_ANIMAL = 1;
    const LUGGAGE_CODE_FACILITIES = 2;
    const LUGGAGE_CODE_EXCESS_BAGGAGE = 3;
    const LUGGAGE_DESCRIPTION_ANIMAL = 'Тварини/птиці';
    const LUGGAGE_DESCRIPTION_FACILITIES = 'Апаратура';
    const LUGGAGE_DESCRIPTION_EXCESS_BAGGAGE = 'Надлишок';

    const CAR_CLASS_FIRST = 'first';
    const CAR_CLASS_SECOND = 'second';
    const CAR_CLASS_THIRD = 'third';
    const CAR_CLASS_RESERVED = 'reserved';
    const CAR_CLASS_NON_RESERVED = 'non_reserved';
    const CAR_CLASS_COMFORTABLE = 'comfortable';

    const SERVICES = [
        1 => 'Харчовий набір',
        2 => 'Чай',
        3 => '2 Чая',
    ];

    const PRIVILEGES_STUDENT = 6;

    const TRAIN_MODEL_OTHER = 0;
    const TRAIN_MODEL_HYUNDAI = 1;
    const TRAIN_MODEL_SCODA = 2;
    const TRAIN_MODEL_TARPAN = 3;
    const TRAIN_MODEL_KVBZ = 4;
    const TRAIN_MODELS = [
        self::TRAIN_MODEL_OTHER => 'Other',
        self::TRAIN_MODEL_HYUNDAI => 'Hyundai',
        self::TRAIN_MODEL_SCODA => 'Scoda',
        self::TRAIN_MODEL_TARPAN => 'Tarpan',
        self::TRAIN_MODEL_KVBZ => 'KVBZ',
    ];

    const SEAT_TYPE_FREE_UPPER = 1;
    const SEAT_TYPE_FREE_LOWER = 2;
    const SEAT_TYPE_FREE_UPPER_SIDE = 3;
    const SEAT_TYPE_FREE_LOWER_SIDE = 4;
    const SEAT_TYPE_FREE_MIDDLE = 5;
    const SEAT_TYPE_OCCUPIED_UPPER = 11;
    const SEAT_TYPE_OCCUPIED_LOWER = 12;
    const SEAT_TYPE_OCCUPIED_UPPER_SIDE = 13;
    const SEAT_TYPE_OCCUPIED_LOWER_SIDE = 14;
    const SEAT_TYPE_OCCUPIED_MIDDLE = 15;
    const SEAT_TYPES = [
        self::SEAT_TYPE_FREE_UPPER => 'Вільне верхнє місце',
        self::SEAT_TYPE_FREE_LOWER => 'Вільне нижнє місце',
        self::SEAT_TYPE_FREE_UPPER_SIDE => 'Вільне бокове верхнє місце',
        self::SEAT_TYPE_FREE_LOWER_SIDE => 'Вільне бокове нижнє місце',
        self::SEAT_TYPE_FREE_MIDDLE => 'Вільне середнє місце',
        self::SEAT_TYPE_OCCUPIED_UPPER => 'Зайняте верхнє місце',
        self::SEAT_TYPE_OCCUPIED_LOWER => 'Зайняте нижнє місце',
        self::SEAT_TYPE_OCCUPIED_UPPER_SIDE => 'Зайняте бокове верхнє місце',
        self::SEAT_TYPE_OCCUPIED_LOWER_SIDE => 'Зайняте бокове нижнє місце',
        self::SEAT_TYPE_OCCUPIED_MIDDLE => 'Зайняте середнє місце',
    ];

    public function __construct()
    {
        parent::__construct();
        if (isset(Yii::$app->session)) {
            $this->sessionId = Yii::$app->session['gdSession'];
        } else {
            $this->sessionId = '';
        }
    }

//	public static function getInstance()
//	{
//		if (null === self::$_instance) {
//			self::$_instance = new self();
//		}
//
//		return self::$_instance;
//	}

    static function getOperationTypeLabel($key)
    {
        $typeLabel = [
            self::OPERATION_TYPE_V => Yii::t('tickets', 'Ваучер'),
            self::OPERATION_TYPE_E => Yii::t('tickets', 'Електронний квиток'),
        ];
        if (array_key_exists($key, $typeLabel)) {
            return $typeLabel[$key];
        }

        return null;
    }

    static function getOperationHint($key)
    {
        $typeLabel = [
            2 => Yii::t('tickets',
                'Купівля квитків з подальшим обміном на паперові в квитковій касі. Роздруковувати ваучер не обов\'язково, досить повідомити касиру номер квитка.'),
            3 => Yii::t('tickets', 'Купівля електронних квитків. Друкувати електронний квиток потрібно обов\'язково.'),
        ];
        if (array_key_exists($key, $typeLabel)) {
            return $typeLabel[$key];
        }

        return null;
    }

    static function getClassLabel($key)
    {
        $classLabel = [
            'first' => Yii::t('tickets', 'Люкс'),
            'second' => Yii::t('tickets', 'Купе'),
            'third' => Yii::t('tickets', 'Плацкарт'),
            'reserved' => Yii::t('tickets', 'Сидячий'),
            'non_reserved' => Yii::t('tickets', 'Загальний'),
            'comfortable' => Yii::t('tickets', 'М\'який'),
        ];
        if (array_key_exists($key, $classLabel)) {
            return $classLabel[$key];
        }

        return null;
    }

    static function getTrainClassLabel($key)
    {
        $trainClassLabel = [
            1 => Yii::t('tickets', 'Нефірмовий'),
            4 => Yii::t('tickets', 'Фірмовий'),
            8 => Yii::t('tickets', 'Експрес'),
        ];
        if (array_key_exists($key, $trainClassLabel)) {
            return $trainClassLabel[$key];
        }

        return null;
    }

    static function getTrainSpeedLabel($key)
    {
        $trainClassLabel = [
            1 => Yii::t('tickets', 'Пасажирський'),
            2 => Yii::t('tickets', 'Швидкий'),
            4 => Yii::t('tickets', 'Швидкісний'),
        ];
        if (array_key_exists($key, $trainClassLabel)) {
            return $trainClassLabel[$key];
        }

        return null;
    }

    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
        Yii::$app->session['gdSession'] = $this->sessionId;
    }

    public function get($service, $method, $params = [])
    {
        if ($this->sessionId) {
            $params['session_id'] = $this->sessionId;
        }
        $result = parent::get($service, $method, $params);
        if ($result->isSuccess()) {
            if ($result->session) {
                $this->setSessionId($result->session['id']);
            }
        } else {
            $code = $result->getCode();
            if ($code == 1700 || $code == 14201) {
                $result->setCode(110);
                $result->setDescription(Yii::t('tickets',
                    'Запит не може бути виконаний із-за заборони на оформлення місць, встановленого Залізницею відправлення поїзда для заданого напряму на дану дату.'));
            } elseif ($code == 12023) {
                $result->setCode(110);
                $result->setDescription(Yii::t('tickets', 'Сесія скінчилась'));
            }
        }

        return $result;
    }

    public function searchStations($q)
    {
        $result = $this->get('rail', 'station', [
            'name' => $q,
            'lang' => Yii::$app->language
        ]);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            foreach ($result->stations['@children'] as $row) {
                $internal = $row['internal'];
                $this->_data['data'][$row['code']] = [
                    'id' => $row['code'],
                    'value' => $row['name'] . ' ' . $row['railroad'],
                    'internal' => $internal,
                    'level' => 1,
                    'order' => 999
                ];
                if (isset($row['@children'])) {
                    foreach ($row['@children'] as $sub) {
                        $this->_data['data'][$sub['code']] = [
                            'id' => $sub['code'],
                            'value' => $sub['name'],
                            //'label' => '-' . $sub['name'],
                            'internal' => $internal,
                            'level' => 2,
                            'order' => 999
                        ];
                    }
                }
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function searchTrains($params)
    {
        $result = $this->get('rail', 'search', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            foreach ($result->trains['@children'] as $row) {
                $this->_data['data'][] = $row;
            }
            for ($i = 0; $i < count($this->_data['data']); $i++) {
                if (array_key_exists('class', @$this->_data['data'][$i]['@children'])) {
                    $classes = [@$this->_data['data'][$i]['@children']['class']];
                } else {
                    $classes = @$this->_data['data'][$i]['@children'];
                }
                $this->_data['data'][$i]['class_list'] = $classes;
                unset($this->_data['data'][$i]['@children']);
            }
            static::cleanArray($this->_data, '@name');
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function trainRoute($params)
    {
        $result = $this->get('rail', 'timetable/train_route', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['data'] = [
                'number' => $result->train['number'],
                'name' => $result->train['name'],
                'first_station' => $result->train['first_station'],
                'last_station' => $result->train['last_station'],
            ];
            if (array_key_exists('@children', $result->train['@children']['stations'])) {
                foreach ($result->train['@children']['stations']['@children'] as $row) {
                    $row = $row['@children'];
                    $station = [
                        'name' => array_key_exists('@text', $row['name'])
                            ? $row['name']['@text']
                            : '',
                        'time_departure' => array_key_exists('@text', $row['time-departure'])
                            ? $row['time-departure']['@text']
                            : '',
                        'time_arrival' => array_key_exists('@text', $row['time-arrival'])
                            ? $row['time-arrival']['@text']
                            : '',
                        'code' => array_key_exists('@text', $row['code'])
                            ? $row['code']['@text']
                            : '',
                        'longitude' => '',
                        'latitude' => '',
                    ];
                    if (array_key_exists('@text', $row['longitude'])
                        && array_key_exists('@text', $row['latitude'])
                    ) {
                        $station['longitude'] = $row['longitude']['@text'];
                        $station['latitude'] = $row['latitude']['@text'];
                    }
                    $this->_data['data']['stations'][] = $station;
                }
            } else {
                $this->_data['data']['stations'] = [];
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function getTrainInfo($params)
    {
        $result = $this->get('rail', 'train', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            foreach ($result->train as $key => $row) {
                if (is_array($row)) {
                    continue;
                }
                $this->_data['data'][$key] = $row;
            }
            foreach ($result->train['@children'] as $row) {
                $row['cars'] = $row['@children']['cars'];
                $row['exchanges'] = $row['@children']['exchanges'];
                unset($row['@children']);
                if (count($row['cars']['@children']) == 1) {
                    $row['cars'][0] = $row['cars']['@children']['car'];
                } else {
                    $row['cars'] = $row['cars']['@children'];
                }
                unset($row['cars']['@children']);
                $row['exchanges'] = $row['exchanges']['@children'];
                $row['services'] = self::getServices($row['services']);
                $this->_data['data']['classes'][] = $row;
            }
            static::cleanArray($this->_data, '@name');
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    public function getCarInfo($params)
    {
        $result = $this->get('rail', 'car', $params);
        if ($result->isSuccess()) {
            $this->_data['success'] = true;
            $this->_data['message'] = '';
            $this->_data['data'] = $result->car;
            $this->_data['data']['services'] = self::getServices($this->_data['data']['services']);
            $seats = [];
            $seatTypes = self::SEAT_TYPES;
            foreach ($this->_data['data']['@children'] as $coupe) {
                foreach ($coupe['@children'] as $seat) {
                    $seats[] = [
                        'number' => $seat['number'],
                        'type' => $seat['type'],
                        'label_type' => array_key_exists($seat['type'], $seatTypes)
                            ? $seatTypes[$seat['type']]
                            : '',
                        'coupe' => $coupe['number'],
                    ];
                }
            }
            $this->_data['data']['seats'] = $seats;
            unset($this->_data['data']['@children']);
            static::cleanArray($this->_data, '@name');
        } else {
            $this->_data['success'] = false;
            $this->_data['message'] = $result->getDescription();
        }
        $this->_data['code'] = $result->getCode();

        return $this->_data;
    }

    private static function getServices($services)
    {
        $services = explode(',', $services);
        $allServices = self::SERVICES;
        $result = [];
        foreach ($services as $service) {
            if (array_key_exists($service, $allServices)) {
                $result[$service] = $allServices[$service];
            }
        }

        return $result;
    }

    public function getAuthKey()
    {
        return $this->signin();
    }

    public function reservation($form)
    {
        ignore_user_abort(true);
        set_time_limit(0);
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            if ($form['selected_seats']) {
                $passengers = [];
                $selectedSeats = [];
                foreach ($form['selected_seats'] as $seat) {
                    $selectedSeats[] = intval($seat['seat']);
                    $passengers[] = $seat['name'] . ':' . $seat['surname'] . ':' . (isset($seat['patronymic'])
                            ? $seat['patronymic']
                            : '') . ':' . (isset($seat['date'])
                            ? $seat['date']
                            : '') . ':' . (isset($seat['document'])
                            ? $seat['document']
                            : '') . ':' . (isset($seat['nationality'])
                            ? $seat['nationality']
                            : '') . ':' . (isset($seat['gender'])
                            ? $seat['gender']
                            : '') . ':' . (isset($seat['birth_place'])
                            ? $seat['birth_place']
                            : '') . ':' . (isset($seat['residence'])
                            ? $seat['residence']
                            : '') . ':' . (isset($seat['document_type'])
                            ? $seat['document_type']
                            : '') . ':' . (isset($seat['animal'])
                            ? $seat['animal']
                            : '') . ':' . (isset($seat['facilities'])
                            ? $seat['facilities']
                            : '') . ':' . (isset($seat['excess_baggage'])
                            ? $seat['excess_baggage']
                            : '');
                }
                $params = [
                    'auth_key' => $authKey,
                    'passengers' => implode('|', $passengers),
                    'range' => implode(',', $selectedSeats),
                    'no_clothes' => (isset($form['no_clothes']) && $form['no_clothes'])
                        ? 1
                        : 0,
                    'uuid' => $this->getUID(),
                    'operation_type' => $form['operation_type'],
                ];
                if (is_array($form['services'])) {
                    $params['services'] = implode(',', $form['services']);
                }
                $result = $this->get('rail', 'reservation', $params);
                if ($result->isSuccess()) {
                    $this->_data['reservation'] = $result->reservation;
                } else {
                    $this->_data['reservation'] = false;
                    $this->_data['code'] = $result->getCode();
                    $this->_data['message'] = $result->getDescription();
                    $this->_data['back_param'] = $params;
                }
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = 500;
                $this->_data['message'] = Yii::t('tickets', 'Не вибрані посадочні місця');
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['message'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function paymentCommit($id, $amount)
    {
        return $this->commit('gd', $id, $amount, $this->getShopApiKeyFor('rail'), $this->getShopSecretKeyFor('rail'));
    }

    public function bookingShow($id)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $this->_data['message'] = '';
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'reservation_id' => $id,
            ];
            $result = $this->get('rail', 'booking_show', $params);
            if ($result->isSuccess()) {
                $this->_data['data'] = $result->booking;
                $this->_data['data']['exchanges'] = $this->_data['data']['@children']['exchanges']['@children'];
                $this->_data['data']['documents'] = $this->_data['data']['@children']['documents']['@children'];
                unset($this->_data['data']['@children']);
                static::cleanArray($this->_data['data'], '@name');
                if (array_key_exists('passenger', $this->_data['data']['documents'])) {
                    $passenger = $this->_data['data']['documents']['passenger'];
                    unset($this->_data['data']['documents']['passenger']);
                    $this->_data['data']['documents'][] = $passenger;
                }
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode();
                $this->_data['message'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['message'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function bookingsList($limit = 10, $offset = 0, $status = null, \DateTime $timeFrom = null, \DateTime $timeTo = null)
    {
        $this->_data = [
            'success' => true,
            'code' => 0,
            'message' => ''
        ];
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'limit' => $limit,
                'offset' => $offset,
            ];
            if ($status) {
                $params['status'] = $status;
            }
            if ($timeFrom) {
                $params['time_from'] = $timeFrom->format('d-m-Y') . ' 00:00';
            }
            if ($timeTo) {
                $params['time_to'] = $timeTo->format('d-m-Y') . ' 00:00';
            }
            $result = $this->get('rail', 'bookings_list', $params);
            if ($result->isSuccess()) {
                $this->_data['data'] = $result->bookings;
                $this->_data['data']['items'] = $this->_data['data']['@children'];
                unset($this->_data['data']['@name']);
                static::cleanArray($this->_data['data'], '@children');
                static::cleanArray($this->_data['data']['items'], '@name');
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode();
                $this->_data['message'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['message'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    /**
     * В течении 40 мин после оплаты можно сделать полный возврат средств и отменить покупку
     */
    public function bookingRevoke($id)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $this->_data['msg'] = '';
        /*if (TEST_SERVER) {
            $this->_data['data'] = array('confirm' => true, 'return_cost' => null);
            return $this->_data;
        }*/

        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'reservation_id' => $id,
            ];
            $result = $this->get('rail', 'revoke', $params);
            if ($result->isSuccess()) {
                if ($result->booking['revoked'] === '') {
                    $confirm = true;
                } else {
                    $confirm = false;
                }
                $this->_data['data'] = [
                    'confirm' => $confirm,
                    'return_cost' => null
                ];
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = '';
        }

        return $this->_data;
    }

    public function eticket($id)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'reservation_id' => $id,
            ];
            $result = $this->get('rail', 'booking_pdf', $params);
            if ($result->isSuccess() && isset($result->pdf['base64_string']) && $result->pdf['base64_string']) {
                $this->_data['data'] = $result->pdf['base64_string'];
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode() == 0
                    ? 500
                    : $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function timeTable($stationCode, $date)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $result = $this->get('rail', 'timetable/station', [
            'station_code' => $stationCode,
            'date' => $date
        ]);
        if ($result->isSuccess()) {
            $this->_data['data'] = $result->trains;
            static::cleanArray($this->_data['data'], '@name');
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = $result->getCode() == 0
                ? 500
                : $result->getCode();
            $this->_data['msg'] = $result->getDescription();
        }

        return $this->_data;
    }

    public function nextTechStop()
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $result = $this->get('rail', 'next_tech_stop');
            if ($result->isSuccess()) {
                $this->_data['data'] = $result->data;
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode() == 0
                    ? 500
                    : $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function cancel($reservation_id)
    {
        $data['success'] = true;
        $data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'reservation_id' => $reservation_id,
            ];
            $result = $this->get('rail', 'cancel', $params);
            if ($result->isSuccess()) {
                $this->_data['success'] = true;
                $this->_data['data']['cancelled'] = $result->booking['cancelled'];
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode() == 0
                    ? 500
                    : $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function getRefundAmount($reservationId, array $passengerIds)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'reservation_id' => $reservationId,
                'passenger_id' => implode(',', $passengerIds),
            ];
            $result = $this->get('rail', 'get_refund_amount', $params);
            if ($result->isSuccess()) {
                $this->_data['success'] = true;
                //$this->_data['data']['cancelled'] = $result->booking['cancelled'];
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode() == 0
                    ? 500
                    : $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public function makeRefund($reservationId, array $passengerIds)
    {
        $this->_data['success'] = true;
        $this->_data['code'] = 0;
        $authKey = self::getAuthKey();
        if ($authKey) {
            $params = [
                'auth_key' => $authKey,
                'reservation_id' => $reservationId,
                'passenger_id' => implode(',', $passengerIds),
            ];
            $result = $this->get('rail', 'make_refund', $params);
            if ($result->isSuccess()) {
                $this->_data['success'] = true;
                //$this->_data['data']['cancelled'] = $result->booking['cancelled'];
            } else {
                $this->_data['success'] = false;
                $this->_data['code'] = $result->getCode() == 0
                    ? 500
                    : $result->getCode();
                $this->_data['msg'] = $result->getDescription();
            }
        } else {
            $this->_data['success'] = false;
            $this->_data['code'] = 500;
            $this->_data['msg'] = Yii::t('tickets', 'Невірний ключ авторизації');
        }

        return $this->_data;
    }

    public static function GetAvailableOperationTypes()
    {
        return [
            self::OPERATION_TYPE_E,
            self::OPERATION_TYPE_V,
            //self::OPERATION_TYPE_B,// этот тип операции не используется в нашей системе
        ];
    }

    public static function getServicesAll()
    {
        $result = [];
        foreach (self::SERVICES as $key => $value) {
            $result[] = [
                'id' => $key,
                'label' => $value,
            ];
        }

        return $result;
    }
}
