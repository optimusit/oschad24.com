<?php

namespace common\providers\webclever;

use Yii;

/**
 * Class WebClever
 * @package common\providers\webclever
 *
 * @method array getCountries() getCountries() Возвращает список всех доступных стран
 * @method array getCities() getCities() Возвращает список всех доступных городов
 * @method array getOrders() getOrders() Возвращает список всех заказов
 * @method array getTrees() getTrees() Возвращает список всех ???
 * @method array getEventTags() getEventTags() Возвращает список всех тегов
 * @method array getEventList() getEventList(array $params) Возвращает список мероприятий (['city' => 0, 'limit' => 100, 'offset' => 0,])
 * @method array getMapInfo() getMapInfo(array $params) Возвращает карту зала (['event_id' => 123])
 * @method array getSectors() getSectors(array $params) Возвращает список секторов зала (['event_id' => 123])
 * @method array getPrices() getPrices(array $params) Цены (['event_id' => 123, 'sector_id' => 456])
 * @method array getPlaces() getPlaces(array $params) Возвращает список мест (['event_id' => 123, 'sector_id' => 456])
 * @method array getSliderEvent() getSliderEvent(array $params) Возвращает список мероприятий для отображения в слайдере для указанного города (['city_id' => 123])
 * @method array getSlider() getSlider(array $params) Возвращает список слайдеров (['slider_id' => 0])
 * @method array getSliderByCity() getSliderByCity(array $params) Возвращает список слайдеров (['city_id' => 1])
 */
class WebClever
{
    const PREFIX = 'WEBCLEVER';

    protected $_apiUrl = null;
    protected $_token = null;
    protected $_data = [];

    public function __construct($sessionId = null)
    {
        $this->_sessionId = $sessionId;
        $this->_token = $this->getToken();
//		if (is_null($this->_sessionId)) {
//			$this->_sessionId = self::Login();
//		}
    }

    public function getApiUrl()
    {
        return Yii::$app->params['provider']['webclever']['api_url'];
    }

    public function getToken()
    {
        return Yii::$app->params['provider']['webclever']['token'];
    }

    protected function errorMsg($code, $description)
    {
        $data = [
            'response' => [
                'result' => [
                    'code' => $code,
                    'description' => $description,
                    'execution_time' => null
                ],
                'data' => []
            ]
        ];

        return $data;
    }

    private function request($method, array $params = [])
    {
        $url = $this->getAPIUrl() . $method . '?';
        $params['token'] = $this->_token;
//		$params1 = [];
//		foreach ($params as $key => $value) {
//			$params1[] = $key . '=' . rawurlencode($value);
//		}
        $url .= http_build_query($params);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
//		curl_setopt($curl, CURLOPT_HTTPHEADER, [
//			'Accept: application/json',
//			'Content-Type: application/json'
//		]);
        $data = curl_exec($curl);
        $logItem = [
            'request' => [
                'url' => $url,
                'params' => $params
            ],
            'response' => $data,
            'response_format' => 'json',
        ];
        Yii::info($logItem, __METHOD__);
        if ($data === false) {
            $data = $this->errorMsg(500,
                'Connection error ' . curl_errno($curl) . '. ' . curl_error($curl) . ' ' . $url);
            Yii::error($data, __METHOD__);
        } else {
            $result = $data;
            $data = json_decode($result);
            if (!$data) {
                $data = $this->errorMsg(500, 'Decoding error. Data: ' . var_export($result, true));
            }
        }
        curl_close($curl);
        $this->_data['dialog'] = $logItem;

        return new WebCleverResponse($method, $data, $url);
    }

    public function __call($name, $arguments)
    {
        if (in_array($name, [
            'getCountries',
            'getCities',
            'getOrders',
            'getTrees',
            'getEventTags',
            'getEventList',
            'getMapInfo',
            'getSectors',
            'getPrices',
            'getPlaces',
            'getSliderEvent',
            'getSlider',
            'getSliderByCity',
        ])) {
            if (count($arguments) > 0) {
                $params = $arguments[0];
            } else {
                $params = [];
            }
            $result = $this->request($name, $params);
            if ($result->isSuccess()) {
                $this->_data['success'] = true;
                $this->_data['message'] = '';
                $this->_data['data'] = $result->getData();
            } else {
                $this->_data['success'] = false;
                $this->_data['message'] = $result->getDescription();
                $this->_data['data'] = [];
            }

            return $this->_data;
        } elseif (method_exists($this, $name)) {
            $this->{$name}($arguments);
        } else {
            throw new \Exception('Недопустимый метод');
        }
    }

//$webclever = new TmsQuery();
//$webclever->token = 5345843;
//$webclever->api_url = 'http://tms.webclever.in.ua/api';
    //$list = $webclever->getCountryList();
    //$list = $webclever->SearchCity(1, 'за');
//		$list = $webclever->getEvents([
//			'limit' => 100,
//			'offset' => 0,
//			'city' => 0,
//		]);
//		$sliderEvent = $webclever->getSliderEvents(3);
//		$trees = $webclever->getTrees();
//		$eventTags = $webclever->getEventTags([]);
//		$mapInfo = $webclever->getMapInfo(3);
//		$event = $webclever->getEvent(['id' => 3]);
//		$previewScheme = $webclever->previewScheme(3);
//		$sectors = $webclever->getSectors(3);
//		$eventTags = $webclever->getEvent(['id' => 3]);
}
