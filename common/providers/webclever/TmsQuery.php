<?php

namespace common\providers\webclever;

use yii\web\HttpException;

class TmsQuery extends Tms
{
    /**
     * @param array $params key=>value that will be adding as GET parameters to query
     * @return array
     * @throws HttpException
     */
    public function getEvents($params)
    {
        $params = $this->createQuery($params);
        $url = $this->api_url . "/getEventList/?" . $params;

        return $this->makeRequest($url);
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function getEvent($params)
    {
        $url = $this->api_url . "/getEvent/?" . $this->createQuery($params);

        return $this->makeRequest($url);
    }

    public function getEventTags($params)
    {
        $url = $this->api_url . "/getEventTags/?" . $this->createQuery($params);

        return $this->makeRequest($url);
    }

    public function getTrees()
    {
        $url = $this->api_url . "/getTrees/?" . $this->createQuery();

        return $this->makeRequest($url);
    }

    public function getMapInfo($eventId)
    {
        $url = $this->api_url . "/getMapInfo/?" . $this->createQuery(['event_id' => $eventId]);

        return $this->makeRequest($url);
    }

    public function getSectors($eventId)
    {
        $url = $this->api_url . "/getSectors/?" . $this->createQuery(['event_id' => $eventId]);

        return $this->makeRequest($url);
    }

    public function previewScheme($eventId)
    {
        $url = $this->api_url . "/previewScheme/?" . $this->createQuery(['event_id' => $eventId]);

        return $this->makeRequest($url);
    }

    public function getSliderEvents($cityId)
    {
        $url = $this->api_url . "/getSliderEvent/?" . $this->createQuery(['city_id' => $cityId]);

        return $this->makeRequest($url);
    }

    /**
     * @return mixed
     * pairs key => value
     * @throws HttpException
     */
    public function getCityList()
    {
        $url = $this->api_url . "/getCities?" . $this->createQuery();

        return $this->makeRequest($url);
    }

    public function getCountryList()
    {
        $url = $this->api_url . "/getCountries?" . $this->createQuery();

        return $this->makeRequest($url);
    }

    public function SearchCity($countryId, $cityName)
    {
        $url = $this->api_url . "/searchCity?" . $this->createQuery([
                'country_id' => $countryId,
                'name' => $cityName
            ]);

        return $this->makeRequest($url);
    }


    public function getOrders()
    {
        $url = $this->api_url . "/getOrders?" . $this->createQuery();

        return $this->makeRequest($url);
    }

    /**
     * @param array $places Server_ids of places
     * @return array of places info
     */
    public function getPlacesInfo($places)
    {
        $url = $this->api_url . "/getPlaces/?" . $this->createQuery(array("places" => json_encode($places)));

        return $this->makeRequest($url);
    }

    public function saveOrder($params)
    {
        $url = $this->api_url . "/saveOrder/?" . $this->createQuery($params);

        return $this->makeRequest($url);
    }

    /**
     * @param array $params
     * @return string
     */

    private function createQuery($params = array())
    {
        $params = $params + array("token" => $this->token);

        return http_build_query($params);
    }
}
