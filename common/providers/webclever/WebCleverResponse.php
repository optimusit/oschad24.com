<?php
namespace common\providers\webclever;

use Yii;
use yii\helpers\ArrayHelper;

class WebCleverResponse
{
    const ERROR_NONE = 0;

    private $_method = null;
    private $_code = 0;
    private $_description = '';
    private $_data;

    function __construct($method, $response, $url)
    {
        $this->_method = $method;
        if ($response instanceof \stdClass) {
            $this->_code = 1;
            $this->_description = json_encode($response);
        } elseif (is_array($response)) {
            $this->_data = $response;
        } elseif (is_string($response)) {
            $this->_code = 2;
            $this->_description = $response;
        }
    }

    public function isSuccess()
    {
        return ($this->_code == self::ERROR_NONE);
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function setDescription($description)
    {
        return $this->_description = $description;
    }
}
