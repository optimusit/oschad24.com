<?php
namespace common\providers\aiwa;

use Yii;
use yii\helpers\Json;
use api\modules\v1\components\ApiException;

class AIWAprovider
{
    const CURRENCY = 980;
    protected $apiUrl = null;
    protected static $_instance;
    protected $email = null;
    protected $merID = null;
    protected $point = null;
    protected $code = null;
    protected $password = null;

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getAPIUrl()
    {
        if (is_null($this->apiUrl)) {
            $this->apiUrl = Yii::$app->params['provider']['aiwa']['api_url'];
            if ($this->apiUrl[strlen($this->apiUrl) - 1] != '/') {
                $this->apiUrl .= '/';
            }
        }

        return $this->apiUrl;
    }

    public function getEmail()
    {
        if (is_null($this->email)) {
            $this->email = Yii::$app->params['provider']['aiwa']['email'];
        }

        return $this->email;
    }

    public function getMerID()
    {
        if (is_null($this->merID)) {
            $this->merID = Yii::$app->params['provider']['aiwa']['merID'];
        }

        return $this->merID;
    }

    public function getPoint()
    {
        if (is_null($this->point)) {
            $this->point = Yii::$app->params['provider']['aiwa']['point'];
        }

        return $this->point;
    }

    public function getCurrency()
    {
        return self::CURRENCY;
    }

    protected function getCode()
    {
        if (is_null($this->code)) {
            $this->code = uniqid();
        }

        return $this->code;
    }

    protected function getPassword()
    {
        if (is_null($this->password)) {
            $this->password = Yii::$app->params['provider']['aiwa']['password'];
        }

        return $this->password;
    }

    protected function decode($data)
    {
        return Json::decode($data);
    }

    protected function signature($data)
    {
        if (is_array($data)) {
            $data = implode('', $data);
        }

        return base64_encode(hex2bin(sha1($data)));
    }

    public function request(array $params = [])
    {
        $url = $this->getAPIUrl();
        if ($params['method'] == 'getPrintForm') {
            $params['mode'] = 'pdf';
        } else {
            $params['mode'] = 'json';
        }
        $params['code'] = $this->getCode();
        Yii::info($url, 'aiwa');
        Yii::info(var_export($params, true), 'aiwa');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);
        $data = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpCode == 200) {
            if ($params['mode'] == 'json') {
                $result = $data;
                $data = $this->decode($result);
                if (!$data) {
                    $msg = 'Decoding error. Data: ' . var_export($result, true);
                    Yii::error($msg, 'aiwa');
                    throw new ApiException('Decoding error', 500);
                }
                Yii::info(var_export($data, true), 'aiwa');
            }
        } else {
            $msg = 'Connection error. HTTP server returns an error code ' . $httpCode;
            throw new ApiException($msg, 500);
        }
        curl_close($curl);

        return $data;
    }

    public function calculate(array $params = [])
    {
        $params['method'] = 'calculate';
        $params['signature'] = $this->signature([
            $this->getPassword(),
            $this->getMerID(),
            $this->getCode(),
            $_SERVER['HTTP_X_REAL_IP'],
        ]);
        $result = $this->request($params);
        if ($result['success']) {
            $return = [
                'success' => true,
                'data' => current($result['data']),
            ];
        } else {
            $return = [
                'success' => false,
                'code' => isset($result['code'])
                    ? $result['code']
                    : $result['codeVal'],
                'message' => ucfirst(isset($result['desc'])
                    ? $result['desc']
                    : $result['codeDesc']),
            ];
        }

        return $return;
    }

    public function saveDoc(array $params = [])
    {
        $params['method'] = 'saveDoc';
        if (is_null($params['id'])) {
            $params['id'] = 'newID';
        }
        $params['currency'] = $this->getCurrency();
        //$params['amount'] = '';
        $params['point'] = $this->getPoint();
        $params['author'] = $this->getEmail();
        $params['status'] = 'clear';
        $params['signature'] = $this->signature([
            $this->getPassword(),
            $this->getMerID(),
            $params['id'],
            $params['amount'],
            $params['author'],
            $params['cover'],
            $params['currency'],
            $params['date-from'],
            $params['date-period'],
            $params['point'],
            $this->getCode(),
        ]);
        $result = $this->request($params);
        if ($result['success']) {
            $return = [
                'success' => true,
                'data' => $result['info'],
            ];
        } else {
            $return = [
                'success' => false,
                'code' => isset($result['code'])
                    ? $result['code']
                    : $result['codeVal'],
                'message' => ucfirst(isset($result['desc'])
                    ? $result['desc']
                    : $result['codeDesc']),
            ];
        }

        return $return;
    }

    public function getPrintForm(array $params = [])
    {
        $params['method'] = 'getPrintForm';
        if (!isset($params['form-type'])
            || !in_array($params['form-type'], [
                'form',
                'bill',
                'annex1'
            ])
        ) {
            $params['form-type'] = 'form';
        }
        $params['signature'] = $this->signature([
            $this->getPassword(),
            $this->getMerID(),
            $this->getCode(),
            $params['id'],
            $params['kind'],
        ]);
        $result = $this->request($params);
        if (stripos($result, '%pdf' !== false)) {
            $return = [
                'success' => true,
                'data' => $result,
            ];
        } else {
            $return = [
                'success' => false,
                'code' => null,
                'message' => ucfirst($result),
            ];
        }

        return $return;
    }

    public function addPayment(array $params = [])
    {
        if (TEST_SERVER) {
            return [
                'success' => true,
            ];
        }
        $params['method'] = 'addPayment';
        $params['author'] = $this->getEmail();
        $params['signature'] = $this->signature([
            $this->getPassword(),
            $this->getMerID(),
            $this->getCode(),
            $params['id'],
            $params['kind'],
        ]);
        $result = $this->request($params);
        if ($result['success']) {
            $return = [
                'success' => true,
            ];
        } else {
            $return = [
                'success' => false,
                'code' => isset($result['code'])
                    ? $result['code']
                    : $result['codeVal'],
                'message' => ucfirst(isset($result['desc'])
                    ? $result['desc']
                    : $result['codeDesc']),
            ];
        }

        return $return;
    }

    public function CheckAvailability()
    {
        $result = self::getInstance()
            ->calculate([
                'kind' => 'accidents5',
                'date-period' => '+12m',
                'cover' => 15000,
                'plan' => 'A',
                'programm' => '1',
                'vendorCount' => '1',
            ]);
        if (isset($result) && isset($result['success']) && $result['success'] == true) {
            return true;
        }

        return false;
    }
}
