<?php
namespace common\providers\kontramarka;

use common\components\AppHelper;
use common\models\kontramarka\Show;
use Yii;
use common\models\kontramarka\Sites;
use yii\base\Exception;

/**
 * Class Kontramarka
 * @package common\providers\kontramarka
 *
 * @method array Sites() Sites() Метод возвращает список всех доступных площадок
 * @method array Shows() Shows(array $params) Метод возвращает список спектаклей доступных на площадке (['siteId' => 74])
 * @method array EventMap() EventMap(array $params) Метод возвращает массив залов с секторами и местами (['siteId' => 74, 'eventId' => 123])
 * @method array Lock() Lock(array $params) Метод для блокировки места и добавления его в корзину (['siteId' => 74, 'eventId' => 123, 'placeId' => 567, 'priceId' => 1])
 * @method array Unlock() Unlock(array $params) Метод для блокировки места и добавления его в корзину (['siteId' => 74, 'eventId' => 123, 'placeId' => 567])
 * @method array BasketList() BasketList() Метод возвращает содержимое корзины
 * @method array BasketClean() BasketClean() Метод очищает содержимое корзины
 * @method array BasketReserve() BasketReserve() Метод резервирует корзину для дальнейшей покупки билетов
 * @method array BasketBuy() BasketBuy(array $params) Метод для проведения покупки билетов ('name' => 'Иванов Иван', 'email' => 'email@gmail.com', 'phone' => '123123123123', '')
 * @method array BasketBook() BasketBook(array $params) Метод для проведения бронирования билетов ()
 */
class Kontramarka
{
    const PREFIX = 'KONTRAMARKA';

    const PLACE_STATUS_FREE = 0;
    const PLACE_STATUS_IN_BASKET = 1;
    const PLACE_STATUS_SOLD = 2;
    const PLACE_STATUS_BOOKED = 3;

    const PLACE_STATUS = [
        self::PLACE_STATUS_FREE => 'свободно',
        self::PLACE_STATUS_IN_BASKET => 'в корзине',
        self::PLACE_STATUS_SOLD => 'продано',
        self::PLACE_STATUS_BOOKED => 'забронировано'
    ];

    protected $_apiUrl = null;
    protected $_sessionId = null;
    protected $_data = [];
    protected $_dictionaries = [
        'Sites',
    ];

    public function __construct($sessionId = null)
    {
        $this->_sessionId = $sessionId;
        if (is_null($this->_sessionId)) {
            $this->_sessionId = self::Login();
        }
    }

    public function getApiUrl()
    {
        return Yii::$app->params['provider']['kontramarka']['api_url'];
    }

    public function getUsername()
    {
        return Yii::$app->params['provider']['kontramarka']['username'];
    }

    public function getPassword()
    {
        return Yii::$app->params['provider']['kontramarka']['password'];
    }

    public function getSessionId()
    {
        return $this->_sessionId;
    }

    protected function errorMsg($code, $description)
    {
        $data = [
            'response' => [
                'result' => [
                    'code' => $code,
                    'description' => $description,
                    'execution_time' => null
                ],
                'data' => []
            ]
        ];

        return $data;
    }

    private function request($method, array $params = [])
    {
        $url = $this->getAPIUrl() . $method . '?';
        $params['sessionid'] = $this->_sessionId;
        $params1 = [];
        foreach ($params as $key => $value) {
            $params1[] = $key . '=' . rawurlencode($value);
        }
        $url .= implode('&', $params1);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Accept: application/json',
            'Content-Type: application/json'
        ]);
        $data = curl_exec($curl);
        $logItem = [
            'request' => [
                'url' => $url,
                'params' => $params
            ],
            'response' => $data,
            'response_format' => 'json',
        ];
        Yii::info($logItem, __METHOD__);
        if ($data === false) {
            $data = $this->errorMsg(500,
                'Connection error ' . curl_errno($curl) . '. ' . curl_error($curl) . ' ' . $url);
            Yii::error($data, __METHOD__);
        } else {
            $result = $data;
            $data = json_decode($result);
            if (!$data) {
                $data = $this->errorMsg(500, 'Decoding error. Data: ' . var_export($result, true));
            }
        }
        curl_close($curl);
        $this->_data['dialog'] = $logItem;

        return new KontramarkaResponse($method, $data, $url);
    }

    private function Login()
    {
        $data = null;
        $username = $this->getUsername();
        $password = $this->getPassword();
        $params = [
            'username' => $username,
            'password' => $password,
        ];

        $url = $this->getAPIUrl() . 'login';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        $data = curl_exec($curl);
        $logItem = [
            'request' => [
                'url' => $url,
                'params' => $params
            ],
            'response' => $data,
            'response_format' => 'json',
        ];
        Yii::info($logItem, __METHOD__);
        if ($data === false) {
            $data = $this->errorMsg(500,
                'Connection error ' . curl_errno($curl) . '. ' . curl_error($curl) . ' ' . $url);
            Yii::error($data, __METHOD__);
        } else {
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($httpCode != 200) {
                $result = $data;
                $data = json_decode($result);
                if (!$data) {
                    $data = $this->errorMsg(500, 'Decoding error. Data: ' . var_export($result, true));
                } else {
                    throw new Exception($data->message, $data->code);
                }
            }
        }
        curl_close($curl);

        return $data;
    }

//	public function Sites()
//	{
//		$result = $this->request('sites');
//		if ($result->isSuccess()) {
//			$this->_data['success'] = true;
//			$this->_data['message'] = '';
//			$this->_data['data'] = $result->getData();
//		} else {
//			$this->_data['success'] = false;
//			$this->_data['message'] = $result->getDescription();
//		}
//
//		return $this->_data;
//	}
//
//	public function Shows($siteId)
//	{
//		$result = $this->request('shows', ['siteId' => $siteId]);
//		if ($result->isSuccess()) {
//			$this->_data['success'] = true;
//			$this->_data['message'] = '';
//			$this->_data['data'] = $result->getData();
//		} else {
//			$this->_data['success'] = false;
//			$this->_data['message'] = $result->getDescription();
//		}
//
//		return $this->_data;
//	}

    public function __call($name, $arguments)
    {
        if (in_array($name, [
            'Sites',
            'Shows',
            'EventMap',
            'Lock',
            'Unlock',
            'Basket_List',
            'Basket_Clean',
            'Basket_Reserve',
        ])) {
            if (count($arguments) > 0) {
                $params = $arguments[0];
            } else {
                $params = [];
            }
            $result = $this->request(strtolower($name), $params);
            if ($result->isSuccess()) {
                $this->_data['success'] = true;
                $this->_data['message'] = '';
                $this->_data['data'] = $result->getData();
            } else {
                $this->_data['success'] = false;
                $this->_data['message'] = $result->getDescription();
                $this->_data['data'] = [];
            }

            return $this->_data;
        } elseif (method_exists($this, $name)) {
            $this->{$name}($arguments);
        } else {
            throw new \Exception('Недопустимый метод');
        }
    }

    public function LoadDictionaries()
    {
        foreach ($this->_dictionaries as $dictionary) {
            $result = $this->{$dictionary}();
            if ($result['success']) {
                $this->SetDictionary($dictionary, $result['data']);
            }
        }
    }

    private function SetDictionary($dictionaryName, $data)
    {
        foreach ($data as $item) {
            /** @var \yii\db\ActiveRecord $model */
            $modelName = '\common\models\kontramarka\\' . $dictionaryName;
            $model = new $modelName();
            $model = $model::findOne(['siteId' => $item->siteId]);
            if (is_null($model)) {
                $model = new $modelName();
            }
            $model->setAttributes(get_object_vars($item));
            if (!$model->save(false)) {
                Yii::error(var_export($model->errors, true), 'kontramarka');
                throw new \Exception(var_export($model->errors, true));
            }
        }
    }

    public function LoadShows()
    {
        $sites = Sites::find()
            ->asArray()
            ->all();
        foreach ($sites as $site) {
            $shows = $this->Shows(['siteId' => $site['siteId']]);
            if ($shows['success']) {
                foreach ($shows['data'] as $show) {
                    try {
                        $this->SetShow(get_object_vars($show));
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), __CLASS__);
                    }
                }
            }
        }
    }

    private function SetShow($data)
    {
        $result = false;
        $events = $data['events'];
        foreach ($events as $event) {
            $show = Show::find()
                ->where([
                    'siteId' => $data['siteId'],
                    'showId' => $data['showId'],
                    'eventId' => $event->eventId
                ])
                ->one();
            if (!$show) {
                $show = new Show();
            }
            $show->siteId = $data['siteId'];
            $show->showId = $data['showId'];
            $show->eventId = $event->eventId;
            $show->hallId = $event->hallId;
            $show->name = $data['name'];
            $show->description = $data['description'];
            $show->poster = $data['poster'];
            $show->date = AppHelper::DateTimeForMySQL(\DateTime::createFromFormat('YmdHis', $event->origin));
            if ($show->validate()) {
                $show->save();
            }
        }

        return $result;
    }
}
