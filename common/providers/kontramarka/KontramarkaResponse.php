<?php
namespace common\providers\kontramarka;

use Yii;

class KontramarkaResponse
{
    const ERROR_NONE = 0;

    private $_method = null;
    private $_code = 0;
    private $_description = '';
    private $_data;

    function __construct($method, $response, $url)
    {
        $this->_method = $method;
        if ($response instanceof \stdClass) {
            $this->_code = $response->code;
            if ($response->code != self::ERROR_NONE) {
                $this->_description = $response->message;
            } else {
                unset($response->code);
            }
        } elseif (is_array($response)) {
            if (array_key_exists('response', $response) && array_key_exists('result', $response['response'])
                && array_key_exists('code', $response['response']['result'])
                && $response['response']['result']['code'] == 500
            ) {
                $this->_code = 1;
            }
        }
        $this->_data = $response;
    }

    public function isSuccess()
    {
        return ($this->_code == self::ERROR_NONE);
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function setDescription($description)
    {
        return $this->_description = $description;
    }
}
