<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
</head>
<body>
<table style="vertical-align: top;">
    <tr>
        <td style="padding-bottom: 100px;">
            <table style="vertical-align: top;">
                <tr>
                    <td width="20%"><img src="<?= $extra['oschad_logo'] ?>" width="100%"></td>
                    <td width="50%"></td>
                    <td width="30%" style="font-size: 120px;font-weight: 900;">Квиток/E-ticket</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 100px;">
            <table style="vertical-align: top;width: 100%;">
                <tr>
                    <td><img src="<?= $orderInfo['event']['DetailsImage'] ?>" width="100%"></td>
                    <td style="padding-left: 100px;">
                        <table>
                            <tr>
                                <td style="font-size: 100px;font-weight: 900;">
                                    <?= $orderInfo['event']['EventName'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 70px;">
                                    <?= $orderInfo['event']['DetailsDate'] ?> <?= $orderInfo['event']['DetailsTime'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 70px;">
                                    <?= $orderInfo['event']['DetailsPlace'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 70px;">
                                    <?= $orderInfo['event']['DetailsAddress'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 70px;">
                                    Сектор/Sector: <?= $ticket['Zone'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 70px;">
                                    Ряд/Row: <?= $ticket['Row'] ?> Місце/Place: <?= $ticket['Place'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 70px;">
                                    Ціна/Price: <?= $ticket['Price'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 70px;">
                                    Власник/Owner: <?= $extra['owner'] ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table style="text-align: right;">
                            <tr>
                                <td style="font-size: 50px;">Контроль/Control</td>
                            </tr>
                            <tr>
                                <td>
                                    <barcode type="C128C" code="<?= $ticket['Barcode'] ?>" size="3"
                                             height="2"></barcode>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?= $ticket['BarcodeText'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?= $extra['oschad_logo'] ?>" width="70%">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?= $extra['karabas_logo'] ?>" width="70%">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?= $extra['vebua_logo'] ?>" width="20%">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 50px;">
                                    Cлужба підтримки: <?= implode(', ',
                                        \common\components\AppHelper::GetSupportPhones()) ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="border-bottom:3px solid #000000;padding-bottom: 100px;">
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 100%;vertical-align: top;">
                <tr>
                    <td style="width: 50%;padding-right: 20pt;">
                        <table>
                            <tr>
                                <td class="terms_head">
                                    Правила користування квитком
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    1. Цей електронный квиток надає право відвідування заходу.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    2. Електронний квиток заборонено копіювати та передавати копії третім особам.
                                    Унікальний
                                    ідентифікатор (штрих-код), який міститься на цьому електронному квитку, гарантує вам
                                    право
                                    на одноразове відвідування заходу. Пред'явлення третіми особами електронного квитка
                                    з
                                    ідентичним ідентификатором позбавляє вас права на відвідування заходу.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    3. У разі виникнення будь-яких проблем, таких як неможливість прочитати
                                    ідентифікатор
                                    (штрих-код), фізичне пошкодження цього електронного квитка або в інших випадках,
                                    необхідно
                                    звернутися в спеціалізований пункт обміну або в каси, перелік яких є на сайті
                                    karabas.com.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    4. Перевірка електронних квитків та прохід на захід здійснюється у відповідності з
                                    правилами,
                                    що встановлені організаторами або адмінісграціей залу, Для проходження процедури
                                    ідентифікації електронного квитка та з метою перевірки права використання цього
                                    електронного
                                    квитка рекомендовано мати при собі документ, що підтверджує особу.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    5. Вартість цього електронного квитка може бути повернена лише у разі відміни,
                                    заміни або
                                    переноса заходу на умовах, встановлених офертой. Вартість сервісного або інших
                                    додаткових
                                    сборів, що взимається при продажу електронного квитка, поверненню не підлягає.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    6. З умовами оферти ви можете ознайомитися на сайті. Ці правила не є офертою.
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 50%;padding-left: 20pt;">
                        <table>
                            <tr>
                                <td class="terms_head">
                                    Terms of Use ticket
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    1. Your electronic ticket permits admission to the event.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    2. It is not permitted to copy the electronictickets, or give a copy to third
                                    parties. A
                                    unique identifying code (bar-code) is printed on the electronic ticket, and
                                    guarantees your
                                    admission for one single entry to the event. If you pass your electronicticket and
                                    unique
                                    identifying code to third parties, your own admission to the event will be
                                    forfeited.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    3. If you encounter any kind of difficulties - for example, your unique identifying
                                    code is
                                    unreadable, or your electronicticket becomes physically damaged, or other similar
                                    incidents
                                    - you should contact one of the special ticket-return locations or ticket-desks
                                    indicated on
                                    our website at www.karabas.com.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    4. Checking your electronicticket and admission to the event take place in
                                    accordance with
                                    the conditions laid down by the organisers or venue administration. While your
                                    electronicticket is being scanned we recommend you keep an ID document to hand, and
                                    keep it
                                    while the validity of your ticket is confirmed.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    5. The price of your electronicticket can be refunded only in case the event is
                                    cancelled,
                                    changed or postponed, according to the advertised conditions. The costs of service
                                    fees or
                                    other handling charges connected with the issue of your ticket cannot be refunded.
                                </td>
                            </tr>
                            <tr>
                                <td class="terms_body">
                                    6. You can read the full conditions of this offer on our site. The document you are
                                    reading
                                    does not constitute these conditions.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table style="width: 100%">
    <tr>
        <td style="text-align: center;font-size: 13pt;">
            Категорично заборонено приносити на захід / Forbidden:
        </td>
    </tr>
</table>
<img src="<?= $extra['forbidden_items'] ?>">
<table style="vertical-align: top;width: 100%;">
    <tr>
        <td style="text-align: center;font-size: 7pt;">
            Усі вище зазначені речі будуть вилучатися службою безпеки.
        </td>
    </tr>
    <tr>
        <td style="text-align: center;font-size: 7pt;">
            Адміністрація залишає за собою право проводити особистий огляд відвідувачів з метою гарантування безпеки
            заходу.
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 3px dashed #000000;padding-top: 7pt;">
        </td>
    </tr>
</table>
<table style="vertical-align: top;width: 100%;">
    <tr>
        <td style="width: 35%;">
            <table>
                <tr>
                    <td style="font-size: 20px;font-weight: 900;">
                        <?= $orderInfo['event']['EventName'] ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;">
                        <?= $orderInfo['event']['DetailsDate'] ?> <?= $orderInfo['event']['DetailsTime'] ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;">
                        <?= $orderInfo['event']['DetailsPlace'] ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;">
                        <?= $orderInfo['event']['DetailsAddress'] ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;">
                        Сектор/Sector: <?= $ticket['Zone'] ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;">
                        Ряд/Row: <?= $ticket['Row'] ?> Місце/Place: <?= $ticket['Place'] ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;">
                        Ціна/Price: <?= $ticket['Price'] ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;">
                        Власник/Owner: <?= $extra['owner'] ?>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 25%;">
            <table>
                <tr>
                    <td style="font-size: 13px;">Контроль/Control</td>
                </tr>
                <tr>
                    <td>
                        <barcode type="C128C" code="<?= $ticket['Barcode'] ?>" size="1"
                                 height="2"></barcode>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $ticket['BarcodeText'] ?>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 15%;">
            <img src="<?= $extra['scan'] ?>" width="100">
        </td>
        <td style="width: 25%;text-align: right;">
            <table style="text-align: right;width: 100%;">
                <tr>
                    <td>
                        <img src="<?= $extra['oschad_logo'] ?>" width="150">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?= $extra['karabas_logo'] ?>" width="150">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?= $extra['vebua_logo'] ?>" width="60">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table style="vertical-align: top;width: 100%;">
    <tr>
        <td style="font-size: 10px;text-align: right;">
            Cлужба підтримки: <?= implode(', ', \common\components\AppHelper::GetSupportPhones()) ?>
        </td>
    </tr>
</table>
</html>
