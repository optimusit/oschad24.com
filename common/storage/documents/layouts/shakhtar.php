<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
</head>
<body>
<?php foreach ($tickets as $ticket) { ?>
    <div class="ticket">
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="font-size: 0.4cm;font-weight: bold;">
                                <?= $ticket['Event']['name']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= $ticket['Event']['date_start'] ?>, Львів
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Арена-Львів, вул. Стрийська, 199
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Зона/Zone: <?= $ticket['SectorInfo']['Zone'] ?>
                                Рівень/Level: <?= $ticket['SectorInfo']['Level'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Сектор/Sector: <?= $ticket['SectorInfo']['Name'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ряд/Row: <?= $ticket['RowText'] ?> Місце/Place: <?= $ticket['SeatText'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ціна/Price: <?= $ticket['Price'] ?> грн.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Власник/Owner: <?= $extra['owner'] ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td style="font-size: 0.4cm;font-weight: bold;">
                                Контроль/Control
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <barcode type="EAN13" code="<?= $ticket['BarCode'] ?>" size="1.1"
                                         height="0.8"></barcode>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Серія <?= $ticket['Series'] ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <img src="<?= $extra['scan'] ?>" width="70">
                </td>
                <td>
                    <table style="text-align: right;width: 100%;">
                        <tr>
                            <td>
                                <img src="<?= $extra['oschad_logo'] ?>" width="100">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="<?= $extra['vebua_logo'] ?>" width="25">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>Cлужба підтримки: <?= implode(', ', \common\components\AppHelper::GetSupportPhones()) ?></td>
                <td style="text-align: right;">www.oschadbank.ua</td>
            </tr>
        </table>
        <table>
            <tr>
                <td>Квиток поверненню та обміну не підлягає</td>
                <td style="text-align: right;">Вхід на стадіон за 1:30 хвилин до початку матчу</td>
            </tr>
        </table>
    </div>
<?php } ?>
</html>
