<?php
namespace admin\components;

use common\models\Partner;
use Yii;
use yii\base\Exception;
use yii\helpers\BaseArrayHelper;
use yii\helpers\Url;
use yii\web\Controller as BaseController;
use yii\filters\VerbFilter;

/**
 * Base controller
 */
class Controller extends BaseController
{
    /** @var Partner $_partner */
    public $_partner = null;

    protected $_partnerPath = null;
    protected $_partnerConfig = null;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
//		$behaviors['access'] = [
//			'class' => AccessControl::className(),
//			'rules' => [
//				[
//					'actions' => [
//						'login',
//						'error'
//					],
//					'allow' => true,
//				],
//				[
//					'allow' => true,
//					'roles' => [
//						'admin',
//						'manager',
//					],
//				],
//			],
//		];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function runAction($id, $params = [])
    {
        $params = BaseArrayHelper::merge(Yii::$app->getRequest()
            ->getBodyParams(), $params);

        return parent::runAction($id, $params);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function goHome()
    {
        $url = Url::to([Yii::$app->getHomeUrl()]);

        return Yii::$app->getResponse()
            ->redirect($url);
    }

    /**
     * @inheritdoc
     */
    public function goBack($defaultUrl = null)
    {
        $url = Url::to([
            Yii::$app->getUser()
                ->getReturnUrl($defaultUrl)
        ]);

        return Yii::$app->getResponse()
            ->redirect($url);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $domain = Yii::$app->request->serverName;
        $partnerPrefix = preg_replace("/." . Yii::$app->params['domain'] . "/", '', $domain);
        $this->_partner = Partner::findOne(['prefix' => $partnerPrefix]);
        if (!$this->_partner) {
            throw new Exception('', 404);
        }
        if ($partnerPrefix != 'vebua') {
            $this->_partnerPath = Yii::$app->params['partner']['views']['path'] . DIRECTORY_SEPARATOR . $partnerPrefix
                . DIRECTORY_SEPARATOR;
            Yii::$app->layoutPath = $this->_partnerPath . 'views' . DIRECTORY_SEPARATOR . 'layouts';
            Yii::$app->viewPath = $this->_partnerPath . 'views';
            $this->_partnerConfig = require($this->_partnerPath . 'config.php');
        }
    }
}
