<?php
namespace admin\components;

use Yii;

class MenuHelper
{
    public static function getMenu()
    {
        $result = static::getMenuRecursive();

        return $result;
    }

    private static function getMenuRecursive($parent = null)
    {
        $role = null;
        $result = [];
        foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->user->id) as $role) {
            $role = $role->name;
            break;
        }
        if (!is_null($role)) {

            if (!$parent) {
                $menuConfig = Yii::$app->params['menu'];
            } else {
                $menuConfig = $parent;
            }


            foreach ($menuConfig as $item) {
                if (in_array($role, $item['roles'])) {
                    $menuItem = [
                        'label' => $item['label'],
                        'url' => $item['url'],
                        '<li class="divider"></li>',
                    ];
                    if (array_key_exists('items', $item)) {
                        $menuItem['items'] = static::getMenuRecursive($item['items']);
                    }
                    $result[] = $menuItem;
                }
            }
        }

        return $result;
    }
}
