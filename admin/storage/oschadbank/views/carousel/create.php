<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Carousel */

$this->title = Yii::t('admin', 'Створити банер');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('admin', 'Банери'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
