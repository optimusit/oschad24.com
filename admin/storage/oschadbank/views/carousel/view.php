<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Carousel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('admin', 'Банери'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'Змінити'), [
            'update',
            'id' => $model->id
        ], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('admin', 'Видалити'), [
            'delete',
            'id' => $model->id
        ], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('admin', 'Ви впевнені, видалити банер?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //			[
            //				'attribute' => 'service_id',
            //				'value' => $model->getService()
            //					->one()
            //					->getName(),
            //			],
            'name',
            [
                'attribute' => 'url',
                'value' => Html::a($model->url, $model->url, ['target' => '_blank']),
                'format' => 'raw'
            ],
            [
                'attribute' => 'image',
                'value' => Html::a(Html::img($model->image, ['style' => 'max-width:100px;max-height:100px;']),
                    $model->image, ['target' => '_blank']),
                'format' => 'raw'
            ],
            'show_begin',
            'show_end',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
