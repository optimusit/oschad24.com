<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Service;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Carousel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'service_id')
        ->dropDownList(ArrayHelper::map(Service::find()
            ->select([
                'id',
                'name_' . Yii::$app->language
            ])
            ->where(['code' => Service::TICKETS_FOOTBALL])
            ->asArray()
            ->all(), 'id', 'name_' . Yii::$app->language), ['style' => 'display:none;'])
        ->label(false) ?>

    <?= $form->field($model, 'name')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_file')
        ->fileInput() ?>

    <?= $form->field($model, 'show_begin')
        ->widget(DateTimePicker::className()) ?>

    <?= $form->field($model, 'show_end')
        ->widget(DateTimePicker::className()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord
            ? Yii::t('admin', 'Створити')
            : Yii::t('admin', 'Змінити'), [
            'class' => $model->isNewRecord
                ? 'btn btn-success'
                : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
