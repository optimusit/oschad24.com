<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Сервіси');
$this->params['breadcrumbs'][] = $this->title;
$url = \yii\helpers\Url::to('service-set');
$script = <<<JS
$('.chk').on('change', function(e) {
	var chk = $(this);
	var id = chk.data('id');
	var value = chk.is(':checked') ? 1 : 0;
    $.post(
        '$url',
        {
        	id:id,
        	value:value
        },
        function(response) {
        }
    );
});
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name_uk',
            'description_uk',
            [
                'class' => 'yii\grid\CheckboxColumn',
                'header' => $labels['active'],
                'checkboxOptions' => function ($model, $key, $index, $widget) {
                    $chk = [
                        'class' => 'chk',
                        'data-id' => $model['id']
                    ];
                    if ($model['active'] == 1) {
                        $chk['checked'] = 'checked';
                    }

                    return $chk;
                },
            ],
        ],
    ]); ?>

</div>
