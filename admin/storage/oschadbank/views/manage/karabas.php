<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Карабас карусель');
$this->params['breadcrumbs'][] = $this->title;
$url = \yii\helpers\Url::to('karabas-set');
$script = <<<JS
$('.chk').on('change', function(e) {
	var id = $(this).data('id');
    $.post(
        '$url',
        {
        	id:id
        },
        function(response) {
        }
    );
});
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>
<div class="partner-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'EventDate',
                'format' => 'datetime',
                'header' => 'Час'
            ],
            [
                'attribute' => 'EventName',
                'header' => 'Подія'
            ],
            [
                //'attribute' => 'image',
                'header' => 'Афіша',
                'value' => function ($model) {
                    return Html::a(Html::img($model['image'], ['style' => 'max-width:100px;max-height:100px;']),
                        $model['image'], ['target' => '_blank']);
                },
                'format' => 'raw'
            ],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'header' => 'В каруселі',
                'checkboxOptions' => function ($model, $key, $index, $widget) {
                    return [
                        'checked' => is_null($model['selected'])
                            ? 0
                            : 1,
                        'class' => 'chk',
                        'data-id' => $model['EventId']
                    ];
                },
            ],
        ]
    ]) ?>
</div>
