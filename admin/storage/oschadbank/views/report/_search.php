<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'service_id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'partner_id') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'income_amount') ?>

    <?php // echo $form->field($model, 'fee') ?>

    <?php // echo $form->field($model, 'order_amount') ?>

    <?php // echo $form->field($model, 'partner_fee') ?>

    <?php // echo $form->field($model, 'partner_amount') ?>

    <?php // echo $form->field($model, 'bank_fee') ?>

    <?php // echo $form->field($model, 'e_fee') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'custom') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'expiration_time') ?>

    <?php // echo $form->field($model, 'afile_name') ?>

    <?php // echo $form->field($model, 'unit_count') ?>

    <?php // echo $form->field($model, 'commission_internal') ?>

    <?php // echo $form->field($model, 'partner_commission_internal') ?>

    <?php // echo $form->field($model, 'viewed') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
