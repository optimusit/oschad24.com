<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\grid\GridView;
use common\models\Order;
use common\models\Service;
use common\models\Provider;
use yii\helpers\ArrayHelper;
use common\models\OrderSearch;

/* @var $this yii\web\View */
/* @var $searchModel OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Замовлення');
$this->params['breadcrumbs'][] = $this->title;
$salesExportUrl = Url::to(['/report/salesexport']);
$script = <<<JS
$('#btnSalesExport').on('click', function(e) {
	var url = "$salesExportUrl";
	var settings = $('#w0').yiiGridView('data').settings;
	var filterSelector = settings.filterSelector;
	var filterUrl = settings.filterUrl;
	var data = {};
	$.each($(filterSelector).serializeArray(), function () {
		data[this.name] = this.value;
	});
	$.each(yii.getQueryParams(filterUrl), function (name, value) {
		if (data[name] === undefined) {
			data[name] = value;
		}
	});
	var form = $('<form id="exportForm" action="' + url + '" method="get"></form>');
	$.each(data, function (name, value) {
		form.append($('<input type="hidden" name="t" value="" />').attr('name', name).val(value));
	});
	$('#exportForm').remove();
	form.appendTo('body').submit();
});
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= \yii\bootstrap\Button::widget([
        'label' => Yii::t('app', 'Завантажити'),
        'options' => [
            'id' => 'btnSalesExport',
            'class' => 'btn btn-primary',
        ],
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter' => true,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => Yii::t('app', '№ з/п'),
                'footer' => $dataProvider->totalCount,
            ],
            [
                'attribute' => 'provider_id',
                'value' => 'provider.name',
                'filter' => Html::activeDropDownList($searchModel, 'provider_id', ArrayHelper::map(Provider::find()
                    ->select([
                        'id',
                        'name'
                    ])
                    ->orderBy('name')
                    ->asArray()
                    ->all(), 'id', 'name'), [
                    'class' => 'form-control',
                    'prompt' => ''
                ])
            ],
            [
                'attribute' => 'service_id',
                'value' => 'service.name',
                'filter' => Html::activeDropDownList($searchModel, 'service_id', ArrayHelper::map(Service::find()
                    ->select([
                        'id',
                        'name_' . Yii::$app->language
                    ])
                    ->orderBy('name_' . Yii::$app->language)
                    ->asArray()
                    ->all(), 'id', 'name_' . Yii::$app->language), [
                    'class' => 'form-control',
                    'prompt' => ''
                ])
            ],
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filter' => $searchModel->getCreateDateFilter(),
            ],
            [
                'attribute' => 'id',
                'value' => function ($model) {
                    return $model->id;
                },
            ],
            [
                'attribute' => 'provider_order_id',
                'value' => function ($model) {
                    return $model->provider_order_id;
                },
            ],
            [
                'attribute' => 'unit_count',
                'value' => function ($model) {
                    return $model->unit_count;
                },
                'footer' => Yii::$app->formatter->asDecimal($searchModel->totalUnitCount),
            ],
            [
                'attribute' => 'amount',
                'format' => [
                    'decimal',
                    2
                ],
                'value' => function ($model) {
                    return $model->amount;
                },
                'footer' => Yii::$app->formatter->asDecimal($searchModel->totalAmount),
            ],
            [
                'attribute' => Yii::t('app', 'Вартість сервісного збору, грн'),
                'format' => [
                    'decimal',
                    2
                ],
                'value' => function ($model) {
                    return $model->getServiceAmount();
                },
                'footer' => Yii::$app->formatter->asDecimal($searchModel->totalServiceAmount),
            ],
            [
                'attribute' => Yii::t('app', 'Вартість еквайрінгу, грн'),
                'format' => [
                    'decimal',
                    2
                ],
                'value' => function ($model) {
                    return $model->getAcquiringAmount();
                },
                'footer' => Yii::$app->formatter->asDecimal($searchModel->totalAcquiringAmount),
            ],
            [
                'attribute' => Yii::t('app', 'Перераховано Банком, грн'),
                'format' => [
                    'decimal',
                    2
                ],
                'value' => function ($model) {
                    return $model->getBankAmount();
                },
                'footer' => Yii::$app->formatter->asDecimal($searchModel->totalBankAmount),
            ],
            [
                'attribute' => Yii::t('app', 'Перераховано провайдеру, грн'),
                'format' => [
                    'decimal',
                    2
                ],
                'value' => function ($model) {
                    return $model->getToProviderAmount();
                },
                'footer' => Yii::$app->formatter->asDecimal($searchModel->totalToProviderAmount),
            ],
            [
                'attribute' => 'income_amount',
                'format' => [
                    'decimal',
                    2
                ],
                'value' => function ($model) {
                    return $model->income_amount;
                },
            ],
            [
                'attribute' => 'status_id',
                'value' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status_id', Order::getStatuses(), [
                    'class' => 'form-control',
                    'prompt' => ''
                ])
            ],
            //			[
            //				'attribute' => 'partner_id',
            //				'value' => 'partner.name',
            //				'filter' => Html::activeDropDownList($searchModel, 'partner_id', ArrayHelper::map(Partner::find()
            //					->select('id,name')
            //					->orderBy('name')
            //					->asArray()
            //					->all(), 'id', 'name'), [
            //					'class' => 'form-control',
            //					'prompt' => ''
            //				])
            //			],
            //			[
            //				'attribute' => 'updated_at',
            //				'value' => function ($model) {
            //					return $model->updated_at > $model->created_at
            //						? Yii::$app->formatter->asDatetime($model->updated_at)
            //						: '';
            //				}
            //			],
            //			[
            //				'attribute' => 'amount',
            //				'footer' => $searchModel->getTotalAmount()
            //			],
        ],
    ]); ?>
</div>
