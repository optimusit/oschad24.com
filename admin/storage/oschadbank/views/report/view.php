<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Замовлення'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_order', [
    'model' => $model,
]) ?>

<?= $this->render('_reservation', [
    'model' => $model,
]) ?>
