<?php

use dosamigos\chartjs\ChartJs;

/* @var $this yii\web\View */

$this->title = 'Application';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-6">
                <h2>SMS шлюз</h2>

                <div class="row">
                    <div class="col-lg-6">
                        Баланс
                    </div>
                    <div class="col-lg-6 pull-right">
                        <?= Yii::$app->formatter->asCurrency($data['smsBalance']['balance']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        Обновление
                    </div>
                    <div class="col-lg-6 pull-right">
                        <?= Yii::$app->formatter->asDatetime($data['smsBalance']['time']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        В очереди на отправку
                    </div>
                    <div class="col-lg-6 pull-right">
                        <?= $data['enqueuedSMS'] ?>
                    </div>
                </div>
                <div class="row <?= $data['stuckSMS'] > 0
                    ? 'bg-danger'
                    : '' ?>">
                    <div class="col-lg-6">
                        Застрявшие
                    </div>
                    <div class="col-lg-6 pull-right">
                        <?= $data['stuckSMS'] ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <h2>Продажи по типу билета</h2>
                    <?= ChartJs::widget([
                        'type' => 'Line',
                        'options' => [
                            'height' => 400,
                            'width' => 400
                        ],
                        'data' => $data['salesByType']['widget']
                    ]); ?>
                </div>
                <div class="row">
                    <h2>Сьогодні</h2>
                    <table class="table">
                        <?php foreach ($data['salesByType']['today'] as $service) { ?>
                            <tr>
                                <td style="width: 10px;">
                                    <div class="img-circle"
                                         style="width: 18px; height: 18px; background: <?= $service['color'] ?>;"></div>
                                </td>
                                <td><?= $service['service'] ?></td>
                                <td><?= $service['count'] ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
