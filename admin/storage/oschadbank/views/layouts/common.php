<?php
use admin\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use admin\assets\BootboxAsset;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $controller \admin\components\Controller */
$controller = $this->context;
AppAsset::register($this);
BootboxAsset::overrideSystemConfirm();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => $controller->_partner->name,
        'brandUrl' => [Yii::$app->homeUrl],
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = \admin\components\MenuHelper::getMenu();
    if (Yii::$app->user->isGuest) {
        $menuItems[] = [
            'label' => Yii::t('admin', 'Вхід'),
            'url' => ['/site/login']
        ];
    } else {
        $menuItems[] = [
            'label' => Yii::t('admin', 'Вихід ({email})', ['email' => Yii::$app->user->identity->getName()]),
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>


    <?= $content ?>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; veb.ua <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
