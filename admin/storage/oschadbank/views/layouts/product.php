<?php
use yii\widgets\Breadcrumbs;

$this->beginContent('@app/storage/oschadbank/views/layouts/common.php');
?>
<div class="container" style="width: 100%;f ">
    <?= Breadcrumbs::widget([
        'homeLink' => [
            'label' => Yii::t('yii', 'Home'),
            'url' => [Yii::$app->homeUrl],
        ],
        'links' => isset($this->params['breadcrumbs'])
            ? $this->params['breadcrumbs']
            : [],
    ]) ?>
    <?= $content ?>
</div>
<?php $this->endContent(); ?>
