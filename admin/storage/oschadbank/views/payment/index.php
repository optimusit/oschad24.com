<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Платежі');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= \admin\widgets\Alert::widget(); ?>

<!--    --><?php //yii\widgets\Pjax::begin([
//        'id' => 'orders',
//        'enablePushState' => false,
//        'timeout' => 60 * 1000,
//        'clientOptions' => ['method' => 'POST']
//    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'provider_order_id',
            'amount',
            [
                'attribute' => 'paid_time',
                'format' => 'datetime',
                'filter' => $searchModel->getPaidDateFilter(),
            ],
            [
                'attribute' => 'expiration_time',
                'format' => 'datetime',
                'filter' => $searchModel->getExpiredDateFilter(),
            ],
            [
                'attribute' => Yii::t('app', 'Просрочен'),
                'format' => 'boolean',
                'value' => function ($model) {
                    return $model->isExpired();
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{refund}',
                'buttons' => [
                    'refund' => function ($url, $model) {
                        return ($model->PaymentCanBeCanceled(true) || $model->PaymentCanBeCanceled())
                            ? Html::a('<span class="glyphicon glyphicon-usd text-danger"></span>', $url, [
                                'title' => Yii::t('app', 'Відмінити платіж'),
                                'target' => '_blank',
                                //'data-confirm' => Yii::t('app', 'Ви впевнені?'),
                            ])
                            : '';
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{bookingrevoke}',
                'buttons' => [
                    'bookingrevoke' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-remove text-danger"></span>', $url, [
                            'title' => Yii::t('app', 'Повернути квиток'),
                            'target' => '_blank'
                            //'data-pjax' => '#orders',
                            //'data-pjax' => '1',
                            //'data-confirm' => Yii::t('app', 'Ви впевнені?'),
                            //'data-method' => 'post',
                        ]);
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{salescomplete}',
                'buttons' => [
                    'salescomplete' => function ($url, $model) {
                        return $model->PaymentCanBeCompleted()
                            ? Html::a('<span class="glyphicon glyphicon-usd text-success"></span>', $url, [
                                'title' => Yii::t('app', 'Завершити платіж'),
                                'data-confirm' => Yii::t('app', 'Ви впевнені?'),
                            ])
                            : '';
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{paymentshistory}',
                'buttons' => [
                    'paymentshistory' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-menu-hamburger"></span>',
                            \yii\helpers\Url::to([
                                '/order/paymentshistory',
                                'id' => $model->id
                            ]), [
                                'title' => Yii::t('app', 'Історія запитів до платіжної системи'),
                                'target' => '_blank'
                            ]);
                    },
                ],
            ],
        ],
    ]); ?>
<!--    --><?php //yii\widgets\Pjax::end(); ?>

</div>
