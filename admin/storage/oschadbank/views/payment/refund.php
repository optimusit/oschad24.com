<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\RefundForm */

$this->title = Yii::t('admin', 'Відміна транзакції / повернення коштів');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('admin', 'Платежі'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
$script = <<<JS
$('#refundform-partial').change(function() {
	if(this.checked) {
		$('#refundform-refundamount').removeAttr('readonly').focus();
	} else {
		$('#refundform-refundamount').val($('#refundform-orderamount').val()).attr('readonly', '');
	}
});
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>
<div class="payment-refund">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="carousel-form">

        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'orderAmount')
            ->hiddenInput()
            ->label(false) ?>
        <?= $form->field($model, 'refundAmount')
            ->textInput([
                'maxlength' => true,
                'readonly' => $model->partial
                    ? false
                    : true,
            ]) ?>
        <?php if ($model->canBePartiallyCanceled) {
            echo $form->field($model, 'partial')
                ->checkbox();
        } else {
            echo Html::tag('span', Html::label(Yii::t('admin', 'Часткове повернення на даний час не можливо')),
                ['style' => 'color: #999999;']);
        } ?>
        <?= $form->field($model, 'comment')
            ->textInput(['maxlength' => true]) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('admin', 'Відміна транзакції'), [
                'class' => 'btn btn-primary',
                'data-confirm' => Yii::t('admin', 'Ви впевнені?')
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
