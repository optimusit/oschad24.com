<div class="">
    <div class="date">
        <h2>Час</h2>
        <?= Yii::$app->formatter->asDatetime($model->created_at); ?>
    </div>
    <div class="request">
        <h2>Запит</h2>
        <?php
        $str = $model->request;
        $str = highlight_string($str, true);
        $str = urldecode($str);
        $str = str_replace('\\\\', '\\', $str);
        $str = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UTF-16BE');
        }, $str);
        echo $str;
        ?>
    </div>
    <div class="response">
        <h2>Відповідь</h2>
        <?php
        $str = $model->response;
        $str = str_replace('<', '&lt', $str);
        $str = str_replace('>', '&gt', $str);
        if ($model->response_format == 'json') {
            $json = json_decode($str);
            $str = json_encode($json, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        echo sprintf('<pre><code class="%s">%s</code></pre>', $model->response_format, $str);
        ?>
    </div>
</div>
<br>
<br>
