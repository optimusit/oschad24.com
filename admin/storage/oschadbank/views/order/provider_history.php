<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
nezhelskoy\highlight\HighlightAsset::register($this);
$this->title = Yii::t('admin', 'Історія запитів до провайдера');
$this->params['breadcrumbs'][] = Yii::t('admin', 'Замовлення');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_provider_history_item',
    ]) ?>
</div>
