<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('admin', 'Історія запитів до платіжної системи');
$this->params['breadcrumbs'][] = Yii::t('admin', 'Замовлення');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => Yii::t('app', '№ з/п')
            ],
            'id',
            'terminal',
            'amount',
            [
                'attribute' => 'trtype',
                'value' => function ($model) {
                    return $model->getType() . ('(' . $model->trtype . ')');
                }
            ],
            'timestamp',
            'nonce',
            [
                'attribute' => 'action',
                'value' => function ($model) {
                    return $model->getTransactionDescription();
                }
            ],
            [
                'attribute' => 'rc',
                'value' => function ($model) {
                    return $model->getName() . (is_null($model->rc)
                        ? ''
                        : '(' . $model->rc . ')');
                }
            ],
            'rrn',
            'int_ref',
            'auth_code',
            'created_at:datetime',
        ],
    ]) ?>
</div>
