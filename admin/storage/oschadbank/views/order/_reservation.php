<?php

use yii\widgets\DetailView;
use common\providers\tickets\GdTickets;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$customData = $model->getCustomData();
if (isset($form['operation_type'])
    && in_array($form['operation_type'], [
        GdTickets::OPERATION_TYPE_E,
        GdTickets::OPERATION_TYPE_V
    ])
) {
    $operationType = GdTickets::getOperationTypeLabel($form['operation_type']);
} else {
    $operationType = GdTickets::getOperationTypeLabel(GdTickets::OPERATION_TYPE_V);
}

$seats = $this->render('_reservation_seats', ['model' => $model]);
?>

<div>
    <h1><?= Yii::t('app', 'Бронювання') ?></h1>
</div>
