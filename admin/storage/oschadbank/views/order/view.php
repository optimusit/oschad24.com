<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Order;
use \yii\bootstrap\Button;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Замовлення'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
$script = <<<JS
$('#btnSendDocuments').on('click', function(e) {
    $.ajax({
        url: $(this).data('url'),
        success: function(data) {
			alert(data);
        }
    });
});

$('#btnDownloadDocuments').on('click', function(e) {
    $.ajax({
        url: $(this).data('url'),
        success: function(data) {
        }
    });
});
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>
<style>
    .row {
        margin-bottom: 15px;
    }
</style>
<h1><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-lg-1">
        <?= Yii::t('app', 'Дії') ?>
    </div>
    <div class="col-lg-11">
        <?php if (in_array($model->status_id, [Order::STATUS_COMPLETED])) {
            echo Button::widget([
                'label' => Yii::t('app', 'Відправити квиток'),
                'options' => [
                    'id' => 'btnSendDocuments',
                    'class' => 'btn btn-primary',
                    'data-url' => Url::to([
                        '/order/senddocuments',
                        'id' => $model->id
                    ]),
                ],
            ]);
            echo ' ';
            echo Html::a(Yii::t('app', 'Переглянути документ'), Url::to([
                '/order/downloaddocuments',
                'id' => $model->id
            ]), [
                'id' => 'btnDownloadDocuments',
                'class' => 'btn btn-primary',
            ]);
        } ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-1">
        <?= Yii::t('app', 'Протоколи') ?>
    </div>
    <div class="col-lg-11">
        <?= Html::a(Yii::t('app', 'Провайдер'), Url::to([
            '/order/providerhistory',
            'id' => $model->id
        ]), [
            'id' => 'btnDownloadDocuments',
            'class' => 'btn btn-primary',
            'target' => '_blank',
        ]) ?>
        <?= Html::a(Yii::t('app', 'Еквайринг'), Url::to([
            '/order/paymentshistory',
            'id' => $model->id
        ]), [
            'id' => 'btnDownloadDocuments',
            'class' => 'btn btn-primary',
            'target' => '_blank',
        ]) ?>
    </div>
</div>
<?= $this->render('_order', [
    'model' => $model,
]) ?>

<?= $this->render('_reservation', [
    'model' => $model,
]) ?>
