<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Order;
use common\models\Service;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Замовлення');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => Yii::t('app', '№ з/п')
            ],
            'id',
            'provider_order_id',
            'name',
            'surname',
            'phone',
            [
                'attribute' => 'service_id',
                'value' => 'service.name',
                'filter' => Html::activeDropDownList($searchModel, 'service_id', ArrayHelper::map(Service::find()
                    ->select([
                        'id',
                        'name_' . Yii::$app->language
                    ])
                    ->orderBy('name_' . Yii::$app->language)
                    ->asArray()
                    ->all(), 'id', 'name_' . Yii::$app->language), [
                    'class' => 'form-control',
                    'prompt' => ''
                ])
            ],
            [
                'attribute' => 'status_id',
                'value' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status_id', Order::getStatuses(), [
                    'class' => 'form-control',
                    'prompt' => ''
                ])
            ],
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filter' => $searchModel->getCreateDateFilter(),
            ],
            'amount',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('yii', 'View'),
                            'aria-label' => Yii::t('yii', 'View'),
                            'target' => '_blank'
                        ];

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    },
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{paymentshistory}',
                'buttons' => [
                    'paymentshistory' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-usd"></span>', $url, [
                            'title' => Yii::t('app', 'Історія запитів до платіжної системи'),
                            'target' => '_blank'
                        ]);
                    },
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{providerhistory}',
                'buttons' => [
                    'providerhistory' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Історія запитів до провайдера'),
                            'target' => '_blank'
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>
</div>
