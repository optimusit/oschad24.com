<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
?>
<div class="order-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'service_id',
                'value' => Html::encode($model->getService()
                    ->one()->name),
            ],
            //			[
            //				'attribute' => 'client_id',
            //				'value' => '',
            //			],
            //			[
            //				'attribute' => 'user_id',
            //				'value' => $model->getUser()->one(),
            //			],
            [
                'attribute' => 'partner_id',
                'value' => $model->getPartner()
                    ->one()->name,
            ],
            'phone',
            'created_at:datetime',
            [
                'attribute' => 'updated_at',
                'value' => $model->updated_at > $model->created_at
                    ? Yii::$app->formatter->asDatetime($model->updated_at)
                    : ''
            ],
            'income_amount',
            'fee',
            'order_amount',
            'partner_fee',
            'partner_amount',
            'bank_fee',
            'e_fee',
            'amount',
            //			'custom:ntext',
            [
                'attribute' => 'status_id',
                'value' => $model->getStatus(),
            ],
            'expiration_time',
            'unit_count',
            'commission_internal:boolean',
            'partner_commission_internal:boolean',
            'viewed:boolean',
            'surname',
            'name',
            'email',
            'phone',
        ],
    ]) ?>
</div>
