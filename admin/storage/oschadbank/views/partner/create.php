<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Partner */

$this->title = Yii::t('admin', 'Создать партнера');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('admin', 'Партнеры'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
