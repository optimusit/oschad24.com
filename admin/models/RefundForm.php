<?php
namespace admin\models;

use Yii;
use common\models\Order;
use yii\base\Model;

/**
 * Refund form
 */
class RefundForm extends Model
{
    public $order_id;
    public $partner_id;
    public $orderAmount;
    public $refundAmount;
    public $partial;
    public $comment;
    public $canBePartiallyCanceled;
    public $canBeCanceled;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'partner_id',
                    'orderAmount',
                    'refundAmount',
                    'partial',
                    'comment',
                ],
                'required'
            ],
            [
                'order_id',
                'integer'
            ],
            [
                'partner_id',
                'integer'
            ],
            [
                'orderAmount',
                'double'
            ],
            [
                'refundAmount',
                'double',
                'min' => 0.01,
                'max' => $this->orderAmount,
            ],
            [
                'refundAmount',
                function ($attribute, $params) {
                    if (!$this->canBePartiallyCanceled && !$this->canBeCanceled) {
                        $this->addError($attribute, Yii::t('admin', 'Повернення не можливо'));

                        return;
                    }
                    if (!$this->canBePartiallyCanceled) {
                        $this->addError($attribute, Yii::t('admin', 'Часткове повернення на даний час не можливо'));

                        return;
                    }
                    if ($this->refundAmount > $this->orderAmount) {
                        $this->addError($attribute,
                            'Сума повернення не повина перевищувати суму транзакції (' . $this->orderAmount . ')');
                    }
                }
            ],
            [
                'partial',
                'boolean'
            ],
            [
                'comment',
                'string',
                'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orderAmount' => 'Сума платежу',
            'refundAmount' => 'Сума повернення',
            'partial' => 'Частково',
            'comment' => 'Підстава',
        ];
    }

    /**
     * @param $order_id
     * @param $partner_id
     * @return RefundForm|null
     */
    public static function findModel($order_id, $partner_id)
    {
        /** @var Order $order */
        if (($order = Order::findOne([
                'id' => $order_id,
                'partner_id' => $partner_id
            ])) !== null
        ) {
            $model = new RefundForm();
            $model->order_id = $order->id;
            $model->partner_id = $order->partner_id;
            $model->orderAmount = $order->amount;
            $model->refundAmount = $order->amount;
            $model->canBePartiallyCanceled = $order->PaymentCanBeCanceled(true);
            $model->canBeCanceled = $order->PaymentCanBeCanceled();

            return $model;
        }

        return null;
    }

    public function PaymentCancel()
    {
        $result = false;
        /** @var Order $order */
        if (($order = Order::findOne([
                'id' => $this->order_id,
            ])) !== null
        ) {
            $result = $order->PaymentCancel($this->refundAmount);
        }

        return $result;
    }
}
