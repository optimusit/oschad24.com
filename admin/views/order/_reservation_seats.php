<?php

use yii\helpers\Html;
use common\providers\tickets\GdTickets;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$formData = $model->getCustomData();
?>
<?php
if ($model->isGdTicket()) :
    if (isset($formData['no_clothes']) && $formData['no_clothes']) {
        echo Yii::t('tickets', 'Відмова від постільної білизни');
    }
    ?>
    <ul>
        <?php
        foreach ($formData['selected_seats'] as $row):
            ?>
            <li>
                <?= Yii::t('tickets', 'Вагон'); ?>
                <span class="label label-info">
					<?= $row['car_number']; ?>
				</span>, <?= Yii::t('tickets', 'место'); ?>
                <span class="label label-info">
					<?= $row['seat']; ?>
				</span>,
				<span>
					<?= Html::encode(GdTickets::getClassLabel($row['class_name'])); ?>,
				</span>
				<span>
					<?= Html::encode($row['surname']); ?>
				</span>
				<span>
					<?= Html::encode($row['name']); ?>
				</span>
                <?php if (isset($row['children']) && $row['children']): ?>
                    (<?= Yii::t('tickets', 'Детский'); ?>)
                <?php endif; ?>
            </li>
        <?php
        endforeach;
        ?>
    </ul>
<?php endif ?>
