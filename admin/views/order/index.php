<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Order;
use common\models\Service;
use common\models\Partner;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Замовлення');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => Yii::t('app', '№ з/п')
            ],
            'id',
            [
                'attribute' => 'service_id',
                'value' => 'service.name',
                'filter' => Html::activeDropDownList($searchModel, 'service_id', ArrayHelper::map(Service::find()
                    ->select('id,name')
                    ->orderBy('name')
                    ->asArray()
                    ->all(), 'id', 'name'), [
                    'class' => 'form-control',
                    'prompt' => ''
                ])
            ],
            [
                'attribute' => 'status_id',
                'value' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status_id', Order::getStatuses(), [
                    'class' => 'form-control',
                    'prompt' => ''
                ])
            ],
            [
                'attribute' => 'partner_id',
                'value' => 'partner.name',
                'filter' => Html::activeDropDownList($searchModel, 'partner_id', ArrayHelper::map(Partner::find()
                    ->select('id,name')
                    ->orderBy('name')
                    ->asArray()
                    ->all(), 'id', 'name'), [
                    'class' => 'form-control',
                    'prompt' => ''
                ])
            ],
            'created_at:datetime',
            [
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    return $model->updated_at > $model->created_at
                        ? Yii::$app->formatter->asDatetime($model->updated_at)
                        : '';
                }
            ],
            'amount',
            //			'viewed:boolean',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('yii', 'View'),
                            'aria-label' => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                        ];

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    },
                    'update' => function ($url, $model, $key) {
                        return '';
                    },
                    'delete' => function ($url, $model, $key) {
                        return '';
                    },
                ]
            ],
        ],
    ]);
    ?>
</div>
