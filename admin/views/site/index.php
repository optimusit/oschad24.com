<?php
/* @var $this yii\web\View */

$this->title = 'Application';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <h2>Баланс</h2>

                <p>ТурбоСМС: <?= $data['smsBalance']['balance'] ?>
                    (<?= Yii::$app->formatter->asRelativeTime($data['smsBalance']['time']) ?>)</p>

                <p>Тикетс: <?= $data['ticketsBalance']['balance'] ?>
                    (<?= Yii::$app->formatter->asRelativeTime($data['ticketsBalance']['time']) ?>)</p>

                <p></p>
            </div>
        </div>
    </div>
</div>
