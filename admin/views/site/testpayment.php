<?php
$authorizationData = get_object_vars($data->Form);
?>
<h3>Переадресация...</h3>
<form id="proceed_payment_form" action="<?php echo $data->Action; ?>" method="POST">
    <?php foreach ($authorizationData as $field => $value): ?>
        <input type="hidden" name="<?php echo $field; ?>" value="<?= $value; ?>">
    <?php endforeach; ?>
</form>
<script>
    window.onload = function () {
        document.getElementById('proceed_payment_form').submit();
    }
</script>
