<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Partner */
/* @var $servicesDataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Партнер') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('admin', 'Партнеры'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="partner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'Редактировать'), [
            'update',
            'id' => $model->id
        ], ['class' => 'btn btn-primary']) ?>
        <!--		--><? //= Html::a('Delete', [
        //			'delete',
        //			'id' => $model->id
        //		], [
        //			'class' => 'btn btn-danger',
        //			'data' => [
        //				'confirm' => 'Are you sure you want to delete this item?',
        //				'method' => 'post',
        //			],
        //		]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'prefix',
            'name',
            'created_at',
            'updated_at',
            'email:email',
            'phone',
            'access_token',
            'secret_key',
            'status',
        ],
    ]) ?>

    <p><?= Yii::t('admin', 'Сервисы партнера') ?></p>

    <?= GridView::widget([
        'dataProvider' => $servicesDataProvider,
        'columns' => [
            'name',
            'description',
            'active:boolean',
        ],
    ]); ?>

</div>
