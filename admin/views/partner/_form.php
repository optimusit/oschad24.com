<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Partner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($node, 'prefix')
        ->textInput(['maxlength' => 32]) ?>

    <?= $form->field($node, 'name')
        ->textInput(['maxlength' => 255]) ?>

    <?= $form->field($node, 'email')
        ->textInput(['maxlength' => 255]) ?>

    <?= $form->field($node, 'phone')
        ->textInput(['maxlength' => 32]) ?>

    <?= $form->field($node, 'balance')
        ->textInput(['maxlength' => 12]) ?>

    <?= $form->field($node, 'credit_limit')
        ->textInput(['maxlength' => 12]) ?>

    <?= $form->field($node, 'edrpou')
        ->textInput(['maxlength' => 12]) ?>

    <?= $form->field($node, 'mfo')
        ->textInput(['maxlength' => 8]) ?>

    <?= $form->field($node, 'rr')
        ->textInput(['maxlength' => 14]) ?>

    <?= $form->field($node, 'bank')
        ->textInput(['maxlength' => 255]) ?>

    <?= $form->field($node, 'internal_num')
        ->textInput(['maxlength' => 6]) ?>

    <?= $form->field($node, 'transit_rr')
        ->textInput(['maxlength' => 14]) ?>

    <?= $form->field($node, 'commission_rr')
        ->textInput(['maxlength' => 14]) ?>

    <?= $form->field($node, 'return_rr')
        ->textInput(['maxlength' => 14]) ?>

    <?= $form->field($node, 'const_file_mb')
        ->textInput(['maxlength' => 14]) ?>

    <?= $form->field($node, 'active')
        ->checkbox() ?>

    <?php ActiveForm::end(); ?>

</div>
