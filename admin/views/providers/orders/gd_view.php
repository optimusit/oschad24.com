<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\models\back\ticketsua\GdBooking */

$this->title = $model->reservation_id;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Бронювання'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="booking-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'reservation_id',
            'booking_id',
            'passenger_departure_name',
            'passenger_arrival_name',
            'departure_date:date',
            'departure_time:time',
            'arrival_date:date',
            'arrival_time:time',
            'cost',
            'currency',
            'train_number',
            'car_number',
            [
                'attribute' => 'status_id',
                'value' => $model->getStatusName(),
            ],
        ],
    ]) ?>
</div>
<div>
    <?= Html::a('Проверить возможность отмены', \yii\helpers\Url::to([
        'refund',
        'id' => $model->reservation_id
    ])) ?>
</div>

