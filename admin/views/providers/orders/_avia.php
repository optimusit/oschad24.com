<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\back\ticketsua\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => Yii::t('app', '№ з/п')
        ],
        'locator',
        'final_price',
        [
            'attribute' => 'booking_date',
            'filter' => $searchModel->getDateFilter(),
        ],
        'final_price',
        [
            'attribute' => Yii::t('app', 'Статус'),
            'value' => function ($model) {
                return $model->getStatusName();
            },
            'filter' => Html::activeDropDownList($searchModel, 'status', $searchModel->getModel()
                ->getStatuses(), [
                'class' => 'form-control',
                'prompt' => ''
            ])
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    $options = [
                        'id' => $model['locator'],
                        'title' => Yii::t('yii', 'View'),
                        'aria-label' => Yii::t('yii', 'View'),
                        'target' => '_blank',
                    ];
                    $url = \yii\helpers\Url::to([
                        'providers/orders/view',
                        'service' => 'bus',
                        'id' => $model->locator
                    ]);

                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                },

            ]
        ],
    ],
]);
?>
