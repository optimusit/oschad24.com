<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\back\ticketsua\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Замовлення');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_' . $searchModel->service, [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel
    ]) ?>
</div>
