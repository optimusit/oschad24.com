<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Сервіси');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $index, $widget, $grid) {
            /* @var $model \common\models\Service */
            $options = [];
            if (!$model->active) {
                $options['style'] = 'background-color:' . 'red' . ';';
            }

            return $options;
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'description',
            'code',
            'script_url',
            'active:boolean',
            //				[
            //					'class' => 'yii\grid\ActionColumn',
            //					'template' => '{view}',
            //				],
        ],
    ]); ?>

</div>
