<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\tree\TreeView;
use common\models\Partner;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Филиалы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--	<p>-->
    <!--		--><? //= Html::a(Yii::t('admin', 'Создать партнера'), ['create'], ['class' => 'btn btn-success']) ?>
    <!--	</p>-->

    <!--	--><? //= GridView::widget([
    //			'dataProvider' => $dataProvider,
    //			'columns' => [
    //				[
    //					'class' => 'yii\grid\SerialColumn',
    //					'header' => '№ п/п'
    //				],
    //				'prefix',
    //				'name',
    //				'created_at:datetime',
    //				'updated_at:datetime',
    //				'email:email',
    //				// 'phone',
    //				// 'access_token',
    //				// 'secret_key',
    //				// 'active',
    //
    //				['class' => 'yii\grid\ActionColumn'],
    //			],
    //		]); ?>

    <?= TreeView::widget([
        'query' => Partner::find()
            ->addOrderBy('root, lft'),
        'headingOptions' => ['label' => Yii::t('admin', 'Структура')],
        'fontAwesome' => false,
        //		'isAdmin' => false,
        'displayValue' => 1,
        'softDelete' => true,
        'cacheSettings' => [
            'enableCache' => true
        ],
        'toolbar' => [
//			TreeView::BTN_CREATE => false,
TreeView::BTN_CREATE_ROOT => [
    'icon' => 'tree-conifer',
    'options' => ['title' => Yii::t('kvtree', 'Add new root')]
],
TreeView::BTN_REMOVE => [
    'icon' => 'trash',
    'options' => [
        'title' => Yii::t('kvtree', 'Delete'),
        'disabled' => true
    ]
],
TreeView::BTN_SEPARATOR,
TreeView::BTN_MOVE_UP => false,
TreeView::BTN_MOVE_DOWN => false,
TreeView::BTN_MOVE_LEFT => false,
TreeView::BTN_MOVE_RIGHT => false,
TreeView::BTN_REFRESH => [
    'icon' => 'refresh',
    'options' => ['title' => Yii::t('kvtree', 'Refresh')],
    'url' => Yii::$app->request->url
],
        ],
        'nodeAddlViews' => [
            'nodeView' => '@admin/views/partner/_form',
            \kartik\tree\Module::VIEW_PART_2 => '@admin/views/partner/_form',
        ]
    ]);
    ?>

</div>
