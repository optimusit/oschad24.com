<?php

namespace admin\controllers;

use Yii;
use yii\filters\AccessControl;
use admin\components\Controller;


/**
 * Product controller
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [
                        'cashier',
                    ],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');
        $authKey = Yii::$app->user->identity->getAuthKey();
        $sessionID = Yii::$app->session->id;
        $ticketsApp = Yii::$app->params['ticketsapp'];
        $redirectUrl = urlencode(Yii::$app->request->getHostInfo());
        $this->layout = 'product';

        return $this->render('index', [
            'ticketsApp' => $ticketsApp,
            'action' => $action,
            'authKey' => $authKey,
            'sessionID' => $sessionID,
            'redirectUrl' => $redirectUrl
        ]);
    }
}
