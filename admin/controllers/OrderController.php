<?php
namespace admin\controllers;

use common\models\GatewayRequestHistory;
use common\models\OrderProviderHistory;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use common\models\Order;
use admin\components\Controller;
use common\models\OrderSearch;

/**
 * Order controller
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [
                        //'admin',
                        'manager',
                    ],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionProviderhistory($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => OrderProviderHistory::find()
                ->where([
                    'order_id' => $id,
                ]),
        ]);

        return $this->render('provider_history', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPaymentshistory($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => GatewayRequestHistory::find()
                ->where([
                    'order_id' => $id,
                ])
                ->orderBy('id'),
        ]);

        return $this->render('payments_history', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSenddocuments($id)
    {
        $order = $this->findModel($id);
        $order->sendDocument();

        return Yii::t('app', 'Лист відправлено!');
    }

    public function actionDownloaddocuments($id)
    {
        $order = $this->findModel($id);
        $documentPath = $order->getDocuments();
        if ($documentPath['code'] == 0) {
            $files = $order->getDocumentFileName();
            if (count($files) > 1) {
                $tmpFile = tempnam('tmp', 'zip');
                $zip = new \ZipArchive();
                $zip->open($tmpFile, \ZipArchive::OVERWRITE);
                foreach ($files as $file) {
                    if (file_exists($file)) {
                        $fileParts = explode(DIRECTORY_SEPARATOR, $file);
                        $fileName = end($fileParts);
                        $zip->addFile($file, $fileName);
                    }
                }
                $zip->close();
                Yii::$app->response->sendFile($tmpFile, $id . '.zip');
                unlink($tmpFile);
            } else {
                Yii::$app->response->sendFile($files[0]);
            }
        }

        return true;
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne([
                'id' => $id,
                'partner_id' => $this->_partner->id
            ])) !== null
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Замовлення відсутне'));
        }
    }
}
