<?php
namespace admin\controllers;

use Yii;
use yii\filters\AccessControl;
use admin\components\Controller;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => [
                        'login',
                        'error'
                    ],
                    'allow' => true,
                ],
                [
                    'allow' => true,
                    'roles' => [
                        'admin',
                        'manager',
                    ],
                ],
            ],
        ];
        $behaviors['verbs']['actions']['logout'] = ['post'];

        return $behaviors;
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'data' => Yii::$app->core->getDashBoardData(),
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->partner = $this->_partner->prefix;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
