<?php
namespace admin\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use common\models\Partner;
use admin\components\Controller;

/**
 * Affiliate controller
 */
class AffiliateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['manager'],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Partner models.
     * @return mixed
     */
    public function actionIndex()
    {
//		$dataProvider = new ActiveDataProvider([
//			'query' => Partner::find()
//				->where(['id' => $this->_partner->id]),
//		]);

        return $this->render('index', [
            //'dataProvider' => $dataProvider,
            'query' => Partner::find()//->roots()
                ->andWhere(['root' => $this->_partner->id])
                ->addOrderBy('root, lft'),

        ]);
    }
}
