<?php
namespace admin\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use common\models\Order;
use admin\components\Controller;
use common\models\OrderSearch;
use common\components\AppHelper;

/**
 * Report controller
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [
                        //'admin',
                        'manager',
                    ],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionSales()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            'partner_id' => $this->_partner->id
        ], true);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionSalesexport()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            'partner_id' => $this->_partner->id
        ]);
        $dataProvider->pagination = false;
        $order = new Order();
        $orders[] = [
            $order->getAttributeLabel('service_id'),
            $order->getAttributeLabel('created_at'),
            $order->getAttributeLabel('id'),
            $order->getAttributeLabel('provider_order_id'),
            $order->getAttributeLabel('unit_count'),
            $order->getAttributeLabel('amount'),
            Yii::t('app', 'Вартість сервісного збору, грн'),
            Yii::t('app', 'Вартість еквайрінгу, грн'),
            Yii::t('app', 'Перераховано Банком, грн'),
            Yii::t('app', 'Перераховано провайдеру, грн'),
            $order->getAttributeLabel('income_amount'),
            $order->getAttributeLabel('status_id'),
        ];
        /** @var Order $model */
        foreach ($dataProvider->getModels() as $model) {
            $orders[] = [
                $model->service->getName(),
                Yii::$app->formatter->asDatetime($model->created_at),
                $model->id,
                $model->provider_order_id,
                $model->unit_count,
                $model->amount,
                $model->getServiceAmount(),
                $model->getAcquiringAmount(),
                $model->getBankAmount(),
                $model->getToProviderAmount(),
                $model->income_amount,
                $model->getStatus(),
            ];
        }
        //$content = $this->array2csv($orders);
        $content = AppHelper::Array2xls($orders);
        Yii::$app->response->sendContentAsFile($content, 'sales.xls');
    }

    private function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);

        return ob_get_clean();
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne([
                'id' => $id,
                'partner_id' => $this->_partner->id
            ])) !== null
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
