<?php
namespace admin\controllers;

use admin\models\RefundForm;
use Yii;
use common\models\OrderSearch;
use common\models\Service;
use common\models\Order;
use admin\components\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Payment controller
 */
class PaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => [
                        'index',
                        'refund',
                        'bookingrevoke',
                        'salescomplete',
                    ],
                    'allow' => true,
                    'roles' => [
                        'manager',
                    ],
                ],
                [
                    'allow' => true,
                    'roles' => [
                        'admin',
                    ],
                ],
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'bookingrevoke' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        return $this->renderGrid();
    }

    public function actionRefund($id)
    {
        $model = $this->findRefund($id);
        $modelLoad = $model->load(Yii::$app->request->post());
        if ($modelLoad && $model->validate() && $model->PaymentCancel()) {
            return $this->redirect([
                'order/paymentshistory',
                'id' => $model->order_id
            ]);
        } else {
            return $this->render('refund', [
                'model' => $model,
            ]);
        }
    }

    public function actionSalescomplete($id)
    {
        $order = $this->findModel($id);
        if ($order->paymentCommitTest()) {
            Yii::$app->session->setFlash('success', 'Запит відправлено');
        } else {
            Yii::$app->session->setFlash('error', 'Помилка');
        }

        return $this->redirect(['index']);
    }

    public function actionBookingrevoke($id)
    {
        $order = $this->findModel($id);
        if ($order->getService()
                ->one()->code == Service::TICKETS_GD
        ) {
//			$provider = new GdTickets();
//			$result = $provider->bookingRevoke($order->provider_order_id);
            if (Yii::$app->request->isAjax) {
                return $this->renderGrid();
            }
        }

        return $this->redirect(['index']);
    }

    protected function renderGrid()
    {
        $searchModel = new OrderSearch();
//		$requestParams = Yii::$app->request->post();
//		$filter = array_key_exists('OrderSearch', $requestParams) ? $requestParams['OrderSearch'] : [];
        $dataProvider = $searchModel->search(Yii::$app->request->get(), [
            'partner_id' => $this->_partner->id,
            'paid' => 1,
            //'status_id' => Order::STATUS_PROCESSING
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param $id
     * @return Order
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if (($model = Order::findOne([
                'id' => $id,
                'partner_id' => $this->_partner->id
            ])) !== null
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Замовлення відсутнє'));
        }
    }

    /**
     * @param $order_id
     * @return RefundForm
     * @throws NotFoundHttpException
     */
    private function findRefund($order_id)
    {
        if (($model = RefundForm::findModel($order_id, $this->_partner->id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Замовлення відсутнє'));
        }
    }
}
