<?php
namespace admin\controllers;

use common\models\Service;
use Yii;
use common\models\karabas\PartnerEvents;
use common\models\karabas\PartnerEventsCarousel;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use admin\components\Controller;
use yii\filters\VerbFilter;

/**
 * Manage controller
 */
class ManageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [
                        'manager',
                    ],
                ],
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'karabas-set' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        return $this->renderContent('');
    }

    public function actionKarabas()
    {
        $partnerEventsCarousel = PartnerEventsCarousel::tableName();
        $partnerEvents = PartnerEvents::tableName();
        $query = (new Query())->select('pe.EventId,pe.EventName,pe.EventDate,pe.ActivityImageLarge as image,pec.EventId as selected')
            ->from("$partnerEvents as pe")
            ->leftJoin("$partnerEventsCarousel as pec", 'pec.EventId = pe.EventId')
            ->where('length(pe.ActivityImageLarge)>0')
            ->andWhere('pe.EventDate>now()')
            ->orderBy('pe.EventDate');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        return $this->render('karabas', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionKarabasSet($id)
    {
        /** @var PartnerEventsCarousel $model */
        $model = PartnerEventsCarousel::findOne(['EventId' => $id]);
        if ($model) {
            $result = $model->delete() > 0
                ? true
                : false;
        } else {
            $model = new PartnerEventsCarousel();
            $model->EventId = $id;
            $result = $model->insert();
        }
        Yii::$app->cache->delete('application.config.extraParams.entertainments.carousel');

        return $result;
    }

    public function actionServices()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Service::find(),
        ]);
        $labels = (new Service())->attributeLabels();

        return $this->render('services', [
            'dataProvider' => $dataProvider,
            'labels' => $labels
        ]);
    }

    public function actionServiceSet($id, $value)
    {
        /** @var Service $model */
        $model = Service::findOne(['id' => $id]);
        $model->active = $value;
        $result = $model->update();

        return $result;
    }
}
