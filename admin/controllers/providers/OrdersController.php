<?php
namespace admin\controllers\providers;

use Yii;
use common\models\back\ticketsua\GdBooking;
use common\models\back\ticketsua\BookingSearch;
use yii\filters\AccessControl;
use admin\components\Controller;

/**
 * Orders controller
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [
                        'admin',
                    ],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView()
    {
        $model = new GdBooking();
        $model->reservationId = Yii::$app->request->queryParams['id'];

        return $this->render('view', ['model' => $model->BookingShow()]);
    }

    public function actionRefund()
    {
        $model = new GdBooking();
        $model->reservationId = Yii::$app->request->queryParams['id'];
        $model->BookingRefund();
    }
}
