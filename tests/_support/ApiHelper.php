<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class ApiHelper extends \Codeception\Module
{
	public function seeResponseContainsAttributes(array $attributes)
	{
		$I = $this->getModule('REST');
		foreach ($attributes as $attribute) {
			$I->seeResponseContains($attribute);
		}
	}
}
