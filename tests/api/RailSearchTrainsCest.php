<?php
use \ApiTester;
use \Codeception\Util\Fixtures;

class RailSearchTrainsCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function tryToTest(ApiTester $I)
    {
		$from = '2210800';
		$to = '2200001';
		$tomorrow = (new \DateTime())->add(new \DateInterval('P1D'))->format('d-m-Y');
		$date = $tomorrow;
		$partnerAccessToken = Fixtures::get('partnerAccessToken');
		$I->wantTo('Найти все поезда из Зппорожья в Киев на завтра');
		$I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
		$I->haveHttpHeader('Accept', 'application/json; q=1.0, */*; q=0.1');
		$I->sendGET('rail/search', [
			'accessToken' => $partnerAccessToken,
			'from' => $from,
			'to' => $to,
			'date' => $date,
			'lang' => 'uk',
		]);
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['passenger_departure_name' => 'ЗАПОРІЖЖЯ 1']);
    }
}
