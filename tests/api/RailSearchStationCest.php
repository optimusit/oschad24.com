<?php
use \ApiTester;
use \Codeception\Util\Fixtures;

class RailSearchStationCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function tryToTest(ApiTester $I)
    {
		$name = 'зап';
		$partnerAccessToken = Fixtures::get('partnerAccessToken');
		$I->wantTo('Найти станцию ЗАПОРІЖЖЯ 1');
		$I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
		$I->haveHttpHeader('Accept', 'application/json; q=1.0, */*; q=0.1');
		$I->sendGET('rail/searchstation', [
			'accessToken' => $partnerAccessToken,
			'name' => $name,
			'lang' => 'uk',
		]);
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['value' => 'ЗАПОРІЖЖЯ 1']);
//		$I->grabValueFrom('value');
		$stationId = $I->grabDataFromResponseByJsonPath('$..response.data');
	}
}
