<?php
$I = new ApiTester($scenario);
$I->wantTo('get partner services');
$I->haveHttpHeader(
	'Content-Type',
	'application/x-www-form-urlencoded'
);
$I->sendGET(
	'partner/services',
	['accessToken' => 'XvojYmv0PIccbw_1Iod0AWE3ImrP2y68']
);
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
//$I->seeResponseContainsAttributes(['response']);
//$I->seeResponseContains('{"result":"ok"}');
