<?php
namespace tests\codeception\common\unit\models;

use api\modules\v1\core\Rail;
use Codeception\Platform\Logger;
use Yii;
use Codeception\Specify;
use tests\codeception\common\unit\DbTestCase;
use tests\codeception\common\fixtures\PartnerFixture;
use tests\codeception\common\fixtures\UserFixture;
use tests\codeception\common\fixtures\PartnerServiceFixture;
use tests\codeception\common\fixtures\ServiceFixture;
use common\models\Partner;
use common\models\Service;
use common\models\PartnerService;
use common\models\User;
use common\models\LoginForm;
use common\models\Order;
use common\models\TicketReservation;

class RailReservationTest extends DbTestCase
{
	/**
	 * @var \tests\codeception\common\UnitTester
	 */
	protected $tester;

	use Specify;

	public function setUp()
	{
		parent::setUp();

		Yii::configure(Yii::$app, [
			'components' => [
				'user' => [
					'class' => 'yii\web\User',
					'identityClass' => 'common\models\User',
				],
			],
		]);
	}

	protected function tearDown()
	{
		parent::tearDown();
	}

	public function testCalculateCommission()
	{
		$order = new Order();
		$order->attributes = [
			'service_id' => Order::TYPE_TICKETS_RAIL,
			'income_amount' => 100,
			'custom' => serialize([]),
		];
		$order->calculateCommission();
		$this->assertEquals($order->amount, 200);
	}

	public function testTicketReservation()
	{
		$order = new Order();
		$order->attributes = [
			'partner_id' => 2,
			'service_id' => Order::TYPE_TICKETS_RAIL,
			'income_amount' => 100,
			'tickets_count' => 1,
			'custom' => serialize([]),
		];
		$this->assertTrue($order->validate());
		$this->assertTrue($order->Save());
		// современем нужен более строгий запрос
		$this->tester->seeInDatabase('order', ['partner_id' => 2]);


		$expirationTime = time() + (60 * 20);
		$orderReservation = new TicketReservation();
//		$expirationTime -= Rail::RESERVATION_END_SPACE;
		$orderReservation->attributes = [
			'order_id' => $order->id,
			'reservation_id' => 12345,
			'cost' => 123,
			'currency' => 'UAH',
			'expiration_time' => Yii::$app->formatter->asDate($expirationTime,
				'yyyy-MM-dd HH:mm:ss'),
		];
		$this->assertTrue($orderReservation->validate());
		$this->assertTrue($orderReservation->Save());
		$this->tester->seeInDatabase('ticket_reservation', ['order_id' => $order->id]);
	}

	/**
	 * @inheritdoc
	 */
	public function fixtures()
	{
		return [
			'partner' => [
				'class' => PartnerFixture::className(),
				'dataFile' => '@tests/codeception/common/unit/fixtures/data/models/partner.php'
			],
			'service' => [
				'class' => ServiceFixture::className(),
				'dataFile' => '@tests/codeception/common/unit/fixtures/data/models/service.php'
			],
			'partnerService' => [
				'class' => PartnerServiceFixture::className(),
				'dataFile' => '@tests/codeception/common/unit/fixtures/data/models/partnerService.php'
			],
			'user' => [
				'class' => UserFixture::className(),
				'dataFile' => '@tests/codeception/common/unit/fixtures/data/models/user.php'
			],
		];
	}
}
