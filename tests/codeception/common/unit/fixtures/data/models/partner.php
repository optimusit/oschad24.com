<?php

return [
	[
		'id' => 1,
		'prefix' => 'vebua',
		'name' => 'veb.ua',
		'created_at' => 1430917485,
		'updated_at' => 0,
		'email' => 'admin@veb.dev',
		'phone' => '000',
		'access_token' => '2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k',
		'secret_key' => '$2y$13$uZvKc1LcUx1jzgYUQjt5d.NJdsh.TXW5vFlwr7Y3z62S5Bp3PKkoa',
		'status' => 1
	],
	[
		'id' => 2,
		'prefix' => 'oschad24',
		'name' => 'oschad.ua',
		'created_at' => 1430917485,
		'updated_at' => 0,
		'email' => 'admin@oschad.dev',
		'phone' => '000',
		'access_token' => '2mbkQZVMtCvnkzt-MbEF67xLnjGAtO_k',
		'secret_key' => '$2y$13$uZvKc1LcUx1jzgYUQjt5d.NJdsh.TXW5vFlwr7Y3z62S5Bp3PKkoa',
		'status' => 1
	],
];
