<?php

return [
	[
		'id' => 1,
		'name' => 'Ж/д билеты',
		'description' => 'Ж/д билеты Ж/д билеты Ж/д билеты Ж/д билеты Ж/д билеты Ж/д билеты Ж/д билеты Ж/д билеты',
		'code' => 'TICKETS_RAIL',
		'controller' => 'rail',
		'script_url' => 'tickets/rail',
	],
	[
		'id' => 2,
		'name' => 'Автобусные билеты',
		'description' => 'Автобусные билеты Автобусные билеты Автобусные билеты Автобусные билеты Автобусные билеты',
		'code' => 'TICKETS_BUS',
		'controller' => 'bus',
		'script_url' => 'tickets/bus',
	],
	[
		'id' => 3,
		'name' => 'Авиабилеты',
		'description' => 'Авиабилеты Авиабилеты Авиабилеты Авиабилеты Авиабилеты Авиабилеты Авиабилеты Авиабилеты',
		'code' => 'TICKETS_AIR',
		'controller' => 'air',
		'script_url' => 'tickets/air',
	],
];
