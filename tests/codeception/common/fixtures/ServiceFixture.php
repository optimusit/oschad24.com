<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * Service fixture
 */
class ServiceFixture extends ActiveFixture
{
	public $modelClass = 'common\models\Service';
}
