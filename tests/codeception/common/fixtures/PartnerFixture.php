<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * Partner fixture
 */
class PartnerFixture extends ActiveFixture
{
	public $modelClass = 'common\models\Partner';
}
