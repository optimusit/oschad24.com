<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

/**
 * PartnerService fixture
 */
class PartnerServiceFixture extends ActiveFixture
{
	public $modelClass = 'common\models\PartnerService';
}
